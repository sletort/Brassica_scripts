#! /bin/bash

## AIMS  : Test function of rapsodynDB.sh
## USAGE : source ./test_brassicaDB.sh (inside test_rapsodynDB & test_polysuccessDB)
## USAGE : brassica_main
## NOTE  : 

function test__get_ref_fasta ()
{
	function __echo ()
	{
		local -r species="$1"
		local path=$( get_ref_fasta $species )

		printf "$species : $path\n"
	}
	loopOverSpecies  __echo
}

# =========================================================
function brassica_main ()
{
	#~ printf "loopOverSpecies to get ref path => echo species : path.\n"
	#~ test__get_ref_fasta

	# faire + propre ?
	get_assembly_path z1
}
