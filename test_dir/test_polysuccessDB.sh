#! /bin/bash

## AIMS  : Test function of rapsodynDB.sh
## USAGE : ./test_rapsodynDB.sh
## NOTE  : 

set -o nounset
set -o errexit # does not work with get_ref_path

declare -r BASEPATH=$( dirname $( readlink -e $0 ) )

source $BASEPATH/../brassicaDB.2.sh POLY

# =========================================================
function test__loopOver ()
{
	function __echo ()
	{
		local -r cul="$1"
		local -r msg="$2"

		printf "\t$cul : $msg\n"
	}
	loopOverSpecies loopOverCultivars __echo 'Vive les choux'
}

# =========================================================
source $BASEPATH/test_brassicaDB.sh

function main ()
{
	printf "Brassica main test\n"
	brassica_main

	printf "\npolysuccess flg: %d\n\n" "$__polysuccess__"

	printf "loopOver => echo all species/cultivars, with a message.\n"
	test__loopOver
}

# =========================================================
# your work.
main

printf "End of %s.\n\n" $( basename $0 )
exit 0
