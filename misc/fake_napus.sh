#! /bin/bash

## AIMS  : generate files of a fake_napus species : Brapa+Boleracea concatenation
## USAGE : ./fake_napus.sh
## NOTE  : NOT TESTED YET

declare -r ROOT_=$( dirname $0 )/..

if [[ ! $__brassicaDB_mine__ ]]
then
	source $ROOT_/brassicaDB.mine.sh
fi


set -o nounset
set -o errexit

# =========================================================
# declare -r INFILE="${1?What is your input file ?}"

# ---------------------------------------------------------
#declare -r MG_MAP_DIR="/PUBLIC_DATA/GENERATED/1kG/map_files"

# =========================================================
function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT     = $0\n"
#	printf "╟ - INFILE     = $INFILE\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

# =========================================================
function genere_annotation ()
{
	local -r brapa=$( myDB__get_ref_gff brapa )
	local -r boleracea=$( myDB__get_ref_gff boleracea )

	local -r fake=$( myDB__get_ref_gff fake_napus )

	{
		printf "# concatenation of %s\n" $( basename $brapa )
		printf "#\tand %s\n" $( basename $boleracea )
		printf "# Brapa's scaffold are coded with 6 digits whereas Boleracea's are coded with 5 digits.\n"

		zgrep -v '^#' $brapa
		zgrep -v '^#' $boleracea \
			| perl -pe 's/^C(.)/C0$1/'
	} | pigz -c \
		> $fake

}

function main ()
{
	# genere_chrom_fasta_files

	genere_annotation
}

# =========================================================
printVars

# your work.
main

printf "End of %s.\n\n" $( basename $0 )
exit 0
