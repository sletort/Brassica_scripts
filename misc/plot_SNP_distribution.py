#!/usr/bin/env python
# -*- coding: utf-8 -*-

## AIMS  : Plot SNP distribution in a spreadsheet.
## USAGE : ./plot_SNP_distribution.py -i in.bed -o out.ods
## AUTHORS : sebastien.letort@irisa.fr
## dev stoppé


import sys
import pdb	# debug
import pprint # debug
import argparse	# option management, close to C++/boost library (?)
import os	# for local libraries
import csv	# manage tsv files also.

# local libraries
f_path = os.path.dirname( os.path.realpath( __file__ ) )
sys.path.append( f_path + "/../lib" )

VERSION = "1.0"
PP = pprint.PrettyPrinter(indent=4)
VERBOSE = False

# ========================================
def main( o_args ):
#def main( infile, outfile ):
	print "La fonction principale"

	return 0

# ----------------------------------------
def defineOptions():
	o_parser = argparse.ArgumentParser( description='Prog descr.' )

	# special
	o_parser.add_argument( '--version', action='version', version='%(prog)s '+VERSION )

	# positionnal / mandatory param
	#	type = int, file
	o_parser.add_argument( 'param1', type=int,
		               help='a parameter.' )

	# optionnal
	o_parser.add_argument( '-f', dest='opt1',
							default='toto',
							help='a flag with a value.' )

	# mandatory option
	#	destination is opt2 by default
	o_parser.add_argument( '--infile', '-i',
						required=True,
						help='a flag opt, long and short.')

	return o_parser

def analyseOptions( o_args ):
	o_args.col -= 1

def verbose( msg ):
	if( VERBOSE ):
		sys.stderr.write( msg + "\n" )

# ----------------------------------------

if __name__ == '__main__':
	o_parser = defineOptions()
	o_args   = o_parser.parse_args()
	analyseOptions( o_args )
#	pp.pprint( o_args )

#	infile  = sys.argv[1]
#	outfile = sys.argv[2]
#	main( infile, outfile )
	main( o_args )

	sys.exit( 0 )

