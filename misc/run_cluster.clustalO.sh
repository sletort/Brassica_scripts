#! /bin/bash

# shell to use
#$ -S /bin/bash

#$ -M sletort@irisa.fr
# send mail before b, at the end e, on stop a, on suspend s, or never n
#$ -m bea

#$ -N Rapsodyn_clustal-omega

# if the script should be run in the current directory
#$ -cwd

# merge stdout and stderr
#$ -j no

## AIMS  : run clustalo on the cluster biogenouest
## USAGE : run_cluster.clustalO.sh fasta_file
## NOTE  : outfiles are generated in the input directory.

# init env X
source /softs/local/env/envclustal-omega.sh

# le script

fasta_file=${1?Your fasta file is missing.}

#bname=$( basename $fasta_file .fasta );
# to manage .fasta and .fa
bname=$( perl -MFile::Basename -e 'print basename( "'$fasta_file'", qw( .fa .fasta ) );' )
path=$( dirname $fasta_file );
outbase=$path/$bname

clustalo --infile $fasta_file --seqtype DNA --infmt fa \
		--distmat-out $outbase.dist_matrix \
		--guidetree-out $outbase.guide_tree \
		--clustering-out $outbase.cluster \
		--outfile $outbase.omega \
		--outfmt clu \
		--force --full

# I'll make dendrogram from dist_matrix after alteration.
perl -lane 'print join( "\t", @F ) if( 1 != $. );' $outbase.dist_matrix \
	> $outbase.dist_mat.tsv

exit 0
