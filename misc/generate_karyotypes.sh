#! /bin/bash

## AIMS  : generate a karyotype file for each reference sequence.
## USAGE : generate_karyotypes.sh
## NOTE  : source brassicaDB.sh first.

# =========================================================
declare -r ROOT_=$( dirname $0 )/..

if [[ ! $__brassicaDB__ ]]
then
	source $ROOT_/brassicaDB.mine.sh
fi


set -o nounset
set -o errexit

# =========================================================
function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT     = $0\n"
	echo   "╟ - DB__A_SPECIES = ${DB__A_SPECIES[@]}\n" # printf do not work :(
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

# generate a $species.seq_len for each species
function compute_seq_length ()
{
	printf "compute the length of each sequence/chrom, for each species.\n"
	for species in $( DB__get_ref_species )
	do
		$ROOT_/generic/mfasta_print_seq_length.py $( DB__get_ref_path $species ) \
				| sort -V \
			> $species.seq_len &
	done
	printf "Waiting for backgrounded job to terminate.\n"
	wait
}

# =========================================================
function main ()
{
	compute_seq_length

	# one $species.seq_len per file have been generated.
	printf "Karyotypes will be generated 'a ma façon', each species is treated differently.\n"

	local p_end='END{ print join( "\t", "Scaffolds", $scaf_len ); }'

	printf "Brapa\n"
	perle='$F[0] =~ s/Chiifu-401_chr(A\d+).*/$1/;'
	perle=$perle' if( $F[0] !~ /scaffold/i ){ print join( "\t", @F ); }else{ $scaf_len +=$F[1]; }'
	perl -lane "$perle $p_end" Brapa.seq_len \
		> $( myDB__get_karyotype Brapa ) &

	printf "Boleracea\n"
	perle='if( $F[0] !~ /scaffold/i ){ print join( "\t", @F ); }else{ $scaf_len +=$F[1]; }'
	perl -lane "$perle $p_end" Boleracea.seq_len \
		> $( myDB__get_karyotype Boleracea ) &

	printf "Bnapus\n"
	perle='$F[0] =~ s/Darmor-bzh_v4-1_//; print join( "\t", @F );'
	perl -lane "$perle" Bnapus.seq_len \
		> $( myDB__get_karyotype Bnapus ) &

	wait
}

# =========================================================
printVars

# your work.
main

printf "End of %s.\n\n" $( basename $0 )
exit 0

