#! /bin/bash

## AIMS  : Compute nb_bases or coverage (-g) for a given cultivar.
## USAGE : compute_coverage.sh culti
## USAGE : compute_coverage.sh -g 850.3Mb -o outdir culti
## NOTE  : in qsub script run it with loopOverCultivar bnapus compute_coverage.sh
## AUTHORS : sebastien.letort@irisa.fr

set -o nounset
set -o errexit

# =========================================================
declare CULTI
declare OUTDIR="."
declare GENOME_LEN=0

# ---------------------------------------------------------
declare VERBOSE=0

# =========================================================
function manageOptions ()
{
# getopts reminder:
#	FLAG, x will look for -x
#	x: will look for -x stg
#	if the option string start with ':', getopts turns to silent error reporting mode.
	while getopts "hvg:o:" opt
	do
		case $opt in
			g)	GENOME_LEN=$OPTARG  ;;
			o)	OUTDIR=$OPTARG  ;;

			v)	VERBOSE=1	;;
			h)
				usage && exit 0 ;;
			\?)	usage && exit 0 ;;
		esac
	done
	shift $((OPTIND-1))

	CULTI="${1?What is the cultivar name ?}"
}

function usage ()
{
		cat <<EOF
USAGE : $0 [options] culti_name

Options :
	-g : genome length (can use G,M,K notation).

	-v : verbose mode, see parameter values.
	-h : print this message and exit.
EOF
}

function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT    = $0\n"
	printf "╟ - CULTI     = $CULTI\n"
	printf "╟ - GENOME_LEN= $GENOME_LEN\n"
	printf "╟ - OUTDIR    = $OUTDIR\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

# =========================================================
function main ()
{
	local i=1

	for f in $( get_raw_seqs $culti )
	do
		local outfile=$OUTDIR/$CULTI.$i.seq_len.tsv
		$__ROOT/generic/mfasta_print_seq_length.py $f $outfile
		$(( ++i ))
	done

}

# =========================================================
manageOptions "$@"

if [[ 1 -eq $VERBOSE ]]
then
	printVars >&2
fi

# your work.
main

printf "End of %s.\n\n" $( basename $0 ) >&2
exit 0
