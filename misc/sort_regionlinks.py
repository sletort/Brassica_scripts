#!/usr/bin/env python
# -*- coding: utf-8 -*-

## AIMS  : sort region file ([chrom start end] x 2 tsv file) considering the strand.
## USAGE : sort_regionlinks.py [--norm] infile > outfile
#~ ## NOTE  : infile can be stdin if '-' is used.
#~ ## NOTE  : bed_file is 0-based, half-open intervals, end is excluded. It like start is 0-based and end 1-based
## AUTHORS : sebastien.letort@irisa.fr


import sys
import pdb	# debug
import pprint # debug
import argparse	# option management, close to C++/boost library (?)
import os	# for local libraries
import csv	# manage tsv files also.
import gzip


# local libraries
f_path = os.path.dirname( os.path.realpath( __file__ ) )
sys.path.append( f_path + "/../lib" )

import RegionLink as rl
import Misc

VERSION = "0.1.0"
PP = pprint.PrettyPrinter(indent=4)
VERBOSE   = False

# ========================================

# ========================================
def action( seq_id, seq ):
	'''The action to perform on the wanted sequence.'''
	print( "{0} : {1}".format( seq_id, str( len( seq ) ) ) )
	print( seq )


def main( o_args ):
	o_tsv   = csv.reader( o_args.o_fe, delimiter="\t" )

	verbose( "Loading infile." )
	lo_links = rl.RegionLink.loadSimpleLinkFile( o_tsv )
	
	lo_links.sort( key=lambda x: x.o_ref )

	#~ if( o_args.norm ):
		#~ rl.RegionLink.writeBedLinkFile( o_args.o_fs, lo_links )
	#~ else:
	rl.RegionLink.writeRefLinkFile( o_args.o_fs, lo_links )

	return 0

# ----------------------------------------
def defineOptions():
	o_parser = argparse.ArgumentParser( description='sort region file ([chrom start end] x 2 tsv file) considering the strand.' )

	# special
	o_parser.add_argument( '--version', action='version', version='%(prog)s ' + VERSION )

	# positionnal / mandatory param
	#	type = int, file
	o_parser.add_argument( 'infile', nargs='?', default='-',
		               help='The region_links filename to parse, can be gzipped.' )

	# optionnal
	o_parser.add_argument( '--verbose', '-v', action='store_true',
		               help='If set give info on what happen.' )
	#~ o_parser.add_argument( '--norm', action='store_true',
							#~ help='will generate a bed file with the target region and a strand field (+/-).' )

	return o_parser

def analyseOptions( o_args ):

	global VERBOSE
	if( o_args.verbose ):
		VERBOSE = True

	if( o_args.infile == '-' ):
		o_fe = sys.stdin
	else:
		if( Misc.is_compressed( o_args.infile) ):
			o_fe = gzip.open( o_args.infile, "r" )
		else:
			o_fe = open( o_args.infile, "r" )
	o_args.o_fe = o_fe

	o_args.o_fs = sys.stdout

def verbose( msg ):
	if( VERBOSE ):
		sys.stderr.write( msg + "\n" )

# ----------------------------------------

if __name__ == '__main__':
	o_parser = defineOptions()
	o_args   = o_parser.parse_args()
	analyseOptions( o_args )
	# PP.pprint( o_args )

	main( o_args )

	sys.exit( 0 )

