#! /bin/bash

## AIMS  : generates some additionnal files from reference file.
## USAGE : seqRef_addons.sh REF_NAME
## NOTE  :
## AUTHORS : sletort@irisa.fr

set -o nounset
set -o errexit

# =========================================================
declare -r REF_NAME="${1?What is the reference to treat ?}"

declare -r BASEPATH=$( dirname $( readlink -e $0 ) )

source $BASEPATH/../brassicaDB.mine.sh
source $BASEPATH/../lib/lib_misc.sh

# ---------------------------------------------------------

# =========================================================
function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT     = $0\n"
	printf "╟ - REF_NAME   = $REF_NAME\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

function describe_fasta ()
{
	local fasta="$1"

	local P_grep=grep
	if [[ Misc__is_gzip ]]
	then
		P_grep=zgrep
	fi

	$P_grep '>' "$fasta" \
# ne fonctionnera qu'avec la séquence du NCBI.
# Pour le moment je garde un script ad-hoc dans le dossier :
#	/home/sletort/seqRefs_addon/READ.ME
}

# =========================================================
function main ()
{
	local seq_ref_path=$( dirname $( myDB__get_ref_fasta $REF_NAME ) )

	describe_fasta "$seq_ref_path"
}

# =========================================================
printVars

# your work.
main

printf "End of %s.\n\n" $( basename $0 )

exit 0
