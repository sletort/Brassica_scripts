#!/usr/bin/env python
# -*- coding: utf-8 -*-

## AIMS  : detect strech of N in fasta file and output as bed file.
## USAGE : detectOcean.py fasta_file > out.bed
## AUTHORS : sebastien.letort@irisa.fr
## NOTE  : PAS FINI !


import sys
import pdb	# debug
import pprint # debug
import argparse	# option management, close to C++/boost library (?)
import os	# for local libraries
import csv	# manage tsv files also.

# local libraries
f_path = os.path.dirname( os.path.realpath( __file__ ) )
sys.path.append( f_path + "/../lib" )

VERSION = "1.0"
PP = pprint.PrettyPrinter(indent=4)
VERBOSE = False

# ========================================
def main( o_args ):
	h_ref = loadRef( o_args.ref )
	# PP.pprint( h_ref )

	a_cols = []
	for x in o_args.infile.readlines():
		x = x.rstrip()
		a_cols.append( h_ref[x] )

	PP.pprint( a_cols )

	return 0

def loadRef( f_in ):
	i = 0
	h_ref = {}
	for x in o_args.ref.readlines():
		x = x.rstrip()
		h_ref[x] = i
		i += 1

	return h_ref
		

# ----------------------------------------
def defineOptions():
	descr = 'A program to reorder column in a file considering a ref. file.'
	o_parser = argparse.ArgumentParser( description=descr )

	# special
	o_parser.add_argument( '--version', action='version',
							version='%(prog)s v'+VERSION )

	# positionnal / mandatory param
	#	type = int, file
	o_parser.add_argument( 'infile',
		               help='the tsv with column to reorder.' )

	# optionnal
	o_parser.add_argument( '-r', '--ref', required=True,
							type=file,
							help='the reference file.' )

	return o_parser

def analyseOptions( o_args ):
	if( "-" == o_args.infile ):
		o_args.infile = sys.stdin
	else:
		o_args.infile = open( o_args.infile, 'r' )

def verbose( msg ):
	if( VERBOSE ):
		sys.stderr.write( msg + "\n" )

# ----------------------------------------

if __name__ == '__main__':
	o_parser = defineOptions()
	o_args   = o_parser.parse_args()
	analyseOptions( o_args )
	PP.pprint( o_args )

	main( o_args )

	sys.exit( 0 )

