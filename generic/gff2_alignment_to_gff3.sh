#! /bin/bash

## AIMS  : Turn a gff2 alignment file to a gff3 file.
## USAGE : gff2_alignment_to_gff3.sh [-m] [-s] infile.gff2 > outfile.gff3
#
## OUTPUT: gff3 record format on stdout.
#
## NOTE  : The metadata should also be altered manually.
## NOTE  : Possible issue. Any comment (and original separators) in infile will be lost in the translation.
#
## BUG   : None found yet.

set -o nounset
set -o errexit

# =========================================================
declare INFILE;

# ---------------------------------------------------------
declare VERBOSE=0
declare META=0
declare SEPARATORS=0

# =========================================================
function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT     = $0\n"
	printf "╟ - INFILE     = $INFILE\n"
	printf "╟ - -----\n"
	printf "╟ - META       = $META\n"
	printf "╟ - SEPARATORS = $SEPARATORS\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

# =============================================
function manageOptions ()
{
# getopts reminder:
#	FLAG, x will look for -x
#	x: will look for -x stg
#	if the option string start with ':', getopts turns to silent error reporting mode.
	while getopts "mshv" opt
	do
		case $opt in
			m)	META=1  ;;
			s)	SEPARATORS=1  ;;

			v)	VERBOSE=1	;;
			h)
				usage && exit 0 ;;
			\?)	usage && exit 0 ;;
		esac
	done
	shift $((OPTIND-1))

	INFILE="${1?Please name the infile}"
}

function usage ()
{
		cat <<EOF
USAGE : $0 [options] infile.gff2 > outfile.gff3

Options :
	-m : if set also output metadata.
	-s : if set, will put a separator '###' between each different seq_id.

	-v : verbose mode, see parameter values.
	-h : print this message and exit.
EOF
}

# =========================================================
# print metadata as is unless gff-version which is rewritten on the first line.
function metadata ()
{
	printf "##gff-version 3.2.1\n"

	# I'll stop by dying
	set +o errexit

	perl -lne 'die unless /^##/; print unless /^##gff-version/;' "$INFILE" \
		2> /dev/null

	set -o errexit
}

function without_sep ()
{
	local p_begin='BEGIN{ $meta = 1; }'
	local perle='if( 1 == $meta && !/^##/ ){ $meta = 0; }'
	perle=$perle' if( 0 == $meta ){ if( /^#/ ){ print; }'
	perle=$perle' else{ $F[2] = "supported_by_sequence_similarity";'
	perle=$perle' $neuf="ID=$.;Target=".join( " ", @F[9..11] ); '
	perle=$perle' print join( "\t", @F[0..7], $neuf ); } }'

	perl -MData::Dumper -lane "$p_begin $perle" "$INFILE"
}

function main ()
{
	local tmp_file=/dev/stdout
	if [[ 1 == $SEPARATORS ]]
	then
		tmp_file=$( mktemp gff3.XXXXX.gff )
	fi

	#reformat
	{
		if [[ 1 == $META ]]
		then
			metadata
		fi

		without_sep
	} > $tmp_file

	if [[ 1 == $SEPARATORS ]]
	then
		local path=$( dirname $( readlink -e $0 ) )
		$path/add_separators_in_gff3_file.sh $tmp_file

		rm $tmp_file
	fi	
}

# =========================================================
manageOptions "$@"

if [[ 1 -eq $VERBOSE ]]
then
	printVars >&2
fi

# your work.
main

if [[ 1 -eq $VERBOSE ]]
then
	printf "End of %s.\n\n" $( basename $0 ) >&2
fi

exit 0
