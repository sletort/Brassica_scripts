#!/usr/bin/env python
# -*- coding: utf-8 -*-

## AIMS  : Rewrite a file after converting one column based on a dictionnary file.
## USAGE : dictonnary_based_conversion.py --dictionnary dico.tsv --infile infile.tsv --outfile outfile.tsv [--col 1]
## USAGE : dictonnary_based_conversion.py -d dico.tsv -i infile.tsv [-c 1] > outfile.tsv
## AUTHORS : sebastien.letort@irisa.fr


import sys
import pdb	# debug
import pprint # debug
import argparse	# option management, close to C++/boost library (?)
import os
import csv

# local library
f_path = os.path.dirname( os.path.realpath( __file__ ) )
sys.path.append( f_path + "/../lib" )
import Misc

pp = pprint.PrettyPrinter( indent=4, stream=sys.stderr )
VERBOSE = False

# ========================================
def main( o_args ):
	verbose( "Main function." )

	h_dico = Misc.load2ColsAsDict( o_args.dictionnary )
	#~ pp.pprint( h_dico )

	o_csv_out = csv.writer( o_args.outfile, delimiter='\t' )

	o_csv_in = csv.reader( o_args.infile, delimiter='\t' )
	for a_elts in o_csv_in:
		old_val = a_elts[o_args.col]
		if( not h_dico.has_key( old_val ) ):
			sys.stderr.write( old_val + " was not found in the dictionnary, leaved it as is.\n" )
		new_val = h_dico.get( old_val, old_val )

		a_elts[o_args.col] = new_val
		o_csv_out.writerow( a_elts )

	return 0

def defineOptions():
	o_parser = argparse.ArgumentParser( description='Rewrite a file after converting one column based on a dictionnary file.' )

	# special
	o_parser.add_argument( '--version', action='version', version='%(prog)s 1.0')

	o_parser.add_argument( '-d', '--dictionnary', type = file,
						required = True,
		               help = 'dictionnary (2 cols tsv file) to use.' )

	o_parser.add_argument( '-i', '--infile', type = file,
						default = sys.stdin,
		               help = 'infile (tsv file) to use.' )

	o_parser.add_argument( '-o', '--outfile', type=argparse.FileType('w'),
						default = sys.stdout,
		               help = 'outfile (tsv file).' )

	# optionnal
	o_parser.add_argument( '-c', '--col', type = int,
							default = 1,
							help = 'the infile column to convert with dictionnary.' )

	return o_parser

def analyseOptions( o_args ):
	o_args.col -= 1

def verbose( msg ):
	if( VERBOSE ):
		sys.stderr.write( msg + "\n" )

# ----------------------------------------

if __name__ == '__main__':
	o_parser = defineOptions()
	o_args   = o_parser.parse_args()
	analyseOptions( o_args )
	#~ pp.pprint( o_args )

	main( o_args )

	sys.exit( 0 )

