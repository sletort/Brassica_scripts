#!/usr/bin/env python
# -*- coding: utf-8 -*-

## AIMS  : test the prog mfasta_extract_seqs.py
## USAGE : pytest test_mfasta_extract_seqs.py
## AUTHORS : sebastien.letort@irisa.fr

# ========================================
import sys
import pprint # debug
import os	# for local libraries

import pytest

# upper dir
f_path = os.path.dirname( os.path.realpath( __file__ ) )
sys.path.append( f_path + "/.." )

PP = pprint.PrettyPrinter(indent=4)

# ========================================
import mfasta_extract_subseqs as X

# ----------------------------------------
def full_plus() : return ( 'c', 1,5, '+' )
def full_minus(): return ( 'c', 1,5, '-' )
def no_strand() : return ( 'c', 1,5 )
def no_end()    : return ( 'c', 1 )
def reversed(): return ( 'c', 5,1 )

def err_strand()  : return ( 'c', 1,5, 8 )
def err_start_noInt() : return ( 'c', 't',5 )
def err_end_noInt()   : return ( 'c', 1, 't' )
def err_start_neg() : return ( 'c', -1,5 )
def err_end_neg()   : return ( 'c', 1, -5 )

# ========================================

# Region class test
@pytest.fixture( scope = 'class',
		params = [
				( err_strand(), TypeError )
				,( err_start_noInt(), ValueError )
				,( err_end_noInt(), ValueError )
				,( err_start_neg(), ValueError )
				,( err_end_neg(), ValueError )
			]
	)
def err_fix( request ):
	return request.param

class TestRegion( object ):

	def test_cstr( self ):
		o_region = X.Region( 'c', 1,5, '+' )
		assert isinstance( o_region, X.Region )

	def test_warn_reversed( self, capsys ):
		seq_id   = 'le_chrom_que_je_veux'
		o_region = X.Region( seq_id, 5,1 )
		out,err  = capsys.readouterr()
		assert seq_id in err

	# faire avec une fixture ?
	def test_err_strand( self ):
		with pytest.raises( TypeError ):
			X.Region( *err_strand() )

	def test_err( self, err_fix ):
		with pytest.raises( err_fix[1] ):
			X.Region( *err_fix[0] )

	'''
	def test_err_start_noInt( self ):
		with pytest.raises( TypeError ):
			X.Region( *err_start_noInt() )

	def test_err_end_noInt( self ):
		with pytest.raises( TypeError ):
			X.Region( *err_end_noInt() )
	'''

@pytest.mark.parametrize( 'params, expected', [
		 ( full_plus(),  full_plus() )
		,( full_minus(), full_minus() )
 		,( no_strand(),  full_plus() )
 		,( no_end(),  ( 'c', 1, None, '+' ) )
		,( reversed(), full_minus() )
	])

def test_params( params, expected ):
	o_region = X.Region( *params )	
	assert o_region.getall() == expected

# how to test sequence extracted ?
# prefix addition ?
