#!/usr/bin/env python
# -*- coding: utf-8 -*-

## AIMS  : generate a (multi) fasta file by extracting some sequences of a multi fasta file.
## USAGE : extractSequenceFasta.py --list id_file --infile f.fasta --outfile o.fasta
## USAGE : extractSequenceFasta.py -l id_file -i f.fasta
## USAGE : extractSequenceFasta.py --id id1,id2 -i f.fasta
## AUTHORS : sebastien.letort@irisa.fr


import sys
import pdb	# debug
import pprint # debug
import argparse	# option management, close to C++/boost library (?)
import os	# for local libraries
from Bio import SeqIO

# local libraries
f_path = os.path.dirname( os.path.realpath( __file__ ) )
sys.path.append( f_path + "/../lib" )

VERSION = "1.0"
PP = pprint.PrettyPrinter(indent=4)
VERBOSE = False

# ========================================
def main( o_args ):

	verbose( "Parsing infile." )

	i = 0
	h_seqs = SeqIO.index( o_args.infile, "fasta" )
	for seq_id in o_args.l_ids:
		rec = h_seqs.get_raw( seq_id ).decode()
		o_args.outfile.write( rec )

	return 0

# ----------------------------------------
def defineOptions():
	o_parser = argparse.ArgumentParser( description='Prog descr.' )

	# special
	o_parser.add_argument( '--version', action='version', version='%(prog)s '+ VERSION )

	# positionnal / mandatory param
	#	type = int, file
#	o_parser.add_argument( '--infile', '-i', type=argparse.FileType('r'), default='-',
	o_parser.add_argument( '--infile', '-i', default='-',
		               help='The fasta file to parse. Default is stdin.' )
	o_parser.add_argument( '--outfile', '-o', type=argparse.FileType('w'), default='-',
		               help='Name of the outfile. Default is stdout.' )

	# optionnal
	o_parser.add_argument( '--verbose', '-v', action='store_true',
		               help='If set give info on what happen.' )

	o_parser.add_argument( '--list', '-l', type=file,
		               help='file containing one id per line.' )
	o_parser.add_argument( '--id',
							help='comma separared list of ids.' )

	return o_parser

def analyseOptions( o_args ):

	global VERBOSE
	if( o_args.verbose ):
		VERBOSE = True

	l_ids = []
	if( o_args.list ):
		l_ids += o_args.list.read().splitlines()
		o_args.list.close()
	if( o_args.id ):
		l_ids += o_args.id.split( "," )

	if( 0 == len( l_ids ) ):
		sys.stderr.write( "You have to specify some ids with --list or --id.\n" )
		sys.exit( 1 )

	o_args.l_ids = l_ids

def verbose( msg ):
	if( VERBOSE ):
		sys.stderr.write( msg + "\n" )

# ----------------------------------------

if __name__ == '__main__':
	o_parser = defineOptions()
	o_args   = o_parser.parse_args()
	analyseOptions( o_args )
#	pp.pprint( o_args )

	main( o_args )

	sys.exit( 0 )

