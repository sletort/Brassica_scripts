#!/usr/bin/env python
#-*- coding:utf-8 -*-

## AIMS  : Parse a fasta file and keep only extremity (N bases) in fasta sequences.
## USAGE : keep_only_extremity_in_fasta.py file.fasta [-N 50] [-o outfile]
## NOTE  : by default will print on stdout.
## NOTE  : Use BioPython

import sys
import pprint
import argparse # option management, close to C++/boost library (?)


VERSION = "1.0"
PP = pprint.PrettyPrinter(indent=4)

# ========================================
from Bio import SeqIO

def main( f_in, f_out, size_extremity ):

	#~ PP.pprint( f_in )
	#~ PP.pprint( f_out )
	#~ PP.pprint( size_extremity )

	len_mini = 2*size_extremity
	N = size_extremity # easier reading in substring
	a_out_records = []

	for record in SeqIO.parse( f_in, "fasta" ) :
		size = len( record.seq )
		L = size - len_mini
		if( 0 < L  ):
			record.seq = record.seq[:N] + "n" * L + record.seq[-N:]
		#~ fs.write( "%s\t%i\n" % ( record.id, len( record ) ) )
		a_out_records.append( record )
	SeqIO.write( a_out_records, f_out, 'fasta' )

def defineOptions():
	o_parser = argparse.ArgumentParser(
					description='Parse fasta file and keep only extremity. \
						Middle sequence will be NNNed.' )

	# special
	o_parser.add_argument( '--version', action='version', version='%(prog)s ' + VERSION )

	# positionnal / mandatory param
	#       type = int, file
	o_parser.add_argument( 'infile', type=file,
							help='The fasta filepath to parse.' )

	# optionnal
	o_parser.add_argument( '-o', '--outfile',
							help='out filepath (fasta).' )
	o_parser.add_argument( '-N', '--size_extremity',
							type=int, default=50,
							help='size of the extremity to keep.' )

	return o_parser

# ----------------------------------------

if __name__ == '__main__':

	o_parser = defineOptions()
	args = o_parser.parse_args()
	#~ PP.pprint( args )

	if( None == args.outfile ):
		fs = sys.stdout
	else:
		fs = open( args.outfile, 'w' )

	main( args.infile, fs, args.size_extremity )
	
	for f in [ args.infile, fs ]:
		f.close()

	sys.exit( 0 )
