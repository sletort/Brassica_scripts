#!/usr/bin/env python
# -*- coding: utf-8 -*-

## AIMS  : extract lines listed in a file from another file
## USAGE : qd_extractLines.py lines_file src_file > wanted_lines
## AUTHORS : sebastien.letort@irisa.fr


import sys
import pdb	# debug
import pprint # debug
import fileinput

PP = pprint.PrettyPrinter(indent=4)

# ========================================

prog = sys.argv[0]
line_filename = sys.argv[1]
src_filename  = sys.argv[2]

o_lines = fileinput.FileInput( line_filename )
o_src   = fileinput.FileInput( src_filename )

err_lines = []
for line in o_lines:
	line = int( line.rstrip() )
	for src_line in o_src:
		if( o_src.lineno() == line ):
			print( src_line.rstrip() )
			break
	else:
		err_lines.append( line )

if( 0 != len( err_lines ) ):
	msg  = src_filename + " has no more than " + str( o_src.lineno() ) + " lines.\n"
	msg += "Cannot reach lines " + ",".join( map( str, err_lines ) )
	sys.stderr.write( msg + "\n" )

o_lines.close()
o_src.close()

sys.exit( 0 )

