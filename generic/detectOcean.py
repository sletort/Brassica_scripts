#!/usr/bin/env python
# -*- coding: utf-8 -*-

## AIMS  : detect strech of N in fasta file and output as bed file.
## USAGE : detectOcean.py [--chr X] [--bed bed_file] [--min_size=5] fasta_file > out.bed
## NOTE  : Bed_file can be stdin if '-' is used. fasta_file must be a filename.
## NOTE  : bed_file is 0-based, half-open intervals, end is excluded. It like start is 0-based and end 1-based
## NOTE  : using bed-file can be confused. Ocean coordinate are output in the bed-region, 0-based,
## NOTE  :	but the seq_id is the samtools region string, 1-based.
## AUTHORS : sebastien.letort@irisa.fr


import sys
import pdb	# debug
import pprint # debug
import argparse	# option management, close to C++/boost library (?)
import csv	# manage tsv files also.
import pysam
import re


VERSION = "1.0"
PP = pprint.PrettyPrinter(indent=4)
VERBOSE   = False

# ========================================
class Region( object ):
	def __init__( self, chrom, start, end ):
		self.__chrom = chrom
		self.__start = int( start )
		self.__end   = int( end )

	@property
	def chrom( self ):
		return self.__chrom

	def samtools_string( self ):
		'''samtools string have start one based.
		
		samtools defines region 1-based, with end included in the interval.
		Pysam does not include then end.
		So to have the correct behaviour, I add 1 to start only.'''
		return "{0}:{1}:{2}".format( self.__chrom, 	str(self.__start+1), self.__end )

class BedFile( list ):
	def __init__( self, o_fe ):
		o_csv = csv.reader( o_fe, delimiter='\t' )
		for a_elts in o_csv:
			print( a_elts )
			self.append( Region( *a_elts[0:3] ) )

class OutputWriter( object ):
	def __init__( self, o_fs ):
		self.__fs = o_fs

	def __del__( self ):
		self.__fs.close()

	def write( self, seq_id, o_ocean ):
		l_fields = self._getFields( seq_id, o_ocean )
		self.__fs.write( "\t".join( l_fields ) + "\n" )

	def _getFields( seq_id, o_ocean ):
		raise Exception( "Not implemented yet !" )

class OutBed( OutputWriter ):
	''' Manage bed output.'''
	def _getFields( self, seq_id, o_ocean ):
		s = o_ocean.start()
		e = o_ocean.end()
		return [ str(x) for x in [ seq_id, s, e ] ]
		

class OutGff( OutputWriter ):
	''' Manage gff output.'''
	def __init__( self, o_fs ):
		OutputWriter.__init__( self, o_fs )
		self.__i = 1

	def _getFields( self, seq_id, o_ocean ):
		l_fields  = [ seq_id, 'detectOcean.py', 'gap' ]
		l_fields += [ o_ocean.start(), o_ocean.end() ]
		l_fields += [ '.', '.', '.' ]
		l_fields.append( ";".join([ 'ID={}'.format( self.__i )]) )

		self.__i += 1

		return [ str( x ) for x in l_fields ]

# ========================================
def action( o_out, seq_id, seq, re_N_str ):
	'''Look for strech of at least min_size N.'''
	verbose( "\tworking with {0}".format( seq_id ) )

	for ocean in re_N_str.finditer( str( seq ) ):
		o_out.write( seq_id, ocean )

def main( o_args ):
	re_N_str = re.compile( "N{" + str(o_args.min_size) + ",}" )

	with pysam.FastaFile( o_args.fasta_file ) as fe:
		verbose( "Working with file : " + fe.filename )

		if( o_args.chr ):
			seq = fe.fetch( reference=o_args.chr )
			action( o_args.o_out, o_args.chr, seq, re_N_str )
		elif( o_args.bed ):
			o_bed = BedFile( o_args.f_bed )
			for o_rec in o_bed:
				reg = o_rec.samtools_string()
				seq = fe.fetch( region=reg )
				action( o_args.o_out, reg, seq, re_N_str )
		else:
			for ref in fe.references:
				seq = fe.fetch( reference=ref )
				action( o_args.o_out, ref, seq, re_N_str )

	if( o_args.bed ):
		o_args.f_bed.close()

	return 0

# ----------------------------------------
def defineOptions():
	o_parser = argparse.ArgumentParser( description='detect strech of N in fasta file and output as bed file.' )

	# special
	o_parser.add_argument( '--version', action='version', version='%(prog)s ' + VERSION )

	# positionnal / mandatory param
	#	type = int, file
	o_parser.add_argument( 'fasta_file',
		               help='The fasta filename to parse, can be bgzipped.' )

	# optionnal
	o_parser.add_argument( '--verbose', '-v', action='store_true',
		               help='If set give info on what happen.' )
	o_parser.add_argument( '--chr',
							help='chrom (seq id) to use.' )
	o_parser.add_argument( '--bed',
							help='filename in bed format, containing regions to use.' )

	o_parser.add_argument( '--min_size', '-m', type=int, default=5,
							help='The minimum number of N to have to e considered as "ocean".' )

	o_parser.add_argument( '--format', '-f', type=str, default="bed",
							help='The output format, bed or gff, default is bed.' )

	o_parser.add_argument( '--outfile', '-o',
							default=sys.stdout, type=argparse.FileType( 'w' ),
							help='The minimum number of N to have to e considered as "ocean".' )

	# mandatory option
	#	destination is opt2 by default
	#~ o_parser.add_argument( '--opt2', '-o',
						#~ action='store_true', # store the value as boolean
						#~ help='a flag opt, long and short.')

	return o_parser

def analyseOptions( o_args ):

	global VERBOSE
	if( o_args.verbose ):
		VERBOSE = True

	if( o_args.bed ):
		if( o_args.bed == '-' ):
			f_bed = sys.stdin
		else:
			f_bed = open( o_args.bed, "r" )
		o_args.f_bed = f_bed

	if( o_args.format ):
		d_allowed = { 'bed': OutBed, 'gff':OutGff }
		if( o_args.format not in d_allowed ):
			raise Exception( 'Output format can only be one of ' + str( l_allowed ) )
		o_args.o_out = d_allowed[o_args.format]( o_args.outfile )

def verbose( msg ):
	if( VERBOSE ):
		sys.stderr.write( msg + "\n" )

# ----------------------------------------

if __name__ == '__main__':
	o_parser = defineOptions()
	o_args   = o_parser.parse_args()
	analyseOptions( o_args )

	main( o_args )

	sys.exit( 0 )
