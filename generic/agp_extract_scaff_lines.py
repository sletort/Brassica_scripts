#!/usr/bin/env python
# -*- coding: utf-8 -*-

## AIMS  : 
## USAGE : 
## NOTE  : 
## AUTHORS : sebastien.letort@irisa.fr


import sys
import pdb	# debug
import pprint # debug
import argparse	# option management, close to C++/boost library (?)
import os	# for local libraries
import csv	# manage tsv files also.
import gzip
from collections import defaultdict


# local libraries
f_path = os.path.dirname( os.path.realpath( __file__ ) )
sys.path.append( f_path + "/../lib" )

import Misc

VERSION = "0.1.0"
PP = pprint.PrettyPrinter(indent=4)
VERBOSE   = False

# ========================================
# ========================================
def action( seq_id, seq ):
	'''The action to perform on the wanted sequence.'''
	print( "{0} : {1}".format( seq_id, str( len( seq ) ) ) )
	print( seq )


def main( o_args ):

	verbose( "Extracting ids ..." )
	# PP.pprint( o_args.l_ids )

	cur_id = o_args.l_ids.pop()
	verbose( "looking for {0}".format( cur_id ) )

	flg = False
	o_csv = csv.reader( o_args.o_fe, delimiter='\t' )
	for a_elts in o_csv:
		try:
			if( a_elts[0].startswith( "#" ) ):
				o_args.output.write( "\t".join( a_elts ) + "\n" )

			elif( a_elts[0] == cur_id ):
				flg = True
				if( not o_args.w or a_elts[4] == "W" ):
					o_args.output.write( "\t".join( a_elts ) + "\n" )

			elif( True == flg ):
				flg = False
				cur_id = o_args.l_ids.pop()
				verbose( "looking for {0}".format( cur_id ) )

				# look not very good code to me ...
				if( a_elts[0] == cur_id ):
					flg = True
					if( not o_args.w or a_elts[4] == "W" ):
						o_args.output.write( "\t".join( a_elts ) + "\n" )

			# else
				# I'm in a urge, I suppose that all ids are in the file
				#~ try:
					#~ while( cur_id < a_elts[0] ):
						#~ cur_id = o_args.l_ids.pop()

					#~ verbose( "looking for {}".format( cur_id ) )
		except IndexError:
			break

	o_args.o_fe.close()


	return 0

# ----------------------------------------
def defineOptions():
	o_parser = argparse.ArgumentParser( description='Prog to extract some lines from AGP file, based on scaff id.' )

	# special
	o_parser.add_argument( '--version', action='version', version='%(prog)s ' + VERSION )

	# mandatory option
	o_parser.add_argument( '--agp', required=True,
						help='the agp file to parse.')
	o_parser.add_argument( '--id', type=str, required=True,
						help='the file containing one scaffold id per line (- for stdin).')


	# optionnal
	o_parser.add_argument( '--verbose', '-v', action='store_true',
		               help='If set give info on what happen.' )
	o_parser.add_argument( '--output', '-o', default=sys.stdout, type=argparse.FileType( 'w' ),
		               help='Output filename, default is stdout.' )
	o_parser.add_argument( '-w', action='store_true',
		               help='If set output only lines with "w" on col. 5.' )

	return o_parser

def analyseOptions( o_args ):

	global VERBOSE
	if( o_args.verbose ):
		VERBOSE = True

	if( Misc.is_compressed( o_args.agp ) ):
		o_args.o_fe = gzip.open( o_args.agp )
	else:
		o_args.o_fe = open( o_args.agp )


	if( o_args.id ):
		if( o_args.id == '-' ):
			f_id = sys.stdin
		else:
			f_id = open( o_args.id, "r" )

		o_args.l_ids = []
		for line in f_id:
			o_args.l_ids.append( line.rstrip() )
		f_id.close()
		#~ o_args.l_ids.sort( reverse=True )
		o_args.l_ids.reverse()

def verbose( msg ):
	if( VERBOSE ):
		sys.stderr.write( msg + "\n" )

# ----------------------------------------

if __name__ == '__main__':
	o_parser = defineOptions()
	o_args   = o_parser.parse_args()
	analyseOptions( o_args )
	#~ PP.pprint( o_args )

	main( o_args )

	sys.exit( 0 )

