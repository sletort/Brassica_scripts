#!/usr/bin/env python
#-*- coding:utf-8 -*-

## AIMS  : Parse a fasta file and print for each sequence its header and length.
## USAGE : mfasta_print_seq_length.py file.fasta [output]
## NOTE  : by default will print on stdout.
## NOTE  : Use BioPython

import sys

import pysam

def main( prefix, fasta_file, fs ):

	with pysam.FastaFile( fasta_file ) as fe:
		for id in fe.references:
			fs.write( ">" + prefix+id + "\n" + fe.fetch( reference=id ) + "\n" )

# ----------------------------------------
if __name__ == '__main__':
	infile  = sys.argv[1] # pysam cannot open stdin.
	prefix  = sys.argv[2]

	#if( infile == '-' or infile == None ):
	#	infile = sys.stdin

	if( 3 < len( sys.argv ) ):
		fs = open( sys.argv[3], 'w' )
	else:
		fs = sys.stdout

	main( prefix, infile, fs )

	fs.close()

	sys.exit( 0 )

