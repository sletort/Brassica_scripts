#! /bin/bash

## AIMS  : Convert a chrom-pos_data file into a starch file usable with bedops.
## USAGE : chrom_pos2starch.sh infile
## NOTE  : Headers (linesbegining with #) will be place as metadata in starch.
## NOTE  : I do not succeed in making starch command parallel, this makes starch fail.
## AUTHORS : sletort@irisa.fr

set -o nounset
set -o errexit

# =========================================================
declare -r INFILE="${1?What is your chrom_pos file ?}"
declare OUTFILE="${2-}"

if [[ "" == $OUTFILE ]]
then
	OUTFILE=$( basename $INFILE .tsv ).starch
fi

# =========================================================
function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT     = $0\n"
	printf "╟ - INFILE     = $INFILE\n"
	printf "╟ - OUTFILE    = $OUTFILE\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}


# =========================================================
# infile & outfile are absolute filepaths.
function main ()
{
	local tmp_dir="$1"

	local header=$( grep ^# $INFILE )

	local tmp_bed=$( mktemp XXXXX.sorted.bed )
	grep -v ^# $INFILE \
			| perl -lane 'print join( "\t", $F[0],$F[1]-1, @F[1..$#F] );' \
			| sort-bed - \
		> $tmp_bed

	bedextract --list-chr $tmp_bed \
		| while read chrom
		do
			bedextract $chrom $tmp_bed \
					| starch - \
				> "$tmp_dir/$chrom.starch"
		done

	starchcat --note="$header" "$tmp_dir"/*.starch \
		> $OUTFILE

	rm -f  $tmp_bed
	rm -rf $tmp_dir
}

# =========================================================
printVars

# your work.

tmp_dir=$( mktemp -d chrom2starch.XXXXX )
main "$tmp_dir"
rm -rf "$tmp_dir"

printf "End of %s.\n\n" $( basename $0 )

exit 0

