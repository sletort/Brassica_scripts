#! /bin/bash

## AIMS  : add '###' between all seq id. I assume infile is sorted.
## USAGE : add_separators_in_gff3_file.sh infile.gff > outfile.gff
#
## OUTPUT: gff record format on stdout.
#
## NOTE  : Does not modify comments or metadata.
#
## BUG   : None found yet.

set -o nounset
set -o errexit

# =========================================================
declare INFILE;

# ---------------------------------------------------------
declare VERBOSE=0

# =========================================================
function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT     = $0\n"
	printf "╟ - INFILE     = $INFILE\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

# =============================================
function manageOptions ()
{
# getopts reminder:
#	FLAG, x will look for -x
#	x: will look for -x stg
#	if the option string start with ':', getopts turns to silent error reporting mode.
	while getopts "hv" opt
	do
		case $opt in
			v)	VERBOSE=1	;;
			h)
				usage && exit 0 ;;
			\?)	usage && exit 0 ;;
		esac
	done
	shift $((OPTIND-1))

	INFILE="${1?Please name the infile}"
}

function usage ()
{
		cat <<EOF
USAGE : $0 [options] infile.gff > outfile.gff

Options :
	-v : verbose mode, see parameter values.
	-h : print this message and exit.
EOF
}

# =========================================================
function main ()
{
	local perle='if( /^#/ ){ print; } else {'
	perle=$perle' if( $prev ne $F[0] ){ if( $prev ne "" ){ print "###"; } $prev = $F[0]; }'
	perle=$perle' print; }'

	perl -lane "$perle" "$INFILE"
}

# =========================================================
manageOptions "$@"

if [[ 1 -eq $VERBOSE ]]
then
	printVars >&2
fi

# your work.
main

if [[ 1 -eq $VERBOSE ]]
then
	printf "End of %s.\n\n" $( basename $0 ) >&2
fi

exit 0
