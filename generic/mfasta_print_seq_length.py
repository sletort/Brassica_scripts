#!/usr/bin/env python
#-*- coding:utf-8 -*-

## AIMS  : Parse a fasta file and print for each sequence its header and length.
## USAGE : mfasta_print_seq_length.py file.fasta [output]
## NOTE  : by default will print on stdout.
## NOTE  : Use BioPython

import sys

# do not recognize bgzip as gzip
import mimetypes
import gzip

from Bio import SeqIO

def main( fe, fs ):

	h_records = {}
	nb_bases  = 0
	for record in SeqIO.parse( fe, 'fasta' ) :
		fs.write( "%s\t%i\n" % ( record.id, len(record) ) )
	fs.close()

# ----------------------------------------
if __name__ == '__main__':
	infile  = sys.argv[1]
	if( 2 < len( sys.argv ) ):
		h_output = open( sys.argv[2], 'w' )
	else:
		h_output = sys.stdout

	t_type = mimetypes.guess_type( infile, strict=0 )
#	print( t_type )
	if( 'gzip' == t_type[1] ):
		fe = gzip.open( infile, "rb")
	else:
		fe = open( infile, "rU")

	main( fe, h_output )

	sys.exit( 0 )

