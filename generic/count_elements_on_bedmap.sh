#! /bin/bash

## AIMS  : Count elements (starch file) by chrom chunks, used to plot distribution.
## USAGE : ./count_elements_on_genome.sh -m map infile.starch > out
## NOTE  : map.
## AUTHORS : sletort@irisa.fr

set -o nounset
set -o errexit

# =========================================================
declare MAP_FILE
declare INFILE

# ---------------------------------------------------------
declare VERBOSE=0

# =========================================================
function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT     = $0\n"
	printf "╟ - MAP_FILE   = $MAP_FILE\n"
	printf "╟ - INFILE     = $INFILE\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

function verbose ()
{
	local msg="$1"

	if [[ 1 -eq $VERBOSE ]]
	then
		printf "$msg\n" >&2
	fi
}

# =========================================================
function manageOptions ()
{
# getopts reminder:
#	FLAG, x will look for -x
#	x: will look for -x stg
#	if the option string start with ':', getopts turns to silent error reporting mode.
	while getopts "m:hv" opt
	do
		case $opt in
			m)	MAP_FILE=$OPTARG  ;;

			v)	VERBOSE=1	;;
			h)
				usage && exit 0 ;;
			\?)	usage && exit 0 ;;
		esac
	done
	shift $((OPTIND-1))

	INFILE="${1?Please name the infile}"

	__assert_vars
}

function __assert_vars ()
{
	local p=$( which bedops 2> /dev/null )
	local err=$?
	if [[ "0" -ne "$err" ]]
	then
		printf "bedops is not in the path.\n" >&2
		exit 1
	fi

	if [[ ! -f $MAP_FILE ]]
	then
		printf "Map file %s is not reacheable.\n" "$MAP_FILE" >&2
		exit 1
	fi
}

function usage ()
{
		cat <<EOF
USAGE : $0 [options] -m map_file infile > count.lst

	-m map_file : Mandatory.
					a starch file representing regions where to count elements.
					It can be generated with build_genome_bedmap.sh
	infile      : Mandatory.
					Starch or bed file with elements to count.

Options :
	-v : verbose mode, see parameter values.
	-h : print this message and exit.
EOF
}

# =========================================================
function main ()
{
	local tmp_list=""
	for chr in $( unstarch --list-chr $MAP_FILE )
	do
		local tmp_bm=$( mktemp ceob.XXXX )
		tmp_list="$tmp_list $tmp_bm"

		bedmap --chrom "$chr" --faster --count "$MAP_FILE" "$INFILE"
			> $tmp_bm
	done
	verbose "Waiting for all chroms to be processed.\n"
	wait

	cat $tmp_list
	rm -f $tmp_list
}

# =========================================================
manageOptions "$@"

if [[ 1 -eq $VERBOSE ]]
then
	printVars >&2
fi

# your work.
main

msg=$( printf "End of %s.\n" $( basename $0 ) )
verbose "$msg"

exit 0
