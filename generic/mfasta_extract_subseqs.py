#!/usr/bin/env python
# -*- coding: utf-8 -*-

## AIMS  : Parse a (bgzipped) fasta file and extract a sub-sequence regarding arguments
## USAGE : mfasta_extract_subseqs.py [--output output] --tsv file.tsv file.fasta
## NOTE  : by default will print on stdout.
## NOTE  : tsv_file can be stdin if '-' is used. fasta_file must be a filename.
## NOTE  : in tsv file, regions are "human meaning". chr7 1 2, will return the first two bases = [1-2] 1-based.
## NOTE  : cf help for details.
## NOTE  : It uses pysam. Old versions have pysam.Fasta*f*ile constructor, which lead to a syntax error on the 'with' line.
## NOTE  : !!samtools faidx may do the job, I don't know yet if it can read regions like this prog.
## DEPENDENCIES : pysam, Bio.Seq.Seq
## AUTHORS : sebastien.letort@irisa.fr


import sys
import pdb	# debug
import pprint # debug
import argparse	# option management, close to C++/boost library (?)
import os	# for local libraries
import csv	# manage tsv files also.


# local libraries
f_path = os.path.dirname( os.path.realpath( __file__ ) )
sys.path.append( f_path + "/../lib" )

import MFasta

VERSION = "0.1.0"
PP = pprint.PrettyPrinter(indent=4)
VERBOSE   = False
DEBUG     = False

# ========================================
class Region( object ):
	'''A class to manage the region.
	
	start & end (int) are 1-based.
	start < end
	strand is + or -
	both start & end positions are included in the output sequence.
	[1,2] = the first two bases.
	'''

	def __init__( self, seq_id, start=None, end=None, strand='+' ):
		'''start & and are int.'''
		( seq_id, start, end, strand ) = self.__check( seq_id, start, end, strand )

		self.__seq_id = seq_id
		self.__start = int( start )
		self.__end   = int( end ) if( end ) else end
		self.reverse = strand

	# __init__

	def __str__( self ):
		elt = ( self.__seq_id, self.__start, self.__end, self.reverse )
		return ":".join( map( str, elt ) )

	def __check( self, seq_id, start, end, strand ):
		( start, end ) = map( int, [ start, end ] )
		if( end ):
			if( end < start ):
				msg  = "WARN: seq_id '{0}' => end '{2}' is lower than start '{1}'."
				msg += "\n\tstrand will be '-'.\n"
				sys.stderr.write( msg.format( seq_id, start, end, strand ) )

				( start, end ) = ( end, start )
				strand = '-'

			if( end <= 0 ):
				raise ValueError( "End should be positive integer." )

		if( start <= 0 ):
			raise ValueError( "Start should be positive integer." )

		strand_allowed = [ '-', '+', '1', '-1' ]
		if( strand not in strand_allowed ):
			raise TypeError( "strand can be {}, nothing else.".format( strand_allowed ) )

		return( seq_id, start, end, strand )
	# __check

	@property
	def seq_id( self ):
		return self.__seq_id
	@property
	def start( self ):
		return self.__start
	@property
	def end( self ):
		return self.__end
	@property
	def reverse( self ):
		return self.__reverse
	@reverse.setter
	def reverse( self, value ):
		if( '+' == value or '1' == value ):
			self.__reverse = False
		else:
			self.__reverse = True

	def getall( self ):
		return self.seq_id, self.start, self.end, self.reverse

# ========================================
def main( o_args ):
	ldo_regions = sorted( build_regions( o_args ) )
	if( DEBUG ):
		for do_ in ldo_regions:
			PP.pprint( str( do_ ) )

	o_es = MFasta.ExtractSeq( o_args.fasta_file )
	for seq_id,o_seq in o_es.extractRegions( ldo_regions, MFasta.build_seqId, prefix=o_args.id_prefix ):
		o_args.outfile.write( ">" + seq_id + "\n" + str( o_seq ) + "\n" )

	if( getattr( o_args, 'f_tsv', None ) ):
		o_args.f_tsv.close()

	return 0

def build_regions( o_args ):
	verbose( "Build all Regions." )
	ldo_regions = []

	#~ if( getattr( o_args, 'id_col', None ) ):
	if( getattr( o_args, 'f_tsv', None ) ):
		verbose( "\t... from tsv file (1-based, included: first two bases = [1-2])" )
		o_csv = csv.reader( o_args.f_tsv, delimiter='\t' )
		for a_elts in o_csv:
			( c,s,e )  = a_elts[0:3]
			if( o_args.id_col ):
				id_seq = a_elts[o_args.id_col]
			else:
				id_seq = '...'

			st = a_elts[3] if( 3 < len( a_elts ) ) else '+'
			ldo_regions.append( { id_seq: Region( c,s,e, st ) } )

	return ldo_regions
# build_regions

# ----------------------------------------
def defineOptions():
	o_parser = argparse.ArgumentParser( description='extract a sub-sequence from (bgzip) fasta file.' )

	# special
	o_parser.add_argument( '--version', action='version', version='%(prog)s ' + VERSION )
	o_parser.add_argument( '--verbose', '-v', action='store_true',
		               help='If set give info on what happen.' )


	# positionnal / mandatory param
	#	type = int, file
	o_parser.add_argument( 'fasta_file',
		               help='The fasta filename to parse, can be bgzipped.' )

	# optionnal
	o_parser.add_argument( '--outfile', '-o', type=argparse.FileType('w'), default=sys.stdout,
		               help='Name of the outfile. Default is stdout.' )
	o_parser.add_argument( '--id_prefix', '-p',
							help='id_prefix, if set, used in new sequence id like ">id_prefix|seq_id|start-end|strand".' )

	# optionnal region format management no more time to spend on it.
	# optionnal ? what the prog does if no region is defined ?
	o_parser.add_argument( '--tsv',
							help='filename in simple tsv format (chrom,start,end[,strand]), containing regions to use.' )

	# optionnal id col, if the new sequences have a new name.
	o_parser.add_argument( '--id_col', type=int,
							help='column index where to find id for each sequence region. (>id|chr|start-end|strand)' )

	return o_parser

def analyseOptions( o_args ):

	global VERBOSE
	if( o_args.verbose ):
		VERBOSE = True

	if( o_args.tsv ):
		if( o_args.tsv == '-' ):
			f_tsv = sys.stdin
		else:
			f_tsv = open( o_args.tsv, "r" )
		o_args.f_tsv = f_tsv

# analyseOptions

def verbose( msg ):
	if( VERBOSE ):
		sys.stderr.write( msg + "\n" )

# ----------------------------------------

if __name__ == '__main__':
	o_parser = defineOptions()
	o_args   = o_parser.parse_args()
	analyseOptions( o_args )
	#~ PP.pprint( o_args )

	if( DEBUG ):
		main( o_args )
	else:
		try:
			main( o_args )
		except ValueError as e:
			sys.stderr.write( str( e ) + "\n" )
			sys.stderr.write( "Check your input data.\n" )
		except IOError as e:
			sys.stderr.write( str( e ) + "\n" )
			sys.stderr.write( "Check your input data.\n" )
		except Exception as e:
			sys.stderr.write( str( e ) + "\n" )


	sys.exit( 0 )

