#!/usr/bin/env python
#-*- coding:utf-8 -*-

## AIMS  : Parse a fasta file and print for each sequence its header and length.
## USAGE : mfasta_extract_seqs.py file.fasta id_file [output]
## NOTE  : by default will print on stdout.
## NOTE  : Use pysam. Old versions have pysam.Fasta*f*ile constructor, which lead to a syntax error on the 'with' line.

import sys

import pysam

def main( fasta_file, id_file, fs ):

	with pysam.FastaFile( fasta_file ) as fe, open( id_file, 'r' ) as f_id:
		for id in f_id:
			id = id.rstrip()
			fs.write( ">" + id + "\n" + fe.fetch( reference=id ) + "\n" )

# ----------------------------------------
if __name__ == '__main__':
	infile  = sys.argv[1]
	id_file = sys.argv[2] # or '-' ? TODO
	if( 3 < len( sys.argv ) ):
		fs = open( sys.argv[3], 'w' )
	else:
		fs = sys.stdout

	main( infile, id_file, fs )

	fs.close()

	sys.exit( 0 )

