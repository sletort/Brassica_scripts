#!/usr/bin/env python
# -*- coding: utf-8 -*-

## AIMS  : given data as id1:id2\tn\tscore, generate a heatmap with dendrowgrams.
## USAGE : ./drawHeatmapDendrowFromMatrix.py data_file --title "jolie HM" --outfile hm.png
## AUTHORS : sletort@irisa.fr


import sys
import pprint # debug
import argparse	# option management, close to C++/boost library (?)
import os	# for local libraries
import csv	# manage tsv files also.

from collections import defaultdict

# local libraries
f_path = os.path.dirname( os.path.realpath( __file__ ) )
sys.path.append( f_path + "/../lib" )

import heatmapAndDendrogram as hd


VERSION = "1.0"
PP = pprint.PrettyPrinter(indent=4)
VERBOSE   = False

# value for "totaly different".
DI_SCORE = 1

# value for "identity".
ID_SCORE = 0

# Value used for missing values.
NA  = 0.5


# ========================================
def loadDataFile( o_fe ):
	dd_scores = defaultdict( dict )

	# load file
	s_culti = set()
	o_csv = csv.reader( o_fe, delimiter='\t' )
	for l in o_csv:
		l_cultis = l[0].split( ":" )
		s_culti.update( l_cultis )

		if( l[2] == "NA" ):
			l[2] = NA
		#~ dd_scores[l_cultis[0]][l_cultis[1]] = MAX - float( l[2] )
		#~ dd_scores[l_cultis[1]][l_cultis[0]] = MAX - float( l[2] )
		dd_scores[l_cultis[0]][l_cultis[1]] = float( l[2] )
		dd_scores[l_cultis[1]][l_cultis[0]] = float( l[2] )

	# compute matrix
	l_culti = sorted( s_culti )
	N = len( l_culti )
	M = [ [] for i in range(N) ]

	for i in range( N ):
		cul1 = l_culti[i]
		d_scores = dd_scores.get( cul1, None )
		if( not d_scores ):
			continue

		for j in range( N ):
			cul2 = l_culti[j]
			if( cul1 == cul2 ):
				M[i].append( ID_SCORE )
			else:
				M[i].append( d_scores.get( cul2, NA ) )

	return M, l_culti

def main( o_args ):
	verbose( "Load data_file." )
	( m_distances, l_culti ) = loadDataFile( o_args.data_file )
	#~ PP.pprint( m_distances )

	hd.saveHeatmapAndDendrogram( m_distances, o_args.title, l_culti, l_culti, o_args.outfile )

	return 0


# ----------------------------------------
def defineOptions():
	descr = 'given data as id1:id2\tn\tscore, generate a heatmap with dendrowgrams.'
	o_parser = argparse.ArgumentParser( description=descr )

	# special
	o_parser.add_argument( '--version', action='version', version='%(prog)s ' + VERSION )

	# positionnal / mandatory param
	#	type = int,?
	o_parser.add_argument( 'data_file',
							type=argparse.FileType( 'r' ),
							default=sys.stdin,
							help='The matrix file as a collection of rows : id1:id2\tn\tscore' )

	# optionnal
	o_parser.add_argument( '--verbose', '-v', action='store_true',
		               help='If set give info on what happen.' )
	o_parser.add_argument( '--title', '-t',
							default='',
							help='a title to add to the picture.' )
	o_parser.add_argument( '--outfile', '-o',
							type=argparse.FileType( 'w' ),
							default='HM_dendrow.png',
							help='outfile, default is HM_dendrow.png.' )

	return o_parser

def analyseOptions( o_args ):

	global VERBOSE
	if( o_args.verbose ):
		VERBOSE = True


def verbose( msg ):
	if( VERBOSE ):
		sys.stderr.write( msg + "\n" )

# ----------------------------------------

if __name__ == '__main__':
	o_parser = defineOptions()
	o_args   = o_parser.parse_args()
	analyseOptions( o_args )
#	PP.pprint( o_args )

	main( o_args )

	sys.exit( 0 )

