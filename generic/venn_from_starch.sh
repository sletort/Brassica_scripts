#! /bin/bash

## AIMS  : generate Venn data from 2 starch files.
## USAGE : venn_from_starch.sh file1 file2 [outname]
## NOTE  : Will generate [outname.]common, [outname.]f1, [outname.]f2 starch files.
## NOTE  : It relies on bedops.
## AUTHORS : sletort@irisa.fr

set -o nounset
set -o errexit

# =========================================================
declare -r INFILE1="${1?What is your first input file ?}"
declare -r INFILE2="${2?What is your second input file ?}"
declare OUTNAME="${3-}"

if [[ "" == $OUTNAME ]]
then
	outname=""
else
	outname="$OUTNAME."
fi

# ---------------------------------------------------------

# =========================================================
function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT     = $0\n"
	printf "╟ - INFILE1    = $INFILE1\n"
	printf "╟ - INFILE2    = $INFILE2\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

# =========================================================
function get_chrom_list ()
{
	local -r file1="$1"
	local -r file2="$2"

	{
		unstarch --list-chr $file1
		unstarch --list-chr $file2
	} | sort -u
}

function compare_one_chrom ()
{
	local -r chrom="$1"
	local -r file1="$2"
	local -r file2="$3"
	local -r outdir="$4"

	bedops --chrom $chrom --element-of $file1 $file2 \
		| starch - > "$outdir/$chrom".common.starch
	bedops --chrom $chrom --not-element-of $file1 $file2 \
		| starch - > "$outdir/$chrom".f1_only.starch
	bedops --chrom $chrom --not-element-of $file2 $file1 \
		| starch - > "$outdir/$chrom".f2_only.starch

}

function main ()
{
	local -r outname="$1"

	local -r chrom_list=$( get_chrom_list $INFILE1 $INFILE2 )

	printf "Build venn files, chrom by chrom.\n"
	local tmp_dir=$( mktemp -d venn_from_starch.XXXX )
	for chr in $chrom_list
	do
		compare_one_chrom $chr $INFILE1 $INFILE2 $tmp_dir &
	done
	wait

	printf "Merge chrom files.\n"
	local note=$( unstarch --note $INFILE1 )
	for type in common f1_only f2_only
	do
		starchcat --note="$note" "$tmp_dir"/*.$type.starch \
			> ${outname}$type.starch &
	done
	wait

	rm -rf "$tmp_dir"
}

# =========================================================
printVars

# your work.
main "$outname"

printf "End of %s.\n\n" $( basename $0 )

exit 0
