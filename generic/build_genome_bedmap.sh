#! /bin/bash

## AIMS  : generate a bedfile of fixed size segment covering the given genome.
## AIMS  :	This map can then be used with bedmap tool.
## USAGE : ./build_genome_bedmap.sh [-f bed|starch] [-b 1M] genome > out
## NOTE  : genome is a [chr_lbl,size] file (tsv).
## AUTHORS : sletort@irisa.fr

set -o nounset
set -o errexit

declare -r __bpath=$( dirname $( readlink -e $0 ) )
source $__bpath/../lib/lib_misc.sh

# =========================================================
declare GENOME="";
declare FORMAT="starch";
declare BLOC_SIZE="1M";

# ---------------------------------------------------------
declare VERBOSE=0

# =========================================================
function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT     = $0\n"
	printf "╟ - GENOME     = $GENOME\n"
	printf "╟ - FORMAT     = $FORMAT\n"
	printf "╟ - BLOC_SIZE  = $BLOC_SIZE\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

function verbose ()
{
	local msg="$1"

	if [[ 1 -eq $VERBOSE ]]
	then
		printf "$msg\n" >&2
	fi
}

# =============================================
function manageOptions ()
{
# getopts reminder:
#	FLAG, x will look for -x
#	x: will look for -x stg
#	if the option string start with ':', getopts turns to silent error reporting mode.
	while getopts "f:b:hv" opt
	do
		case $opt in
			f)	FORMAT=$OPTARG  ;;
			b)	BLOC_SIZE=$OPTARG ;;

			v)	VERBOSE=1	;;
			h)
				usage && exit 0 ;;
			\?)	usage && exit 0 ;;
		esac
	done
	shift $((OPTIND-1))

	GENOME="${1?Please name the infile}"

	BLOC_SIZE=$( Misc__string_size_to_integer $BLOC_SIZE )
	__assert_vars
}

function __assert_vars ()
{
	local p=$( which bedops 2> /dev/null )
	local err=$?
	if [[ "0" -ne "$err" ]]
	then
		printf "bedops is not in the path.\n" >&2
		exit 1
	fi

	if [[ ! -f $GENOME ]]
	then
		printf "Genome %s is not reacheable.\n" "$GENOME" >&2
		exit 1
	fi

	if [ $FORMAT != "starch" -a $FORMAT != "bed" ]
	then
		printf "The format %s is not known, please use 'starch' or 'bed'.\n" \
			"$FORMAT" >&2
		exit 1
	fi
}

function usage ()
{
		cat <<EOF
USAGE : $0 [options] genome > map.starch

	genome           : The genome file. 2 cols: chrom\tsize. Mandatory

Options :
	-f string=starch : the output format, can be starch or bed.
	-b string=1M     : The size of the bloc where to count the elements.
	                     Can be an integer or shortcut like 1K,1M,1G

	-v : verbose mode, see parameter values.
	-h : print this message and exit.
EOF
}

# =========================================================
function main ()
{
	local tmp_lst=""
	while read chrom len
	do
		local tmp_bm=$( mktemp bgm.XXXX.starch )
		tmp_lst="$tmp_lst $tmp_bm"

		printf "$chrom\t0\t$len\n" \
				| bedops --chop $BLOC_SIZE - \
				| starch - \
			> $tmp_bm &
	done < $GENOME
	verbose "Waiting for all chroms to be processed.\n"
	wait

	starchcat $tmp_lst
	rm -f $tmp_lst
}

# =========================================================
manageOptions "$@"

if [[ 1 -eq $VERBOSE ]]
then
	printVars >&2
fi

# your work.
main

msg=$( printf "End of %s.\n" $( basename $0 ) )
verbose "$msg"

exit 0
