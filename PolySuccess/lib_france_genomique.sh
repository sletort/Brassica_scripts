#! /bin/bash

## USAGE : source lib_france_genomique.sh
## NOTE  : This are path definition of important files.
## AUTHORS : sletort@irisa.fr

set -o nounset
set -o errexit

# =========================================================
declare -r FG_RDF_PATH=/groups/brassica/sletort/RDF/FranceGenomique
mkdir -p $FG_RDF_PATH

declare -r FG_CULTIVAR_TTL=$FG_RDF_PATH/cultivars.ttl
declare -r FG_SEQ_TTL=$FG_RDF_PATH/seqs.ttl

declare    ROOT_PATH=$( dirname $( readlink -e ${BASH_SOURCE[0]} ) )
declare -r FG_CULTIVAR_DESCR_FILE="$ROOT_PATH/metadata/cultivar.descr.tsv"


declare -A __H_KEY_SPECIES
declare -A __H_KEY_CULTIVARS

# ---------------------------------------------------------
function lib__rapso__show_vars ()
{
	cat <<EOF
	FG_RDF_PATH :     $FG_RDF_PATH
	FG_CULTIVAR_TTL : $FG_CULTIVAR_TTL
	FG_SEQ_TTL      : $FG_SEQ_TTL

	FG_CULTIVAR_DESCR_FILE : $FG_CULTIVAR_DESCR_FILE
EOF
}

# infile is a 3 col file, I load only the 2 first
function lib__load_cultivar_descr ()
{
	local cultivar_file="$1"

	while IFS='	' read name species key_path
	do
#       if [[ \#* != "$species" ]] # assymetric operator ?
		if [[ "$name" != \#* ]]
		then
			__H_KEY_SPECIES[$key_path]="$species"
			__H_KEY_CULTIVARS[$key_path]="$name"
		fi
	done < $cultivar_file
}

function lib__uri_encode ()
{ # should be better written in the future
	printf "$1" | perl -ple 's/(\.)/\\$1/g'
}

function lib__get_keys ()
{
	printf "${!__H_KEY_SPECIES[*]}"
}
function lib__get_species ()
{
	local -r key="$1"

	printf "${__H_KEY_SPECIES[$key]}"
}
function lib__get_cultivar ()
{
	local -r key="$1"

	lib__uri_encode "${__H_KEY_CULTIVARS[$key]}"
}
function lib__get_keypath ()
{
	local -r key="$1"

	printf "$key"
}

function __lib__show_H_SPECIES ()
{
	for c in "${!__H_KEY_SPECIES[@]}"
	do
		printf "c : ${__H_KEY_SPECIES[$c]}\n"
	done
}
