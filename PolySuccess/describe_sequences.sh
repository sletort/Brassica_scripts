#! /bin/bash

## AIMS  : generate a turtle (rdf) description of France-Genomique sequences.
## USAGE : describe_sequences.sh cultivar_file.tsv
## NOTE  : This script aims to be used once only.
## AUTHORS : sletort@irisa.fr

declare -r SRC_PATH=$( dirname $( readlink -e $0 ) )

source $SRC_PATH/lib_france_genomique.sh

set -o nounset
set -o errexit

# =========================================================
declare -r CULTIVAR_FILE="${1-$FG_CULTIVAR_DESCR_FILE}"

# ---------------------------------------------------------
declare -r USER_SERVER=sletort
declare -r DATA_SERVER=genocluster2.genouest.org
declare -r DATA_ROOT_PATH=/groups/brassica/db/projects/france-genomique
declare -r MASK="b*"

# ---------------------------------------------------------
declare -A A_CULTIVARS

# =========================================================
function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT       = $0\n"
	printf "╟ - DATA_SERVER  = $DATA_SERVER\n"
	printf "╟ - DATA_ROOT_PATH = $DATA_ROOT_PATH\n"
	printf "╟ - MASK         = $MASK\n"
	printf "╟ - CULTIVAR_FILE= $CULTIVAR_FILE\n"
	printf "╟ - SEQ_TTL      = $FG_SEQ_TTL\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

# =========================================================
function init_seq_turtle ()
{
	cat <<EOF
@prefix edam: <http://edamontology.org/> .
@prefix obo: <http://http://purl.obolibrary.org/obo/> .
@prefix so: <http://purl.org/obo/owl/SO#> .
@prefix so_: <http://purl.obolibrary.org/obo/so-xp.obo#> .
@prefix cultivars: <file://${FG_CULTIVAR_TTL}#> .
@prefix fg_seq: <file://${FG_SEQ_TTL}#> .

EOF
}

function guessCultivar ()
{
# with the directory structure this function could be far most simple : read the directory name :).
	local -r filepath="$1"

	for k in $( lib__get_keys )
	do
		local keypath=$( lib__get_keypath $k )
		if [[ "$filepath" == $DATA_ROOT_PATH/$MASK/*/$keypath* ]]
		then
			printf "%s" $( lib__get_cultivar $k )
		fi
	done
}


function main ()
{
	printf "Loading $CULTIVAR_FILE\n"
	lib__load_cultivar_descr "$CULTIVAR_FILE"

	init_seq_turtle > $FG_SEQ_TTL

	local quality
	local uri
	for f in $( ssh ${USER_SERVER}@$DATA_SERVER find $DATA_ROOT_PATH/b* -name '*.fastq.gz' )
	do
		local cultivar=$( guessCultivar "$f" )
		printf "$f\n\t$cultivar\n" >&2

		if [[ "" == "$cultivar" ]]
		then
			printf "No cultivar found for $f\n" >&2
			cultivar="$f"
			continue
		fi

		if [[ $f == *"_1_C"* || $f == *"_reads1"* ]]
		then
			uri="fg_seq:${cultivar}_f"
			quality="so_:has_quality so:SO_0001030 ; # forward"
		elif [[ $f == *"_2_C"* || $f == *"_reads2"* ]]
		then
			uri="fg_seq:${cultivar}_r"
			quality="so_:has_quality so:SO_0001031 ; # reverse"
		fi

		tee <<EOF
$uri
	a so:SO_0000007 ; # read_pair
	a so:SO_0001507 ; # variant collection
	obo:RO_0001025 <file://$DATA_SERVER$f> ; # located_in
	edam:has_format edam:format2182 ; # fastq-like (text)
	$quality
EOF

		if [[ "$cultivar" != "$f" ]]
		then
			printf "\tso_:sequence_of cultivars:$cultivar\n"
		fi
		printf ".\n\n"
	done >> $FG_SEQ_TTL
}

# =========================================================
printVars

# your work.
main

printf "End of %s.\n\n" $( basename $0 )

exit 0
