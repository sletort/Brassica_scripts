#! /bin/bash

## AIMS  : generate a turtle (rdf) description of france-genomique cultivars.
## USAGE : describe_cultivars.sh [cultivar.descr.tsv]
## NOTE  : This script aims to be used once only.
## AUTHORS : sletort@irisa.fr

declare -r SRC_PATH=$( dirname $( readlink -e $0 ) )

source $SRC_PATH/lib_france_genomique.sh

set -o nounset
set -o errexit

# =========================================================
declare -r CULTIVAR_FILE="${1-$FG_CULTIVAR_DESCR_FILE}"

# ---------------------------------------------------------
# id in NCBITAXON
declare -A H_SPECIES=([brapa]=3711 [boleracea]=3712 [bnapus]=3708 [bnapus_synt]=3708)

# =========================================================
function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT         = $0\n"
	printf "╟ - CULTIVAR_FILE  = $CULTIVAR_FILE\n"
	printf "╟ - FG_CULTIVAR_TTL= $FG_CULTIVAR_TTL\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

# =========================================================
function print_cultivar_turtle ()
{
	local outfile="$1"

{
	printf "@prefix obi:  <http://purl.obolibrary.org/obo/> .\n"
	printf "@prefix ncbi: <http://purl.bioontology.org/ontology/NCBITAXON/> .\n"
	printf "@prefix cultivars: <file://$outfile#> .\n"
	printf "\n"

	for key in $( lib__get_keys )
	do
		local cultivar=$( lib__get_cultivar $key )
		printf "$cultivar\n" >&2

		printf "cultivars:$cultivar\n"
		printf "\ta obi:OBI_0001185 ; # cultivar\n"

		sp=$( lib__get_species $key )
		id=${H_SPECIES[$sp]}
		printf "\trdfs:subClassOf ncbi:$id ;	# $sp\n"

		if [[ "$sp" == *_synt ]]
		then
			printf "\trdfs:subClassOf ncbi:442485 ; # oleracea x rapa pekinensis\n"
		fi
		printf ".\n"
	done
} > "$outfile"

	# I used outfile as a parameter to use it as prefix in the file.
}


function main ()
{
	printf "Loading $CULTIVAR_FILE\n"
	lib__load_cultivar_descr "$CULTIVAR_FILE"

	print_cultivar_turtle "$FG_CULTIVAR_TTL"
}

# =========================================================
printVars

# your work.
main

printf "End of %s.\n\n" $( basename $0 )

exit 0
