#! /bin/bash

# shell to use
#$ -S /bin/bash

#$ -M sletort@irisa.fr
# send mail before b, at the end e, on stop a, on suspend s, or never n
#$ -m bea

#$ -N discoSNP.ref.3SNP_40b

# if the script should be run in the current directory
#$ -cwd
##$ -i input_file
##$ -o output_file

# merge stdout and stderr
#$ -j no


# ne pas utiliser, ça ne sert qu'à afficher ce qui va être envoyer à la queue ?
# check submitted task ( -> ça veut dire quoi ?)
##$ -verify


## AIMS  : run a job on the cluster biogenouest
## USAGE : qsub -l h=cl1n027 run_cluster.discoSNP.sh

# init env X
source /softs/local/env/envpython-2.7.sh
source /softs/local/env/envbwa-0.7.10.sh
#/softs/local/env/envgcc-4.9.1.sh

source /omaha-beach/sletort/brassicaDB.mine.sh

# le script
# -r file of files = entry
# -t extend found polymorphisms with unitigs
# -l remove low complexity bubbles
# -k length of used kmers
# -n do not compute the genotypes
# -u max number of used threads
# -g réutiliser le fichier h5 existant.
# -P search up to P SNPs in a unique bubble
# -G reference genome file

## -R use the reference file also in the variant calling, not only for mapping results

root=${__BRASSICA_DB_SNP_DETECTION_ROOT}
$root/run_discoSnp++.sh -r $root/fof.txt -t -l -k 41 -n -u 40 -g -P 3 -G $( DB__get_ref_path bnapus )

exit 0
