#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

## AIMS  : Generate VCF file from Discosnp++ filtered blast result.
## USAGE : ./generate_vcf_from_disco_filtered_blast.py [] [-o outname] filtered.tsv.gz samples.lst
## AUTHORS : sletort@irisa.fr
## NOTE  : input is a tsv file like chrom, pos, var_id, ref, all_up|all_low, '|' separated list of upper counts, '|' separated list of lower counts.
## NOTE  : I suppose that all counts are for forward and reverse pairs : seq1_f|seq1_r|seq2_f|seq2_r...

import sys
import pprint # debug
import argparse	# option management, close to C++/boost library (?)
import gzip
import csv
#~ import pysam	# generation des VCF ... too complex to create a vcf from scratch !

import os	# for local libraries

# local libraries
f_path = os.path.dirname( os.path.realpath( __file__ ) )
sys.path.append( f_path + "/../lib" )

import Vcf # as pysam is too complex
from Misc import open_infile

VERSION   = "0.1"
PP = pprint.PrettyPrinter( indent=4, stream=sys.stderr )
VERBOSE   = False

REFERENCE    = 'Darmor-bzh_v4-1' # should be read from the infile, i think
CHROM_PREFIX = REFERENCE + '_'

# ========================================
def init_Vcf_writer( outfile, l_samples ):
	o_info_disco = Vcf.Info({
					'ID'     : 'DS',
					'Number' : '1',
					'Type'   : 'Integer',
					'Description' : 'DiscoSnp_id',
				})
	o_fmt_gt     = Vcf.Format({
					'ID'     : 'GT',
					'Number' : '1',
					'Type'   : 'String',
					'Description': '"Genotype (0:Ref, 1: alt1, 2, alt2, ...)."',
				})
	o_fmt_depth  = Vcf.Format({
					'ID'     : 'DP',
					'Number' : 'R',	# one value per allele
					'Type'   : 'Integer',
					'Description': '"Number of reads holding the allele."',
				})
	o_fmt_f_depth  = Vcf.Format({
					'ID'     : 'DPF',
					'Number' : 'R',	# one value per allele
					'Type'   : 'Integer',
					'Description': '"Number of reads holding the allele on forward strand."',
				})
	o_fmt_r_depth  = Vcf.Format({
					'ID'     : 'DPR',
					'Number' : 'R',	# one value per allele
					'Type'   : 'Integer',
					'Description': '"Number of reads holding the allele on reverse strand."',
				})
	d_meta = {
			'reference' : REFERENCE,
			'source'    : __file__,
			'INFO'      : [ o_info_disco ],
			'FORMAT'    : [ o_fmt_gt, o_fmt_depth,
			                o_fmt_f_depth, o_fmt_r_depth ]
		}

	o_vcf  = Vcf.Writer( outfile, d_meta, l_samples )

	return o_vcf

def manage_alleles( ref, all_up, all_down ):
	'''discoSNP produce up and down alleles, I need to convert them to ref, and alternate alleles.
		This function will build alternate allele string and determine the indices
		that will be used with forward/reverse count latter.
		return i_up, i_down, l_alleles'''

	if ref == all_up:
		return 0,1, [ ref, all_down ]
	if ref == all_down:
		return 1,0, [ ref, all_up ]

	# else no ref
	return 1,2, [ ref, all_up, all_down ]

# generator
def parse_file( infile, fn_build_sample_dict ):
	with open_infile( infile ) as o_fe:
		o_tsv = csv.reader( o_fe, delimiter='\t' )
		for l_elts in o_tsv:
			# could use the InputRecord class I made in build_inra_matrix.
			#	but I only need direct access to the first 5 columns ...
			# PP.pprint( l_elts[0:5] )
			chrom = l_elts[0].lstrip( CHROM_PREFIX )
			( pos, var_id, ref, alt_str ) = l_elts[1:5]

			( i_u, i_d, l_alleles ) = manage_alleles( ref, *alt_str.split( '|' ))

			ld_samples = fn_build_sample_dict( i_u, i_d, *l_elts[5:7] )

			o_rec = Vcf.VariantRecord( chrom, pos, l_alleles )
			o_rec.updateInfo( ( 'DS',str( var_id ) ) )
			for d_sample in ld_samples:
				o_rec.addSampleData( d_sample )

			yield o_rec
	return

def get_sample_dict_builder( l_samples ):
	''''''
	def build_sample_dict( i_up, i_down, str_count_up, str_count_down ):
		'''str_count_* are string for discoSNP count upper/lower path.
			each count contains forward and reverse count.'''
		# i_up/down is the allele indice of up/down alleles, max is 2
		# N_alleles is the number of alleles to consider.
		N_alleles = max( i_up, i_down ) + 1

		def __compute_DP( l_forward, l_reverse ):
			l_dp = [ 0 ] * N_alleles
			
			for i in range( N_alleles ):
				if '.' != l_forward[i]:
					l_dp[i] += int( l_reverse[i] ) + int( l_forward[i] )

			return l_dp

		def __get_sample_counts_as_dict( i, l_up, l_down ):
			'''i is the sample index'''
			#~ sample = l_samples[i]
			up_f   = l_up[2*i]
			up_r   = l_up[2*i+1]
			down_f = l_down[2*i]
			down_r = l_down[2*i+1]
			# PP.pprint([ sample, up_f, up_r, down_f, down_r ])

			l_forward = [ '.' ] * N_alleles	# default values
			l_forward[i_up]   = str(up_f)
			l_forward[i_down] = str(down_f)
			str_forward = ','.join( l_forward )
			#PP.pprint({ 'DPF': str_forward })

			l_reverse = [ '.' ] * N_alleles # default values
			l_reverse[i_up]   = str(up_r)
			l_reverse[i_down] = str(down_r)
			str_reverse = ','.join( l_reverse )
			#PP.pprint({ 'DPR': str_reverse })

			l_dp = __compute_DP( l_forward, l_reverse )
			str_dp = ','.join([ str( x ) for x in l_dp ])
			#PP.pprint({ 'DP': str_dp })

			max_dp = max( l_dp )
			str_gt = '.'
			for i in range( len(l_dp) ):
				if l_dp[i] == max_dp:
					str_gt = str( i )
					break

			return { 'GT': str_gt, 'DP': str_dp,
						'DPF': str_forward, 'DPR': str_reverse }
		#PP.pprint([ i_up, i_down ])

		l_up   = [ int(x) for x in str_count_up.split( '|' ) ]
		l_down = [ int(x) for x in str_count_down.split( '|' ) ]

		n_expected = 2*len( l_samples )
		if len( l_up ) != n_expected :
			raise Exception( "Bad counts : there's not enough counts in up_counts {}.".format(str_count_up) )
		if len( l_down ) != n_expected:
			raise Exception( "Bad counts : there's not enough counts in down_counts.".format(str_count_down) )

		ld_samples = []
		for i in range( len(l_samples) ):
			d_sample = __get_sample_counts_as_dict( i, l_up, l_down )
			ld_samples.append( d_sample )

		return ld_samples

	return build_sample_dict

# ----------------------------------------
def main( o_args ):

	l_samples = [ x.rstrip() for x in o_args.samples.readlines() ]
	o_vcf     = init_Vcf_writer( o_args.outfile, l_samples )

	fn_build_sample_dict = get_sample_dict_builder( l_samples )

	verbose( "Reading the input file" )
	lo_variants = []
	for l_var_elts in parse_file( o_args.blast_filtered, fn_build_sample_dict ):
		# PP.pprint( l_var_elts )
		lo_variants.append( l_var_elts )

		# PP.pprint([ chrom, pos, var_id, ref, alt ])

	verbose( "sort all the {} records".format( len( lo_variants ) ) )
	lo_variants.sort( key=lambda o_rec: ( o_rec.seq_id, int(o_rec.pos) ) )

	verbose( "write all the records." )
	o_vcf.writeHeader()
	for o_rec in lo_variants:
		o_rec.write( o_vcf )

	return 0

# ----------------------------------------
def defineOptions():
	descr = "Generate VCF file from Discosnp++ filtered blast result."
	o_parser = argparse.ArgumentParser( description=descr )

	# special
	o_parser.add_argument( '--version', action='version', version='%(prog)s ' + VERSION )

	# positionnal / mandatory param
	o_parser.add_argument( 'blast_filtered',nargs='?',
							#~ type=argparse.FileType( 'r' ),
							default=sys.stdin,
							help='discosnp++ blast filtered result. can be gziped.' )

	# mandatory param
	o_parser.add_argument( '--samples', '-s',
						type=argparse.FileType( 'r' ),
						required=True,
		               help='sample list, in the order of the variant count.' )

	# optionnal
	o_parser.add_argument( '--outfile', '-o',
						type=argparse.FileType( 'w' ),
						default=sys.stdout,
		               help='vcf outfile.' )

	#~ o_parser.add_argument( '--output-type', '-O',
						#~ #type=argparse.FileType( 'w' ),
						#~ choices=[ 'b', 'u', 'z', 'v' ]
						#~ default='u',
		               #~ help='Output compressed BCF (b), uncompressed BCF (u), compressed VCF (z), uncompressed VCF (v).' )

	o_parser.add_argument( '--verbose', '-v', action='store_true',
		               help='If set give info on what happen.' )

	return o_parser

def analyseOptions( o_args ):

	global VERBOSE
	if( o_args.verbose ):
		VERBOSE = True

	return

def verbose( msg ):
	if( VERBOSE ):
		sys.stderr.write( msg + "\n" )


# ----------------------------------------

if __name__ == '__main__':
	o_parser = defineOptions()
	o_args   = o_parser.parse_args()
	analyseOptions( o_args )
	# PP.pprint( o_args )

	sys.exit( main( o_args ) )

