#! /usr/bin/env python2.7

# indel are not considered !
# do not print heads anymore => will work on chunks

## AIMS  :
## USAGE : ./build_inra_matrix.py blast_filtered > matrix.tsv
## AUTHORS : sletort@irisa.fr
## NOTE  : input is a tsv file like chrom, pos, var_id, ref, all_up|all_low, '|' separated list of upper counts, '|' separated list of lower counts.
## NOTE  : I suppose that all counts are for forward and reverse pairs : seq1_f|seq1_r|seq2_f|seq2_r...

import sys
import os
import pdb
import pprint
import fileinput

MIN_COUNT_THRES = 5
#~ MAX_COUNT_THRES = 200 # 100 forward + 100 reverse ?
MAX_COUNT_THRES = 100
MIN_FREQ_HOMO   = 0.95

NO_COUNT_TAG   = 'NA'
FEW_COUNT_TAG  = 'XcovMin'	# counts < MIN_COUNT_THRES
MUCH_COUNT_TAG = 'XcovMax'	# MAX_COUNT_THRES < counts
HETERO_TAG     = 'Xfreq'
NO_STRAND_TAG  = 'Xfor'		# forward = 0 or reverse = 0

# cols idx
CHROM = 0
POS   = 1
ID    = 2
REF   = 3
ALT   = 4
UPPER = 5
LOWER = 6

# ----------------------------------------
class InputRecord:
# TODO make an iterator on samples ?

	# index for attribute __l_counts
	__UP_FOR  = 0
	__UP_REV  = 1
	__LOW_FOR = 2
	__LOW_REV = 3

	def __init__( self, line ):
		l_elts = line.rstrip().split( "\t" )

		self.__chrom = l_elts[CHROM]
		self.__pos   = l_elts[POS]
		self.__id    = l_elts[ID]
		self.__ref   = l_elts[REF]

		l_alleles = l_elts[ALT].split( "|" )
		self.__upper_allele = l_alleles[0]
		self.__lower_allele = l_alleles[1]

		# for each record, 4 counts : (Up,Low)x(For,Rev)
		#	and for each sample !
		# l_counts is a list of 2D matrix
		self.__l_counts = []
		l_up_counts  = [ int( x ) for x in l_elts[UPPER].split( "|" ) ]
		l_low_counts = [ int( x ) for x in l_elts[LOWER].split( "|" ) ]
		self.__setCounts( l_up_counts, l_low_counts )

		#~ print self.__l_counts

	def __setCounts( self, l_up_counts, l_low_counts ):
		for i in range( 0,len(l_up_counts), 2 ) :
			self.__l_counts.append( l_up_counts[i:i+2] + l_low_counts[i:i+2] )

	def getUpperCounts( self, i ):
		l_ = [ self.__l_counts[i][x] for x in [ self.__UP_FOR, self.__UP_REV ] ]
		return sum( l_ )

	def getLowerCounts( self, i ):
		l_ = [ self.__l_counts[i][x] for x in [ self.__LOW_FOR, self.__LOW_REV ] ]
		return sum( l_ )

	def getForwardCounts( self, i ):
		l_ = [ self.__l_counts[i][x] for x in [ self.__UP_FOR, self.__LOW_FOR ] ]
		return sum( l_ )

	def getReverseCounts( self, i ):
		l_ = [ self.__l_counts[i][x] for x in [ self.__UP_REV, self.__LOW_REV ] ]
		return sum( l_ )

		
	@property
	def chrom( self ):
		return self.__chrom

	@property
	def pos( self ):
		return self.__pos

	@property
	def id( self ):
		return self.__id

	@property
	def ref( self ):
		return self.__ref

	@property
	def upper_allele( self ):
		return self.__upper_allele

	@property
	def lower_allele( self ):
		return self.__lower_allele

	@property
	def n_samples( self ):
		return len( self.__l_counts )

# ----------------------------------------
def print_out( h_output, l_elts ):
	h_output.write( "\t".join( l_elts ) + "\n" )


# return a qualified TAG or max_allele
def getTag( max_count, min_count, max_allele ):
	if( max_count < MIN_COUNT_THRES ):
		return FEW_COUNT_TAG

	if( MAX_COUNT_THRES < max_count ):
		return MUCH_COUNT_TAG


	freq = max_count / ( max_count + min_count )
	if( freq < MIN_FREQ_HOMO ):
		return HETERO_TAG
	else:
		return max_allele


def main( infile, h_output ):

#	head_flg = 0
#	l_heads = [ "#Chrom", "Pos", "Ref"]

	for line in fileinput.input( infile ):
		o_record = InputRecord( line )

#		if( 0 == head_flg ):
#			# add headers C1 C2 C3 ... twice
#			l_heads.extend( ["C"+str(i) for i in range(n_samples) ] )
#			l_heads.extend( ["C"+str(i) for i in range(n_samples) ] ) # for exploration only
#			print_out( h_output, l_heads )
#			head_flg = 1

		l_out = [ o_record.chrom, o_record.pos, o_record.id, o_record.ref ]

		# tout mettre dans la fonction getTag ?
		for i in range( 0, o_record.n_samples ):
			up_count  = o_record.getUpperCounts( i )
			low_count = o_record.getLowerCounts( i )

			if( 0 == up_count and 0 == low_count ):
				tag = NO_COUNT_TAG
			else:
				for_count = o_record.getForwardCounts( i )
				rev_count = o_record.getReverseCounts( i )

				if( 0 == for_count or 0 == rev_count ):
					tag = NO_STRAND_TAG
				elif( low_count <= up_count ):
					tag = getTag( up_count, low_count, o_record.upper_allele )
				else:
					tag = getTag( low_count, up_count, o_record.lower_allele )

			l_out.append( tag )

#		l_out.extend( [ l_upper[i]+'|'+l_lower[i] for i in range( n_samples ) ] ) # exploration
		print_out( h_output, l_out )


# ----------------------------------------

if __name__ == '__main__':
	infile  = sys.argv[1]
	if( 2 < len( sys.argv ) ):
		h_output = open( sys.argv[2], 'w' )
	else:
		h_output = sys.stdout

	main( infile, h_output )

	sys.exit( 0 )

