#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import pdb
import pprint
import fileinput

#---------------------------
# num of cols
Q_ID     =  0
Q_START  =  1
Q_END    =  2
Q_SEQ    =  3
S_ID     =  4
S_START  =  5
S_END    =  6
S_SEQ    =  7
Q_LEN    =  8
LEN      =  9
BITSCORE = 10
SCORE    = 11
P_IDENT  = 12
N_IDENT  = 13
MISMATCH = 14
POSITIVE = 15
GAP_OPEN = 16
GAPS     = 17
P_POS    = 18
BTOP     = 19
SNP_OFFSET = 20 # not part of blast output, my addition (do deal with insertion when query seq is on minus strand

WINDOW_LEN = 48 # kmer length - 1 for disco
NB_MISMATCH_MAX = 3 # before or after the SNP

#---------------------------
# WARN : subject can be on minus strand => s_end < s_start

#---------------------------
# count mismatch before and after the SNP
def check_alignement( q_start, q_end, q_seq, s_seq ):
	# full query is WINDOW_LEN*2 + 1 long, so q_seq is shorter
	mismatch_5p = q_start - 1
	mismatch_3p = WINDOW_LEN*2 + 1 - q_end
	snp_pos     = WINDOW_LEN # to make code easier to read

	snp_pos_offset = 0 # will store indel length
	global_i = q_start - 1
	for i in range( len(q_seq) ):
		if( q_seq[i] == '-' ):
			if( global_i < snp_pos ):
				mismatch_5p += 1
				snp_pos_offset += 1
			elif( snp_pos < global_i ):
				mismatch_3p +=1
		else:
			if( q_seq[i] != s_seq[i] ):
				if( global_i < snp_pos ):
					mismatch_5p += 1
				elif( snp_pos < global_i ):
					mismatch_3p += 1
			global_i += 1

	return [ mismatch_5p, mismatch_3p, snp_pos_offset ]


# corrected_SNP_pos is 0-based
def extract_allele_from_seq( corrected_snp_pos, q_seq, s_seq ):
	for i in range( len( q_seq ) ):
		if( q_seq[i] == '-' ): # et pour les InDel ?check_alignement
			corrected_snp_pos += 1
		if( i == corrected_snp_pos ):
			return s_seq[i]
	return None

def __parse_read_counts( l_count_strs ):
	l_counts = []
	for count_str in l_count_strs:
		l_counts.append( count_str.split('_')[2] )
	return l_counts

def __split_record_id( record_id ):
	l_elts = record_id.split( '|' )

	nb_elts_left   = len( l_elts ) - 3
	first_upper_count = 3
	first_lower_count = 3 + nb_elts_left/2
	l_upper_counts = __parse_read_counts( l_elts[first_upper_count:first_lower_count] )
	l_lower_counts = __parse_read_counts( l_elts[first_lower_count:] )

	alleles = l_elts[2]
	d_elts  = dict( SNP_id    = l_elts[0],   SNP_pos  = l_elts[1],
					upper_all = alleles[0], lower_all = alleles[1],
					l_upper_counts = l_upper_counts,
					l_lower_counts = l_lower_counts )
	return d_elts


# alleles should be in upper case (we are into a kmer sequence)
# ... what about subject sequence ?
# je ne sais pas encore passer les variables par référence
def complementAlleles( l_alleles ):
	l_out = []
	for allele in l_alleles:
		if( allele == 'A' ):
			l_out += 'T'
		elif( allele == 'C' ):
			l_out += 'G'
		elif( allele == 'G' ):
			l_out += 'C'
		elif( allele == 'T' ):
			l_out += 'A'
		elif( allele == '-' ):
			l_out += allele
		else:
			print "case not coded into complementAlleles function.\n"
			l_out += allele
	return l_out


# d_records : clef = query_id
# value = [ 1, cols of the blast row]
# NOTE : I do not remember why '1' as first value ... I was begining in python
def printSelectedRecords( h_output, d_records ):
#	pp = pprint.PrettyPrinter(indent=4)
#	pp.pprint( d_records )
#	print len( d_records )
	for k in sorted( d_records.iterkeys() ):
		l_elts   = d_records[k][1]
		d_record_elts = __split_record_id( l_elts[Q_ID] )

		corrected_SNP_pos  = int( d_record_elts['SNP_pos'] ) - int(l_elts[Q_START]) +1 + l_elts[SNP_OFFSET]
		# I use (corrected_SNP_pos-1) to make it 0-based
		ref_all = extract_allele_from_seq( corrected_SNP_pos-1, l_elts[Q_SEQ], l_elts[S_SEQ] )

		# strand check
		s_start = int( l_elts[S_START] )
		s_end   = int( l_elts[S_END] )
		alt1    = d_record_elts['upper_all']
		alt2    = d_record_elts['lower_all']
		if( s_end < s_start ): #minus strand
			[ref_all, alt1, alt2] = complementAlleles( [ref_all, alt1, alt2] )
			pos = s_start - corrected_SNP_pos + 1
		else:
			pos = s_start + corrected_SNP_pos - 1

		l_out = [ l_elts[S_ID], str( pos ),
					d_record_elts['SNP_id'],
					ref_all,
					'|'.join( [ alt1, alt2 ] ),
					'|'.join( d_record_elts['l_upper_counts'] ),
					'|'.join( d_record_elts['l_lower_counts'] ) ]
		h_output.write( "\t".join( l_out ) + "\n" )


def main( infile, h_output ):
	aln_mini_len = 2*( WINDOW_LEN - NB_MISMATCH_MAX )

	d_records = {}
	for line in fileinput.input( infile ):
		l_elts = line.rstrip().split( "\t" )
		l_elts.append( 0 ) # SNP_OFFSET

		# not the best sequence : with this we kept one of 2 ex aequo
		if( l_elts[Q_ID] in d_records
			and None == d_records[ l_elts[Q_ID] ][1] ):
				continue
		

		if( len( l_elts[Q_SEQ] ) < aln_mini_len ):
			continue

#		pdb.set_trace()

		( mismatch_5p, mismatch_3p, snp_pos_offset ) = check_alignement( int( l_elts[Q_START] ), int( l_elts[Q_END] ), l_elts[Q_SEQ],l_elts[S_SEQ] )
		l_elts[SNP_OFFSET] = snp_pos_offset
#		print l_elts[Q_ID], " (", l_elts[BTOP], ") :", mismatch_5p, " 5p <=> 3p ", mismatch_3p
		if( mismatch_5p <= NB_MISMATCH_MAX and mismatch_3p <= NB_MISMATCH_MAX ):
			if( l_elts[Q_ID] not in d_records ):
				d_records[ l_elts[Q_ID] ] = [1, l_elts];
			else:
				d_records[ l_elts[Q_ID] ][1] = None

	# remove non uniq alignments
	l_keys = d_records.keys()
	for k in l_keys:
		if( None == d_records[k][1] ):
			del d_records[k]

	printSelectedRecords( h_output, d_records )

# ----------------------------------------

if __name__ == '__main__':
	infile  = sys.argv[1]
	if( 2 < len( sys.argv ) ):
		h_output = open( sys.argv[2], 'w' )
	else:
		h_output = sys.stdout

	main( infile, h_output )

	sys.exit( 0 )

