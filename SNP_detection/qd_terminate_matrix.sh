#! /bin/bash

## AIMS  : adjust produced matrix to fit the reference matrix.
## AIMS  : 	Change chrom name, reorder columns and produce a starch file.
## USAGE : qd_terminate_matrix.sh infile.tsv 
## NOTE  : produce infile.starch.
## NOTE  : Column order is hard coded, works only on the 29 species matrix.
## AUTHORS : sletort@irisa.fr

set -o nounset
set -o errexit

# =========================================================
declare -r INFILE="${1?What is your matrix file ?}"

# ---------------------------------------------------------

# =========================================================
function check_dependencies ()
{
	local path=$( which starch 2> /dev/null )
	local err=$?

	if [[ 0 -ne $err ]]
	then
		printf "starch is not in the path, maybe bedops is not loaded.\n" \
			>&2
	fi
}

function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT     = $0\n"
	printf "╟ - INFILE     = $INFILE\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

# =========================================================
function main ()
{
	local bname=$( basename $INFILE .tsv )

	# change chrom name
	local perle='$F[0] =~ s/^Darmor-bzh_v4-1_/chr/;'

	# reorder and turn to bed
	perle=$perle' print join( "\t", $F[0], $F[1]-1, @F[1..2, 10..13, 19..31, 3..9, 14..18] );'

	local header=$( head -n 1 $INFILE \
					| perl -lane "$perle" \
					| cut -f 1-3 --complement )

	tail -n +2 $INFILE \
			| perl -lane "$perle" \
			| starch --note="$header" - \
		> $bname.starch
}

# =========================================================
check_dependencies

printVars

# your work.
main

printf "End of %s.\n\n" $( basename $0 )

exit 0
