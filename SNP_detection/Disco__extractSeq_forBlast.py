#!/usr/bin/env python
#-*- coding:utf-8 -*-

import sys
import os
import pdb
import pprint

DISCO_PATH = os.environ['HOME']+'/logiciels/DiscoSNP++-2.2.4-Source/tools'
sys.path.append( DISCO_PATH )

from functionObjectVCF_creator import *
from ClassVCF_creator import *


def getCoverageString( path ):
	"""compute a string representing the path's coverage.
		It is a colon separated list of each species count."""

	#compute/extract coverage.
	path.GetCoverage()

	l_species_count = []
	for count in path.listCoverage:
		l_species_count.append( count )

	return ":".join( l_species_count )


def computeVariantString( a_var, offset ):
	"""Build a string for one variant and return it.
		It a a colon separated duplet.
		position (1-based):alleles.
		Offset (unitigLeft for the moment) will be added to each variant position,
		to fit the whole sequence."""

	pos = int( a_var[0] ) + offset + 1
	allele_string = a_var[1] + a_var[2]

	return ':'.join( [ str(pos), allele_string ] )

def main( infile, outfile ):

	h_input  = open( infile, 'rU' )
	h_output = open( outfile, 'w' );

	i = 0
	# +/- copy of VCF_creator.py code
	while True:
		head1 = h_input.readline()
		if not head1: break;
		seq1  = h_input.readline();
		head2 = h_input.readline()
		seq2  = h_input.readline()

		if( head1.startswith( '>INDEL_' ) ):
			continue

		v, vcf_field = InitVariant( head1, head2 )
		if( 1 == v ):
#ne fonctionne pas			print( "WARNING : ", head1, head2, file=sys.stderr )
			sys.exit( 1 )

#		table=UnmappedTreatement(variant_object,vcf_field_object,nbGeno,seq1,seq2)
#		variant_object.FillVCF(VCFFile,nbGeno,table,vcf_field_object)

		# print new fasta record
#		print v.variantID
#		print v.contigLeft
#		print v.unitigLeft
#		print v.unitigRight
#		print v.dicoAllele #?
#		print v.rank
#		print v.mappingPositionCouple #?
#		print v.upper_path.GetSeq(seq1) # ?

		l_elts  = [ v.variantID ]
		l_elts.append( getCoverageString( v.upper_path ) )
		l_elts.append( getCoverageString( v.lower_path ) )


		# loop over variants found on the branch
		for var in sorted( v.dicoAllele.keys() ):
			var_str = computeVariantString( v.dicoAllele[var], int( v.unitigLeft ) )
			l_elts.append( var_str )

		h_output.write( '>' + '|'.join( l_elts ) + "\n" )

		if( v.contigLeft ):
			start_pos    = int( v.contigLeft ) - int( v.unitigLeft )
			pre_kmer_len = int( v.contigLeft )
		else:
			start_pos    = 0
			pre_kmer_len = int( v.unitigLeft )

		kmer_size = int( v.dicoAllele['P_1'][0] )

		post_kmer_len = 0
		if( v.contigRight ):
			post_kmer_len += int( v.contigRight )
		stop_pos = len( seq1 ) - post_kmer_len

#		stop_pos = pre_kmer_len + kmer_size*2+1 + int( v.unitigRight )
		h_output.write( seq1[start_pos:stop_pos] + "\n" )

	h_input.close()
	h_output.close()

# ----------------------------------------
if __name__ == '__main__':
	infile  = sys.argv[1]
	outfile = sys.argv[2]
	main( infile, outfile )
	sys.exit( 0 )

