#!/usr/bin/env python
#-*- coding:utf-8 -*-

import sys
import os
import pdb
import pprint

DISCO_PATH = os.environ['HOME']+'/logiciels/DiscoSNP++-2.2.4-Source/tools'
sys.path.append( DISCO_PATH )

from functionObjectVCF_creator import *
from ClassVCF_creator import *

def main( infile, outfile ):

# note inra use 50 as seq len around SNP, we will use 49 (=kmer size).
	h_input  = open( infile, 'rU' )
	h_output = open( outfile, 'w' );

	# +/- copy of VCF_creator.py code
	while True:
		head1 = h_input.readline()
		if not head1: break;
		seq1  = h_input.readline();
		head2 = h_input.readline()
		seq2  = h_input.readline()

		if( head1.startswith( '>INDEL_' ) ):
			continue

		v, vcf_field = InitVariant( head1, head2 )
		if( 1 == v ):
#ne fonctionne pas			print( "WARNING : ", head1, head2, file=sys.stderr )
			sys.exit( 1 )

#		table=UnmappedTreatement(variant_object,vcf_field_object,nbGeno,seq1,seq2)
#		variant_object.FillVCF(VCFFile,nbGeno,table,vcf_field_object)

		# print new fasta record
#		print v.variantID
#		print v.contigLeft
#		print v.unitigLeft
#		print v.unitigRight
#		print v.dicoAllele #?
#		print v.rank
#		print v.mappingPositionCouple #?
#		print v.upper_path.GetSeq(seq1) # ?

		kmer_size = int( v.dicoAllele['P_1'][0] ) + 1
		SNP_pos   = kmer_size
		l_elts    = [ v.variantID, str(SNP_pos) ]
		l_elts.append( v.dicoAllele['P_1'][1]+v.dicoAllele['P_1'][2] )

		# how to loop over upper/lower ?
		v.upper_path.GetCoverage()
		v.lower_path.GetCoverage()
		for count in v.upper_path.listCoverage:
			l_elts.append( 'upper_C_' + count )
		for count in v.lower_path.listCoverage:
			l_elts.append( 'lower_C_' + count )

		h_output.write( '>' + '|'.join( l_elts ) + "\n" )

		if( v.contigLeft ):
			start_pos = int( v.contigLeft )
		else:
			start_pos = int( v.unitigLeft )
		stop_pos  = start_pos + kmer_size*2-1
		h_output.write( seq1[start_pos:stop_pos] + "\n" )

	h_input.close()
	h_output.close()

# ----------------------------------------
if __name__ == '__main__':
	infile  = sys.argv[1]
	outfile = sys.argv[2]
	main( infile, outfile )
	sys.exit( 0 )

