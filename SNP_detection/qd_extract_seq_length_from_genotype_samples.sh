#!/bin/bash

## AIMS  : extract sequence length from a list of chrom/pos file.
## USAGE : qd_extract_seq_length_from_genotype_samples.sh $sample_file $datadir [$outfile]
## NOTE  : sample_file must have chrom and pos on col 1 and 2
## NOTE  : a directory named $sample_file.files will be created to contain intermediate files.

set -o nounset
set -o errexit

# ==============================================
declare -r SAMPLE_FILE="${1?What is the sample file ?}"
declare -r DATADIR="${2?Which data directory?}"
declare -r OUTDIR="${3?Which outdir}"
declare -r OUTFILE="${4-/dev/stdout}"

# ==============================================
function extract_chrom_pos ()
{
	local -r sample_file="${1}"
	local -r outdir="${2}"

	for c in $( cut -f 1 $sample_file | uniq )
	do
		grep -w ^$c $sample_file \
				| perl -lane '$F[0] =~ s/^chr/Darmor-bzh_v4-1_/; print "^$F[0]\t$F[1]";' \
			> $outdir/$c.chrom_pos.pattern.lst &
	done
	wait
}

function extract_ids_from_one_matrix ()
{
	local -r matrix_file="${1}"
	local -r outdir="${2}"

	for f_pattern in $outdir/*.chrom_pos.pattern.lst
	do
		grep -wf $f_pattern $matrix_file \
				| cut -f 3
	done
}


function extract_seq_len_with_ID ()
{
	local -r matrix_file="${1}"
	local -r outdir="${2}"

	local bname=$(basename $matrix_file .matrix.tsv)

	grep -wf $outdir/$bname.uniq_id_pattern.lst ${matrix_file/%.matrix.tsv/.fa} -A 1 \
			| grep -v '\-\-' \
			| perl -lne 'print length $_ if( 0 == $.%2 );'
}

function generate_length_file ()
{
	local -r datadir="${1}"
	local -r outdir="${2}"

	for f in $datadir/Blast/Disco_a?.matrix.tsv
	do
	{
		local bname=$(basename $f .matrix.tsv)

		extract_ids_from_one_matrix $f $outdir \
				| sort -n | uniq \
				| perl -pe 's/^/>/' \
			> $outdir/$bname.uniq_id_pattern.lst

		extract_seq_len_with_ID $f $outdir \
			> $outdir/$bname.length.lst
	}&
	done

	printf "Waiting for backgrounded jobs to terminate.\n" >&2
	wait

	cat $outdir/*.length.lst
}

function main()
{
	mkdir -p $OUTDIR

	extract_chrom_pos ${SAMPLE_FILE} ${OUTDIR}

	generate_length_file ${DATADIR} ${OUTDIR} \
		> ${OUTFILE}
}

# ==============================================
main

exit 0

