#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

## AIMS  : Merge disco vcf records if no incohence found.
## USAGE : ./stack_disco_vcf_records.py in.vcf
## AUTHORS : sletort@irisa.fr
## NOTE  : At least one adhoc patch : to change meta value of DS : DiscoSNp id

import sys
import pprint # debug
import argparse	# option management, close to C++/boost library (?)
import gzip
import csv
#~ import pysam	# generation des VCF ... too complex to create a vcf from scratch !
from datetime import date

import os	# for local libraries

# local libraries
f_path = os.path.dirname( os.path.realpath( __file__ ) )
sys.path.append( f_path + "/../lib" )

import Vcf # as pysam is too complex
from Misc import open_infile, verbose, set_verbose
import stack_vcf

VERSION   = "0.1"
PP = pprint.PrettyPrinter( indent=4, stream=sys.stderr )

# ----------------------------------------
def define_info_manager( o_buff ):
	def __fn_DS( o_combi, v ):
		'''o_combi is the InfosCombinator object and v the value associated to the key.'''
		#~ o_combi.d_infos['DS'].append( v )
		prev = o_combi.d_infos.get( 'DS', None )
		if None is prev:
			o_combi.d_infos['DS'] = str( v )
		else:
			o_combi.d_infos['DS'] += ',' + str( v )
	# __fn_DS

	o_buff.o_info = stack_vcf.InfosCombinator({
			'DS': __fn_DS
		})

def define_format_manager( o_buff ):
	# I think i could skip using i_sample as argument with a better use of exception and/or class.
	def get_fn_DPx( key, threshold ):

		def __fn_DPx( i_sample, l_v, ll_indices ):
			'''i_sample is the sample index, l_v the list of values associated to the key, one per record
				and ll_indices is the list of new allele indices.'''
			def __set( l_out, i, val ):
				'''set val in l_out[i] if possible else raise an exception'''
				val = int( val )
				if 0 == l_out[i]:
					l_out[i] = val
					return

				if val == l_out[i]:
					return

				# special treatment, -> lose information !
				if 1 == l_out[i] and threshold < val:
					l_out[i] = val
					return

				if 1 == val and threshold < l_out[i]:
					return
				# -----

				msg  = "I don't know yet how to combine {} values for sample #{}.".format( key, str( i_sample ) )
				msg += " {} <=> {}\n".format( str( l_out[i] ), str( val ) )
				# PP.pprint({ 'i_sample' : i_sample })
				raise stack_vcf.CombinatorException( msg, i_sample )
			# __set

			# PP.pprint({	'i': i_sample, 'l_v': l_v, 'll_i': ll_indices })

			n_alleles = max([ max(l_) for l_ in ll_indices ])
			l_new_counts = [ 0 for _ in range(n_alleles+1) ]
			for i,l_counts in enumerate( l_v ):
				if '.' == l_counts:
					continue
				if '0' != l_counts[0]: # ref count
					__set( l_new_counts, 0, l_counts[0] )
				for j in range( 1, len(l_counts) ): # alt counts
					__set( l_new_counts, ll_indices[i][j], l_counts[j] )
				# PP.pprint({ 'new_counts': l_new_counts })

			#PP.pprint({ 'return': l_new_counts })

			return l_new_counts
		# __fn_DPx

		return __fn_DPx
	# get_fn_DPx

	def __fn_Tag( i_sample, l_v, ll_indices ):
		tag = None
		for i,v in enumerate( l_v ):
			# Tag/v is a one value list
			if '.' == v[0] \
				or tag == v[0]:
				continue
			if None is tag:
				tag = v[0]
			else:
				msg  = "I don't know yet how to combine Tag values !"
				msg += " {} <=> {}\n".format( tag, v[0] )
				raise stack_vcf.CombinatorException( msg, i_sample )

		return tag
	# __fn_Tag

	o_buff.o_format = stack_vcf.FormatCombinator({
			'GT' : stack_vcf.fn_FORMAT_GT,
			'DP' : get_fn_DPx( 'DP', 15 ),
			'DPF': get_fn_DPx( 'DPF', 8 ),
			'DPR': get_fn_DPx( 'DPR', 8 ),
			'Tag': __fn_Tag,
		})

def main( o_args ):
	verbose( "Let's get begin !" )
	o_in   = Vcf.Reader( o_args.vcf_in )
	l_meta = o_in.getRawMeta()

	# AdHoc patch to change meta value of DS : DiscoSNp id
	for i,line in enumerate( l_meta ):
		if line.startswith( '##INFO=<ID=DS,Number=1' ):
			l_meta[i] = line.replace( 'Number=1', 'Number=.' )

	o_out  = Vcf.Writer.build_from_rawMeta_lines( o_args.vcf_out, l_meta, o_in.l_samples )
	o_out.writeHeader()

	o_buff = stack_vcf.RecordsBuffer( o_in )
	define_info_manager( o_buff )
	define_format_manager( o_buff )
	while o_buff.fill():
		for o_rec in o_buff.stack( o_args.delete_duplicated_positions ):
			if None is not o_rec:
				o_rec.write( o_out )

	for o_rec in o_buff.stack( o_args.delete_duplicated_positions ):
		if None is not o_rec:
			o_rec.write( o_out )

	msg = "{} records compressed in {} new records.\n"
	msg = msg.format( str( o_buff.compressed_lines ), str( o_buff.counter ) )
	sys.stderr.write( msg )

	return 0

# ----------------------------------------
def defineOptions():
	descr = "Merge vcf records if no incohence found."
	o_parser = argparse.ArgumentParser( description=descr )

	# special
	o_parser.add_argument( '--version', action='version', version='%(prog)s ' + VERSION )

	# positionnal / mandatory param
	o_parser.add_argument( 'vcf_in', nargs='?',
							type=open_infile, # does not work with python3 'cause of the magic_dict keys (utf-8 pb).
							default=sys.stdin,
							help='vcf file to analyse.' )

	# optionnal
	o_parser.add_argument( '--vcf_out', '-o',
						type=argparse.FileType( 'w' ),
						default=sys.stdout,
		               help='vcf outfile.' )

	o_parser.add_argument( '--delete_duplicated_positions', '-d',
						action='store_true',
		               help='if set do not print records that cannot be stacked.' )

	#~ o_parser.add_argument( '--output-type', '-O',
						#~ #type=argparse.FileType( 'w' ),
						#~ choices=[ 'b', 'u', 'z', 'v' ]
						#~ default='u',
		               #~ help='Output compressed BCF (b), uncompressed BCF (u), compressed VCF (z), uncompressed VCF (v).' )

	o_parser.add_argument( '--verbose', '-v', action='store_true',
		               help='If set give info on what happen.' )

	return o_parser

def analyseOptions( o_args ):

	if( o_args.verbose ):
		set_verbose( True )

	#~ if "-" == o_args.vcf_in:
		#~ o_args.o_vcf = sys.stdin
	#~ else:
		#~ o_args.o_vcf = open_infile( o_args.vcf_in )

	return

# ----------------------------------------

if __name__ == '__main__':
	o_parser = defineOptions()
	o_args   = o_parser.parse_args()
	analyseOptions( o_args )
	# PP.pprint( o_args )

	sys.exit( main( o_args ) )

