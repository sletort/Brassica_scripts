#! /bin/bash

## AIMS  : extract from INRA_MATRIX SNPs present in at least one of our species, with our rules.
## USAGE : ./extract_reachable_SNP_from_Inra_matrix.sh [species_name]
## USAGE : 	where species_name is a comma separated list of species.
## NOTE  : if no species name is provided, then SPECIES_FILE will be read.
## NOTE  : our species seems to be in cols 21-24 in inra's matrix for s1-s4
## NOTE  : our rules = One SNP each 50 bases max


declare -r INRA_MATRIX=/home/genouest/brassica/cfalenti/rapso-snp/finalMatrixV2.tsv
declare -r SRC_PATH=/home/genouest/genscale/sletort/Rapsodyn/detection/
declare -r OUT_PATH=$SRC_PATH/ref_db

declare -r SPECIES_FILE=$SRC_PATH/raw_data/species
declare -r SPECIES_NAME="${1}"
declare NB_SPECIES # will be set in set_NB_SPECIES

function set_NB_SPECIES ()
{
	local col_str="$1"

	NB_SPECIES=$( printf "$col_str\n" | tr "," "\n" |  wc -l )
}

function extract_our_species ()
{
	local cols="$1"
	local outfile="$2"
	local extra_filter="$3"

	# extraire seulement les SNPs de nos espèces
	local perle='$a = 0;
		map{ $a |=1 if( $_ ne $F[2] && $_ ne "NA" '$extra_filter' )  } @F[3..'$((3+NB_SPECIES-1))'];
		print if( $a );'

	cut -f 1-3,$cols $INRA_MATRIX \
			| head -n 1 \
		> $outfile

	cut -f 1-3,$cols $INRA_MATRIX \
			| tail -n +2 \
			| perl -lane "$perle" \
			| sort -Vk 1,2 \
		>> $outfile
}

# infile is : chrom, pos, ref, species-i
function filter_our_rules ()
{
	local infile="$1"
	local outfile="$2"

	# nous avions la consigne : 1 SNP toutes les 50 bases max.
	# at this step infile will be chrom, pos, ref, species-i(1-4), chrom, pos, ref, species-i(1-4)
	p_begin='BEGIN{ $print_flg = 1; }'
	perle='if( /^#/ || $F[0] ne $F['$((3+NB_SPECIES))'] || ($F[1]+50)<$F['$((3+NB_SPECIES+1))'] )
	{
		print if( $print_flg );
		$print_flg=1;
	}
	else{	$print_flg=0;	}'

	head -n 1 $infile \
		> $outfile

	# to have row1 and row2 on the same row.
	local tmp_file=$( mktemp xtract_XXXX )
	tail -n +2 $infile > $tmp_file

	tail -n +2 $tmp_file | paste $tmp_file - \
			| perl -lane "$p_begin $perle" \
			| cut -f 1-$((3+NB_SPECIES)) \
		>> $outfile
	rm $tmp_file
}

function build_species_column_string ()
{
	local species
	if [[ -z $SPECIES_NAME ]]
	then
		species=$( cut -f 1 "$SPECIES_FILE" | tr "\n" "," )
	else
		species="$SPECIES_NAME"
	fi

	# print lines number if containing one of our species
	local p_begin='BEGIN{ $sp="'$species'"; %H_MY = map{ $_ => 1 } split( ",", $sp ); }'
	local perle='push( @a_lines, $. ) if( $H_MY{ $F[0] } );'
	local p_end='END{ print join( ",", @a_lines ); }'

	# turn header into column
	head -n 1 $INRA_MATRIX | tr "\t" "\n" \
		| perl -lane "$p_begin $perle $p_end"

	# will return 21,22,23,24
}

function main ()
{
	printf "get which column  contain our species.\n"
	local col_str=$( build_species_column_string )
	set_NB_SPECIES $col_str
	printf "Our species are on cols $col_str ($NB_SPECIES species).\n"

	printf "generate the file with only our species.\n"
	printf "\tOne row means that at least one species is not ref nor NA.\n"
	species_file=ref_inra_matrix.our_species.tsv
	extract_our_species "$col_str" "$OUT_PATH/$species_file"
	printf "The reference matrix for our species is $OUT_PATH/$species_file.\n"

	filter_our_rules "$OUT_PATH/$species_file" "$OUT_PATH/reachable_SNP.$species_file"
	printf "The reference matrix (1SNP per 50bases) for our species is $OUT_PATH/reachable_SNP.$species_file.\n"

	printf "generate a more stringent file.\n"
	printf "\tat least one species is not ref nor NA nor X.\n"
	# more stringent, if the SNP was filter out (allele=X) due to its position, frequency, coverage or whatever, I consider it is ref. 
	species_file=ref_inra_matrix.our_species.notX.tsv
	extract_our_species "$col_str" "$OUT_PATH/$species_file" '&& $_ !~ /^X/ && $_ !~ /[-+]/'
	printf "The filtered reference matrix for our species is $OUT_PATH/$species_file.\n"

	filter_our_rules "$OUT_PATH/$species_file" "$OUT_PATH/reachable_SNP.$species_file"
	printf "The filtered reference matrix (1SNP per 50bases) for our species is $OUT_PATH/reachable_SNP.$species_file.\n"
}

main
exit 0

