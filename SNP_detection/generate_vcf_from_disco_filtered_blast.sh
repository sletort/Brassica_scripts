#! /bin/bash

## AIMS  : generate one VCF file for discosnp++ results.
## USAGE : ./generate_vcf_from_disco_filtered_blast.sh disco_mode
## NOTE  : disco_mode is b0 or b1, I'll use blast directory

source /softs/local/env/envpython-2.7.15.sh
source /softs/local/env/envhtslib-1.6.sh
source /softs/local/env/envbcftools-1.3.sh
source /softs/local/env/envbedops-2.4.15.sh

set -o nounset
set -o errexit

# =========================================================
declare -r DISCO_MODE="${1?What is the discoSNP mode to explore ?}"

# ---------------------------------------------------------
declare -r DISCO_PATH="/groups/rapsodyn/SNP_detection/discoSNP++"

declare -r SAMPLE_FILE="sample.ordered.lst"	# cultivar's name in the same order as the discoSNP in/output

declare -r BLAST_SUFFIX=".blast_filtered.tsv.gz"
declare -r BLAST_VCF="Disco_SNP"."$DISCO_MODE"

# =========================================================
function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT     = $0\n"
	printf "╟ - DISCO_MODE = $DISCO_MODE\n"
	printf "╟ ---------------------------------- - - - -\n"
	printf "╟ - DISCO_PATH   = $DISCO_PATH\n"
	printf "╟ - SAMPLE_FILE  = $SAMPLE_FILE\n"
	printf "╟ - BLAST_SUFFIX = $BLAST_SUFFIX\n"
	printf "╟ - BLAST_VCF = $BLAST_VCF\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

# =========================================================
function __get_analyse_dir()
{
	printf "$DISCO_PATH/$DISCO_MODE/analyse"
}

function init_sample_file()
{
	local generate_sample_list="$__ROOT/SNP_detection/generate_disco_cultivar_correspondance.sh"
	local disco_resdir="$DISCO_PATH/$DISCO_MODE/discoSNP/"

	$generate_sample_list "$disco_resdir/discoRes_read_files_correspondance.txt" \
		> "$SAMPLE_FILE"
}

function main ()
{
	local a_files=()

	init_sample_file

	for f in $( __get_analyse_dir )/Blast/*$BLAST_SUFFIX
	do
		local bname=$( basename $f $BLAST_SUFFIX )
		printf "$bname\n"

		local vcf_file=$bname.vcf.gz
	{
		$__ROOT/SNP_detection/generate_vcf_from_disco_filtered_blast.py --samples "$SAMPLE_FILE" $f \
				| $__ROOT/SNP_detection/flag_Disco_vcf.py \
				| bgzip -c \
			> $vcf_file
		bcftools index $vcf_file
	}&

		a_files+=( $vcf_file )
	done
	printf "Waiting for backgrounded job to terminate.\n"
	wait

	bcftools concat -a -O b -o $BLAST_VCF.bcf ${a_files[*]}
	bcftools index $BLAST_VCF.bcf

	for f in ${a_files[*]}
	do
		rm -f $f $f.csi
	done
}

# =========================================================
printVars

# your work.
main

printf "End of %s.\n\n" $( basename $0 )
exit 0
