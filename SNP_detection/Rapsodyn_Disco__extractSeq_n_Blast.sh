#! /bin/bash

## AIMS  : run the full detection from DiscoSNP++ results to SNP matrix construction. = blast + filter + format
## USAGE : qsub Rapsodyn_Disco__extractSeq_n_Blast.sh infile outfile species kmer_size
## ALGO  : Will split DiscoSNP result and run Blast on each results, then filter each output before merging all.


# shell to use
#$ -S /bin/bash

#$ -M sletort@irisa.fr
# send mail before b, at the end e, on stop a, on suspend s, or never n
#$ -m bea

#$ -N Rapsodyn_Disco__extractSeq_n_Blast.sh

# if the script should be run in the current directory
#$ -cwd

# merge stdout and stderr
#$ -j no


# init env
source /softs/local/env/envblast+.sh
source /softs/local/env/envpython-2.7.sh

source $__ROOT/brassicaDB.sh

set -o nounset
set -o errexit


# le script
declare INFILE=${1?"Need an infile (fasta)"}
declare OUTFILE=${2?"Need an outfile (fasta)"}
declare SPECIES=${3?"Need a species file"}
declare KMER_SIZE=${4?"What is the kmer-size used ?"}

# time ./Disco__extractSeq_n_Blast.sh "$INFILE" "$OUTFILE"
declare -r INRA_RULES=${5-0}

declare -r CHUNK_DIR=chunks
declare -r NB_FILES=20
declare -r BLAST_DB=$( get_ref_fasta Bnapus )
declare -r PIGZ=$HOME/logiciels/bin/pigz

declare __SOFT_DIR="$__ROOT/SNP_detection"

# ========================================
# cuisine perso
declare -a A_RULES=()
declare -ra A_INRA_RULES=( Blast_inra $__SOFT_DIR/Disco__extractSeq_forBlast_inra_rules.py $__SOFT_DIR/parseBlastOutput_inra_rules.py )
declare -ra A_GS_RULES=( Blast $__SOFT_DIR/Disco__extractSeq_forBlast.py $__SOFT_DIR/parseBlastOutput.py )
declare -r i_WORKDIR=0
declare -r i_EXTRACT_SEQ=1
declare -r i_PARSE_BLAST_OUTPUT=2
if [[ 1 -eq INRA_RULES ]]
then
	A_RULES=( "${A_INRA_RULES[@]}" )
else
	A_RULES=( "${A_GS_RULES[@]}" )
fi

# -----------------------------------
function split_infile ()
{
	local infile="$1"

	if [[ -e "$CHUNK_DIR" ]]
	then
		printf "$CHUNK_DIR already exist.\nFiles are supposed splitted.\n"
		return 0
	fi

	mkdir -p "$CHUNK_DIR"

	printf "Splitting infile into $NB_FILES (To speed up further treatments).\n"
	local nb_seq=$( grep -c '>' $infile )

	# so nb_seq/2 pairs
	# I want $NB_FILES files but there should be 4n lines (2 seqs = 1 pair)
	# so 4*( round.inf( (2*nb_seq)/(4*$NB_FILES) +1))
	local nb_lines=$(( 4*( (2*nb_seq) / (4*NB_FILES) +1 ) ))

	split --lines=$nb_lines "$infile" "$CHUNK_DIR/Disco_"
}

function print_header ()
{
	local species_heads=$( cat $SPECIES | tr "\n" "\t" )
	printf "#Chrom\tPos\tRef\t$species_heads\n"

#	perle='@a_=qw( #Chrom Pos Ref );
#			$n = (@F-3);
#			@c = map{ "C$_" }( 1..$n );
#			print join( "\t", @a_, @c, @c );'
#	for file in $PREFIX*.matrix.tsv
#	do
#		head -n 1 $file | perl -lane "$perle"
#		break
#	done
}

function parse_n_build ()
{
	local infile="$1"

	local seq_out='qseqid qstart qend qseq sseqid sstart send sseq'
	local test_out='qlen length bitscore score pident nident mismatch positive gapopen gaps ppos btop'
	local blast_out_fmt="6 $seq_out $test_out"

	local bname=$( basename $infile )

	local workdir="${A_RULES[$i_WORKDIR]}"

	local fasta_file="$workdir/$bname.fa"
	if [[ ! -e "$fasta_file" ]]
	then
			"${A_RULES[$i_EXTRACT_SEQ]}" "$infile" "$fasta_file"
	fi

	local blast_file="$workdir/$bname.blast.tsv"
	if [[ ! -e "$blast_file" ]]
	then
		blastn -task megablast -query "$fasta_file" -db "$BLAST_DB" -outfmt "$blast_out_fmt" \
			> "$blast_file"
	fi

	local filtered_file="$workdir/$bname.blast_filtered.tsv"
	if [[ ! -e "$filtered_file" ]]
	then
		# code is no more compatible with INRA rules
#		"${A_RULES[$i_PARSE_BLAST_OUTPUT]}" --kmer-size $KMER_SIZE "$blast_file" \
		$__SOFT_DIR/parseBlastOutput.py --kmer-size $KMER_SIZE "$blast_file" \
			> "$filtered_file"
		$PIGZ "$blast_file"
	fi

	$__SOFT_DIR/build_inra_matrix.py "$filtered_file" \
		> "$workdir/$bname.matrix.tsv"
}

function merge_matrix_chunks ()
{
	local workdir="$1"
	local outfile="$2"

	# matrix are chrom pos id ref alleles... => I remove id
	printf "\tConcatenate :\n"
	local tmp_file1=$( mktemp --tmpdir=. Rapso_Disco_XXXX )
	for file in "$workdir/"*.matrix.tsv
	do
		perl -lane 'print join( "\t", @F[0,1,3..$#F] );' $file \
			>> $tmp_file1
	done

	print_header > "$outfile"

	#I want to filter on the 3 first cols (chrom, pos ref)
	local N_fields=3

	##FILTER
	# this bloc remove rows as soon as their first N_fields columns are equals
	#	To do so, I'll place the allele column at the begining
	#		then I'll use uniq -u with --skip-fields option to ignore those alleles columns
	local nb_cols=$( head -n 1 $tmp_file1 | perl -lane 'print scalar @F;' )
	local nb_scrap_cols=$(( nb_cols - N_fields ))

	local p_begin='BEGIN{ $" = "\t"; }'
	local p_reorder1='print "@F['$N_fields'..$#F,0..'$((N_fields-1))']";'
	local p_reorder2='print "@F['$nb_scrap_cols'..$#F,0..'$((nb_scrap_cols-1))']";'

#	local tmp_file2=$( mktemp --tmpdir=. Rapso_Disco_XXXX )

	printf "\tSort and remove duplicates :\n"
	sort -k1d,1d -k2n,2n $tmp_file1 \
			| perl -lane "$p_begin $p_reorder1" \
			| uniq -u --skip-fields=$nb_scrap_cols \
			| perl -lane "$p_begin $p_reorder2" \
		>> "$outfile"
	#/FILTER

	rm $tmp_file1
}

# -----------------------------------
function main ()
{
	if [[ -e $OUTFILE ]]
	then
		printf "$OUTFILE already exists.\n"
		exit 1
	fi

	workdir="${A_RULES[$i_WORKDIR]}"

	if [[ ! -d "$workdir" ]]
	then
		mkdir -p "$workdir"
	fi

	split_infile "$INFILE"

	printf "Formatting each sequences for Blast.\n"
	printf "Then do Blast if not already done.\n"
	for file in $CHUNK_DIR/*
	do
			parse_n_build "$file" &
	done
	wait

	printf "Merging matrix chunks.\n"
	merge_matrix_chunks "$workdir" "$OUTFILE"
}

# ---------------------------------------------------------

main


exit 0
