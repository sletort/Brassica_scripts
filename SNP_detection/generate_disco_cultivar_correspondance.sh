#! /bin/bash

## AIMS  : generate a file with discoSNP column output for each cultivar.
## USAGE : ./generate_disco_cultivar_correspondance.sh

set -o nounset
set -o errexit

# brassicaDB sould has been sourced.

# =========================================================
declare -r DISCO_FILE="${1?What is the discoSNP read_files_correspondance.txt ?}"

# ---------------------------------------------------------
declare -A H_CULTI_PATTERN=()
declare -A H_PATTERN_CULTI=()

# =========================================================
function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT     = $0\n"
	printf "╟ ---------------------------------- - - - -\n"
	printf "╟ - DISCO_FILE = $DISCO_FILE\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

# =========================================================
function load_meta_file()
{
	while read culti sp pattern
	do
		H_CULTI_PATTERN["$culti"]="$pattern"
		H_PATTERN_CULTI["$pattern"]="$culti"
	done < \
		<( grep -v ^# "$__META_FILE" ) # process substitution
}

function main ()
{
	load_meta_file

	while read col filepath
	do
		for p in ${!H_PATTERN_CULTI[@]}
		do
			if [[ "$filepath" = *"$p"* ]]
			then
				printf "${H_PATTERN_CULTI[$p]}\n"
				break
			fi
		done
	done < "$DISCO_FILE" \
		| uniq

	#~ for c in ${!H_CULTI_PATTERN[@]}
	#~ do
		#~ grep ${H_CULTI_PATTERN[$c]} "$DISCO_FILE" \
				#~ | perl -lane '$F[0] =~ s/C_//; print $F[0];'
	#~ done
}

# =========================================================
printVars >&2

# your work.
main

printf "End of %s.\n\n" $( basename $0 )  >&2
exit 0
