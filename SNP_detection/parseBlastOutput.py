#! /usr/bin/env python
# -*- coding: utf-8 -*-

## AIMS  : This prog parses a Blast output and filters it to only keep hits with few errors and uniqs.
## USAGE : 
## NOTE  : output is a tsv file like chrom, pos, var_id, ref, all_up|all_low, '|' separated list of upper counts, '|' separated list of lower counts.

import sys
import argparse
import os
import pdb
import pprint
import fileinput

class Var:
	def __init__( self, var_str ):
		l_elts = var_str.split(':')

		self.__pos = int( l_elts[0] )
		self.__allele_string = l_elts[1]

	def __str__( self ):
		return "(" + str( self.pos ) + ", " + self.__allele_string + ")"

	@property
	def pos( self ):
		return self.__pos

	@property
	def lower_all( self ):
		return self.__allele_string[1]

	@property
	def upper_all( self ):
		return self.__allele_string[0]

#---------------------------
# num of cols # should be defined only in class Record, but it does not work/i don't know how to.
Q_ID     =  0
Q_START  =  1
Q_END    =  2
Q_SEQ    =  3
S_ID     =  4
S_START  =  5
S_END    =  6
S_SEQ    =  7
Q_LEN    =  8
LEN      =  9
BITSCORE = 10
SCORE    = 11
P_IDENT  = 12
N_IDENT  = 13
MISMATCH = 14
POSITIVE = 15
GAP_OPEN = 16
GAPS     = 17
P_POS    = 18
BTOP     = 19
SNP_OFFSET = 20 # not part of blast output, my addition (to deal with insertion when query seq is on minus strand

class Record:
	def __init__( self, line ):
		l_elts = line.rstrip().split( "\t" )
		l_elts.append( 0 ) # SNP_OFFSET

		self.__id = l_elts[Q_ID]
		self.__q_start = int( l_elts[Q_START] )
		self.__q_end   = int( l_elts[Q_END] )
		self.__q_seq   = l_elts[Q_SEQ]
		self.__q_len   = int( l_elts[Q_LEN] )

		self.__align_len = int( l_elts[LEN] )

		self.__s_id    = l_elts[S_ID]
		self.__s_start = int( l_elts[S_START] )
		self.__s_end   = int( l_elts[S_END] )
		self.__s_seq   = l_elts[S_SEQ] # ref seq

		# id|upper_counts|lower_counts|var1[|var2...]
		l_var = self.id.split( '|' )
		self.__var_id         = l_var[0]
		self.__l_upper_counts = l_var[1].split(':')
		self.__l_lower_counts = l_var[2].split(':')

		# each sample is a [pos, allele_string]
		self.__l_vars      = []
		for i in range( 3, len( l_var ) ):
			self.__l_vars.append( Var( l_var[i] ) )

	def __str__( self ):
		msg  = "Member of class Record :"
		msg += "\n\trecord id  : " + self.id
		msg += "\n\tvariant id : " + self.var_id
		msg += "\n\tQuery : [" + str( self.q_start ) + ";" + str( self.q_end ) + "], len = " + str( self.qlen )
		msg += "\n\t" + self.query_seq
		msg += "\n\tSubject : [" + str( self.s_start ) + ";" + str( self.s_end ) + "]" # : len = " + self.qlen
		msg += "\n\t" + self.subject_seq
		msg += "\n\trecord is composed of " + str( self.getNbVar() ) + " variants."
		for i in self.__l_vars:
			msg += "\n\t\t" + str( i )
		msg += "\n\tupper counts : " + str( self.upper_counts )
		msg += "\n\tlower counts : " + str( self.lower_counts )
		msg += "\n"
		return msg


	# getters - I do not get the real interest of property
	@property
	def id( self ):
		return self.__id

	@property
	def var_id( self ):
		return self.__var_id

	@property
	def q_start( self ):
		return self.__q_start

	@property
	def q_end( self ):
		return self.__q_end

	@property
	def qlen( self ):
		return self.__q_len

	@property
	def query_seq( self ):
		return self.__q_seq

	@property
	def s_id( self ):
		return self.__s_id

	@property
	def s_start( self ):
		return self.__s_start

	@property
	def s_end( self ):
		return self.__s_end

	@property
	def subject_seq( self ):
		return self.__s_seq

	@property
	def lower_counts( self ):
		return self.__l_lower_counts

	@property
	def upper_counts( self ):
		return self.__l_upper_counts

	@property
	def align_len( self ):
		return self.__align_len

	def getVar( self, i ):
		if( self.getNbVar() <= i ):
			i =	self.getNbVar() - 1

		return self.__l_vars[i]

	def getNbVar( self ):
		return len( self.__l_vars )



	# methods
	def alignMinusStrand( self ):
		return ( self.s_end < self.s_start  )

	"""Return the position of each var on the subject sequence."""
	def getSubjectSnpPosNoffset( self ):
		"""snp_pos is 0-based, it's the SNP position on the alignement : """
		def __computeSnpPos( self, var_i, offset ):
			snp_pos  = self.getVar( var_i ).pos - self.q_start
			snp_pos += offset

			return snp_pos
		# __computeSnpPos


		l_pos = []

		var_i = 0
		insertions = 0
		deletions  = 0
		snp_pos = __computeSnpPos( self, var_i, deletions )

		#~ sys.stderr.write( "var : " + str(self.getVar( var_i ) )  + "\n" )
		#~ sys.stderr.write( "q_start  : " + str( self.q_start )  + "\n" )
		#~ sys.stderr.write( "deletions: " + str( deletions )  + "\n" )
		#~ sys.stderr.write( "snp_pos  : " + str( snp_pos )  + "\n" )

		for i in xrange( self.align_len+1 ):
			#~ try:
				if( self.subject_seq[i] == '-' ):
					insertions  += 1

				if( self.query_seq[i] == '-' ):
					deletions += 1
					snp_pos   += 1
				elif( i == snp_pos ):
					if( not self.alignMinusStrand() and ( self.subject_seq[i] == '-' ) ):
						l_pos.append( [i+1,insertions-1] )
					else:
						l_pos.append( [i+1,insertions] )
					var_i += 1
					if( var_i == self.getNbVar() ):
						return l_pos
					snp_pos = __computeSnpPos( self, var_i, deletions )

			#~ except:
				#~ e = sys.exc_info()[0]
				#~ sys.stderr.write( str( e ) + "\n" )
				#~ sys.stderr.write( "id = " + self.id + "\n" )
				#~ sys.stderr.write( "i = " + str( i ) + "\n" )
				#~ sys.stderr.write( "query   = " + self.query_seq + "\n" )
				#~ sys.stderr.write( "subject = " + self.subject_seq + "\n" )

		sys.stderr.write( "Obviously I miss stg ! check getVarOffsets method." )
		return l_pos

		
	# This is the slowest function.
	def check_alignement( self, nb_mismatch_max ):
		"""count mismatch before and after the principal SNP"""
		# full query is Q_LEN, so q_seq is shorter or equal
		mismatch_5p = self.q_start - 1
		mismatch_3p = self.qlen - self.q_end
		if( nb_mismatch_max < mismatch_5p or nb_mismatch_max < mismatch_3p ):
			return False

		var_i = 0
		snp_pos = self.getVar( var_i ).pos
		query_i = self.q_start - 1 # 0-based
		#~ sys.stderr.write( "CA:snp_pos " + str( snp_pos ) + "\n" )
		#~ sys.stderr.write( "CA:query_i " + str( query_i ) + "\n" )

		for align_i in range( len(self.query_seq) ):

			if( query_i == snp_pos ):
				var_i += 1 # next var
				snp_pos = self.getVar( var_i ).pos
				query_i += 1

				# I place a test here to skip useless computation (and not to test on each base)
				if( nb_mismatch_max < mismatch_5p ):
					return False
				continue

			# deletion
			if( self.query_seq[align_i] == '-' ):
				if( query_i < snp_pos ):
					#~ sys.stderr.write( "CA:deletion on " + str( align_i ) + "\n" )
					mismatch_5p    += 1
				elif( snp_pos < query_i ):
					mismatch_3p +=1

			# insertion
			elif( self.subject_seq[align_i] == '-' ):
				if( query_i < snp_pos ):
					mismatch_5p += 1
					query_i += 1
					#~ sys.stderr.write( "CA:insertion on " + str( align_i ) + "\n" )
				elif( snp_pos < query_i ):
					mismatch_3p +=1
			else:
				if( self.query_seq[align_i] != self.subject_seq[align_i] ):
					if( query_i < snp_pos ):
						#~ sys.stderr.write( "CA:mutation on " + str( align_i ) + "\n" )
						mismatch_5p += 1
					elif( snp_pos < query_i ):
						mismatch_3p += 1

				query_i += 1

		#~ sys.stderr.write( "CA:mm5 " + str( mismatch_5p ) + "\n" )
		#~ sys.stderr.write( "CA:mm3 " + str( mismatch_3p ) + "\n" )
		if( nb_mismatch_max < mismatch_3p ):
			return False
		return True


WINDOW_LEN = 48 # kmer length - 1 for disco
NB_MISMATCH_MAX = 3 # before or after the SNP

#---------------------------
# WARN : subject can be on minus strand => s_end < s_start

# snp_pos is 1-based
# snp_pos is the position of
def extract_allele_from_seq( snp_pos, q_seq, s_seq ):
	snp_pos -=1 # turn 0-based

	for i in range( len( q_seq ) ):
		if( q_seq[i] == '-' ): # et pour les Insertions ?
			snp_pos += 1
		if( i == snp_pos ):
			sys.stderr.write( "read on pos " + str( i+1 ) + "\n" )
			return s_seq[i]

	return None

# alleles should be in upper case (we are into a kmer sequence)
# ... what about subject sequence ?
# je ne sais pas encore passer les variables par référence
def complementAlleles( l_alleles ):
	l_out = []
	for allele in l_alleles:
		if( allele == 'A' ):
			l_out += 'T'
		elif( allele == 'C' ):
			l_out += 'G'
		elif( allele == 'G' ):
			l_out += 'C'
		elif( allele == 'T' ):
			l_out += 'A'
		elif( allele == '-' ):
			l_out += allele
		else:
			print "case not coded into complementAlleles function.\n"
			print "***" + "/".join( l_alleles ) + "***"
			l_out += allele
	return l_out


def printSelectedRecords( h_output, d_records ):
#	pp = pprint.PrettyPrinter(indent=4)
#	pp.pprint( d_records )
#	print len( d_records )
	for k in sorted( d_records.iterkeys() ):
		o_record  = d_records[k][0]

		l_offsets = o_record.getSubjectSnpPosNoffset()

		for var_i in xrange( o_record.getNbVar() ):
			o_var = o_record.getVar( var_i )
			[ snp_pos,offset ] = l_offsets[var_i]

			#~ sys.stderr.write( "offset : " + str( offset ) + "\n" )
			#~ sys.stderr.write( "snp_pos: " + str( snp_pos ) + "\n" )

			ref_all = o_record.subject_seq[snp_pos-1]
			alt1    = o_var.upper_all
			alt2    = o_var.lower_all
			s_start = o_record.s_start
			s_end   = o_record.s_end

			# strand check
			if( o_record.alignMinusStrand() ):
				[ref_all, alt1, alt2] = complementAlleles( [ref_all, alt1, alt2] )
				pos = s_start - snp_pos + offset + 1
			else:
				pos = s_start + snp_pos - offset - 1

			l_out = [ o_record.s_id, str( pos ),
						o_record.var_id,
						ref_all,
						'|'.join( [ alt1, alt2 ] ),
						'|'.join( o_record.upper_counts ),
						'|'.join( o_record.lower_counts ) ]
			h_output.write( "\t".join( l_out ) + "\n" )


# pas top, la fonction sera encore appelé plus tard.
# en fait, c'est toute la fonction qui sera recalculée dans printSelectedRecords
#	à revoir.
def compute_snp_pos ( q_id, q_start ):
	d_record_elts = __split_record_id( q_id )
	snp_pos = d_record_elts['SNP_pos']
	return snp_pos


def main( args ):

	aln_mini_len = 2*( args.kmer_size-1 - args.max_mismatch )

	if( type( args.outfile ) is str ):
		h_output = open( args.outfile, 'w' )
	else:
		h_output = args.outfile

	# build a hash of records
	d_records = {}
	for line in fileinput.input( args.infile ):
		o_record = Record( line )

		# not the best sequence : with this we kept one of 2 ex aequo
		if( o_record.id in d_records
			and None == d_records[ o_record.id ][1] ):
				continue
		
		if( len( o_record.query_seq ) < aln_mini_len ):
			continue

		# maybe offset could be calculated later or simply manage a s_pos and q_pos
		if( o_record.check_alignement( NB_MISMATCH_MAX ) ):
			if( o_record.id not in d_records ):
				d_records[ o_record.id ] = [ o_record, 1 ];
			else:
				d_records[ o_record.id ][1] = None

	# remove non uniq alignments
	l_keys = d_records.keys()
	for k in l_keys:
		if( None == d_records[k][1] ):
			del d_records[k]

	# guess !
	printSelectedRecords( h_output, d_records )

	return

def buildParamParser():
	descr = 'This prog parses a Blast output and filters it to only keep hits with few errors and uniqs.'
	o_parser = argparse.ArgumentParser( description = descr );

	# options
	o_parser.add_argument( '-e', '-m', '--error-allowed', '--max-mismatch',
								type=int, dest='max_mismatch', default=3,
								help='[=3] number of errors/mismatches allowed before and after SNP pos.' )
	o_parser.add_argument( '-o', '--outfile',
								type=str, default=sys.stdout,
								help='[=stderr] output filepath.' )

	# mandatories params
	o_parser.add_argument( '-k', '--kmer-size',
								type=int, required=True,
								help='kmer-size used to run DiscoSNP++.' )
	o_parser.add_argument( 'infile',
								type=str,
								help='Blast output, a tsv file (19 fields) produced by Rapsodyn_Disco__extractSeq_n_Blast.sh.')

	return o_parser

# ----------------------------------------

if __name__ == '__main__':
	o_parser = buildParamParser()
	args = o_parser.parse_args()

	main( args )

	sys.exit( 0 )

