#! /bin/bash

## AIMS  : detects false SNPs = duplicated positions in the SNP matrix.
## USAGE : qd_check_duplicate.sh starch_file
## NOTE  : rely on bedops.
## AUTHORS : sletort@irisa.fr

set -o nounset
set -o errexit

# =========================================================
declare -r INFILE="${1?What is your input file ?}"

# ---------------------------------------------------------
#declare -r MG_MAP_DIR="/PUBLIC_DATA/GENERATED/1kG/map_files"

# =========================================================
function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT     = $0\n"
	printf "╟ - INFILE     = $INFILE\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

# =========================================================
function main ()
{
	for chr in $( unstarch --list-chr $INFILE )
	do
		ret=$( unstarch $chr --has-duplicate $INFILE )
		if [[ 1 -eq $ret ]]
		then
			unstarch $chr $INFILE \
				| cut -f 1-3 | uniq -d \
				| bedops -e 1 $INFILE -
		fi
	done
}

# =========================================================
printVars >&2

# your work.
main

printf "End of %s.\n\n" $( basename $0 )
exit 0
