#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

## AIMS  : Add Inra flags to VCF obtained with discoVCF (can be used with rapsoSNP ?).
## USAGE : ./flag_Disco_vcf.py in.vcf > out.vcf
## USAGE : ./flag_Disco_vcf.py -o out.vcf in.vcf
## AUTHORS : sletort@irisa.fr

import sys
import pprint # debug
import argparse	# option management, close to C++/boost library (?)
# from pysam import VariantFile # for reading only (for the moment) !
#	Nope ! needs to be sorted/indexed or to have seq_id defined in the metaheader.

import os	# for local libraries

# local libraries
f_path = os.path.dirname( os.path.realpath( __file__ ) )
sys.path.append( f_path + "/../lib" )

import Vcf # as pysam is too complex

VERSION   = "0.1"
PP = pprint.PrettyPrinter( indent=4, stream=sys.stderr )
VERBOSE   = False


MIN_COUNT_THRES = 5
#~ MAX_COUNT_THRES = 200 # 100 forward + 100 reverse ?
MAX_COUNT_THRES = 100
MIN_FREQ_HOMO   = 0.95

NO_COUNT_TAG   = 'NA'
FEW_COUNT_TAG  = 'XcovMin'	# counts < MIN_COUNT_THRES
MUCH_COUNT_TAG = 'XcovMax'	# MAX_COUNT_THRES < counts
HETERO_TAG     = 'Xfreq'
NO_STRAND_TAG  = 'Xfor'		# forward = 0 or reverse = 0

# ========================================
def get_count_tag( max_count, sum_counts ):
	if( max_count < MIN_COUNT_THRES ):
		return FEW_COUNT_TAG

	if( MAX_COUNT_THRES < max_count ):
		return MUCH_COUNT_TAG

	freq = max_count / sum_counts
	if( freq < MIN_FREQ_HOMO ):
		return HETERO_TAG

	return None
	
def set_tag( d_sample ):
	def __format_DPx_as_int( l_dpx ):
		l_out = []
		for str_i in l_dpx:
			if '.' == str_i:
				l_out.append( 0 )
			else:
				l_out.append( int( str_i ) )
		return l_out

	d_sample['DP']  = __format_DPx_as_int( d_sample['DP'] )
	d_sample['DPF'] = __format_DPx_as_int( d_sample['DPF'] )
	d_sample['DPR'] = __format_DPx_as_int( d_sample['DPR'] )
	#PP.pprint( d_sample['DP'] )

	counts = sum( d_sample['DP'] )
	if 0 == counts:
		d_sample['Tag'] = NO_COUNT_TAG
	else:
		if 0 == sum( d_sample['DPF'] ) \
				or 0 == sum( d_sample['DPR'] ):
			d_sample['Tag'] = NO_STRAND_TAG
		else:
			tag = get_count_tag( max( d_sample['DP'] ), counts  )
			if tag is not None:
				d_sample['Tag'] = tag
	#~ PP.pprint( d_sample )
	return

# ----------------------------------------
def main( o_args ):

	o_in   = Vcf.Reader( o_args.vcf_in )
	l_meta = o_in.getRawMeta()

	o_out = Vcf.Writer.build_from_rawMeta_lines( o_args.vcf_out, l_meta, o_in.l_samples )
	o_fmt_tag = Vcf.Format({
					'ID'     : 'Tag',
					'Number' : '1',
					'Type'   : 'String',
					'Description': '"Inra tag, represent SNP confidency/quality."',
				})
	o_out.appendMeta({ 'FORMAT':[ o_fmt_tag ] })

	o_out.writeHeader()

	for o_rec in o_in:
		o_rec.loopOverSamples( set_tag )
		o_rec.write( o_out )

	return 0

# ----------------------------------------
def defineOptions():
	descr = "Add Inra filters to VCF obtained with discoVCF (can be used with rapsoSNP ?)."
	o_parser = argparse.ArgumentParser( description=descr )

	# special
	o_parser.add_argument( '--version', action='version', version='%(prog)s ' + VERSION )

	# positionnal / mandatory param
	o_parser.add_argument( 'vcf_in',nargs='?',
							type=argparse.FileType( 'r' ),
							default=sys.stdin,
							help='vcf infile. FORMAT should contain DP,DPF and DPR.' )

	# optionnal
	o_parser.add_argument( '--vcf_out', '-o',
						type=argparse.FileType( 'w' ),
						default=sys.stdout,
		               help='vcf outfile.' )

	#~ o_parser.add_argument( '--output-type', '-O',
						#~ #type=argparse.FileType( 'w' ),
						#~ choices=[ 'b', 'u', 'z', 'v' ]
						#~ default='u',
		               #~ help='Output compressed BCF (b), uncompressed BCF (u), compressed VCF (z), uncompressed VCF (v).' )

	o_parser.add_argument( '--verbose', '-v', action='store_true',
		               help='If set give info on what happen.' )

	return o_parser

def analyseOptions( o_args ):

	global VERBOSE
	if( o_args.verbose ):
		VERBOSE = True

	return

def verbose( msg ):
	if( VERBOSE ):
		sys.stderr.write( msg + "\n" )


# ----------------------------------------
if __name__ == '__main__':
	o_parser = defineOptions()
	o_args   = o_parser.parse_args()
	analyseOptions( o_args )
	# PP.pprint( o_args )

	sys.exit( main( o_args ) )

