#!/usr/bin/env python
# -*- coding: utf-8 -*-

## AIMS  : plot on a same picture two chrom (thinked to be A and C).
## USAGE : ./plot_SNP_density.py -w 10k -I 10k/distrib.chrA01.tsv 10k/distrib.chrC01.tsv [-O test.svg]
## AUTHORS : sebastien.letort@irisa.fr
## NOTE : If chrom file are not *.chr*.tsv, their name will not be correctly extracted.


import sys
import pdb	# debug
import pprint # debug
import argparse	# option management, close to C++/boost library (?)
import os.path

import matplotlib
import matplotlib.pyplot as plt
import csv
import re


VERSION = "1.0"
PP = pprint.PrettyPrinter(indent=4)
VERBOSE = False

RE_CHROM = re.compile( '\.chr([ACU]((\d\d)|nn)(_random)?)\.tsv' )
RAPSO_COL=1
DISCO_COL=2

def verbose( msg ):
	if( VERBOSE ):
		sys.stderr.write( msg + "\n" )


# ========================================
def main( o_args ):
	# PP.pprint( o_args.fileA, o_args.fileC )

	n_files = len( o_args.infiles )

	height  = 4 * n_files
	fig, t_axes = plt.subplots( nrows=n_files, sharex=True, figsize=(18,height) )

	for i in range( 0, n_files ):
		set_xlabel = ( i ==n_files-1 )
		plot_file( o_args.infiles[i], t_axes[i], o_args.window, set_xlabel )

	# plt.show()
	( bname, ext) = os.path.splitext( o_args.outfile.name )
	ext = ext[1:]
	plt.savefig( o_args.outfile, format=ext, bbox_inches='tight' )

def plot_file( infile, axe, win_size, set_xlabel=False ):
	( l_x, l_rapso, l_disco, l_heads ) = loadGraphData( infile )

	l_r, = axe.plot( l_x, l_rapso, '+', markersize=1 )
	l_d, = axe.plot( l_x, l_disco, 'x', markersize=1  )

	x_label = l_heads.pop( 0 )
	if( set_xlabel ):
		axe.set_xlabel( x_label )
	axe.legend( (l_r,l_d), l_heads )

	chrom = extractChromName( infile )
	title = 'SNP density in chrom ' + chrom
	title += '( ' + win_size + ' window)'
	axe.set_title( title )


def extractChromName( filename ):
	chrom = "Unk"
	m = RE_CHROM.search( filename )
	if( m ):
		chrom = m.group( 1 )

	return chrom


def loadGraphData( infile ):
	with open( infile, 'r' ) as fe:
		o_fe = csv.reader( fe, delimiter='\t' )

		l_x  = []
		l_y1 = []
		l_y2 = []
		#~ i=0

		# skip header
		l_heads = o_fe.next()
		l_heads = [ l_heads[0], l_heads[RAPSO_COL], l_heads[DISCO_COL] ]

		for l_row in o_fe:
			l_x.append( float( l_row[0] ) )
			l_y1.append( int( l_row[RAPSO_COL] ) )
			l_y2.append( int( l_row[DISCO_COL] ) )

			#~ i += 1
			#~ if( 10 < i ) : break

	return l_x, l_y1, l_y2, l_heads

# ----------------------------------------
def defineOptions():
	descr = "..."
	o_parser = argparse.ArgumentParser( description=descr )

	o_parser.add_argument( '-I', '--infiles', nargs='+',
							required = True,
						help    = 'The A chrom distribution file.' )
	#~ o_parser.add_argument( '-A', '--fileA', type=file,
							#~ required = True,
						#~ help    = 'The A chrom distribution file.' )
	#~ o_parser.add_argument( '-C', '--fileC', type=file,
							#~ required = True,
						#~ help    = 'The C chrom distribution file.' )

	o_parser.add_argument( '-w', '--window', type=str,
							required = True,
						help    = 'The window size used (for title only).' )

	o_parser.add_argument( '-O', '--outfile', type=argparse.FileType( 'w' ),
						default = sys.stdout,
						help    = 'The output filename, default is stdout.' )

	return o_parser

# ----------------------------------------

if __name__ == '__main__':
	o_parser = defineOptions()
	o_args   = o_parser.parse_args()
	#~ analyseOptions( o_args )
	if( VERBOSE ): PP.pprint( { "args": o_args } )

	main( o_args )

	sys.exit( 0 )

