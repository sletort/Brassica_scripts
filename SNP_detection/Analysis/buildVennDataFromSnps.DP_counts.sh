#! /bin/bash

## AIMS  : build Venn data from SNPs.DP_counts.
## USAGE : ./buildVennDataFromSnps.DP_counts.sh infile outfile [missing_values_char=.]
## NOTE  : Darmor (supposed the last column) is used as ref.
## NOTE  : Output is a count of each combination, to use in 5Sets.venn.irisa.sh script.

set -o nounset
set -o errexit

# =========================================================
declare -r INFILE="${1?What is your input file (.tsv.gz) ?}"
declare -r OUTFILE="${2?What is your output directory () ?}"
declare -r MISSING="${3-.}"

# ---------------------------------------------------------
declare -r SCRIPT_PATH=$( dirname $0 )

# =========================================================
function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT     = $0\n"
	printf "╟ - INFILE     = $INFILE\n"
	printf "╟ - OUTFILE    = $OUTFILE\n"
	printf "╟ - MISSING    = $MISSING\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

# =========================================================
# set ref to Darmor or to major allele (or let it)
function setDarmorRef ()
{
	local p_init='@a_ = split //;'
	local perle='if( 1 eq $a_[4] ){ tr/01/10/ }; '

	perl -plne "$p_init $perle"
}

# counts are for ref/alt
# set 0 if ref >(alt+5), 1 if alt > (ref+5), else MISSING
# cf note (A)
function turnToBoolean ()
{
	local perle='@a_ = map{ ( $sum,$ref,$alt ) = split /[:,]/; '
	perle=$perle' if( $ref > ($alt+5) ){ 0; }'
	perle=$perle' elsif( $alt > ($ref+5) ){ 1; }'
	perle=$perle' else { "."; } '
	perle=$perle' } @F[4..$#F]; print @a_;'

	perl -lane "$perle"
}

# turn bool flag to letter : 01011 will become BDE
# cf note (A)
function turnToLetters()
{
	local p_init
	local perle
	local p_out

	p_init='@a_bools = split //, $_; @a_letters=();'
	perle='map{ push( @a_letters, sprintf( "%c", 65+$_) ) if( 1 == $a_bools[$_] ); } 0..$#a_bools;'
	p_out='print join( "", @a_letters );'
#	p_out='print "$F[0]\t" . join( "", @a_letters );'
	perl -lane "$p_init $perle $p_out"
}

# for 5Sets.venn.irisa.sh
function main ()
{
	# note (A) : turnToBoolean and turnToLetters could be merged in one function
	#	I did not do it because boolean values can be requiered by another Venn script.
	zcat "$INFILE" \
			| turnToBoolean \
			| setDarmorRef \
			| turnToLetters \
			| sort | uniq -c \
		> "$OUTFILE"
}

#~ # for http://bioinformatics.psb.ugent.be/webtools/Venn/
#~ function main ()
#~ {
	#~ # note (A) : turnToBoolean and turnToLetters could be merged in one function
	#~ #	I did not do it because boolean values can be requiered by another Venn script.
	#~ zcat "$INFILE" \
			#~ | turnToBoolean \
			#~ | setDarmorRef \
			#~ | turnToLetters \
			#~ | nl \
		#~ > "$OUTFILE"

	#~ for l in A B C D
	#~ do
		#~ grep $l "$OUTFILE" > "$OUTFILE".$l
	#~ done
#~ }

# =========================================================
printVars

# your work.
main

printf "End of %s.\n\n" $( basename $0 )
exit 0
