#! /bin/bash

## AIMS  : count SNPs on a non sliding window over all chroms.
## USAGE : generate_SNP_density_file.sh WIN_SIZE_str rapso.starch disco.starch
## NOTE  : WIN_SIZE_str can be 1k, 1M, ...
## NOTE  : Data will be generated in a directory named like the window size.
## AUTHORS : sletort@irisa.fr

set -o nounset
set -o errexit

# =========================================================
declare -r WIN_SIZE_str="${1?What is the window size to use ?}"
declare -r GENOME="${2?The rapsoSNP starch file is needed.}"
declare -r RAPSO_STARCH="${3?The rapsoSNP starch file is needed.}"
declare -r DISCO_STARCH="${4?The discoSNP starch file is needed.}"

# ---------------------------------------------------------
declare -r BASE_PATH=$( dirname $( readlink -e $0 ) )/../../

# =========================================================
function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT       = $0\n"
	printf "╟ - WIN_SIZE_str = $WIN_SIZE_str\n"
	printf "╟ - GENOME       = $GENOME\n"
	printf "╟ - RAPSO_STARCH = $RAPSO_STARCH\n"
	printf "╟ - DISCO_STARCH = $DISCO_STARCH\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

# =========================================================
# extract and slightly reformat rapso SNP count starch file.
function rapso_data ()
{
	local data_file="$1"
	local chr="$2"

	printf "position (Mb)\trapso\n";
	unstarch $chr $data_file \
		| perl -lane '$m = ($F[1]+$F[2])/(2 * 1_000_000); print join( "\t", $m, $F[3] );'
}

function disco_data
{
	local map_file="$1"
	local disco_file="$2"
	local chr="$3"

	printf "disco\n"
	bedmap --chrom $chr --faster --count "$map_file" "$disco_file"
}


function main ()
{
	printf "Count SNPs on RapsoSNP.\n"
	mkdir -p $WIN_SIZE_str
	"$BASE_PATH"/generic/count_elements_on_genome.sh \
			-g "$GENOME" -b $WIN_SIZE_str "$RAPSO_STARCH" \
		> $WIN_SIZE_str/rapso.starch


	printf "Chrom by chrom, extract Rapso count\n"
	printf "and count SNPs on DiscoSNP.\n"
	for chr in $( unstarch --list-chr "$RAPSO_STARCH" )
	#for chr in chrA01
	do
	{
		local outdir=$WIN_SIZE_str/$chr
		mkdir $outdir

		rapso_data $WIN_SIZE_str/rapso.starch $chr \
			> $outdir/rapso.$chr.tsv

		disco_data $WIN_SIZE_str/rapso.starch "$DISCO_STARCH" $chr \
				> $outdir/disco.$chr.lst

		paste $outdir/rapso.$chr.tsv $outdir/disco.$chr.lst \
			> $WIN_SIZE_str/distrib.$chr.tsv

		rm -rf "$outdir"
	} &
	done
	wait
	printf "Waiting for backgrounded job to terminate.\n" >&2

cat > $WIN_SIZE_str/read.me <<EOF
	distrib.*.tsv are chrom_pos, SNP counts in rapso, disco_b0, disco_b1."
EOF
}

# =========================================================
printVars

# your work.
main

printf "End of %s.\n\n" $( basename $0 ) >&2

exit 0
