# Snakemake file
## AIMS  : (mainly try snakemake) build the fasta file of the scaffold that do not matche on the seq reference.
## USAGE : input/output filepath are relative to the directory from where the script is called.
## USAGE : CUL=$( loopOverCultivars bnapus echo ) DATA_DIR=Finishing snakemake
## NOTE  : Last check I've done, file do not work ! first rule seems OK, but not the 2d

# script path should be relative to snakefile
# but shell commands are relative to working directory
ROOT = "/groups/genscale/sletort/depots/gitlab.inria/Brassica_scripts"

CUL      = os.environ.get( "CUL", "" ).split()
DATA_DIR = os.environ.get( "DATA_DIR", "" )

# ==================================================================
#tgt = [ "{0}/gs_bnapus_{1}/unmapped_scaff.lst".format( DATA_DIR, c ) for c in CUL ]
tgt = [ "{0}/gs_bnapus_{1}/{1}.unmapped_scaff.sm.fa".format( DATA_DIR, c ) for c in CUL ]

# FAUX !! utilisez AdHoc/qd_extract_unmapped.sh
perle  = 'next if( $F[1]<500 ); if( 2 == @F ){ print $F[0]; }'
perle += 'else{	for( $i=3; $i<@F; $i+=2 ){	$delta = $F[1] - $F[$i];'
perle += 'if( 500 < $delta ){ print "$F[0]"; last; } } }'

# ==================================================================
rule extract_unref_ids:
	input:
		"{DATA_DIR}/gs_bnapus_{cultivar}/scaff_chrom_size.tsv"
	output:
		"{DATA_DIR}/gs_bnapus_{cultivar}/unmapped_scaff.lst"
	shell:
		"perl -lane '{perle}' {input} > {output}"

rule extract_unref_seqs:
	input:
		"{DATA_DIR}/gs_bnapus_{cultivar}/unmapped_scaff.lst",
		"/groups/rapsodyn/Assembly/{cultivar}/{cultivar}.fa.gz"
	output:
		"{DATA_DIR}/gs_bnapus_{cultivar}/{cultivar}.unmapped_scaff.sm.fa"
	shell:
		'cul=$( basename {input[1]} .fa.gz ) ; \
		{ROOT}/generic/mfasta_extract_seqs.py {input[0]} {input[1]} \
				| {ROOT}/generic/mfasta_prefix_seq_ids.py - "$cul|" \
			> {output[0]}'

rule all:
	input: tgt
	run:
		print( "made " + str( tgt ) )

