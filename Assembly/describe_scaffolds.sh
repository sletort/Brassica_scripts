#! /bin/bash

## AIMS  : parse the scaff_chrom_size file to summarize scaffold distribution.
## USAGE : ./describe_scaffolds.sh [-h] [-v] AlignOnRef/
#
## OUTPUT : for each cultivars a tsv file name scaff_descr.tsv
## OUTFILE:
#
## NOTE  :
#
## BUG   : None found yet.

source /softs/local/env/envperl-5.22.0.sh

set -o nounset
set -o errexit

source $__ROOT/brassicaDB.sh
source $__ROOT/lib/lib_misc.sh

# =========================================================
declare INDIR

# ---------------------------------------------------------
declare VERBOSE=0
declare -r FILENAME="scaff_descr.tsv"

#~ trap before_exiting EXIT

# =========================================================
#~ function before_exiting ()
#~ {
	#~ printf "TRAP : $0 exit with code $?.\n" >&2
#~ }

function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT    = $0\n"
	printf "╟ - INDIR     = $INDIR\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

# =============================================
function manageOptions ()
{
# getopts reminder:
#	FLAG, x will look for -x
#	x: will look for -x stg
#	if the option string start with ':', getopts turns to silent error reporting mode.
	while getopts "hv" opt
	do
		case $opt in
			v)	VERBOSE=1	;;
			h)
				usage && exit 0 ;;
			\?)	usage && exit 0 ;;
		esac
	done
	shift $((OPTIND-1))

	INDIR="${1?What is the directory containing the gs_bnapus_$cul directory ?}"
}

function usage ()
{
		cat <<EOF
USAGE : $0 [options] INDIR

Options :

	-v : verbose mode, see parameter values.
	-h : print this message and exit.
EOF
}

# =========================================================
# I could do a tiny proper prog
function __describe_scaffolds ()
{
	local infile="$1"

	local scaff_descr=$(
			cut -f 2 $infile | describe_data.pl -json
		)

	local N=$( printf $scaff_descr | jq .n )
	printf "# scaffolds\t%d\n" $N
	printf "Size distrib.\n"
	printf "min max\t%d\t%d\n"   $( printf $scaff_descr | jq .min,.max )
	printf "q1 q2 q3\t%.2f\t%.2f\t%.2f\n"   $( printf $scaff_descr | jq .q1,.q2,.q3 )


	local p_end='END{ print "$s"; }'
	perle='++$s if( 2 == @F );'
	local n=$( perl -wlane "$perle $p_end" $infile )
	printf "# unmapped\t%d\t%.1f%%\n" $n $( perl -le "print 100*$n/$N" )

	printf "# align one chrom\n"
		perle='++$s if( 4 == @F && $F[1] == $F[3] );'
		n=$( perl -wlane "$perle $p_end" $infile )
		printf "# map 100%%\t%d\t%.1f%%\n" $n $( perl -le "print 100*$n/$N" )

		perle='if( 4 == @F && $F[1] != $F[3] && 500 <= $F[3] ){ $delta = $F[1] - $F[3]; ++$s if( $delta < 500 ); }'
		n=$( perl -wlane "$perle $p_end" $infile )
		printf "# less than 500 bases unmapped\t%d\t%.1f%%\n" $n $( perl -le "print 100*$n/$N" )

		perle='if( 4 == @F && 500 <= $F[3] ){ $delta = $F[1] - $F[3]; ++$s if( 500 <= $delta ); }'
		n=$( perl -wlane "$perle $p_end" $infile )
		printf "# more than 500 bases unmapped\t%d\t%.1f%%\n" $n $( perl -le "print 100*$n/$N" )

	printf "# mapped many chroms\n"
		perle='next unless( 4 < @F );'
		perle=$perle' for( $i=3; $i<@F; $i+=2 ){ ++$s && last if( $F[1] == $F[$i] ); }'
		n=$( perl -wlane "$perle $p_end" $infile )
		printf "# map 100%% on at least one chrom\t%d\t%.1f%%\n" $n $( perl -le "print 100*$n/$N" )

		perle='next unless( 4 < @F );'
		perle=$perle' $go = 0; for( $i=3; $i<@F; $i+=2 ){ '
		perle=$perle' 	if( $F[$i] == $F[1] ){ $go = 0; last; }'
		perle=$perle' 	$delta = $F[1] - $F[$i];'
		perle=$perle'	if( 500 <= $delta ){ $go = 1 if( 500 <= $F[$i] ); }'
		perle=$perle' } ++$s if( 1 == $go );'
		n=$( perl -wlane "$perle $p_end" $infile )
		printf "# more than 500 unmapped on at least one chrom\t%d\t%.1f%%\n"  $n $( perl -le "print 100*$n/$N" )

		perle='next unless( 4 < @F );'
		perle=$perle' $go = 1; for( $i=3; $i<@F; $i+=2 ){ '
		perle=$perle' 	if( $F[$i] == $F[1] ){ $go = 0; last; }'
		perle=$perle' 	$delta = $F[1] - $F[$i];'
		perle=$perle'	if( 500 <= $delta ){ $go = 0; }'
		perle=$perle' } ++$s if( 1 == $go );'
		n=$( perl -wlane "$perle $p_end" $infile )
		printf "# no more than 500 unmapped on all chrom\t%d\t%.1f%%\n"  $n $( perl -le "print 100*$n/$N" )

	return $N
}

function describe_scaffolds ()
{
	local cul="$1"
	local data_dir=$INDIR/gs_bnapus_$cul/against_ref/scaffs/

	# to make it run parallel
	{
		# save some time
		local tmp_file=$( mktemp scaff_XXXX.tsv )
		if [[ -e $data_dir/scaff_chrom_size.tsv.gz ]]
		then
			zcat $data_dir/scaff_chrom_size.tsv.gz > $tmp_file
		else
			# pas top de faire une copie ...
			cp $data_dir/scaff_chrom_size.tsv $tmp_file
		fi

		# all scaffolds
		local outfile="$data_dir/${FILENAME}"
		__describe_scaffolds "$tmp_file" \
			> "$outfile"

		# ...
		printf "\n\nbig scaffolds (size >= 1000)\n" \
			>> "$outfile"

		local tmp_file2=$( mktemp scaff_XXXX.tsv )
		perl -lane 'print if( 1000 <= $F[1] )' $tmp_file \
			> $tmp_file2
		__describe_scaffolds "$tmp_file2" \
			>> "$outfile"

		rm "$tmp_file" "$tmp_file2"
	}&
}


#using HEREDOC does not make it clearer
#	function to compute the % string ? (this should make thing smarter)
function __summarize_all ()
{
	local infile="$1"

	local p_end='END{ print "$s"; }'

	local perle='$s += $F[1] if( $F[0] =~ /^# scaffolds$/);'
	local n_scaff=$( perl -F"\t" -lane "$perle $p_end" $infile )

	perle='$s += $F[1] if( $F[0] =~ /^# unmapped$/);'
	local n_unmapped=$( perl -F"\t" -lane "$perle $p_end" $infile )
	local pc_unmapped=$( perl -le "printf( \"%.2f\n\", 100*$n_unmapped/$n_scaff );" )

	perle='$s += $F[1] if( $F[0] =~ /^# map 100%$/);'
	local n_fully_mapped_on_one=$( perl -F"\t" -lane "$perle $p_end" $infile )
	local pc_fully_mapped_on_one=$( perl -le "printf( \"%.2f\n\", 100*$n_fully_mapped_on_one/$n_scaff );" )

	perle='$s += $F[1] if( $F[0] =~ /^# less than 500 bases unmapped$/);'
	local n_part_mapped_one_big_delta=$( perl -F"\t" -lane "$perle $p_end" $infile )
	local pc_part_mapped_one_big_delta=$( perl -le "printf( \"%.2f\n\", 100*$n_part_mapped_one_big_delta/$n_scaff );" )

	perle='$s += $F[1] if( $F[0] =~ /^# more than 500 bases unmapped$/);'
	local n_part_mapped_one_small_delta=$( perl -F"\t" -lane "$perle $p_end" $infile )
	local pc_part_mapped_one_small_delta=$( perl -le "printf( \"%.2f\n\", 100*$n_part_mapped_one_small_delta/$n_scaff );" )

	perle='$s += $F[1] if( $F[0] =~ /^# map 100% on at least one chrom$/);'
	local n_fully_mapped_and_more=$( perl -F"\t" -lane "$perle $p_end" $infile )
	local pc_fully_mapped_and_more=$( perl -le "printf( \"%.2f\n\", 100*$n_fully_mapped_and_more/$n_scaff );" )

	perle='$s += $F[1] if( $F[0] =~ /^# more than 500 unmapped on at least one chrom$/);'
	local n_part_mapped_on_many_big_delta=$( perl -F"\t" -lane "$perle $p_end" $infile )
	local pc_part_mapped_on_many_big_delta=$( perl -le "printf( \"%.2f\n\", 100*$n_part_mapped_on_many_big_delta/$n_scaff );" )

	perle='$s += $F[1] if( $F[0] =~ /^# no more than 500 unmapped on all chrom$/);'
	local n_part_mapped_on_many_small_delta=$( perl -F"\t" -lane "$perle $p_end" $infile )
	local pc_part_mapped_on_many_small_delta=$( perl -le "printf( \"%.2f\n\", 100*$n_part_mapped_on_many_small_delta/$n_scaff );" )

	cat <<EOF
# scaffolds	$n_scaff
# unmapped	$n_unmapped	$pc_unmapped
# align one chrom
# map 100%	$n_fully_mapped_on_one	$pc_fully_mapped_on_one
# less than 500 bases unmapped	$n_part_mapped_one_big_delta	$pc_part_mapped_one_big_delta
# more than 500 bases unmapped	$n_part_mapped_one_small_delta	$pc_part_mapped_one_small_delta
# mapped many chroms
# map 100% on at least one chrom	$n_fully_mapped_and_more	$pc_fully_mapped_and_more
# more than 500 unmapped on at least one chrom	$n_part_mapped_on_many_big_delta	$pc_part_mapped_on_many_big_delta
# no more than 500 unmapped on all chrom	$n_part_mapped_on_many_small_delta	$pc_part_mapped_on_many_small_delta
EOF
}

# once again a proper prog should be way better !
function summarize_all ()
{
	mkdir -p "$INDIR"/integration/descr
	local outfile="$INDIR"/integration/descr/scaffolds.tsv
	local tmp_file=$( mktemp scaff_XXXX.tsv )

	# extract "full" info
	for f in "$INDIR"/gs_bnapus_*/against_ref/scaffs/"$FILENAME"
	do
		perl -pe 'die if( /big scaffolds/ );' $f 2> /dev/null
	done > $tmp_file
	__summarize_all "$tmp_file" > "$outfile"

	# only big scaff info
	printf "\n\nbig scaffolds (size >= 1000)\n" \
		>> "$outfile"
	for f in "$INDIR"/gs_bnapus_*/against_ref/scaffs/"$FILENAME"
	do
		perl -ne 'if( /big scaffolds/ ){ $big_flag = 1; } print if( $big_flag );' $f
	done > $tmp_file
	__summarize_all "$tmp_file" >> "$outfile"

	rm "$tmp_file"
}

function main ()
{
	printf "1- Describe scaffolds for each cultivar.\n"
	loopOverCultivars bnapus describe_scaffolds
	#describe_scaffolds amber
	printf "Waiting for backgrounded job to terminate.\n"
	wait

	printf "2- summarize the whole study in one file.\n"
	summarize_all
}

# =========================================================
manageOptions "$@"

if [[ 1 -eq $VERBOSE ]]
then
	printVars >&2
fi

# your work.
main

printf "End of %s.\n\n" $( basename $0 ) >&2
exit 0
