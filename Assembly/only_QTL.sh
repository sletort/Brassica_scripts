#! /bin/bash

## AIMS  : Find everything I can, but only on QTL.
## USAGE : ./only_QTL.sh [-h] [-v] [-o outname] QTL_file.bed
## NOTE  : This script is to run after the assembly, before alignment on reference.
## NOTE  : conda has to be activated on the cluster to use bcftools merge

source /softs/local/env/envblast+.sh
source /softs/local/env/envpython-2.7.sh
source /softs/local/env/envvcftools-0.1.5.sh # for vcf-sort
source /softs/local/env/envbcftools-1.3.1.sh
source /softs/local/env/envconda.sh # will activate bcftools, hope it will not interfer with the other bcftools.

set -o nounset
set -o errexit

source $__ROOT/brassicaDB.sh

# to get get_assembly_filepath (which locally extract the fasta file)
# maybe this function should be in another file ?
source $__ROOT/lib/lib_assembly.sh

# =========================================================
declare QTL_FILE
declare OUTNAME="asm_blast" # merged file will be $OUTNAME.bcf
declare -r DISCO_MODE="b0"	# data to use as discoSNP results

# ---------------------------------------------------------
declare VERBOSE=0
declare _CWD=$( dirname $( readlink -e $0 ) )
source "$_CWD"/../lib/lib_misc.sh

declare -A A_QTLs=()
declare -r QTL_FASTA="QTL_ref.fasta"
declare -r BLAST_EXT="mblast.raw.tsv" # files will be $cul.$BLAST_EXT
declare -r VCF_EXT="mblast.vcf.gz" # files will be $cul.$VCF_EXT
declare -r CULTI_DIR="cultivars"
declare -r DISCO_DIR="discosnp_vcf"


# EXTRACT_PROG will take arguments as tsv file, human reading : first 2 bases = "seq_id\t1\t2\n"
declare -r EXTRACT_PROG="$__ROOT/generic/mfasta_extract_subseqs.py"
declare -r STACK_PROG="$__ROOT/SNP_detection/stack_disco_vcf_records.py"

# =========================================================
function manageOptions ()
{
# getopts reminder:
#	FLAG, x will look for -x
#	x: will look for -x stg
#	if the option string start with ':', getopts turns to silent error reporting mode.
	while getopts "hvo:" opt
	do
		case $opt in
			o)	OUTNAME=$OPTARG  ;;

			v)	VERBOSE=1	;;
			h)
				usage && exit 0 ;;
			\?)	usage && exit 0 ;;
		esac
	done
	shift $((OPTIND-1))

	QTL_FILE="${1?What is the QTL bedfile ?}"
}

function usage ()
{
		cat <<EOF
USAGE : $0 [options] QTL.bed

Options :
	-o : outname. The final output file will be $outname.bcf.
	-v : verbose mode, see parameter values.
	-h : print this message and exit.
EOF
}

function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT    = $0\n"
	printf "╟ - QTL_FILE  = $QTL_FILE\n"
	printf "╟ - OUTNAME   = $OUTNAME\n"
	printf "╟ ------------------------\n"
	printf "╟ - QTL_FASTA = $QTL_FASTA\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

# =========================================================
function init ()
{
	#~ Misc__log "Load QTL bed file"
	#~ while read chrom start end id
	#~ do
		#~ A_QTLs[$id]="$chrom\t$start\t$end"
	#~ done < "$QTL_FILE"

	Misc__log "Extracting QTL sequences"
	# I need to turn bed file ([0;2[) to human reading file ([1;2])
	perl -lane '$F[1] += 1; print join( "\t", @F[0..2], "+", @F[3..$#F] );' "$QTL_FILE" \
			| "$EXTRACT_PROG" --tsv - --outfile "$QTL_FASTA" --id_col 4 $( get_ref_fasta bnapus )

	Misc__log "Build a blast db"
	makeblastdb -in "$QTL_FASTA" -dbtype nucl -parse_seqids


	mkdir -p $CULTI_DIR
}

function build_vcf_from_blast_results ()
{
	local cul="$1"

	local blast_res="$cul.$BLAST_EXT"
	local   vcf_out="$cul.$VCF_EXT"
	local  ref_path=$( get_ref_fasta bnapus )

	# solve some multialignment. When a scaff align on 2 locations, choose the best one.
	local    filter="$_CWD/AdHoc/filter_blast.py"

	# remove _uid_* from scaff id and sort file on chrom-start-end reorder link with ref on strand +.
	local  reformat="$_CWD/AdHoc/reformat_blast.py"
	local    to_vcf="$_CWD/AdHoc/mblast2VCF.py"

	local bname=$( basename $blast_res .raw.tsv )
	{
		"$filter" -l $cul.filter.log -v $blast_res \
				| "$reformat" | tee $bname.tsv \
				| "$to_vcf" -R "$ref_path" --sample $cul \
				| vcf-sort | bgzip \
			> $vcf_out
		tabix -p vcf $vcf_out
	}&
}

function align_assembly ()
{
	local cul="$1"

	#~ if [[ "amber" == $cul ]]
	#~ then
		#~ return 0
	#~ fi

	{
		local assembly_filepath=$( Ass__get_assembly_filepath "$cul" )
		Misc__log "Will align assembly file : %s\n" "$assembly_filepath"

		local headers='sseqid sstart send qseqid qstart qend'
		headers=$headers' sstrand qlen length'
		headers=$headers' score evalue pident nident mismatch'
		headers=$headers' sseq qseq'

		local raw_blast="$cul.$BLAST_EXT"

		# Note I used 97% of identity because it seems to solve lots of ambiguities
		#	seen on amber (first 10 duplets), the 2d alignments are
		#		95.190, 96.770, 95.111, 96.679, 100.000, 99.883, 98.204, 95.413, 99.907, 96.944
		#	-outfmt "10 qseqid qstart qend qseq sseq" \	#format used in rapso SNP
		blastn -task megablast \
			-word_size 32 -perc_identity 97 -qcov_hsp_perc 80 \
			-db "$QTL_FASTA" -query $assembly_filepath \
			-outfmt "6 $headers" \
			-out $raw_blast \
			-num_threads 4

		mv $raw_blast $CULTI_DIR/

		rm "$assembly_filepath"
	}&

}

function merge_variants ()
{
# because bcftools merge is not working on the cluster.
source activate bcftools
	bcftools merge -O b --output $OUTNAME.bcf "$CULTI_DIR/"*.vcf.gz

	bcftools query -l $OUTNAME.bcf \
		> samples.lst

	# output change depending on sample order in input.
source deactivate
}

function flag_variants ()
{
	local -a l_chroms=$( bcftools view -h $OUTNAME.bcf | perl -lne 'if( /^##contig=<ID=(.+)>/ ){ print $1; }' )
	local -r chrom_files=$OUTNAME.chrom_files.lst

	rm -f $chrom_files
	for c in $l_chroms
	do
	{
		# I can succeed in reading stdin with pysam.VariantFile (fetch fail)
		tmp=$( mktemp before_flag.$c.XXXX.bcf )
		bcftools view -r $c -O b -o $tmp $OUTNAME.bcf

		local outfile=$OUTNAME.$c.flagged.vcf.gz
		local logfile=$OUTNAME.$c.flagged.log
		$_CWD/AdHoc/flag_vcf.py -v $tmp 2> $logfile \
				| bgzip -c \
			> $outfile

		printf "$outfile\n" >> $chrom_files
		rm -f "$tmp"
		pigz $logfile
	}&
	done
	printf "Waiting for all chroms to be flagged.\n"
	wait

	bcftools concat --file-list $chrom_files \
		-O b -o $OUTNAME.flagged.bcf
	bcftools index $OUTNAME.flagged.bcf
}

function generate_discoSNP_vcf ()
{
	local generate_vcf="$__ROOT/SNP_detection/generate_vcf_from_disco_filtered_blast.sh"

	mkdir -p "$DISCO_DIR"
	local old_pwd=$( pwd )
	cd "$DISCO_DIR"

	# generate a "Disco_SNP.$DISCO_MODE.bcf"
	$generate_vcf $DISCO_MODE
	local disco_vcf="Disco_SNP.$DISCO_MODE.bcf"

	# limit vcf to QTL region
	# assume QTL_FILE is relative to old_pwd (no time to make good code)
	perl -pe 's/^chr//;' "$old_pwd/$QTL_FILE" \
		> qtl.bed
	local disco_tmp=$( basename $disco_vcf .bcf ).tmp.vcf.gz
	bcftools view --regions-file qtl.bed -O z "$disco_vcf" \
			--samples-file $old_pwd/samples.lst \
		> $disco_tmp

	local disco_out=$( basename $disco_vcf .bcf ).stacked.vcf.gz
	$STACK_PROG $disco_tmp --vcf_out $disco_out -v \
		2> disco_stack.$DISCO_MODE.log

	rm -f $disco_tmp

	bgzip $disco_out
	tabix -p vcf $disco_out.gz

	cd $old_pwd
}

function main ()
{
	# assert_dependencies
	init

	loopOverCultivars bnapus align_assembly
	# align_assembly amber
	printf "Waiting for backgrounded blast to terminate.\n"
	wait

	local path=$( pwd )
	cd "$CULTI_DIR"

#	build_vcf_from_blast_results montego
	loopOverCultivars bnapus build_vcf_from_blast_results
	printf "Blast alignment are turned into VCF files.\n"
	wait

	cd "$path"
	merge_variants
	flag_variants

	# should not be done here, but in SNP_detection.
	generate_discoSNP_vcf
}


# =========================================================

manageOptions "$@"

if [[ 1 -eq $VERBOSE ]]
then
	printVars >&2
fi

# your work.
main

printf "End of %s.\n\n" $( basename $0 ) >&2
exit 0
