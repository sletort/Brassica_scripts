#! /bin/bash

## AIMS  : align with blat and build chain/nets = big blocs.
## USAGE : ./align_build_bloc.sh [-h] [-v] cultivar
## NOTE  : alignment with blat is correct for sequences with id > 95% and >40 bases.
## NOTE  : will launch $NB_CPU

source /softs/local/env/envucsc-357.sh

source $__ROOT/brassicaDB.sh
source $__ROOT/lib/lib_misc.sh

set -o nounset
set -o errexit

# =========================================================
declare CUL
declare OUTDIR # will be set to $CUL

# ---------------------------------------------------------
declare VERBOSE=0

declare -r REF_BNAME=bnapus.v4_1
declare -r NB_CPU=40 # number of parallel jobs to allow
declare -r TILE_SIZE=12 # blat word/seed, cf -tileSize blat's option

# =========================================================
function manageOptions ()
{
# getopts reminder:
#	FLAG, x will look for -x
#	x: will look for -x stg
#	if the option string start with ':', getopts turns to silent error reporting mode.
	while getopts "hvo:" opt
	do
		case $opt in
			o)	OUTDIR=$OPTARG  ;;

			v)	VERBOSE=1	;;
			h)
				usage && exit 0 ;;
			\?)	usage && exit 0 ;;
		esac
	done
	shift $((OPTIND-1))

	CUL="${1?What is the cultivar to align ?}"
	OUTDIR=$CUL
}

function usage ()
{
		cat <<EOF
USAGE : $0 [options] cultivar

Options :
	-o : outdir. The directory of work, will contain intermediate files.
	-v : verbose mode, see parameter values.
	-h : print this message and exit.
EOF
}

function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT   = $0\n"
	printf "╟ - CULTIVAR = $CUL\n"
	printf "╟ ------------------------\n"
	printf "╟ - OUTDIR   = $OUTDIR\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

# =========================================================
function get_assembly_filepath ()
{ # copied from lib_AlignAssemblyToRef.sh
	local cultivar="$1"

	local found=0
	local path=$( get_assembly_path $cultivar )
	for f in "$path"/${cultivar}.fa "$path"/${cultivar}.fa.gz
	do
		if [[ -e $f ]]
		then
			if Misc__is_gzip $f
			then
				local tmp_file=$( mktemp --tmpdir=$PWD ${cultivar}.XXXXX.fa )
				printf "\tUnzipping file.\n" >&2
				gzip -dc $f > $tmp_file
				printf "$tmp_file"
			else
				printf "$f"
			fi
			found=1
			break
		fi
	done

	if [[ 0 -eq $found ]]
	then
		printf "No suitable file could be found in $path.\n" >&2
		exit 1
	fi
}

# ------------------------------
function init_env ()
{
	_make_dirs

	#Note : split by chrom can be more relevant for the next step
	#	this should avoid the need of splitting by chrom at the end of this step.
	printf "prepare files (2bit, chrom.sizes and partitions).\n"
	_prepare_files $( get_ref_fasta bnapus ) $REF_BNAME


	cd $OUTDIR

	# extraction of the fasta file
	local cul_fasta=$( get_assembly_filepath $CUL )
	_prepare_files "$cul_fasta" $CUL
	rm -f "$cul_fasta"

	cd -
}

function _make_dirs ()
{
	mkdir -p $OUTDIR/run.blat
	mkdir -p $OUTDIR/run.chain
}

function _prepare_files ()
{
	local fasta="$1"
	local bname="$2"

	if [[ ! -e $bname.2bit ]]
	then
		Misc__verbose "generate 2bit and chrom.sizes for $fasta."
		faToTwoBit "$fasta" "$bname.2bit"
		twoBitInfo $bname.2bit stdout \
				| sort -k2nr \
			> $bname.chrom.sizes
	else
		Misc__verbose "use previously computed files."
	fi
}

# ------------------------------
function run_blat ()
{
	cd $OUTDIR/run.blat

	local ass_bname=$CUL

	# generate $ass_bname.lst/lft files, .lst file splitted in 'parts' dir.
	$__ROOT/Assembly/AdHoc/vqd_make_chunks.pl $ass_bname
	_split_files $ass_bname

	# repMatch is the # of repetition of the seq in the whole genome to be considered as repeted sequence
	printf "generate ooc (repetitions) file.\n"
	blat "../../$REF_BNAME.2bit" /dev/null /dev/null \
		-tileSize=$TILE_SIZE -makeOoc=$TILE_SIZE.ooc -repMatch=256

	_run_blats ../../$REF_BNAME $ass_bname

	_split_psl_by_chroms $ass_bname

	_rebuild_assembly_coords $ass_bname

	# cleanning
	rm -rf parts

	cd -
}

function _split_files ()
{
	local bname="$1"
	local file="$bname.lst"

	Misc__verbose "split blat job in pieces, in 'parts' directory."
	mkdir -p parts

	local n_lines=$( wc -l $file | cut -d' ' -f 1 )
	local n_lines_per_subfile=$(( 1 + $n_lines / $NB_CPU ))

	split -l $n_lines_per_subfile $file parts/${bname}_
}

function _run_blats ()
{
	local ref_bname="$1"
	local ass_bname="$2"

	mkdir -p psl

	local options='-fastMap -noHead'
	options=$options" -tileSize=$TILE_SIZE -ooc=$TILE_SIZE.ooc"
	options=$options" -minScore=100 -minIdentity=98" # values from http://genomewiki.ucsc.edu/images/f/fa/BlatJob.csh.txt
		# works well on sample tested

	for f in parts/*
	do
		local f_bname=$( basename $f )
		blat $ref_bname.2bit "$f" psl/"$f_bname.psl" $options &
	done
	printf "Blat was launched, waiting for jobs to terminate.\n"
	wait
}

function _split_psl_by_chroms ()
{
	local bname="$1"

	local tmp_dir=$( mktemp --directory --tmpdir=. blat_XXXX )

	for f in psl/${bname}_*.psl
	do
		local filename=$( basename $f .psl )
		local suffix=${filename##${bname}_}

		mkdir -p $tmp_dir/$suffix
		splitFileByColumn -col=14 -chromDirs $f $tmp_dir/$suffix &
	done
	printf "Waiting for backgrounded splitFileByColumn to terminate."
	wait

	# paths are like $tmp_dir/$suffix/$main_chrom_name/chrom_name.psl
	for d in $tmp_dir/*/*
	do
		local chrom_dir=$( basename $d )
		for f in $d/*.psl
		do
			filename=$( basename $f .psl ).chunks.psl
			mkdir -p psl_chroms/$chrom_dir
			cat $f >> psl_chroms/$chrom_dir/$filename
		done
	done

	rm -rf $tmp_dir
}

function _rebuild_assembly_coords ()
{
	local ass_bname="$1"

	Misc__verbose "LiftUp psl file, = rebuild assembly coords."
	for chrom_dir in psl_chroms/*
	do
	{
		for f in "$chrom_dir"/*.chunks.psl
		do
			local outname=${f%.chunks.psl}
			liftUp -pslQ -nohead $outname.psl $ass_bname.lft warn $f \
				&& rm -f $f
		done
	}&
	done
	printf "Waiting for chrom-psl files to be parsed."
	wait
}

# ------------------------------
function run_chain ()
{
	cd $OUTDIR/run.chain

	local ass_bname=$CUL

	printf "chain alignment with axtChain.\n"
	_chain_by_chroms $ass_bname

	# I don't get the interest in merging and splitting after.
	Misc__verbose "Concatenate chain_chroms."
	chainMergeSort chain_chroms/*/*.chain \
		> $ass_bname.chain

	printf "Build nets.\n"
	chainPreNet $ass_bname.chain ../../$REF_BNAME.chrom.sizes ../$ass_bname.chrom.sizes stdout \
		| chainNet stdin ../../$REF_BNAME.chrom.sizes ../$ass_bname.chrom.sizes $ass_bname.net $REF_BNAME.net

	# $REF_BNAME.net will not be used for the moment

	# chainStitchId - Join chain fragments with the same chain ID into a single
	printf "Extract chains that belongs to nets.\n"
	netChainSubset $ass_bname.net $ass_bname.chain stdout \
		| chainStitchId stdin $ass_bname.net.chain

	cd -
}

function _chain_by_chroms ()
{
	local bname="$1"

	# probablement à bosser chrom par chrom ou trier le psl, à vérifier
	# par chrom = 6s pour A01 => évite le chargement des index du chrom de la ref pour chaque seq
	#	+ le temps d'écrire les fichiers "par chrom" très long sur le test, peut-être à cause du grand nb de fichier présent sur la partition.
	# sans décomposer par chrom > 12h !

	for chrom_dir in ../run.blat/psl_chroms/*
	do
	{
		local outdir="chain_chroms/"$( basename $chrom_dir )
		mkdir -p $outdir

		for f in "$chrom_dir"/*.psl
		do
			local chain_file=$outdir/$( basename $f .psl ).chain

			# I use linearGap medium, because it seems to be used for close species.
			local options='-linearGap=medium -verbose=0 -psl'
			axtChain $options $f ../../$REF_BNAME.2bit ../$ass_bname.2bit \
					$chain_file
		done
	}&
	done
	printf "Waiting for chrom-psl files to be parsed."
	wait
}

# ------------------------------
function assert_progs ()
{
	for p in blat liftUp splitFileByColumn \
	         axtChain chainMergeSort chainPreNet \
	         netChainSubset chainStitchId
	do
		which $p   > /dev/null
	done
}

function main()
{
	assert_progs

	init_env

	run_blat
	run_chain
}

# =========================================================
manageOptions "$@"

if [[ 1 -eq $VERBOSE ]]
then
	printVars >&2
fi

# your work.
main
