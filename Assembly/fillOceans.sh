#! /bin/bash

## AIMS  : determine sequence where it's 'NN..NN' on the ref.
## USAGE : ./fillOceans.sh [-h] [-v] -o ocean_filepath INDIR OUTFILE
## ALGO  : 
#
## OUTPUT :
## OUTFILE:
#
## NOTE  : Ocean and alignment should be starch files.
#
## BUG   : None found yet.

# its place ? or check the availability of sort-bed ?
source /softs/local/env/envbedops-2.4.15.sh
source /softs/local/env/envclustal-omega.sh
source /softs/local/env/envsamtools-1.3.1.sh # for bgzip

source "$__ROOT/brassicaDB.sh"

set -o nounset
set -o errexit

source "$__ROOT/lib/lib_misc.sh"

# =========================================================
declare OCEAN_FILE="where_is_the_oceans";
declare _CWD=$( dirname $( readlink -e $0 ) )

declare INDIR
declare OUTFILE

# ---------------------------------------------------------
declare VERBOSE=0

trap before_exiting EXIT

# =========================================================
function before_exiting ()
{
	printf "TRAP : $0 exit with code $?.\n" >&2
}

function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT    = $0\n"
	printf "╟ - INDIR     = $INDIR\n"
	printf "╟ - OCEAN     = $OCEAN_FILE\n"
	printf "╟ - OUTFILE   = $OUTFILE\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

# =============================================
function manageOptions ()
{
# getopts reminder:
#	FLAG, x will look for -x
#	x: will look for -x stg
#	if the option string start with ':', getopts turns to silent error reporting mode.
	while getopts "o:O:hv" opt
	do
		case $opt in
			o)	OCEAN_FILE=$OPTARG  ;;

			v)	VERBOSE=1	;;
			h)
				usage && exit 0 ;;
			\?)	usage && exit 0 ;;
		esac
	done
	shift $((OPTIND-1))

	INDIR="${1?Please name the directory containing assembly analysis (the one containing gs_bnapus_* directories.}"
	OUTFILE="${2?Please name the outfile.}"
}

function usage ()
{
		cat <<EOF
USAGE : $0 [options] INDIR OUTFILE

Options :
	-o filepath : ocean starch filepath (obtain from gff2bed)

	-v : verbose mode, see parameter values.
	-h : print this message and exit.
EOF
}

# =========================================================
function remove_if_empty_file ()
{
	local file="$1"
	local msg="${2-}"

	if [[ ! -s "$file" ]]
	then
		if [[ 1 -eq $VERBOSE ]]
		then
			printf "$msg" >&2
		fi
		rm "$file"
	fi
}

# =========================================================
function fill_oceans_covered_by_contigs ()
{
	printf "\t1.1- Extract alignment regions that cover Ocean.\n"
	loopOverCultivars bnapus extract_ctg_alignment
	printf "\tWaiting for backgrounded job to terminate.\n"
	wait

	printf "\t1.2- make bed file from those selected regions.\n"
	generate_bedfile_from_selected_regions

	printf "\t1.3- run clustalO with those bed files.\n"
 	run_clustal_on_selected_regions "contig"

	printf "\t1.4- determine ocean sequence.\n"
 	determine_ocean_sequence
}

function extract_ctg_alignment ()
{
	local -r cul="$1"

	local align_file="$INDIR/gs_bnapus_$cul/against_ref/contigs/coords.tsv.gz"
	local tmp_file="$cul.ctg_coords.bed"
	local outname="$cul.ctg_ocean.tsv"

	# --delim was placed in the command line because I did not succeed in make "\t" interpreted.
	local bedmap_opts='--skip-unmapped --echo --echo-map '
	{
		zcat "$align_file" | sort-bed - \
			> "$tmp_file"
		for chr in $( bedextract --list-chr $OCEAN_FILE )
		do
			mkdir $chr 2> /dev/null

			local outfile="$chr/$outname"
			bedmap --delim "\t" $bedmap_opts --chrom $chr $OCEAN_FILE $tmp_file \
					| perl -pi -e 's/NODE/'$cul'|NODE/;' \
				> "$outfile"

			remove_if_empty_file "$outfile" "No contig holding an ocean on $chr in $cul.\n"
		done
	}&
}

function generate_bedfile_for_cultivar_regions ()
{
	local cul="$1"
	#~ local scaff_ocean="$1"

	local scaff_len=$( get_assembly_path $cul )/scaff.seq_len.tsv.gz
	local tmp_scaff_len=$( mktemp $cul.scaff_len.XXXX )
	gzip -dc $scaff_len > $tmp_scaff_len

	local -r prog="$_CWD/AdHoc/generate_bedfile_for_scaff_ocean.py"
	for chr in $( unstarch --list-chr $OCEAN_FILE )
	#~ for chr in chrA03
	do
	{
		echo $chr
		unstarch $chr/$cul*.starch \
				| $prog --scaff_len $tmp_scaff_len \
			> $chr/$cul.scaff.bed
	}
	done
	Misc__verbose "	Waiting for backgrounded job to terminate."
	wait

	rm -f $tmp_scaff_len
}

function run_clustal_on_selected_regions ()
{
	local type="$1"

	local option=""
	if [[ "$type" == "contig" ]]
	then
		option="-c"
	fi

	for d in $( ls -d chr* )
	do
	{
		for bed_file in $( ls $d/*.bed )
		do
			local base_filename="${bed_file%.bed}"
			local fasta_file="$base_filename.fa.gz"
			"$_CWD/AdHoc/extract_subregions_from_bed_files.sh" $option -v "$bed_file"

			# not with qsub, I want to detect the end.
			#qsub $__ROOT/misc/run_cluster.clustalO.sh "$fasta_file"

			local align_file="$base_filename.align.fa"
			clustalo --infile $fasta_file --seqtype DNA --infmt fa \
							--outfile $base_filename.omega \
							--outfmt fa \
							--force
		done
	} &
	done
	printf "\tWaiting for extraction to complete.\n"
	wait
}

function determine_ocean_sequence ()
{
	local -r prog="$_CWD/AdHoc/determine_ocean_sequence.py"

	for mfasta_file in $( ls */*.omega )
	do
		local base_filename="${mfasta_file%.omega}"
		local region_file="$base_filename.regions.tsv"
		local res_file="$base_filename.ocean.tsv"

		$prog -c -o $res_file $mfasta_file $region_file
		perl -ane 'print join( "\t", @F[0..2], "" );' "$region_file" \
			&& head -n 1 "$res_file" | cut -f 2,3
	done > "$OUTFILE"
}

# =========================================================
function fill_oceans_covered_by_scaffolds ()
{
	Misc__log "	2.1- Extract alignment regions that cover Ocean."
	#~ loopOverCultivars bnapus extract_scaff_alignment
	#extract_scaff_alignment amber
	#extract_scaff_alignment aviso
	#extract_scaff_alignment zeruca
	#~ Misc__verbose "	Waiting for backgrounded job to terminate."
	#~ wait

	#~ Misc__log "	2.1.2- Integrate = merge all cultivar files."
	#~ merge_scaff_oceans

	printf "\t2.2- make bed file of cultivar regions.\n"
	#~ loopOverCultivars bnapus generate_bedfile_for_cultivar_regions "x"
	generate_bedfile_for_cultivar_regions amber
	#~ generate_bedfile_for_cultivar_regions aviso
	#~ generate_bedfile_for_cultivar_regions zeruca
	# maybe it is better not to launcher this in parallel to use the caches well.
	Misc__verbose "	Waiting for backgrounded job to terminate."
	wait

	#~ printf "\t1.3- run clustalO with those bed files.\n"
 	#~ run_clustal_on_selected_regions "scaff"

	#~ printf "\t1.4- determine ocean sequence.\n"
 	#~ determine_ocean_sequence
}

function extract_scaff_alignment ()
{
	local -r cul="$1"

	local align_file="$INDIR/gs_bnapus_$cul/against_ref/scaffs/$cul.starch"
	local outname="$cul.scaff_ocean"

	# --delim was placed in the command line because I did not succeed in make "\t" interpreted.
#cut -f 1-3 ctg_oceans.tsv \
#					| perl -pi -e 's/scaffold_/'$cul'|scaffold_/;'
	local bedmap_opts='--skip-unmapped --echo --echo-map '
	local perle='@a_ocean = @F[0..3];'
	perle=$perle' for( $i=10; $i<@F; $i+=6 ){ '
	perle=$perle' 	@a_scaff = @F[$i..$i+5]; $a_scaff[3] = "'$cul'|".$a_scaff[3];'
	perle=$perle' print join( "\t", @a_ocean, @a_scaff ); }'
	local -r SEP="\t"
	{
		#~ for chr in chrA01 chrA03
		for chr in $( unstarch --list-chr $OCEAN_FILE )
		do
			mkdir -p $chr

			local outfile="$chr/$outname.starch"
			bedmap --delim $SEP $bedmap_opts --chrom $chr "$OCEAN_FILE" "$align_file" \
					| perl -F";|\t" -lane "$perle" \
					| starch - \
				> "$outfile"

	#			remove_if_empty_file "$outfile" "No contig holding an ocean on $chr in $cul.\n"
			if [[ 0 -eq $( unstarch --elements "$outfile" ) ]]
			then
				Misc__verbose "No contig holding an ocean on $chr in $cul."
				rm -f "$outfile"
			fi
		done
	}&
}

function merge_scaff_oceans ()
{
	for chr in $( unstarch --list-chr $OCEAN_FILE )
	do
		starchcat $chr/*.scaff_ocean.starch \
			> $chr.scaff_ocean.starch

		rm -rf "$chr"
	done

	starchcat *.scaff_ocean.starch \
		> scaff_ocean.starch
	rm -f *.scaff_ocean.starch
}

# ---------------------------------------------------------
function main ()
{
	#~ printf "1- Oceans covered by contigs.\n"
	#~ fill_oceans_covered_by_contigs

	printf "2- Oceans covered by scaffolds.\n"
	fill_oceans_covered_by_scaffolds
}

# =========================================================
manageOptions "$@"

if [[ 1 -eq $VERBOSE ]]
then
	printVars >&2
fi

# your work.
main

printf "End of %s.\n\n" $( basename $0 ) >&2
exit 0
