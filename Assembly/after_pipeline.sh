#! /bin/bash

## AIMS  : Purge directory after the run of the pipeline.
## USAGE : bash after_pipeline.sh CULTIVAR $PATH
## NOTE  : fasta file will be bgzip, so source samtools/htslib.
## NOTE  : NG50 relies on python lib -> source python ;)
## AUTHORS : sletort@irisa.fr

set -o nounset
set -o errexit

declare BNAME=$( dirname $( readlink -e $0 ) )
source $BNAME/../lib/lib_misc.sh

# =========================================================
declare -r CULTIVAR="${1?What is your cultivar ?}"
declare -r DIR_PATH="${2?Where are the files ?}"
declare -r GENOME_SIZE="${3?Estimated genome size ?}"

declare -r ZIP=$HOME/logiciels/pigz-2.3.3/pigz

# ---------------------------------------------------------

# =========================================================
function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT     = $0\n"
	printf "╟ - CULTIVAR   = $CULTIVAR\n"
	printf "╟ - DIR_PATH   = $DIR_PATH\n"
	printf "╟ - GENOME_SIZE= $GENOME_SIZE\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

# =========================================================
function cleaning ()
{
	printf "removing files.\n"
	rm -rf matplotlib-* ${CULTIVAR}.lib_0.bam.done ${CULTIVAR}.lib_0 ${CULTIVAR}.fasta

	printf "zipping files.\n"

	# agp & gff files
	$ZIP ${CULTIVAR}_besst/BESST_output/pass1/*.???

	# repeats & Statistics files
	$ZIP ${CULTIVAR}_besst/BESST_output/*.???

	# h5 file
	$ZIP ${CULTIVAR}_k101.h5

	# quality files
	$ZIP *.seq_len.tsv

	# all fasta files will be zipped with bgzip
	for f in ${CULTIVAR}_besst/BESST_output/pass1/*.fa \
				${CULTIVAR}_k101.contigs.fa ${CULTIVAR}_k101.unitigs.fa
	do
		bgzip $f &
	done
	wait

	printf "linking the assembly file.\n"
	ln -s ${CULTIVAR}_besst/BESST_output/pass1/Scaffolds_pass1.fa.gz ${CULTIVAR}.fa.gz

}

function show_times ()
{
	local perle="print if( /GATB-pipeline starting/ || /Finished Multi-k GATB-Pipeline at k=101/"
	perle=$perle" || /Execution of 'python BESST\/runBESST\'/ || /pipeline finished/ )"

	perl -lne "$perle" $CULTIVAR.log

}

function genere_quality_files ()
{
	$BNAME/NG50.sh ${CULTIVAR}.fasta $GENOME_SIZE \
		> ${CULTIVAR}.NG50.tsv
	mv ????.seq_len.tsv scaff.seq_len.tsv

	$BNAME/NG50.sh ${CULTIVAR}_k101.contigs.fa $GENOME_SIZE \
		> ctg.NG50.tsv
	mv ????.seq_len.tsv ctg.seq_len.tsv

	wc -l *.seq_len.tsv > tmp.nb_seq
}

function main ()
{
	# stop script if bgzip does not exist
	which bgzip > /dev/null
	cd $DIR_PATH

	show_times
	genere_quality_files
	cleaning
}

# =========================================================
printVars >&2

# your work.
main

printf "End of %s.\n\n" $( basename $0 )

exit 0
