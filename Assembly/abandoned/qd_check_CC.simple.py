#!/usr/bin/env python
# -*- coding: utf-8 -*-

## AIMS  : determinate Region coordinates that are likely to be shared by cultivars and not ref. Anchored on ref.
## USAGE : qd_check_CC.simple.py align_files align_ref
##       : 	align_files is the samples/?_nodes/CC_*.mblast.tsv
##       : 	align_ref is the samples/?_nodes/CC_*.align_ref.tsv
## AUTHORS : sebastien.letort@irisa.fr


import sys
import pdb	# debug
import pprint # debug
import argparse	# option management, close to C++/boost library (?)
import os	# for local libraries
import errno
import csv	# manage tsv files also.
import networkx as nx
import matplotlib.pyplot as plt
from collections import defaultdict

# local libraries
f_path = os.path.dirname( os.path.realpath( __file__ ) )
sys.path.append( f_path + "/../../lib" )
import RegionLink as rl

VERSION = "1.0"
PP = pprint.PrettyPrinter(indent=4)
VERBOSE   = False

# ========================================
class BlastResult( rl.RegionLink ):
	def load_mblast_file( cls, o_file ):
		'''Return a list of BlastResult. (based on RegionLink.loadSimpleLinkFile)'''
		o_parser = csv.reader( o_file, delimiter="\t" )

		h_scaffs = {}
		lo_br = []
		for a_elts in o_parser:
			o_ref  = rl.Region( *a_elts[0:3] )
			o_tgt  = rl.Region( *a_elts[3:6] )

			if( o_ref.reversed == o_tgt.reversed ):
				strand = 1
			else:
				strand = -1

			o_br = BlastResult( o_ref, o_tgt, strand )
			lo_br.append( o_br )

			for t_ in [ (o_ref,7), (o_tgt,8) ]:
				if( t_[0].chrom not in h_scaffs ):
					h_scaffs[t_[0].chrom] = int( a_elts[t_[1]] )

		return lo_br,h_scaffs
	load_mblast_file = classmethod( load_mblast_file )

class InfilesLoader( object ):
	def __init__( self, lo_br, d_scaffs, do_link_ref, do_link_TE ):
		self.__lo_mblast = lo_br
		self.__d_scaffs  = d_scaffs

		# dictionnary of o_link objects
		self.__do_link_ref = do_link_ref
		self.__do_link_TE  = do_link_TE

	@property
	def lo_mblast( self ):
		return self.__lo_mblast

	@property
	def d_scaffs( self ):
		return self.__d_scaffs

	@property
	def do_link_ref( self ):
		return self.__do_link_ref

	@property
	def do_link_TE( self ):
		return self.__do_link_TE

	def load( cls, o_mblast_file, o_align_ref_file, o_TE_file ):

		# --- file1
		( lo_br, h_scaffs ) = BlastResult.load_mblast_file( o_args.align_file )
		verbose( str( len( lo_br ) ) + " links have been loaded from mblast file." )

		# --- file2
		of_align = csv.reader( o_align_ref_file, delimiter='\t' )
		lo_links = rl.RegionLink.loadSimpleLinkFile( of_align )

		# in those links ref is the true Reference sequence
		do_links = defaultdict( list )
		for o_link in lo_links:
			do_links[ o_link.o_tgt.chrom ].append( o_link )

		# --- file3
		do_te = None
		if( o_TE_file ):
			do_te = defaultdict( list )
			of_te = csv.reader( o_TE_file, delimiter='\t' )
			for a_elts in of_te:
				if( a_elts[0].startswith( '#' ) ):
					continue
				do_te[a_elts[0]].append( rl.Region( a_elts[0],a_elts[3],a_elts[4] ) )

		return InfilesLoader( lo_br, h_scaffs, do_links, do_te )
	load = classmethod( load )

class Node( object ):
	def __init__( self, o_region ):
		self.__lo_regions = [ o_region ]

	def append( self, x ):
		self.__lo_regions.append( x )

	# some accessor to the first region in the node
	@property
	def chrom( self ):
		return self.__lo_regions[0].chrom
	@property
	def start( self ):
		return self.__lo_regions[0].start
	@property
	def end( self ):
		return self.__lo_regions[0].end
	#~ @property
	def length( self ):
		return self.end - self.start + 1
	@property
	def reversed( self ):
		return self.__lo_regions[0].reversed

	def __str__( self, short = False ):
		if short:
			return str( self.__lo_regions[0] )
		return "\n".join( [ str( x ) for x in self.__lo_regions ] )

	@property
	def regions( self ):
		return self.__lo_regions

	def intersect( self, o_node ):
		for o_reg1 in o_node.regions:
			for o_reg2 in self.regions:
				if( o_reg1.chrom == o_reg2.chrom ):
					return True
		return False

	def subRegion( self, start=None, end=None ):
		return self.__lo_regions[0].subRegion( start, end )

class RootRef( object ):
	def __init__( self, o_root ):
		if( o_root ):
			nb_bases  = o_root.regions[0].length()
			nb_scaffs = len( o_root.regions ) - 1

			#~ PP.pprint( str( o_root.regions[0] ) )
			#~ PP.pprint( { "ref" : [ nb_scaffs, nb_bases ] } )
		else:
			nb_bases  = 0
			nb_scaffs = 0

		self.__o_root = o_root
		self.__bases  = nb_bases
		self.__scaffs = nb_scaffs

	@property
	def n_scaffs( self ):
		return self.__scaffs

	@property
	def root_node( self ):
		return self.__o_root

	def isBetterThan( self, o_root_ref ):
		if( self.n_scaffs < o_root_ref.n_scaffs ):
			return False
		return True

# ========================================
def main( o_args ):
	verbose( "En mode verbeux." )

	verbose( "Load infiles." )
	o_files = InfilesLoader.load( o_args.align_file, o_args.align_ref_file, o_args.te_gff )
	#~ PP.pprint( o_files.d_scaffs )

	# here a node is an exact region, same chrom/start/end
	dG_region = build_graph_from_refAlign( o_files.do_link_ref )
	verbose( "# edges : " + str( dG_region.number_of_edges() ) )
	verbose( "# nodes : " + str( dG_region.number_of_nodes() ) )
	#~ draw( dG_region )

	dG_scaffs = build_graph_from_mblast( o_files.lo_mblast )
	verbose( "# edges : " + str( dG_scaffs.number_of_edges() ) )
	verbose( "# nodes : " + str( dG_scaffs.number_of_nodes() ) )
	#~ draw( dG_scaffs )

	# work by CC
	G = nx.Graph( dG_region )
	n_cc = nx.number_connected_components( G )
	verbose( "G has {} connected components.".format( ( n_cc ) ) )


	o_best_ref = RootRef( None )
	dG_up_max = dG_down_max = None
	for l_nodes in nx.connected_components(G):
		verbose( "\n--CC--{}".format( [ str( x ) for x in l_nodes ] ) )

		if( len( l_nodes ) <= 2 ):
			verbose( "Not enough cultivars share the region." )
			continue

		( o_root, dG_up, dG_down ) = study_CC( dG_region, dG_scaffs, o_files.d_scaffs, l_nodes )
		draw( dG_up )
		draw( dG_down )

		o_root_ref = RootRef( o_root )
		if( o_root_ref.isBetterThan( o_best_ref ) ):
			o_best_ref  = o_root_ref
			dG_up_max   = dG_up
			dG_down_max = dG_down
	#for

	# warning : best ref does not mean that up/down have the max shared seqs
	#	but I think the most revelant still.
	#~ describe_tree( dG_up_max, o_best_ref.root_node, 'up' )
	#~ describe_tree( dG_down_max, o_best_ref.root_node, 'down' )

	#~ draw( dG_up_max )
	draw( dG_down_max )

	#~ l_out_up = extract_seqs_up( dG_up_max, o_best_ref.root_node )
	#~ for x in l_out_up:
		#~ print( x.recString() )
	l_out_down = extract_seqs_down( dG_down_max, o_best_ref.root_node )
	for x in l_out_down:
		print( x.recString() )

#end main

def X__build_groups( l_regions ):
	lt_groups = []
	i = 0
	while( True ):
		min_len = l_regions[i].length()
		max_len = max( min_len+200, 1.1*min_len )

		j=i+1
		for j in range( i+1,len( l_regions ) ):
			if( max_len < l_regions[j].length() ):
				if( j == i+1 ): # one member group = forbidden
					l = l_regions[j].length()
					max_len = max( l+200, 1.1*l )
				else:
					break
		# for

		if( j == len( l_regions ) - 1 ):
			j += 1
		lt_groups.append( ( i, j, min_len ) )

		if( j == len( l_regions ) ):
			break
		i = j
	# while

	return lt_groups
# __build_groups

def __build_groups( l_regions ):
	from itertools import islice
	from collections import defaultdict, deque

	def sliding( it, n ):
		it = iter( it )
		d = deque( islice( it, n-1 ), maxlen=n )
			
		for item in it:
			d.append( item )
			yield tuple( d )
	# sliding

	def groupons( l ):
		first_val = l[0].length()
		thres = max( first_val*1.1, first_val+200 )

		lt_ = defaultdict( list )
		for x in sliding( l, 2 ):
			lt_[first_val].append( x[0] )
			if thres < x[1].length():
				first_val = x[1].length()
				thres = max( first_val*1.1, first_val+200 )
		else:
			lt_[first_val].append( l[-1] )

		return lt_
	# groupons

	def crunch_single( lt_ ):
		l_k = sorted( lt_.keys(), reverse=True )
		reste = None
		for i in range( len( l_k ) ):
			if( reste ):
				lt_[l_k[i]] += reste
				reste = None
			if( 1 == len( lt_[l_k[i]] ) ):
				reste = lt_.pop( l_k[i] )
		else:
			if( 1 == len( lt_[l_k[-1]] ) ):
				lt_[l_k[0]] += lt_pop( l_k[1] )

		return
	# crunch single

	lt_ = groupons( l_regions )
	crunch_single( lt_ )
	return lt_
# __build_groups

def extract_seqs_up( o_tree, o_root ):
	'''Generate a bed file that contains regions to extract.'''

	ref_start = -1
	for t in o_tree.edges_iter( o_root, data='link' ):
		if( isinstance( t[2], dict ) ):
			# old version of networkx
			o_link = t[2]['link']
		else:
			o_link = t[2]

		if( ref_start < o_link.o_ref.start ):
			ref_start = o_link.o_ref.start
	ref_end = ref_start + 100 - 1
	l_out_regions = [ o_root.subRegion( ref_start, ref_end ) ]

	l_regions = []
	for t in o_tree.edges_iter( o_root, data='link' ):
		if( isinstance( t[2], dict ) ):
			# old version of networkx
			o_link = t[2]['link']
		else:
			o_link = t[2]

		delta_ref = ref_start - o_link.o_ref.start
		o_region = t[1].subRegion( end = t[1].end+delta_ref )
		l_regions.append( o_region )

	# il faut déterminer les groupes avant de couper les start
	l_regions.sort( key=lambda x: x.length() )
	lt_groups = __build_groups( l_regions )

	'''
	for i,j,seq_len in lt_groups:
		for o_region in l_regions[i:j]:
	'''

	for seq_len,t in sorted( lt_groups.items(), key=lambda x:x[0] ):
		for o_region in t:
			# ----
			if( o_region.reversed ):
				o_region.end   = o_region.start + seq_len - 1
			else:
				o_region.start = o_region.end - seq_len + 1
	l_out_regions += l_regions

	return l_out_regions
# new_extract_seqs

def extract_seqs_down( o_tree, o_root ):
	'''Generate a bed file that contains regions to extract.'''

	# ----
	ref_end = -1
	for t in o_tree.edges_iter( o_root, data='link' ):
		if( isinstance( t[2], dict ) ):
			# old version of networkx
			o_link = t[2]['link']
		else:
			o_link = t[2]

		if( -1 == ref_end or o_link.o_ref.end < ref_end ):
			ref_end = o_link.o_ref.end
	ref_start = ref_end - 100 + 1
	# ----
	l_out_regions = [ o_root.subRegion( ref_start, ref_end ) ]
	PP.pprint( str( o_root ) )

	l_regions = []
	for t in o_tree.edges_iter( o_root, data='link' ):
		if( isinstance( t[2], dict ) ):
			# old version of networkx
			o_link = t[2]['link']
		else:
			o_link = t[2]

	# ----
		delta_ref = ref_end - o_link.o_ref.end
		o_region  = t[1].subRegion( start = t[1].start-delta_ref )
	# ----
		l_regions.append( o_region )

	# il faut déterminer les groupes avant de couper les end
	l_regions.sort( key=lambda x: x.length() )
	lt_groups = __build_groups( l_regions )

	'''
	for i,j,seq_len in lt_groups:
		for o_region in l_regions[i:j]:
	'''
	for seq_len,t in sorted( lt_groups.items(), key=lambda x:x[0] ):
		for o_region in t:
			# ----
			if( o_region.reversed ):
				o_region.start = o_region.end - seq_len + 1
			else:
				o_region.end   = o_region.start + seq_len - 1
	l_out_regions += l_regions

	return l_out_regions
# extract_seqs_down


def describe_tree( o_tree, o_root, key ):
	
	draw( o_tree )
	nb_bases  = 0
	nb_scaffs = 0
	for o_node in o_tree.successors_iter( o_root ):
		nb_bases  += o_node.length()
		nb_scaffs += 1
	PP.pprint( { key : [ nb_scaffs, nb_bases ] } )
	

def study_CC( dG_ref, dG_scaffs, d_scaffs, l_nodes ):
	
	dG_regions_up   = nx.DiGraph()
	dG_regions_down = nx.DiGraph()
	o_root = None

	for o_ref,o_tgt, o_link in dG_ref.edges_iter( l_nodes, data='link' ):
		verbose( "* integrate {}".format( str( o_tgt ) ) )
		# PP.pprint( str( o_link ) )
		if( isinstance( o_link, dict ) ):
			# old version of networkx
			o_link = o_link['link']

		if( not o_root ):
			o_root = Node( o_ref )

		if( 0 == len( dG_regions_up ) ):
			verbose( "\tInitiate tree with ref.\n" )
			dG_regions_up.add_node( o_root )
			dG_regions_down.add_node( o_root )
			#~ draw( dG_regions_up )

		o_root.append( o_link.o_tgt )
		( o_up, o_down ) = get_amont_aval( o_link.o_tgt, d_scaffs )
		verbose( "\t" + str( { "amont": str( o_up ) } ) )
		verbose( "\t" + str( { "aval": str( o_down ) } ) )


		if( o_up ):
			dG_regions_up.add_path([ o_root, o_up ], link=o_link )

		if( o_down ):
			dG_regions_down.add_path([ o_root, o_down ], link=o_link )

		verbose( "\n" )

	return o_root, dG_regions_up, dG_regions_down


def get_amont_aval( o_scaff, d_scaffs ):

	if( 1 == o_scaff.start ):
		o_amont = None
	else:
		o_amont = o_scaff.subRegion( 1, o_scaff.start - 1 )

	if( d_scaffs[o_scaff.chrom] == o_scaff.end ):
		o_aval  = None
	else:
		o_aval  = o_scaff.subRegion( o_scaff.end + 1, d_scaffs[o_scaff.chrom] )

	if( o_scaff.reversed ):
		return [ o_aval, o_amont ]

	return [ o_amont, o_aval ]


def draw( o_graph ):

	if( not o_graph ):
		sys.stderr.write( "Empty graph." )
		return

	nx.draw_networkx( o_graph )
	plt.show() # ne sais pas pk, peut pas être sauvé et vu en même temps.
	plt.close()

	#~ nx.write_graphml( o_graph, "tmp.graphml" )

def build_graph_from_mblast( lo_br ):
	'''lo_br is a list of BlastResult object.'''

	G = nx.Graph()
	for o_br in lo_br:
		data = G.get_edge_data( o_br.o_ref.chrom, o_br.o_tgt.chrom )
		if( data and ( data['link'].revert() != o_br ) ):
			PP.pprint({ "already_in": str( data['link'] ) })
			PP.pprint({ "new_link"  : str( o_br ) })
			err_msg = "There are reciproqual edges, with different links.\n"
			raise Exception( err_msg )

		G.add_path( [ o_br.o_ref.chrom, o_br.o_tgt.chrom ], link=o_br )

	return G

def build_graph_from_refAlign( do_links ):
	'''do_links is a dictionnary of RegionLink object.'''

	def __get_best_ref( lo_regions, o_ref ):
		for o_reg in lo_regions:
			if( o_ref.intersect( o_reg ) ):
				return o_reg

	# compute contig-ref regions
	lo_ref_regions = []
	for lo_links in do_links.values():
		lo_ref_regions += [ x.o_ref for x in lo_links ]
	lo_ref_regions.sort( key = lambda x: ( x.chrom, x.start, x.end ) )

	i = 0
	while( i < len( lo_ref_regions )-1 ):
		if( lo_ref_regions[i].intersect( lo_ref_regions[i+1] ) ):
			o_region = rl.Region.merge([ lo_ref_regions[i], lo_ref_regions[i+1] ])[0]
			del lo_ref_regions[i:i+2]
			lo_ref_regions.insert( i, o_region )
		else:
			i += 1
	
	# now build the graph, ref will be one the contig-ref region previously computed.
	G = nx.DiGraph()
	for lo_links in do_links.values():
		for o_link in lo_links:

			o_ref = __get_best_ref( lo_ref_regions, o_link.o_ref )
			G.add_path( [ o_ref, o_link.o_tgt ], link=o_link )

	return G

# ----------------------------------------
def defineOptions():
	o_parser = argparse.ArgumentParser( description='Prog descr.' )

	# special
	o_parser.add_argument( '--version', action='version', version='%(prog)s ' + VERSION )

	# positionnal / mandatory param
	#	type = int, file
	o_parser.add_argument( 'align_file', nargs='?',
	                   type=file,
		               help='a tsv file with at least 7 cols (chr,s,e)x2,sign. default stdin.' )

	o_parser.add_argument( 'align_ref_file', nargs='?',
	                   type=file,
		               help='a tsv file with at least 7 cols (chr,s,e)x2,sign. default stdin.' )

	# optionnal
	o_parser.add_argument( '--verbose', '-v', action='store_true',
		               help='If set give info on what happen.' )

	o_parser.add_argument( '--te_gff', type=file,
		               help='a gff file with transposable element.' )
	#~ o_parser.add_argument( '-f', dest='opt1',
							#~ default='toto',
							#~ help='a flag with a value.' )

	#~ # mandatory option
	#~ #	destination is opt2 by default
	#~ o_parser.add_argument( '--opt2', '-o',
						#~ action='store_true', # store the value as boolean
						#~ help='a flag opt, long and short.')

	return o_parser

def analyseOptions( o_args ):

	global VERBOSE
	if( o_args.verbose ):
		VERBOSE = True


def verbose( msg ):
	if( VERBOSE ):
		sys.stderr.write( msg + "\n" )

# ----------------------------------------

if __name__ == '__main__':
	o_parser = defineOptions()
	o_args   = o_parser.parse_args()
	analyseOptions( o_args )
	#~ PP.pprint( o_args )

	# I lose stack and line number when try/except
	main( o_args )
	#~ try:
		#~ main( o_args )
	#~ except Exception as e:
		#~ sys.stderr.write( str( e ) )

	sys.exit( 0 )
