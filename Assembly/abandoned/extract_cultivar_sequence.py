#!/usr/bin/env python
# -*- coding: utf-8 -*-

## AIMS  : extract a seq or a sub-sequence of a cultivar assembly and output as fasta file.
## USAGE : extract_cultivar_sequence.py cul seq [start=0] [end=seq_len]
## NOTE  : the first 2 bases are obtained with start=0 and end=2
## NOTE  : to have the reversed complemented strand, use end < start
## AUTHORS : sebastien.letort@irisa.fr


import sys
import pdb	# debug
import pprint # debug
import argparse	# option management, close to C++/boost library (?)
import os	# for local libraries
import csv	# manage tsv files also.
import pysam


# local libraries
f_path = os.path.dirname( os.path.realpath( __file__ ) )
sys.path.append( f_path + "/../lib" )

VERSION = "1.0"
PP = pprint.PrettyPrinter(indent=4)
VERBOSE   = False

# ========================================
def main( o_args ):
	ass_path = get_assembly_filepath( o_args.cultivar )
	verbose( "the assembly filepath {}".format( ass_path ) )


	if( o_args.end < o_args.start ):
		( start, end ) = map( int, [ o_args.end, o_args.start ] )
		reverse = True
	else:
		( start, end ) = map( int, [ o_args.start, o_args.end ] )
		reverse = False

	x= map( str, [start,end,reverse] )
	verbose( "start={0} end={1} strand={2}".format( *x ) )

	"""
	with pysam.FastaFile( fasta_file ) as fe:
		seq_str = fe.fetch( seq, start, end )
		o_seq   = Seq( seq_str )
		if( reverse ):
			o_seq = o_seq.reverse_complement()

		new_id = "|".join([ seq_id, str(start)+"-"+str(end), str( reverse ) ])
		fs.write( ">" + new_id + "\n" + str( o_seq ) + "\n" )
	"""

	return 0

def get_assembly_filepath( cultivar ):
	print( cultivar )

# ----------------------------------------
def defineOptions():
	descr='extract a seq or a sub-sequence of a Rapsodyn cultivar assembly.'
	o_parser = argparse.ArgumentParser( description=descr )

	# special
	o_parser.add_argument( '--version', action='version', version='%(prog)s ' + VERSION )

	# positionnal / mandatory param
	#	type = int, file
	o_parser.add_argument( 'cultivar', type=str,
		               help='the cultivar name.' )
	o_parser.add_argument( 'scaff_id', type=str,
		               help='the sequence name (=scaffold id).' )

	# optionnal
	o_parser.add_argument( '--verbose','-v', action='store_true',
		               help='If set give info on what happen.' )
	o_parser.add_argument( '--start','-s', type=int,
							default=0,
							help='the first base to extract (0-based).' )
	o_parser.add_argument( '--end','-e', type=int,
							default=0,
							help='the last base to extract (1-based).' )

	return o_parser

def analyseOptions( o_args ):

	global VERBOSE
	if( o_args.verbose ):
		VERBOSE = True


def verbose( msg ):
	if( VERBOSE ):
		sys.stderr.write( msg + "\n" )

# ----------------------------------------

if __name__ == '__main__':
	o_parser = defineOptions()
	o_args   = o_parser.parse_args()
	analyseOptions( o_args )
	PP.pprint( o_args )

	main( o_args )

	sys.exit( 0 )

