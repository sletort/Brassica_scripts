#! /bin/bash

for chr in $( unstarch --list-chr all_ref_align.starch )
do
{
	unstarch $chr all_ref_align.starch > $chr.tsv
	./check_partially_unaligned_seqs.py $chr.tsv all_scaff_len.tsv 2> $chr.err
	rm -f $chr.tsv
}&
done
