#! /bin/bash

threshold=0.1
perl -lane 'push( @a_ids, $F[0] ); $h_{ $F[0] } = [ $F[0] ]; for $i ( 2..$.-1 ) { if( $F[$i] < '$threshold' ) { $k = $a_ids[$i-1]; push( @{ $h_{$k} }, $F[0] ); } } END{ for $rl ( values %h_ ) { print join( "\t", $a_ids[0], map { @a_elts = split /\|/; $a_elts[0] } @$rl ) if( 1 < @$rl ); } }' $1
