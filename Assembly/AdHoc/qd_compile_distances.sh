#! /bin/sh

chrom="${1?Your chrom ?}"

perle='($cul,$id, @x) = split( /\|/,$F[0] ); push( @a_cul, $cul );'
perle=$perle' for $i (1..$.-1){ print join( "\t", $a_cul[$.-1], $a_cul[$i-1], $F[$i] ); } '

for f in bed_files/$chrom/${chrom}_*.fa.gz.dist_mat.tsv
do
	perl -lane "$perle" $f
done > all_distances.tsv

perl -lane '
	map{ $h_culs{$_}= 1; } @F[0,1];
	push( @{ $h_{$F[0]}{$F[1]} }, $F[2] );
	END{
		@a_culs = sort keys %h_culs;
		for $i (0..$#a_culs-1){
			$cul1 = $a_culs[$i];
			for $j ($i+1..$#a_culs){
				$cul2 = $a_culs[$j];
				$ra_1 = $h_{$cul1}{$cul2};
				$ra_2 = $h_{$cul2}{$cul1};
				@a_vals = ( @$ra_1, @$ra_2 );
				$n = @a_vals;
				printf( "%s:%s\t%d\t", $cul1, $cul2, $n );
				if( 0 == $n ){
					printf( "NA\n" );
				}
				else{
					$sum=0;
					map{ $sum += $_ } @a_vals;
					$moy = $sum/$n;
					printf( "%.4f\n", $moy );
				}
			}
		}
	}' all_distances.tsv \
	> matrices.sum_up.tsv

