#! /bin/bash

## AIMS  : Build p/a matrices about QTL for the latest reference.
## USAGE : ./qd_build_matrices_pa.sh
## NOTE  : 1 presence, -1 abcense, 0 no data
## NOTE  : This is just the script of what have been ran. -> Makefile ?
#
## OUTPUT : generate several files

set -o nounset
set -o errexit

declare -r cwd=$( dirname $( readlink -e $0 ) )
source "$cwd/lib.analyse_QTL.sh"

# =========================================================

# ---------------------------------------------------------
# No TE yet
# declare -r TE_GFF=$( jq_extract_from_json '.genomes.bnapus."v4.1".annot.te' )
declare GENE_GFF=$( get_ref_annot bnapus genes )
declare -r DATA_DIR="/omaha-beach/sletort/assemblage/New.Finishing"
declare -r ALIGN_DIR="$DATA_DIR/AlignOnRef"
declare -r QTL_FILE="$DATA_DIR/QTL.bed"

# =========================================================
function rename_matrices ()
{
	local -r suffix="$1"

	for qtl_id in "${!A_QTLs[@]}"
	do
		mv "$qtl_id.matrix.tsv" "$qtl_id.${suffix}.matrix.tsv"
		mv "$qtl_id.elt.bed" "$qtl_id.$suffix.bed"
	done
}

function main ()
{
	#~ printf "Build TE matrices.\n"
	#~ $__ROOT/Assembly/build_matrix_pa.sh \
			#~ -b "$QTL_FILE" "$TE_GFF" "$ALIGN_DIR"
	#~ rename_matrices "TE"
	#~ mv elts.starch TE.starch

	printf "Build Gene matrices.\n"
	printf "Annotation file contains all gene's structure, I only consider gene.\n"
	printf "\tI have to exclude some lines.\n"
	local -r gene_gff="genes.gff"
	#~ perl -lane 'print if( $F[2] eq "gene" );' "$GENE_GFF" \
		#~ > $gene_gff
	$__ROOT/Assembly/build_matrix_pa.sh \
			-b "$QTL_FILE" "$gene_gff" "$ALIGN_DIR"
	rename_matrices "Gene"
	mv elts.starch Gene.starch
}

# =========================================================
# your work.
main

printf "End of %s.\n\n" $( basename $0 ) >&2
exit 0
