#!/usr/bin/env python
# -*- coding: utf-8 -*-

## AIMS  : .
## USAGE : X.py [--chr X] [--bed bed_file] fasta_file
## NOTE  : 
## AUTHORS : sebastien.letort@irisa.fr


import sys
import pdb	# debug
import pprint # debug
import argparse	# option management, close to C++/boost library (?)
import os	# for local libraries
import csv	# manage tsv files also.
import pysam
#~ from collections import defaultdict
from collections import Counter


# local libraries
f_path = os.path.dirname( os.path.realpath( __file__ ) )
sys.path.append( f_path + "/../lib" )

VERSION = "0.1.0"
PP = pprint.PrettyPrinter(indent=4)
VERBOSE   = False

# ========================================
class Region( object ):
	def __init__( self, chrom, start, end ):
		self.__chrom = chrom
		self.__start = int( start )
		self.__end   = int( end )

	@property
	def start( self ):
		return self.__start
	@property
	def end( self ):
		return self.__end

	def samtools_string( self ):
		'''samtools string have start one based.
		
		samtools defines region 1-based, with end included in the interval.
		Pysam does not include then end.
		So to have the correct behaviour, I add 1 to start only.'''
		return "{0}:{1}:{2}".format( self.__chrom, 	str(self.__start+1), self.__end )

# ========================================
def action( seq_id, seq ):
	'''The action to perform on the wanted sequence.'''
	print( "{0} : {1}".format( seq_id, str( len( seq ) ) ) )
	print( seq )

def main( o_args ):

	verbose( "computing deltas.\n" )
	delta_start = o_args.o_ocean.start - o_args.o_ref.start
	delta_end   = o_args.o_ocean.end   - o_args.o_ref.start

	verbose( "extracting sequences.\n" )
	#~ d_oceans = defaultdict( list )
	d_oceans = Counter()
	with pysam.FastaFile( o_args.fasta_file ) as o_fe:
		verbose( "Working with file : " + o_fe.filename )

		N = o_fe.nreferences
		for seq_id in o_fe.references:
			seq = o_fe.fetch( reference = seq_id )
			ocean_seq = seq[delta_start:delta_end+1]
			d_oceans[ocean_seq] += 1

	# make consensus
	if( True == o_args.consensus ):
		l_consensus = None
		for k in d_oceans:
			if( None == l_consensus ):
				l_consensus = list( k )
			else:
				for i in range( 0, len( l_consensus ) ):
					if( l_consensus[i] != k[i] ):
						l_consensus[i] = 'N'
		l_elts = [ '#', str( N ), "".join( l_consensus ) ]
		o_args.outfile.write( "\t".join( l_elts ) + "\n" )

	# print out
	for k,c in sorted( d_oceans.iteritems(), key=lambda x: x[1] ):
		pc = (0.0+c)/N
		l_elts = [ str( pc ), str( c ), k ]
		o_args.outfile.write( "\t".join( l_elts ) + "\n" )

	return

# ----------------------------------------
def defineOptions():
	o_parser = argparse.ArgumentParser( description='Prog descr.' )

	# special
	o_parser.add_argument( '--version', action='version', version='%(prog)s ' + VERSION )

	# positionnal / mandatory param
	#	type = int, file
	o_parser.add_argument( 'fasta_file',
		               help='The fasta filename to parse, can be bgzipped.' )

	o_parser.add_argument( 'region_file',
							type=file,
							help='The tsv filename with ocean and extracted region (representing the fasta file).' )

	# optionnal
	o_parser.add_argument( '--verbose', '-v', action='store_true',
		               help='If set give info on what happen.' )

	o_parser.add_argument( '--outfile', '-o',
						type=argparse.FileType( 'w' ),
						default=sys.stdout,
		               help='If set give info on what happen.' )

	o_parser.add_argument( '--consensus', '-c',
						action='store_true',
		               help='If set will generate a consensus from all sequences.' )

	return o_parser

def analyseOptions( o_args ):

	global VERBOSE
	if( o_args.verbose ):
		VERBOSE = True

	if( o_args.region_file ):
		o_file = csv.reader( o_args.region_file, delimiter='\t' )
		for l_row in o_file:
			o_ocean = Region( *l_row[0:3] )
			o_ref   = Region( l_row[0], *l_row[3:5] )

		o_args.o_ocean = o_ocean
		o_args.o_ref   = o_ref

		o_args.region_file.close()


def verbose( msg ):
	if( VERBOSE ):
		sys.stderr.write( msg + "\n" )

# ----------------------------------------

if __name__ == '__main__':
	o_parser = defineOptions()
	o_args   = o_parser.parse_args()
	analyseOptions( o_args )
	# PP.pprint( o_args )

	main( o_args )

	sys.exit( 0 )

