#!/usr/bin/env python
# -*- coding: utf-8 -*-

## AIMS  : extract unaligned sequence of scaffolds, by batch sharing the same potential ref seq.
## USAGE : check_partially_unaligned_seqs.py "$align_file" $ALL_SCAFF_LEN [--debug] [--verbose] [--outdir $outdir]
## AUTHORS : sebastien.letort@irisa.fr


import sys
import pprint # debug
import argparse	# option management, close to C++/boost library (?)
import os	# for local libraries
import csv	# manage tsv files also.
from collections import defaultdict
import copy
import re

# local libraries
f_path = os.path.dirname( os.path.realpath( __file__ ) )
sys.path.append( f_path + "/../../lib" )

import RegionLink as rl
import FeatureOutputWriter as fow

VERSION = "1.0"
PP = pprint.PrettyPrinter( indent=4, stream=sys.stderr )
VERBOSE = False
DEBUG   = False

MIN_UNALIGNED_LEN = 500
ANCHOR_SIZE       = 100
MAX_ROI_LEN       = 5000 # Region of interest max size

ALIGN_DBG_GFF   = "align.dbg.gff"
UNALIGN_DBG_GFF = "unalign.dbg.gff"
ROI_DBG_GFF     = "roi.dbg.gff"
SOI_DBG_GFF     = "soi.dbg.gff"

# ========================================
class SequenceOfInterestBuilder( object ):
	MIN_UNALIGNED_LEN = MIN_UNALIGNED_LEN

	def __init__( self, lo_links ):

		'''
		def __getBounds( lo_links ):
			s = set()
			for o_link in lo_links:
				s.update( o_link.o_ref.feature() )
			return sorted( list( s ) )
		# __setBounds
		'''

		#sys.stderr.write( str( type( self ) ) + "\n" )
		self.__lo_links = lo_links
		#~ self.__l_bounds = __getBounds( self.__lo_links )

		self.__o_ref     = None
		self.__o_infiles = None
		#for o_ in self.__lo_links:
		#	print "===\t" + o_.recString()

	@property
	def lo_links( self ):
		return self.__lo_links
	@property
	def g_start( self ):
		return self.__o_ref.start
	@property
	def g_end( self ):
		return self.__o_ref.end
	@property
	def o_infiles( self ):
		return self.__o_infiles
	@o_infiles.setter
	def o_infiles( self, o_infiles ):
		self.__o_infiles = o_infiles

	def ref_region( self ):
		return self.__o_ref

	def __getLinksIncluded( self, ref_start,ref_end ):
		PP.pprint([ ref_start, ref_end ])
		lo_links = []
		for o_link in self.lo_links:
			if( o_link.o_ref.end <= ref_start
				or ref_end <= o_link.o_ref.start ):
				continue

			lo_links.append( o_link )

		# could not be empty
		return lo_links
	# __getLinks

	def __build_partition( self ):
		o_link1 = self.lo_links[0]
		dlo_links = { o_link1.o_ref: [ o_link1 ] }

		dlo_stored_links = {}
		# lo_links are sorted on ref coords
		for o_link in self.lo_links[1:]:
			print( o_link )
			t_items = dlo_links.items()
			for o_r,lo_links in t_items:
				print( "\t" + str( o_r ) )
				if( o_r.end < o_link.o_ref.start ):
					dlo_stored_links[o_r] = lo_links
					del dlo_links[o_r]
					dlo_links[o_link.o_ref] = [ o_link ]
				else: # overlapping => make 3 parts
					del dlo_links[o_r]

					if( o_r.start < o_link.o_ref.start ):
						o_new = rl.Region( o_r.seq_id, o_r.start, o_link.o_ref.start-1 )
						dlo_links[o_new] = lo_links
					# no else <= sorted on ref

					if( o_r.end < o_link.o_ref.end ):
						middle_end = o_r.end
						o_new = rl.Region( o_r.seq_id, middle_end+1, o_link.o_ref.end )
						dlo_links[o_new] = [ o_link ]
					else:
						middle_end = o_link.o_ref.end
						o_new = rl.Region( o_r.seq_id, middle_end+1, o_r.end )
						dlo_links[o_new] = lo_links
					
					o_new = rl.Region( o_r.seq_id, o_link.o_ref.start, middle_end )
					dlo_links[o_new] = lo_links + [ o_link ]

		for k,l in dlo_links.items():
			dlo_stored_links[k] = l
			#~ print( k )
			#~ for v in l:
				#~ print( "\t" + str( v ) )

		return dlo_links
	# __build_partition

	def __getSequenceOfInterest( cls, o_link, g_start, g_end ):
		delta_start = o_link.o_ref.start - g_start
		delta_end   = o_link.o_ref.end   - g_end

		if( not o_link.o_tgt.reversed ):
			start  = o_link.o_tgt.start - delta_start
			end    = o_link.o_tgt.end   - delta_end
		else:
			start  = o_link.o_tgt.end   + delta_start
			end    = o_link.o_tgt.start + delta_end

		# particular case
		if( start < 0 ):
			verbose( "Probable big ref deletion in target.\nWill calculate start pos from the end." )
			ref_len = g_end - g_start + 1
			if( not o_link.o_tgt.reversed ):
				start = end - ref_len
				if( start < o_link.o_tgt.start ):
					start = o_link.o_tgt.start
			else:
				start = end + ref_len
				if( o_link.o_tgt.end < start ):
					start = o_link.o_tgt.end

		if( end < 0 ):
			verbose( "Probable big ref deletion in target.\nWill calculate start pos from the end." )
			ref_len = g_end - g_start + 1
			if( not o_link.o_tgt.reversed ):
				end = start + ref_len
				if( o_link.o_tgt.end < end ):
					end = o_link.o_tgt.end
			else:
				end = start - ref_len
				if( end < o_link.o_tgt.start ):
					end = o_link.o_tgt.start

		o_region = rl.Region( o_link.o_tgt.seq_id, start, end )

		return o_region
	__getSequenceOfInterest = classmethod( __getSequenceOfInterest )
	# __getSequenceBounds


	def getTargetRegions_3( self ):
		'''Here i will not try to maximise the size of regions.
		***ABANDON: __build_partition is too slow***'''
		verbose( "---------getTargetRegions _simple------" )

		dlo_links = self.__build_partition()

		# I'll just eliminate small ones

		for o_ref,lo_links in dlo_links.items():
			verbose( str( o_ref ) )
			if( o_ref.length() < MIN_UNALIGNED_LEN ):
				verbose( str( o_ref ) + "\ttoo small." )
				continue
			if( len( lo_links ) < 2 ):
				verbose( str( o_ref ) + "\tnot enough links." )
				continue

			self.__o_ref = o_ref # to allow region access when receiving the yielded list

			lo_soi = []
			for o_link in lo_links:
				o_soi = __getSequenceOfInterest( o_link, o_ref.start, o_ref.end )
				debug( "add {}".format( str( o_soi ) ) )
				lo_soi.append( o_soi )
			yield lo_soi

	# getTargetRegions_3

	def getTargetRegions_2( self ):
		verbose( "---------getTargetRegions------" )

		ref_chrom = self.lo_links[0].o_ref.seq_id

		# ------------------------
		s_bounds = set()
		for o_link in self.lo_links:
			s_bounds.update( o_link.o_ref.feature() )
		l_bounds = sorted( s_bounds )

		dlo_links = {}
		for i in range( len( l_bounds ) - 1 ):
			start = l_bounds[i]
			end   = l_bounds[i+1]
			length= end - start

			if( length < MIN_UNALIGNED_LEN ):
				verbose( "[{0};{1}] too small.".format( str( start ), str( end ) ) )
				continue

			o_ref = rl.Region( ref_chrom, start, end )
			verbose( str( o_ref ) )
			lo_links = []
			for o_link in self.lo_links:
				if( o_link.o_ref.end <= o_ref.start ):
					continue
				if( o_ref.end <= o_link.o_ref.start ):
					break

				# link has a full seq on the region
				lo_links.append( o_link )
			if( 1 < len( lo_links ) ):
				dlo_links[ o_ref ] = lo_links
			else:
				verbose( "Not enough links in region." )
		# ------------------------
		
		for o_ref,lo_links in dlo_links.items():
			self.__o_ref = o_ref # to allow region access when receiving the yielded list
			debug( str( o_ref ) )

			lo_soi = []
			for o_link in lo_links:
				o_soi = self.__getSequenceOfInterest( o_link, o_ref.start, o_ref.end )
				debug( "add {}".format( str( o_soi ) ) )
				lo_soi.append( o_soi )
			yield lo_soi

		

	# getTargetRegions_2

	def getTargetRegions( self ):
		def __getSequenceOfInterest( o_link, g_start, g_end ):
			delta_start = o_link.o_ref.start - g_start
			delta_end   = o_link.o_ref.end   - g_end

			if( not o_link.o_tgt.reversed ):
				start  = o_link.o_tgt.start - delta_start
				end    = o_link.o_tgt.end   - delta_end
			else:
				start  = o_link.o_tgt.end   + delta_start
				end    = o_link.o_tgt.start + delta_end

			# particular case
			if( start < 0 ):
				verbose( "Probable big ref deletion in target.\nWill calculate start pos from the end." )
				ref_len = g_end - g_start + 1
				if( not o_link.o_tgt.reversed ):
					start = end - ref_len
					if( start < o_link.o_tgt.start ):
						start = o_link.o_tgt.start
				else:
					start = end + ref_len
					if( o_link.o_tgt.end < start ):
						start = o_link.o_tgt.end

			if( end < 0 ):
				verbose( "Probable big ref deletion in target.\nWill calculate start pos from the end." )
				ref_len = g_end - g_start + 1
				if( not o_link.o_tgt.reversed ):
					end = start + ref_len
					if( o_link.o_tgt.end < end ):
						end = o_link.o_tgt.end
				else:
					end = start - ref_len
					if( end < o_link.o_tgt.start ):
						end = o_link.o_tgt.start

			o_region = rl.Region( o_link.o_tgt.seq_id, start, end )

			#~ if( delta_start < MIN_UNALIGNED_LEN ):
				#~ next_turn = False
			#~ else:
				#~ next_turn = True
			if( 0 == delta_start ):
				next_turn = False
			else:
				next_turn = True

			return( o_region, next_turn )
		# __getSequenceBounds

		verbose( "---------getTargetRegions------" )
		last_end = 0
		while( 1 < len( self.lo_links ) ):
			g_end = min([ x.o_ref.end for x in self.lo_links ])

			if( g_end - last_end < MIN_UNALIGNED_LEN ):
				lo_local_links = []
			else:
				max_start      = g_end - MIN_UNALIGNED_LEN
				lo_local_links = list( self.__getLinksUpstream( max_start ) )
				last_end = g_end

			last_start = None
			while( 1 < len( lo_local_links ) ):
				#for x in lo_local_links:
				#	print( x )
				g_start = max([ x.o_ref.start for x in lo_local_links ])

				if( last_start and last_start - g_start < MIN_UNALIGNED_LEN ):
					for i in reversed( range( len( lo_local_links ) ) ):
						if( lo_local_links[i].o_ref.start >= g_start ):
							del lo_local_links[i]
					continue
				last_start = g_start


				o_region = self.lo_links[0].o_ref.subRegion( start = g_start, end = g_end )
				self.__o_ref = o_region # to allow region access when receiving the yielded list
				if( DEBUG ):
					sys.stderr.write( "\tnx groupe\n" )
					sys.stderr.write( "\tref : " + str( self.__o_ref ) + "\n" )
					sys.stderr.write( "\tnb links : " + str( len( lo_local_links ) ) + "\n" )

				lo_soi = []
				for i in reversed( range( len( lo_local_links ) ) ):
					( o_soi, next_turn ) = __getSequenceOfInterest( lo_local_links[i], g_start, g_end )
					debug( "add {}".format( str( o_soi ) ) )
					lo_soi.append( o_soi )
					if( not next_turn ):
						debug( "*discarded for next step." )
						del lo_local_links[i]

				if( 2 <= len( lo_soi ) ):
					yield lo_soi
				else:
					verbose( "\t*There are less than 2 scaffolds that share valuable sequences, region is skipped." )

			for i in reversed( range( len( self.lo_links ) ) ):
				if( self.lo_links[i].o_ref.end <= g_end ):
					del self.lo_links[i]
	# getTargetRegions
			

	def __getLinksUpstream( self, pos ):
		for o_link in self.lo_links:
			if( o_link.o_ref.start <= pos ):
				yield o_link
			else:
				break
	# __getLinksUpstream

class TargetLinks( rl.ChromLinks ): # I did not check that scaff align on the same chrom ...
	"""a targetlink is a list of RegionLink where ref.chrom are equal and tgt.chrom are equal.
	
	l
	"""
	def __init__( self, lo_links, scaff_id, s=None, e=None ):

		lo_links.sort( key=lambda x: x.o_ref )

		super( TargetLinks, self ).__init__( lo_links )
		self.__scaff_id  = scaff_id
		if( s ):
			self.__scaff_start = s
		else:
			self.__scaff_start = min( x.o_tgt.start for x in lo_links )

		if( e ):
			self.__scaff_end   = e
		else:
			self.__scaff_end   = max( x.o_tgt.end for x in lo_links )

	@property
	def scaff_id( self ): return self.__scaff_id

	@property
	def scaff_len( self ): return self.__scaff_end - self.__scaff_start + 1

	def __getitem__( self, i ):
		return self.lo_links[i]

	def negateMatches( self, anchor_size=0 ):
		'''unmatched region will become matches.

		Dubious gaps (very long one) will be discarded, cf __is_dubious_gap.
		... long method, do a negator object ?'''
		def __is_dubious_gap( o_link1, o_link2, scaff_len ):
			s = o_link2.o_ref.start
			e = o_link1.o_ref.end
			delta  = s - e
			# print( "{0} - {1} = {2}".format( str( s ), str( e ), str( delta ) ) )
			# distribution of final sequence shows that Q3 seq len ~8000 on chrom A10
			# scaff len max = 79783
			return ( delta > scaff_len
					and delta > 100000 )
		# __is_dubious_gap

		def __makeTwoLinksFromDubiousOne( o_dubious ):
			'''a dubious link appear when 2 successive aligned parts do not align closed on the ref.
			I'll build 2 new links with the same tgt region, but two different ref regions.'''

			offset = o_dubious.o_tgt.length() - 1

			# the 3' ref one
			o_new1 = copy.deepcopy( o_dubious )
			o_new1.o_ref.end = o_new1.o_ref.start + offset

			# the 5' ref one
			o_new2 = copy.deepcopy( o_dubious )
			o_new2.o_ref.start = o_new2.o_ref.end - offset

			return o_new1, o_new2
		# __makeTwoLinksFromDubiousOne

		def __links_overlap( o_link1, o_link2 ):
			if( o_link1.o_tgt.reversed ):
				if( o_link1.o_tgt.start <= o_link2.o_tgt.end ):
					return 1, o_link2.o_tgt.end - o_link1.o_tgt.start + 1
			else:
				if( o_link2.o_tgt.start <= o_link1.o_tgt.end ):
					return 1, o_link1.o_tgt.end - o_link2.o_tgt.start + 1

			if( o_link2.o_ref.start <= o_link1.o_ref.end ):
				return 2, o_link1.o_ref.end - o_link2.o_ref.start + 1

			return 0,None
		# __links_overlap

		def __writeDuplicationWarning( code, length, o_l1, o_l2 ):
			msg  = "Links are overlapping, no gap !"
			msg += "\n\t" + str( o_l1 )
			msg += "\n\t" + str( o_l2 )
			if( 1 == code ):
				msg += "\n\t" + "sequence duplication deleted on target ({} bases).".format( str( length ) )
			else:
				msg += "\n\t" + "sequence duplication on target ({} bases).".format( str( length ) )
			sys.stderr.write( msg + "\n" )
		# __writeDuplicationWarning

		# -------------------
		l_ = [] # will store lists

		# the ref 3'/upstream region
		lo_new_links = self.__beforeFirstLink( anchor_size )

		# the gaps between alignments
		for i in range( 1,len( self.lo_links ) ):
			( o_l1,o_l2 ) = ( self.lo_links[i-1],self.lo_links[i] )

			# we do not consider local reorientation.
			# all segments in a scaff have the same orientation.
			( code, length ) = __links_overlap( o_l1, o_l2 )
			if( 0 != code ):
				__writeDuplicationWarning( code, length, o_l1, o_l2 )
				continue

			# passer i-1 et i ?
			o_link = self.__build_gap_link( o_l1, o_l2, anchor_size )
			if( not o_link ):
				continue

			is_dubious = __is_dubious_gap( o_l1, o_l2, self.scaff_len )
			if( not is_dubious ):
				debug( "\tAdd link : " + str( o_link ) )
				lo_new_links.append( o_link )
			else:
				debug( "Dubious link." )
				( o_new1, o_new2 ) = __makeTwoLinksFromDubiousOne( o_link )
				debug( "\tAdd link : " + str( o_new1 ) )
				lo_new_links.append( o_new1 )
				verbose( "Add {} regions".format( str( len( lo_new_links ) ) ) )
				l_.append( lo_new_links )
				debug( "start a new 'scaff'." )
				debug( "\tAdd link : " + str( o_new2 ) )
				lo_new_links = [ o_new2 ]

		lo_new_links += self.__afterLastLink( anchor_size )   # the ref 5'/downstream region if exist

		verbose( "Add {} regions".format( str( len( lo_new_links ) ) ) )
		l_.append( lo_new_links )


		lo_out_links = []
		for i in range( 0, len( l_ ) ):
			new_id = '_'.join([ self.scaff_id, str(i+1) ])

			if( 0 == len( l_[i] ) ):
				sys.stderr.write( "\t* no unaligned part found, is it true ?\n" )
				sys.stderr.write( "\t* scaff_id : {0}\n".format( new_id ) )
			else:
				lo_out_links.append( TargetLinks( l_[i], new_id ) )

		return lo_out_links

# ------------------
	def __getExtremity( self, o_link, strand ):
		if( strand == o_link.strand ):
			bout     = 1
			tgt_bout = o_link.o_tgt.start
		else:
			bout     = self.scaff_len
			tgt_bout = o_link.o_tgt.end
			
		return bout, tgt_bout
	# __getExtremity

	def __beforeFirstLink( self, anchor_size ):
		"""Build the most ref-upstream (3') unaligned region, if it exists"""
		o_link = self.lo_links[0]
		debug( "First link :", str( o_link ) )

		( bout, tgt_bout ) = self.__getExtremity( o_link, 1 )
		# PP.pprint({ 'first': { 'end': tgt_bout, 'bout':bout } })

		if( tgt_bout == bout ):
			debug( "\tNo sequence left upstream." )
			return [] # First 

		# the new target region starts at the end of the scaff until the base preceding the first to align the ref.
		offset = o_link.strand * ( anchor_size - 1 )
		o_tgt = rl.Region( self.scaff_id, bout, tgt_bout+offset )
		if( o_tgt.length() < MIN_UNALIGNED_LEN ):
			msg = "\tTarget length is too small ({}).".format( str( o_tgt.length() ) )
			debug( msg )
			return []

		offset = ( anchor_size - 1 )
		ref_end   = o_link.o_ref.start + offset
		ref_start = ref_end - o_tgt.length() + 1
		o_ref     = o_link.o_ref.subRegion( ref_start, ref_end )

		o_link = rl.RegionLink( o_ref, o_tgt, o_link.strand )
		debug( "\tAdd link : " + str( o_link ) )

		return [ o_link ]
	# initiate

	def __afterLastLink( self, anchor_size ):
		"""Build the most ref-dowstream (5') unaligned region, if it exists"""
		o_link = self.lo_links[-1]
		debug( "Last link :", str( o_link ) )

		( bout, tgt_bout ) = self.__getExtremity( o_link, -1 )
		# PP.pprint({ 'ultimate': { 'end': tgt_bout, 'bout':bout } })
			
		if( tgt_bout == bout ):
			debug( "\tNo sequence left downstream." )
			return []

		# this last gap starts the first base after the last that aligns the ref, until the end of the scaff
		offset = o_link.strand * ( anchor_size - 1 )
		o_tgt = rl.Region( self.__scaff_id, tgt_bout-offset, bout )
		if( o_tgt.length() < MIN_UNALIGNED_LEN ):
			msg = "\tTarget length is too small ({}).".format( str( o_tgt.length() ) )
			debug( msg )
			return []

		ref_start = o_link.o_ref.end + 1
		ref_end   = ref_start + o_tgt.length()
		o_ref     = o_link.o_ref.subRegion( ref_start, ref_end )
		
		o_link = rl.RegionLink( o_ref, o_tgt, o_link.strand )
		debug( "\tAdd link : " + str( o_link ) )

		return [ o_link ]
	# terminate

	def __build_gap_link( self, o_link1, o_link2, anchor_size ):
		debug( "gap between:", str( o_link1 ), str( o_link2 ) )

		offset = anchor_size - 1
		# tgt region
		if( 1 == o_link2.strand ):
			tgt_start = o_link1.o_tgt.end   - offset
			tgt_end   = o_link2.o_tgt.start + offset
			o_tgt     = rl.Region( self.scaff_id, tgt_start, tgt_end )
		else:
			tgt_start = o_link1.o_tgt.start + offset
			tgt_end   = o_link2.o_tgt.end   - offset
			o_tgt     = rl.Region( self.scaff_id, tgt_start, tgt_end )

		if( o_tgt.length() < MIN_UNALIGNED_LEN ):
			msg = "\tTarget length is too small ({}).".format( str( o_tgt.length() ) )
			debug( msg )
			return None

		# ref region
		ref_start = o_link1.o_ref.end   - offset
		ref_end   = o_link2.o_ref.start + offset
		o_ref = o_link2.o_ref.subRegion( ref_start, ref_end )

		# link
		strand = -1 if( o_tgt.reversed ) else 1
		o_link = rl.RegionLink( o_ref, o_tgt, strand )

		return o_link
	# __build_gap_link

# ------------------
class InfilesLoader( object ):
	def __init__( self, do_links, d_scaff_len ):

		self.__d_scaff_len = d_scaff_len
		self.__lo_links = []
		for scaff_id,lo_links in do_links.items():
			o_tl = TargetLinks( lo_links, scaff_id, s=1, e=d_scaff_len[scaff_id] )
			self.__lo_links.append( o_tl )

		#~ self.__id_pattern  = re.compile( '\.\d+$' )

		#~ # to manage splitted scaffolds
		#~ #  I need to store the (start,end) of each chunk.
		#~ self.__dt_scaffs = None
		#~ self.__setScaffTuples( d_scaff_len )

	@property
	def d_scaff_len( self ):
		return self.__d_scaff_len

	@property
	def lo_links( self ): return self.__lo_links

	#~ def __setScaffTuples( self, d_scaff_len ):
		#~ d_ = dict()
		#~ for scaff_id,l in d_scaff_len.items():
			#~ d_[scaff_id] = ( 1, l )

		#~ self.__dt_scaffs = d_
	#~ # __setScaffTuples

	#~ def getScaffStart( self, scaff_id ):
		#~ return self.__dt_scaffs[scaff_id][0]
	#~ def getScaffEnd( self, scaff_id ):
		#~ return self.__dt_scaffs[scaff_id][1]

	#~ def getScaffLen( self, scaff_id ):
		#~ return self.__d_scaff_len[scaff_id]

	def __load_ref_align( cls, o_ref_align ):
		# o_ref_align => RegionLinks
		of_align = csv.reader( o_ref_align, delimiter='\t' )
		lo_links = rl.RegionLink.loadSimpleLinkFile( of_align )

		# in those links ref is the true Reference sequence
		do_links = defaultdict( list )
		for o_link in lo_links:
			do_links[ o_link.o_tgt.chrom ].append( o_link )

		return do_links
	__load_ref_align = classmethod( __load_ref_align )

	def __load_seq_len( cls, o_scaff_len ):
		of_seq = csv.reader( o_scaff_len, delimiter='\t' )
		d_scaff_len = {}
		for seq_id,seq_len in of_seq:
			d_scaff_len[seq_id] = int( seq_len )

		return d_scaff_len
	__load_seq_len = classmethod( __load_seq_len )

	def load( cls, o_ref_align, o_scaff_len ):
		'''Objects are file handles on tsv files.'''
		do_links = cls.__load_ref_align( o_ref_align )
		d_scaff_len = cls.__load_seq_len( o_scaff_len )

		return InfilesLoader( do_links, d_scaff_len )
	load = classmethod( load )	

# ========================================
def main( o_args ):
	verbose( "Load infiles." )
	o_files = InfilesLoader.load( o_args.align_ref_file, o_args.scaff_len )
	if( DEBUG ):
		generate_align_gff( o_files.lo_links, o_files.d_scaff_len )

	verbose( "extraction of the unmatched sequences." )
	# this line should be commented to perform the whole line analysis.
	lo_tgt_links = turn_align_intervals_to_unalign_intervals( o_files.lo_links )
	#S lo_tgt_links = o_files.lo_links # <- line to use in whole line analysis
	if( DEBUG ):
		generate_unalign_gff( lo_tgt_links, o_files.d_scaff_len )

	# dispensable step used to allow parallelisation is needed
	verbose( "identify regions of interest." )
	dlo_ROI = identify_regions_of_interest( lo_tgt_links )
	dlo_ROI = split_big_regions_of_interest( dlo_ROI )
	if( DEBUG ):
		generate_ROI_gff( dlo_ROI )

	#~ verbose( "determine sequences of interest." )
	#~ llo_SOI = determine_sequences_of_interest( dlo_ROI, o_files )
	#~ if( DEBUG ):
		#~ generate_SOI_gff( llo_SOI )

	llo_SOI = []
	for k,o_links in dlo_ROI.iteritems():
		l_ = [ k ]
		l_ += [ o_.o_tgt for o_ in o_links ]
		llo_SOI.append( l_ )

	verbose( "generate bed files." )
	generateBedfiles( o_args.outdir, llo_SOI )

	return 0
# main

# ===========DEBUG FUNC=======================
def __generate_gff_with_tgt_links( o_gff, lo_tgt_links, d_scaff_len ):
	lo_tgt_links.sort( key=lambda x: x[0].o_ref )

	for o_tgt_links in lo_tgt_links:
		if( not o_tgt_links ) : continue

		# I'll suppose that all tgts are on the same strand, but it is dangerous ...
		if( 1 == o_tgt_links[0].strand ):
			delta_s = o_tgt_links[0].o_tgt.start - 1

			t_end   = max( x.o_tgt.end for x in o_tgt_links )
			delta_e = d_scaff_len[o_tgt_links[0].o_tgt.seq_id] - t_end
		else:
			delta_s = d_scaff_len[o_tgt_links[0].o_tgt.seq_id] - o_tgt_links[0].o_tgt.end

			t_end   = min( x.o_tgt.start for x in o_tgt_links )
			delta_e = t_end - 1

		g_start = o_tgt_links[0].o_ref.start - delta_s

		g_end   = max( x.o_ref.end for x in o_tgt_links ) + delta_e
		o_r = rl.Region( o_tgt_links[0].o_ref.seq_id, g_start, g_end )

		o_gff.writeMatch( o_r, o_tgt_links.scaff_id, strand=o_tgt_links[0].strand_symbol )

		for i in range( len( o_tgt_links ) ):
			o_gff.writeMatchPart( o_tgt_links[i],
								ID="{0}_{1}".format( o_tgt_links.scaff_id, str( i+1 ) ),
								Parent=o_tgt_links.scaff_id )
		o_gff.writeSep()
# __generate_gff_with_tgt_links
	
def generate_align_gff( lo_tgt_links, d_scaff_len ):
	with fow.OutGff( ALIGN_DBG_GFF ) as o_gff:
		o_gff.comment( "Alignment input." )
		__generate_gff_with_tgt_links( o_gff, lo_tgt_links, d_scaff_len )
# generate_align_gff	

def generate_unalign_gff( lo_tgt_links, d_scaff_len ):
	with fow.OutGff( UNALIGN_DBG_GFF ) as o_gff:
		o_gff.comment( "unAlignment input." )
		__generate_gff_with_tgt_links( o_gff, lo_tgt_links, d_scaff_len )
# generate_unalign_gff

def generate_ROI_gff( dlo_ROI ):
	with fow.OutGff( ROI_DBG_GFF ) as o_gff:
		o_gff.comment( "Regions of interest." )

		# pb with scaff_id suffix.
		# several scaff link can be in the same ROI.
		i = 1
		for o_ref in sorted( dlo_ROI.keys() ):
			lo_links  = dlo_ROI[o_ref]
			region_id = 'R' + str( i )
			o_gff.writeMatch( o_ref, region_id )

			j = 1
			for o_link in lo_links:
				seq_id = "_".join([ o_link.o_tgt.seq_id, str(i), str(j) ])
				o_gff.writeMatchPart( o_link, ID=seq_id, \
										Parent = region_id )
				j += 1
			o_gff.writeSep()
			i += 1
# generate_ROI_gff

def generate_SOI_gff( llo_SOI ):
	with fow.OutGff( SOI_DBG_GFF ) as o_gff:
		o_gff.comment( "Sequences of interest." )

		#~ llo_SOI.sort( key= lambda l_x:l_x[0])

		i = 1
		for lo_soi in llo_SOI:
			o_ref = lo_soi[0]
			id = ".".join([ o_ref.seq_id, str( i ) ])
			o_gff.writeMatch( o_ref, id )
			
			j = 1
			for o_soi in lo_soi[1:]:
				# I cheat, id will contain all the attributes
				id  = ".".join([ o_soi.seq_id, str(i), str(j) ])
				tgt = "Target={0} {1} {2}".format( o_soi.seq_id, str( o_soi.start ), str( o_soi.end ) )
				attr= ";".join([ id, tgt ])
				j  += 1
				o_gff.writeMatch( o_ref, attr )

			i += 1
			o_gff.writeSep()
# generate_SOI_gff

# ===========/DEBUG FUNC=======================
def turn_align_intervals_to_unalign_intervals( lo_links ):

	lo_new_links = []
	for o_links in lo_links:
		verbose( o_links.scaff_id )
		lo_new_links += o_links.negateMatches( anchor_size=ANCHOR_SIZE )

	return lo_new_links

def identify_regions_of_interest( lo_tgt_links ):
	"""regions of interest are Ref regions where tgt are not overlapping between cultivars.
	
	lo_tgt_links: list of region links
	"""

	def __store_if_valid( dlo_roi, o_region, lo_roi ):
		if( 1 < len( lo_roi )
			and MIN_UNALIGNED_LEN < o_region.length() ):
			dlo_roi[ o_region ] = lo_roi
			# print( "Stored" )
	# __store_if_valid
	
	def __init_list( o_link ):
		'''the region will be altered, so i need a copy of the object'''
		o_region = copy.copy( o_link.o_ref )
		lo_roi   = [ o_link ]

		return o_region, lo_roi
	# __init_list
		
	# first step is to mix all links
	lo_links = []
	for o_tgt_links in lo_tgt_links:
		lo_links += o_tgt_links.lo_links
	lo_links.sort( key=lambda x: x.o_ref )

	( o_region, lo_roi ) = __init_list( lo_links[0] )

	dlo_roi = dict()
	for o_link in lo_links[1:]:
		# print "Region (1) : " + str( o_region )
		# print str( o_link ) + " -- "
		if( o_region.intersect( o_link.o_ref ) ):
			if( o_link.o_ref.start < o_region.start ):
				o_region.start = o_link.o_ref.start
			if( o_region.end < o_link.o_ref.end ):
				o_region.end = o_link.o_ref.end
			lo_roi.append( o_link )
		else:
			__store_if_valid( dlo_roi, o_region, lo_roi )
			( o_region, lo_roi ) = __init_list( o_link )
			# print "Region (n) : " + str( o_region )
	# print( "=========" )

	__store_if_valid( dlo_roi, o_region, lo_roi )

	return dlo_roi
# identify_regions_of_interest

def split_big_regions_of_interest( dlo_roi ):

	dlo_small_roi = dict()
	for o_region, lo_links in dlo_roi.iteritems():

		if( len( o_region ) < MAX_ROI_LEN ):
			dlo_small_roi[o_region] = lo_links
		else:
			# make n_chunk subregion from it
			n_chunk   = 1 + len( o_region ) / MAX_ROI_LEN
			chunk_len = len( o_region ) / n_chunk
			start = o_region.start
			for i in range( n_chunk ):
				end = start + chunk_len
				if( o_region.end < end ):
					end = o_region.end
				o_new_roi = o_region.subRegion( start, end )
				start = end+1

				## definition de lo_roi ... !
				lo_new_links = []
				for o_link in lo_links:
					if( o_new_roi.end < o_link.o_ref.start ):
						break

					try:
						l = len( o_new_roi.overlap( o_link.o_ref ) )
						if( MIN_UNALIGNED_LEN < l ):
							lo_new_links.append( o_link )
						# else overlap is too small to check.
					except:
						pass

					#~ if( o_new_roi.intersect( o_link.o_ref ) ):
						#~ lo_new_links.append( o_link )

				dlo_small_roi[o_new_roi] = lo_new_links

			# all chunk have been treated
			dlo_small_roi[o_new_roi] = lo_new_links

	return dlo_small_roi

def determine_sequences_of_interest( dlo_roi, o_infiles ):
	llo_SOI = []

	for o_ref_region, lo_links in dlo_roi.items():
		verbose( o_ref_region.recString() )
		o_soi_builder = SequenceOfInterestBuilder( lo_links )
#		o_gse.o_infiles = o_infiles
		lo_SOI = []
		for lo_regions in o_soi_builder.getTargetRegions_2():
			#~ print( "?" )
			#~ print( o_soi_builder.ref_region() )
			lo_SOI  = [ o_soi_builder.ref_region() ]
			lo_SOI += lo_regions

			llo_SOI.append( lo_SOI )

	return llo_SOI
# determine_sequences_of_interest

def generateBedfiles( outdir, llo_SOI ):

	for lo_SOI in llo_SOI:
		o_ref = lo_SOI[0]

		local_outdir = "/".join([ o_args.outdir, o_ref.chrom ])
		if( not os.path.exists( local_outdir ) ):
			os.makedirs( local_outdir )

		filename = "_".join( map( str, [ o_ref.chrom, o_ref.start, o_ref.end ]) )
		outbase  = "/".join([ local_outdir, filename ])
		bed_outfile = outbase + ".bed"

		with open( bed_outfile, "w" ) as o_bed:
			for o_region in lo_SOI:
				if( o_region == o_ref
					and o_region.start <= 0 ):
					o_region.start = 1

				o_bed.write( o_region.recString() + "\n" )
				#~ continue # I do not extract here.

				#~ l_elts = o_region.chrom.split( "|", 1 )
				#~ if( 1 == len( l_elts ) ):
					#~ ass_path = Rapsodyn.Paths.get_ref_fasta( 'bnapus' )
					#~ cultivar = 'Bnapus'
					#~ seq_id = l_elts[0]
				#~ else:
					#~ cultivar = l_elts[0]
					#~ seq_id   = l_elts[1]
					#~ ass_path = Rapsodyn.Paths.get_assembly_path( cultivar )
					#~ ass_path += "/{}.fa.bgz".format( cultivar )

				#~ o_r = rl.Region( seq_id, *o_region.feature() )
				#~ o_r.reversed = o_region.reversed
				#~ # PP.pprint( str( o_r ) )
				#~ o_es = MFasta.ExtractSeq( ass_path )
				#~ for seq_id, o_seq in o_es.extractRegions( [ o_r ], MFasta.build_seqId, cultivar ):
					#~ o_fs.write( ">" + seq_id + "\n" + str( o_seq ) + "\n" )

	return
# generateBedfiles


# ----------------------------------------
def defineOptions():
	o_parser = argparse.ArgumentParser( description='Prog descr.' )

	# special
	o_parser.add_argument( '--version', action='version', version='%(prog)s ' + VERSION )

	# positionnal / mandatory param
	#	type = int, file
	o_parser.add_argument( 'align_ref_file',
	                   type=file,
		               help='a tsv file with at least 6 cols (chr,s,e)x2.' )

	o_parser.add_argument( 'scaff_len',
	                   type=file,
		               help='a tsv file with 2 cols: scaff_id, length.' )


	# optionnal
	o_parser.add_argument( '--verbose', '-v', action='store_true',
		               help='If set give info on what happen.' )
	o_parser.add_argument( '--debug', action='store_true',
		               help='If set output much more information and generate some files.' )

	o_parser.add_argument( '--outdir', '-O', default="bed_files",
		               help='Output directory, where to store generated bedfiles.' )
	#~ o_parser.add_argument( '--bedfile', '-B', action='store_true',
		               #~ help='If set generate one bedfile per fasta outfile.' )


	return o_parser

def analyseOptions( o_args ):

	global VERBOSE
	if( o_args.verbose ):
		VERBOSE = True

	global DEBUG
	global ALIGN_DBG_GFF,UNALIGN_DBG_GFF,ROI_DBG_GFF,SOI_DBG_GFF
	if( o_args.debug ):
		DEBUG = True
		( dir, filename ) = os.path.split( o_args.align_ref_file.name )
		bname = filename.split(".")[0] # don't want real basename
		ALIGN_DBG_GFF   = bname + "." + ALIGN_DBG_GFF
		UNALIGN_DBG_GFF = bname + "." + UNALIGN_DBG_GFF
		ROI_DBG_GFF     = bname + "." + ROI_DBG_GFF
		SOI_DBG_GFF     = bname + "." + SOI_DBG_GFF

	if( not os.path.isdir( o_args.outdir ) ):
		os.makedirs( o_args.outdir )

def verbose( msg ):
	if( VERBOSE ):
		sys.stderr.write( msg + "\n" )

def debug( *t_msg ):
	if( DEBUG ):
		for msg in t_msg:
			sys.stderr.write( "\t" + msg + "\n" )

# ----------------------------------------

if __name__ == '__main__':
	o_parser = defineOptions()
	o_args   = o_parser.parse_args()
	analyseOptions( o_args )
	# PP.pprint( o_args )

	main( o_args )

	sys.exit( 0 )

