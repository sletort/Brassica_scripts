#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

## AIMS  : reformat blast result (obtain in only_QTL.sh). Set coordinate and scaff id.
## USAGE : reformat_mblast.coordinates.py mblast.tsv > mblast_reformatted.tsv
## AUTHORS : sletort@irisa.fr
## NOTE  : blast should be run with -outfmt "6 sseqid sstart send qseqid qstart qend sstrand qlen lengthscore evalue pident nident mismatch sseq qseq
## TODO  : merge with reformat_blast.py and modify mblast2VCF.py in consequence.

import sys
import pprint # debug
import argparse	# option management, close to C++/boost library (?)

import csv	# manage tsv files also.

import os	# for local libraries

# local libraries
f_path = os.path.dirname( os.path.realpath( __file__ ) )
sys.path.append( f_path + "/../../lib" )

from MiscSeq import MiscSeq
from lib_only_QTL import *

VERSION   = "0.1"
PP = pprint.PrettyPrinter(indent=4)
VERBOSE   = False

# ========================================
class BlastResultReformatorIterator( BlastResultIterator ):
	def next( self ):
		return BlastResultReformator( next( self._it ) )

class BlastResultReformator( BlastResult ):
	'''
		ref_id, start, end
		scaff_id, start, end
		strand
		scaff_len length
		score evalue pident nident mismatch
		sseq qseq
		filter_score
	'''
	def recString( self ):
		# back to the original for chrom id
		#~ self.o_link.o_ref.chrom = self.__l_elts[0]
		return self.o_link.recString( mode='ref' )

# ========================================
def main( o_args ):

	verbose( "Reading Blast results" )
	o_tsv = csv.reader( o_args.infile, delimiter='\t' )

	lo_br = []
	for o_reformator in BlastResultReformatorIterator( o_tsv ):
		lo_br.append( o_reformator )

	for o_br in sorted( lo_br, key=lambda x: x.o_link.o_ref ):
		o_args.outfile.write( o_br.recString() + "\n" )

	return 0

# ----------------------------------------
def defineOptions():
	descr = "reformat blast result (part of the only_QTL.sh pipeline)."
	o_parser = argparse.ArgumentParser( description=descr )

	# special
	o_parser.add_argument( '--version', action='version', version='%(prog)s ' + VERSION )

	# positionnal / mandatory param
	o_parser.add_argument( 'infile', nargs='?',
							type=argparse.FileType( 'r' ),
							default=sys.stdin,
							help='The tsv blast result (produced by only_QTL.sh for the moment).' )

	# optionnal
	o_parser.add_argument( '--outfile', '-o',
						type=argparse.FileType( 'w' ),
						default=sys.stdout,
		               help='outfile, = entry + one score column.' )

	o_parser.add_argument( '--verbose', '-v', action='store_true',
		               help='If set give info on what happen.' )

	return o_parser

def analyseOptions( o_args ):

	global VERBOSE
	if( o_args.verbose ):
		VERBOSE = True

	return

def verbose( msg ):
	if( VERBOSE ):
		sys.stderr.write( msg + "\n" )

# ----------------------------------------

if __name__ == '__main__':
	o_parser = defineOptions()
	o_args   = o_parser.parse_args()
	analyseOptions( o_args )
	# PP.pprint( o_args )

	main( o_args )

	sys.exit( 0 )

