#! /bin/bash

## AIMS  : Analyse the QTLs based on partially unmapped scaffolds.
## USAGE : ./analyse_QTL.sh [-h] [-v] -t [scaffs|contigs] Finishing_dir
#
## OUTPUT :
## OUTFILE:
#
## NOTE  :
#
## BUG   : None found yet.

source lib.analyse_QTL.sh

# =========================================================

# filename, not path.
#	files will be generated in the current directory.
declare -r ALL_SCAFF_LEN="all_scaff_len.tsv"

# progs
declare -r DETERMINE_SOI="$CWD_/check_partially_unaligned_seqs.py"

# comment those lines when everything is ok.
#	I don't want the script to be run by chance.
printf "Warn: to perform whole line analysis, $DETERMINE_SOI should be altered (cf comment inside)" >&2
exit 1

# =========================================================
function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT    = $0\n"
	printf "╟ - INDIR     = $INDIR\n"
	printf "╟ - TYPE      = $TYPE\n"
	printf "╟ - BED_DIR   = $BED_DIR\n"
	printf "╟ -------------\n"
	printf "╟ - ALL_SCAFF_LEN    = $ALL_SCAFF_LEN\n"
	printf "╟ - REPORT           = $REPORT\n"
	#~ printf "╟ - DETERMINE_SOI    = $DETERMINE_SOI\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

# =========================================================
function extract_alignment ()
{
	local cul="$1"
	local bed_region="$2"

	{
		zcat "$INDIR"/gs_bnapus_$cul/against_ref/$TYPE/$cul.tsv.gz \
				| perl -lane '$F[3] =~ s/^/'$cul'|/; print join( "\t", @F );' \
			> $cul.tmp.tsv

		printf "$bed_region" \
			| bedops -e 1 $cul.tmp.tsv -
	}
	# cannot be backgrounded in qsub
}

function determine_soi ()
{
	local bname="$1"
	local align_file="$2"

	local err_file=$( basename "$align_file" .tsv ).err
	local outdir="$BED_DIR"

	$DETERMINE_SOI "$align_file" ../../"$ALL_SCAFF_LEN" --debug --verbose \
			--outdir "$outdir" \
		2> "$err_file"

	# print on stdout (copied into REPORT file)
	__analyse_determine_soi_gff_file "$bname"
}

# ---------------------------------------------------------
function analyse_QTL ()
{
	local qtl="$1"

	local bname="rapsodyn_${qtl}"

	printf "$qtl :\n" | tee -a $REPORT

	printf "\tExtract alignments present on QTLs.\n"
	local align_file="$bname.align.tsv"

	loopOverCultivars bnapus extract_alignment "${A_QTLs[$qtl]}\n" \
			| sort-bed - \
		> "$align_file"

	describe_QTL "$align_file" | tee -a $REPORT
	rm *.tmp.tsv

	# ===============================
	printf "Determine sequence of interest for each cultivars over the QTL.\n"
	determine_soi "$bname" "$align_file" | tee -a $REPORT

	build_similarity_matrices

	#~ summarize_info_groups $bname
	local summary_file="$bname.matrices.sum_up.tsv"
	summarize_info_groups_for_dendrogram $bname \
		> "$summary_file"
	$__ROOT/generic/drawHeatmapDendrowFromMatrix.py \
		--title "Sequence comparison for mapped scaffolds on QTL $qtl." \
		--outfile "$qtl.HM_dendrow.png" \
		"$summary_file"
}

function init ()
{
	for cul in $( loopOverCultivars Bnapus echo )
	do
		zcat $( get_assembly_path $cul )/scaff.seq_len.tsv.gz \
			| perl -pe "s/_uid_\d+//; s/^/$cul|/;" \
			| sort -V
	done > "$ALL_SCAFF_LEN"

	wait
}


# =========================================================
manageOptions "$@"

if [[ 1 -eq $VERBOSE ]]
then
	printVars >&2
fi

# run init & analyse_QTL that should be define here.
main

printf "End of %s.\n\n" $( basename $0 ) >&2
exit 0
