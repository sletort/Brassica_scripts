#! /bin/bash

## AIMS  : provide function used by both analyse_QTL.sh & analyse_QTL_whole_lines.sh.
## USAGE : source lib.analyse_QTL.sh

set +o nounset
set +o errexit

source /softs/local/env/envbedops-2.4.15.sh
source /softs/local/env/envpython-2.7.sh

set -o nounset
set -o errexit

source $__ROOT/brassicaDB.sh

# =========================================================
declare INDIR
declare TYPE
declare QTL_FILE

# ---------------------------------------------------------

declare VERBOSE=0
declare -r CWD_=$( dirname $( readlink -e $0 ) )

declare -A A_QTLs=()
	#~ [test]="chrA01\t2\t100000"
	#~ [A02_SY]="chrA02\t3135342\t4088771"
	#~ [C02_SY]="chrC02\t6031695\t6675040"
	#~ [A05_SY]="chrA05\t3802833\t5013593"
	#~ [C04_SY]="chrC04\t4956918\t8525147"
#~ )

declare -r REPORT="rapsodyn_QTL_report.txt"

declare BED_DIR="bed_files"


# =========================================================
function assert_dependencies ()
{
	starch --version 2> /dev/null
}

function manageOptions ()
{
# getopts reminder:
#	FLAG, x will look for -x
#	x: will look for -x stg
#	if the option string start with ':', getopts turns to silent error reporting mode.
	while getopts "hvt:q:" opt
	do
		case $opt in
			t)	TYPE=$OPTARG  ;;
			q)	QTL_FILE=$OPTARG  ;;

			v)	VERBOSE=1	;;
			h)
				usage && exit 0 ;;
			\?)	usage && exit 0 ;;
		esac
	done
	shift $((OPTIND-1))

	INDIR="${1?What is the Finishing directory ?}"
	INDIR=$( readlink -e "$INDIR" ) # make it absolute path

set +o nounset
	if [[ "$TYPE" != 'scaffs' && "$TYPE" != 'contigs' ]]
	then
		printf -- "-t argument is mandatory and should take 'scaffs' or 'contigs' as value.\n" >&2
		exit 1
	fi
set -o nounset
}

function usage ()
{
		cat <<EOF
USAGE : $0 [options] Finishing_dir

Mandatory options :
	-t : type of data to analyse 'scaffs' or 'contigs'
	-q : qtl bed file

Options :
	-v : verbose mode, see parameter values.
	-h : print this message and exit.
EOF
}

# =========================================================

function describe_QTL ()
{
	local align_file="$1"

	printf "\t*QTL description:\n"
	local -r n_align=$( wc -l $align_file | cut -d ' ' -f 1 )
	local -r n_scaff=$( cut -f 4 $align_file | sort -u | wc -l )
	local -r n_culti=$( cut -f 4 $align_file | perl -F'\|' -lane 'print $F[0]' | sort -u | wc -l )

	printf "\t%s: %d alignments of %d scaffolds for %d cultivars.\n" \
		"$k" $n_align $n_scaff $n_culti

	printf "\n"
}

function __analyse_determine_soi_gff_file ()
{
	local bname="$1"

	# can lead to pb if directory contain a dot.
	printf "\n\t*Some details about partially aligned sequences inside the QTL :\n"

	local infile="${bname}.align.dbg.gff"
	local n_scaff=$( grep -cw 'match' "$infile" )
	local n_chunk=$( grep -cw 'match_part' "$infile" )
	printf "\t%d chunks of %d aligned scaffolds.\n" $n_chunk $n_scaff

	infile="${bname}.unalign.dbg.gff"
	n_scaff=$( grep -cw 'match' "$infile" )
	n_chunk=$( grep -cw 'match_part' "$infile" )
	printf "\t%d chunks of %d unaligned scaffolds.\n" $n_chunk $n_scaff

	infile="${bname}.roi.dbg.gff"
	local n_roi=$( grep -c 'ID=R' "$infile" )
	printf "\tContains %d regions of interest.\n" $n_roi

	#~ # WARN do not work if chrom is not 'chr*'
	#~ infile="${bname}.soi.dbg.gff"
	#~ local n_soi=$( grep -c 'ID=chr' "$infile" )
	#~ printf "\tContains %d sequences of interest.\n" $n_soi

	printf "\n"
}

# summarize the information from all chroms/QTL
# parse the distance matrix (=reformatted dist_matrix file)
function summarize_info_groups_for_dendrogram ()
{
	local -r bname="$1"

	# make a file which contain distance matrix data as a list of couple = score
	for f in bed_files/*/*.dist_matrix.tsv
	do
		#~ local seq_len=$( basename $f | perl -lne "$p_len" )

		perl -lane '
				($cul,$id, @x) = split( /\|/,$F[0] ); push( @a_cul, $cul );
				
				for $i (1..$.-1)
				{
					$value_string = sprintf( "%.2f", $F[$i] );
					print join( "\t", $a_cul[$.-1], $a_cul[$i-1], $value_string );
				}
			' $f
	done > "$bname.all_distances.tsv"

	# integrate all distances into one file.
	# each couple get the mean distance
	perl -lane '
		map{ $h_culs{$_}= 1; } @F[0,1];
		push( @{ $h_{$F[0]}{$F[1]} }, $F[2] );
		END{
			@a_culs = sort keys %h_culs;
			for $i (0..$#a_culs-1)
			{
				$cul1 = $a_culs[$i];
				for $j ($i+1..$#a_culs)
				{
					$cul2 = $a_culs[$j];
					$ra_1 = $h_{$cul1}{$cul2};
					$ra_2 = $h_{$cul2}{$cul1};
					@a_vals = ( @$ra_1, @$ra_2 );
					$n = @a_vals;
					printf( "%s:%s\t%d\t", $cul1, $cul2, $n );
					if( 0 == $n ){	printf( "NA\n" );	}
					else
					{
						$sum_score = 0;
						$sum_bases = 0;
						map{ $sum_score += $_ } @a_vals;
						$moy = $sum_score/$n;
						printf( "%.4f\n", $moy );
					}
				}
			}
		}' "$bname.all_distances.tsv"
}

function build_similarity_matrices ()
{
	for dir_path in "$BED_DIR"/*
	do
		for bed_file in "$dir_path"/*.bed
		do
			if [[ ! -e "$bed_file" ]]
			then
				printf "'$bed_file' does not exist. stg goes wrong.\n" >&2
			else
			{
				"$CWD_"/run_clustalO_from_bedfile.sh -v "$bed_file"
	#~ printf "Skip clustal.\n"
				bname_matrix=${bed_file%.bed}.dist_matrix
				if [[ -e $bname_matrix ]]
				then
					tail -n +2 $bname_matrix \
							| perl -lane 'print join( "\t", @F )' \
						> $bname_matrix.tsv
				fi
			}&
			fi
		done
		printf "Waiting for clustal Omega (and some personnal scripts) to terminate.\n" >&2
		wait
	done
}

function main ()
{
	assert_dependencies

	init

	mkdir -p $TYPE
	cd $TYPE
	local pwd=$PWD

	for k in "${!A_QTLs[@]}"
	do
		mkdir $k 2> /dev/null
		cd $k

		# paralellisation make the report uggly.
		# make one dir per QTL ?
		analyse_QTL $k

		cd $pwd
	done

	# make jobs parallel imply to manage REPORT separately
	wait
}

