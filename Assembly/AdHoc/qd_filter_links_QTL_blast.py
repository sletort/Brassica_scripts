#!/usr/bin/env python
# -*- coding: utf-8 -*-

## AIMS  : filter alignments. 
## USAGE : qd_filter_links_QTL_blast.py scaff_sizes alignments > filtered.tsv
## NOTE  : Remove all alignments if not uniquely fully aligned.
## NOTE  : keep the *only* fully aligned scaff if exist.
## NOTE  : Remove alignments that looks like repetition (all the same scaff coordinates).
## NOTE  : only difference with qd_filter_links is that this script manage REF-TGT whereas qd_filter_links manage TGT-REF.
## NOTE  :	those scripts will be removed once region_links package will be available.
## AUTHORS : sebastien.letort@irisa.fr


import sys
import pprint # debug
import csv
import gzip
from operator  import itemgetter

# from http://stackoverflow.com/questions/2632199/how-do-i-get-the-path-of-the-current-executed-file-in-python#
from inspect import getsourcefile
import os.path
cur_file = os.path.realpath( getsourcefile( lambda:0 ) )
sys.path.insert( 0, os.path.dirname( cur_file ) + '/../../lib' )

import RegionLink as rl
import Misc

PP = pprint.PrettyPrinter(indent=4)

# we remove alignments that cover >= PC_THRES of the scaff len.
PC_THRES = 0.95

# ========================================
def load_scaff_sizes( size_file ):
	d_scaff_sizes = {}
	if( Misc.is_compressed( size_file ) ):
		# python3 => mode='rt'
		o_fe = gzip.open( size_file, mode='rt' )
	else:
		o_fe = open( size_file )

	d_scaff_sizes = { x[0]: int(x[1]) for x in Misc.load2ColsAsDict( o_fe ).items() }
	o_fe.close()

	return d_scaff_sizes

def compute_alignment_len( l_row ):
	s = int( l_row[4] )
	e = int( l_row[5] )
	if( s < e ):
		size = e - s + 1
	else:
		size = s - e + 1

	return size

def look_for_fully_aligned( l_alignments, scaff_size ):
	l_kept = None
	for l_row in l_alignments:
		size = compute_alignment_len( l_row )

		if( size == scaff_size ):
			if( None == l_kept ):
				l_kept = l_row
			else:
				# not uniquely fully aligned
				return []

	# never fully aligned
	if( None == l_kept ):
		return l_alignments

	return [ l_kept ]

def print_buffer( l_buffer ):
	for l_row in l_buffer:
		print( "\t".join( l_row ) )

# ========================================

prog = sys.argv[0]
size_file = sys.argv[1]
alignment_file = sys.argv[2]

d_scaff_sizes = load_scaff_sizes( size_file )
#~ PP.pprint( d_scaff_sizes )


with open( alignment_file, 'r' ) as o_fe:
	o_tsv = csv.reader( o_fe, delimiter="\t" )
	rl.loadSimpleLinkFile( o_tsv )

# links are supposed ordered by scaff coordinates
ll_buffer = [ [ o_links[0] ] ]
	for a_elts in o_links[1:]:
		#~ PP.pprint({ 'a_elts': a_elts })

		if( 0 == len( l_buffer )
				or a_elts[0] == l_buffer[0][0] ):
			# 1st record or same scaff
			l_buffer.append( a_elts )
	
sys.exit( 0 )

		else:

			# note : l_buffer can contain only one scaff
			scaff_size = d_scaff_sizes[l_buffer[0][3]]

			# l_buffer can now be empty
			l_buffer = look_for_fully_aligned( l_buffer, scaff_size )


			if( 1 == len( l_buffer ) ):
				print_buffer( l_buffer )
			elif( 1 < len( l_buffer ) ):
				# still more than one alignment
				should_be_kept = False
				
				s_ref = int( l_buffer[0][1] )
				e_ref = int( l_buffer[0][2] )
				for l_row in l_buffer:
					s = int( l_row[1] )
					e = int( l_row[2] )

					# not is_strict_repetition
					if( ( s != s_ref and s != e_ref )
							or ( e != e_ref and e != s_ref ) ):
						should_be_kept = True
						break

				if( should_be_kept ):
					print_buffer( l_buffer )

			l_buffer = [ a_elts ]


# ------
			#~ size_thres = PC_THRES * scaff_size
			#~ for l_row in l_buffer:
				#~ s = int( l_row[4] )
				#~ e = int( l_row[5] )
				#~ if( s < e ):
					#~ size = e - s
				#~ else:
					#~ size = s - e

				#~ if( size_thres <= size ):
					#~ should_be_kept = False
					#~ break

	#~ PP.pprint( l_buffer )

print_buffer( l_buffer )
											
sys.exit( 0 )
