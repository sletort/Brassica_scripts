#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

## AIMS  : 	Add the confidency annotation, in FORMAT/Conf flag.
## USAGE : filter_vcf.py [-v] in.vcf > ou.vcf
## AUTHORS : sletort@irisa.fr
## NOTE  : At least 2 lvl of filter : when sure, and not sure.

import sys
import pprint # debug
import argparse	# option management, close to C++/boost library (?)

#~ from pysam import VariantFile
import pysam
from collections import defaultdict, Counter

import os	# for local libraries

# local libraries
f_path = os.path.dirname( os.path.realpath( __file__ ) )
sys.path.append( f_path + "/../../lib" )

VERSION   = "0.1"
PP = pprint.PrettyPrinter( indent=4, stream=sys.stderr )
VERBOSE   = False

# Format definitions
FORMAT_ID = 'Conf'
FORMAT_DESCR  = 'Confidency.'
FORMAT_DESCR += ' D: Doubt.'
FORMAT_DESCR += ' nD: No reason to doubt.'
FORMAT_Doubt  = 'D'
FORMAT_noDoubt= 'nD'

# ==========================================================
class RecordBuffer( object ):
	'''Records will be buffered while scaffolds overlap.'''
	def __init__( self, l_samples ):
		self.__lo_rec = []
		self.__do_scaff_buff = {}
		self.__o_g_scaff_buff = GlobalRecordScaffoldBuffer() # a global/multi-sample scaffold buffer
		self.__d_flags = {} # [sample_id][scaff_id] = flag

		for sample_id in l_samples:
			self.__do_scaff_buff[ sample_id ] = RecordScaffoldBuffer()
			self.__d_flags[ sample_id ]       = {}

	@property
	def lo_rec( self ):
		return self.__lo_rec

	@property
	def do_scaff_buff( self ):
		return self.__do_scaff_buff

	@property
	def o_g_scaff_buff( self ):
		return self.__o_g_scaff_buff

	def add_record( self, o_rec ):
		self.lo_rec.append( o_rec )
		# verbose( "0-" + str( o_rec ) )

		verbose( "parsing position {}".format( o_rec.pos ) )

		# to patch bug reported https://github.com/pysam-developers/pysam/issues/664
		l_hap__bug_patch = []
		for o_sample in o_rec.samples.itervalues():
			l_hap__bug_patch.append( o_sample['HAP'] )

		for i,o_sample in enumerate( o_rec.samples.itervalues() ):
			# verbose( "n-" + str( o_rec ) )
			# verbose( str( o_sample.items() ) )
			if( None == o_sample['HAP']
					and None != l_hap__bug_patch[i] ):
				o_sample['HAP'] = l_hap__bug_patch[i]

			# verbose( "\t*{}*".format( o_sample.name ) )
			if( 0 == o_sample['GT'][0] ):
				self.__manage_scaffold_extremity( o_sample )
			elif( None == o_sample['GT'][0] ):
				self.__manage_missing_value( o_sample, o_rec )
			else:
				self.__manage_variant( o_sample )

		# to patch bug reported https://github.com/pysam-developers/pysam/issues/664
		# Previous trick is not enough
		# verbose( str( l_hap__bug_patch ) )
		# verbose( str( o_rec.samples.values() ) )
		for i,o_sample in enumerate( o_rec.samples.values() ):
			if( None == o_sample['HAP'] ):
				o_sample['HAP'] = l_hap__bug_patch[i]

		# verbose( "x-" + str( o_rec ) )

		return

	def __manage_scaffold_extremity( self, o_sample ):
		o_scaff_buff = self.do_scaff_buff[ o_sample.name ]
		chrom_pos    = self.lo_rec[-1].pos
		scaff_id     = o_sample['HAP'] ## ici scaff_id peut être None ou '.'
		# -> si c'est None c'est qu'il y a eu un pb avant.
		if( None == scaff_id ):
			raise Exception( o_sample.items() )

		if( scaff_id not in o_scaff_buff ):
			verbose( "\tNew scaff {}".format( scaff_id ) )
			o_scaff_buff.add_scaff( scaff_id, chrom_pos )
			#~ self.o_g_scaff_buff.add_scaff( o_sample.name, scaff_id, chrom_pos )
		else:
			verbose( "\tEnd of scaff {}".format( scaff_id ) )
			d_formats = o_scaff_buff.terminate( scaff_id, chrom_pos )
			self.__d_flags[o_sample.name].update( d_formats )

	def __manage_variant( self, o_sample ):
		'''add inconsistency if at the same pos 2 scaffs have different alt'''
		o_scaff_buff = self.do_scaff_buff[ o_sample.name ]
		chrom_pos    = self.lo_rec[-1].pos
		scaff_id     = o_sample['HAP']

		verbose( "\tVariant in scaff {}".format( scaff_id ) )
		if( 1 < o_scaff_buff.n_opened_scaff ):
			l_shares = [] # list of scaff_id that share the variant

			if( self.lo_rec[-2].pos == chrom_pos ):
				verbose( "\t\tPosition already registered, deeper check." )
				l_shares = self.__look_for_scaff_sharing_the_variant( o_sample.name )

			# add inconsistency if at the same pos 2 scaffs have different alt.
			o_scaff_buff.addVariant( scaff_id, l_shares ) # all variants or only SNP ?

		return

	def __manage_missing_value( self, o_sample, o_rec ):
		verbose( "\tMissing value." )

		o_scaff_buff = self.do_scaff_buff[ o_sample.name ]
		l_scaff_ids  = list( o_scaff_buff.getOpenedScaffs() )
		n = len( l_scaff_ids )
		# PP.pprint({ 'n': n })
		if( 0 != n ):
			# verbose( str( l_scaff_ids ) )
			o_sample['GT']  = ( 0,0 )
			o_sample['HAP'] = l_scaff_ids[0]
			# BUG-1 : a record can be filled 0/0 even if the real GT is 1, but seen further on next record.
			verbose( "\t\t{} has been filled ?".format( l_scaff_ids[0] ) )
			if( 1 < n ):
				verbose( "\t\t/!\\other scaff(s) could also been filled, but vcf does not manage this." )

			# verbose( "**-" + str( o_rec ) )
		return

	def __look_for_scaff_sharing_the_variant( self, sample_id ):
		'''Look back for all previous scaff holding the variant, for the sample_id.'''
		l_sharing = []
		for o_record in reversed( self.lo_rec[:-1] ):
			if( o_record.pos != self.lo_rec[-1].pos ):
				break

			if( o_record.alts
				and set( self.lo_rec[-1].alts ).intersection( o_record.alts ) ):
				l_sharing.append( o_record.samples[sample_id]['HAP'] )

		return l_sharing

	def writeAll( self, o_out ):
		# update sample format
		for o_rec in self.lo_rec:
			# verbose( "+++" + str( o_rec ) )
			for sample_id in self.do_scaff_buff.keys():
				o_sample = o_rec.samples[sample_id]
				# verbose( str( o_sample.items() ) )
				# verbose( "---dbg: {}, {}".format( o_rec.pos, sample_id ) )

				# what make scaff_id eq None ?
				# cf https://github.com/pysam-developers/pysam/issues/651
				# None when input is bcf
				# '.' when input is vcf.
				if( None == o_sample['HAP'] ):
					o_sample['HAP'] = '.'
				scaff_id = o_sample['HAP']
				if( '.' != scaff_id ): #not a missing data
					flag = self.__d_flags[sample_id][scaff_id]
					if( flag ):
						o_sample[FORMAT_ID] = flag
				else:
				# bug pysam: if I don't set this the value output is ..
					o_sample[FORMAT_ID] = '.'
			#verbose( "---" + str( o_rec ) )

		# write records
		for o_rec in self.lo_rec:
			o_out.write( o_rec )
	# writeAll

	def endOfRegion( self ):
		for o_scaff_buff in self.do_scaff_buff.values():
			if( not o_scaff_buff.areAllTerminated() ):
				return False
		return True

	def checkConfidence( self ):
		self.o_g_scaff_buff.updateFlagsRegardingAllSamples( self.__d_flags )

class RecordScaffoldBuffer( object ):
	"""RecordScaffoldBuffer manage scaffolds in the RecordBuffer."""

	def __init__( self ):
		'''__d_scaffs is a dict { scaff_id: start }
			start is set to None when the scaff ends.
		   __ddo_overlaps is a matrix to manage overlaps (d[scaff_1][scaff_2] = o_overlap).
		   __n_opened_scaff is the number of opened scaff.
		'''
		self.__d_scaffs = {}
		self.__ddo_overlaps = defaultdict( dict )
		self.__n_opened_scaff = 0

	def __contains__( self, scaff_id ):
		'''Used to test if the scaff_id is opened.'''
		return( scaff_id in self.__d_scaffs
				and None != self.__d_scaffs[scaff_id] )

	@property
	def n_opened_scaff( self ):
		return self.__n_opened_scaff

	@property
	def _d_scaffs( self ):
		return self.__d_scaffs

	@property
	def ddo_overlaps( self ):
		return self.__ddo_overlaps


	def _add_scaff( self, scaff_id, chrom_pos ):
		self.__d_scaffs[ scaff_id ] = chrom_pos
		self.__n_opened_scaff += 1
		return
		
	def add_scaff( self, scaff_id, chrom_pos ):
		for k in self._d_scaffs:
			if( None == self._d_scaffs[k] ):
				continue

			# opened scaff
			verbose( "\tOverlap between {} and {}".format( scaff_id, k ) )
			self._addOverlap( scaff_id, k, chrom_pos )

		self._add_scaff( scaff_id, chrom_pos )
		return

	def _overlapBuilder( self, scaff_1, scaff_2, start ):
		'''_overlapBuilder instances the overlap'''
		return ScaffoldOverlap( scaff_1, scaff_2, start )

	def _addOverlap( self, scaff_1, scaff_2, start ):
		o_overlap = self._overlapBuilder( scaff_1, scaff_2, start )
		self.__ddo_overlaps[scaff_1][ scaff_2 ] = o_overlap
		self.__ddo_overlaps[scaff_2][ scaff_1 ] = o_overlap

	def _getOverlaps( self, scaff_id ):
		return self.__ddo_overlaps[scaff_id]

	def getOpenedScaffs( self ):
		for k,v in self._d_scaffs.iteritems():
			if( None != v ):
				yield k

		return

	def addVariant( self, scaff_id, l_sharing ):
		for k, o_overlap in self._getOverlaps( scaff_id ).iteritems():
			if( None == self.__d_scaffs[k] ):
				continue

			if( k in l_sharing ):
				verbose( "\t{} wasn't inconsistent with this one (shared variant)".format(k) )
				o_overlap.removeInconsistency( k )
			else:
				verbose( "\tThe variant is presumed inconsistent with {}".format(k) )
				o_overlap.addInconsistency( scaff_id )
		return

	def _end_scaff( self, scaff_id ):
		self.__d_scaffs[ scaff_id ] = None
		self.__n_opened_scaff -= 1

		return

	def terminate( self, scaff_id, chrom_pos ):
		def __integrate( l_flags ):
			'''l_flags has one value per overlap (can be None)'''
			# PP.pprint( l_flags )
			if( 0 == len( l_flags ) ):
				return FORMAT_noDoubt

			if( 1 == len( l_flags ) ):
				return l_flags[0]

			# make stg right seems very complicated
			#	- need to care about length, error outside the overlap
			# if any doubt mark it
			c_ = Counter( l_flags )
			if( 1 != len( c_.most_common() ) ):
				return FORMAT_Doubt # at leat one Doubt

			return l_flags[0] # all flags the same

		self._end_scaff( scaff_id )

		l_flags = []
		for k,o_overlap in self._getOverlaps( scaff_id ).iteritems():
			#~ PP.pprint({ k: self.__d_scaffs[k] })
			if( None != self.__d_scaffs[ k ] ):
				o_overlap.ends( chrom_pos )
				verbose( "\tOverlap between {} and {} is {} long.".format( scaff_id, k, len( o_overlap ) ) )

			verbose( "\tCheck insconsistencies in {} vs {}.".format( scaff_id, k ) )
			flag = o_overlap.getInconsistencyFlag()
			l_flags.append( flag )

		flag = __integrate( l_flags ) or FORMAT_noDoubt # when no overlap.
		verbose( "\t=> {}.".format( flag or 'ok'  ) )

		return { scaff_id: flag }

	def areAllTerminated( self ):
		for x in self._d_scaffs.values():
			if( None != x ):
				return False # at least one not terminate
		return True

class GlobalRecordScaffoldBuffer( RecordScaffoldBuffer ):
	"""RecordScaffoldBuffer manage scaffolds in the RecordBuffer.
		Note: GlobalScaffoldOverlap is use as a convenance, but I could use a simple dict."""
	SEP = chr( 30 ) # Record Separator ascii code.

	@classmethod
	def _get_id( cls, sample_id, scaff_id ):
		return cls.SEP.join([ sample_id, scaff_id ])

	def _overlapBuilder( self, scaff_1, scaff_2, start ):
		return GlobalScaffoldOverlap( scaff_1, scaff_2, start )

	def add_scaff( self, sample_id, scaff_id, chrom_pos ):
		global_id = self._get_id( sample_id, scaff_id )

		for g_id in self._d_scaffs:
			# skip if we already have a scaff for this culti
			#	or no data (to work on)
			if( g_id.startswith( sample_id+self.SEP )
					or None == self._d_scaffs[g_id] ):
				continue

			# opened scaff
			verbose( "\tOverlap between {} and {}".format( global_id, g_id ) )
			self._addOverlap( global_id, g_id, chrom_pos )

		self._add_scaff( global_id, chrom_pos )

	def updateFlagsRegardingAllSamples( self, d_flags ):
		# I cannot manage some not so complicated cases like :
		# 2 scaff from A are Doubtfull, they overlap B which overlap C
		# B and C are from different samples and are not Doubtful.
		# my guess is that pb is in A, but the solution I propose place all scaff in Doubt
		# ... ok, I let it, but remember that a doubt can be very slight.
		
		# PP.pprint({ 'd_flags': d_flags })
		# PP.pprint({ 'ddo': self.ddo_overlaps })
		# dd = defaultdict
		# g_id = sample_name + SEP + scaff_id
		for g_id1, do_ in self.ddo_overlaps.iteritems():
			sample1,scaff1 = g_id1.split( self.SEP )
			if( FORMAT_Doubt == d_flags[sample1][scaff1] ):
				verbose( "All samples overlapping {} will be altered.".format( g_id1 ) )
				# o_ol = overlap object, ... and unused wow !
				for g_id2, o_ol in do_.iteritems():
					verbose( "{} altered.".format( g_id2 ) )
					sample2,scaff2 = g_id2.split( self.SEP )
					d_flags[sample2][scaff2] = FORMAT_Doubt

class ScaffoldOverlap( object ):
	'''I'm not sure that having a score for each scaff is needed anymore.'''
	def __init__( self, scaff_1, scaff_2, start ):
		self.__start = start
		self.__d_scaff_err = { scaff_1: 0, scaff_2: 0 }
		self.__size  = None

	def __len__( self ):
		return self.__size

	def addInconsistency( self, scaff_id ):
		'''scaff_id holds the variant/inconsistency'''
		self.__d_scaff_err[scaff_id] += 1

	def removeInconsistency( self, scaff_id ):
		'''scaff_id holds the variant/inconsistency'''
		self.__d_scaff_err[scaff_id] -= 1

	def __get_other_scaff_id( self, scaff_id ):
		# reminder: only 2 keys.
		for k in self.__d_scaff_err.keys():
			if( k != scaff_id ):
				return k

	def getInconsistencyFlag( self ):
		# PP.pprint( self.__d_scaff_err )
		s = sum( self.__d_scaff_err.values() )
		if( 0 == s ):
			return FORMAT_noDoubt

		return FORMAT_Doubt

	def ends( self, chrom_pos ):
		self.__size = chrom_pos - self.__start + 1

class GlobalScaffoldOverlap( ScaffoldOverlap ):
	pass

# ==========================================================
def update_meta( o_header ):
	o_header.formats.add( FORMAT_ID, 1, 'String', FORMAT_DESCR )

def main( o_args ):

	o_in  = pysam.VariantFile( o_args.infile )  # auto-detect input format
	
	update_meta( o_in.header )
	l_samples = [ x for x in o_in.header.samples ]
	o_out = pysam.VariantFile( o_args.outfile, 'w', header=o_in.header )

	o_buff = RecordBuffer( l_samples )
	for o_rec in o_in.fetch():

		o_buff.add_record( o_rec )

		if( o_buff.endOfRegion() ):

			#~ o_buff.checkConfidence()
			o_buff.writeAll( o_out )
			o_buff = RecordBuffer( l_samples )

	o_buff.writeAll( o_out )

	o_in.close()
	o_out.close()

	return 0

# ----------------------------------------
def defineOptions():
	descr = "Fill genotypes for regions covered by scaffolds in merged vcf (cf only_QTL.sh)."
	o_parser = argparse.ArgumentParser( description=descr )

	# special
	o_parser.add_argument( '--version', action='version', version='%(prog)s ' + VERSION )

	# positionnal / mandatory param
	o_parser.add_argument( 'infile', # nargs='?',
							#~ type=argparse.FileType( 'r' ),
							#~ default=sys.stdin,
							help='The merged vcf to fill (produced in only_QTL script).' )

	# optionnal
	o_parser.add_argument( '--outfile', '-o',
						type=argparse.FileType( 'w' ),
						default=sys.stdout,
		               help='vcf outfile.' )

	o_parser.add_argument( '--verbose', '-v', action='store_true',
		               help='If set give info on what happen.' )

	return o_parser

def analyseOptions( o_args ):

	global VERBOSE
	if( o_args.verbose ):
		VERBOSE = True

	return

def verbose( msg ):
	if( VERBOSE ):
		sys.stderr.write( msg + "\n" )


# ----------------------------------------

if __name__ == '__main__':
	o_parser = defineOptions()
	o_args   = o_parser.parse_args()
	analyseOptions( o_args )
	# PP.pprint( o_args )

	sys.exit( main( o_args ) )
