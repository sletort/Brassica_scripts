#!/usr/bin/env python
# -*- coding: utf-8 -*-

## AIMS  : filter blast result to get only the best solution.
## USAGE : filter_blast.py [-l log file] mblast.tsv > filtered.blast
## AUTHORS : sletort@irisa.fr
## NOTE  : blast should be run with -outfmt "6 sseqid sstart send qseqid qstart qend sstrand qlen lengthscore evalue pident nident mismatch sseq qseq
## NOTE  :	should be change to be more adaptative, by parsing the blast cmd for example.
## NOTE  : The log file will contain explanation for score attribution

import sys
import pprint # debug
import argparse	# option management, close to C++/boost library (?)

import csv	# manage tsv files also.
from itertools import groupby

import os	# for local libraries

# local libraries
f_path = os.path.dirname( os.path.realpath( __file__ ) )
sys.path.append( f_path + "/../../lib" )

from lib_only_QTL import BlastResult

VERSION   = "0.1"
PP = pprint.PrettyPrinter(indent=4)
VERBOSE   = False
O_LOG     = None

# ========================================
def main( o_args ):

	verbose( "Reading Blast results" )

	o_tsv = csv.reader( o_args.infile, delimiter='\t' )
	for t_align in groupby( o_tsv, key=lambda l_: l_[3] ): # work scaff by scaff
		l_best,score = filter_blast_res( *t_align )
		if( l_best ):
			l_best.append( score )
			o_args.outfile.write( "\t".join([ str(x) for x in l_best ]) + "\n" )
			#~ PP.pprint([ l_best, score ])

	if( O_LOG ):
		O_LOG.close()

	return 0

def filter_blast_res( scaff_id, o_gen ):
	'''
		$F[7]	qlen
		$F[10]	e-value
		$F[11]	% ident
		$F[12]	nident
		$F[13]	mismatch
	'''
	ll_elts = convert_type( o_gen )
	ll_elts.sort( key = lambda x: (x[10],x[11]) )

	l_best  = ll_elts[0] # record should be sorted by relevance

	# no choice
	if( 1 == len( ll_elts ) ):
		log( "{0}\tno choice".format( scaff_id ) )
		return l_best, 0

	if( res_fully_aligned_without_error( ll_elts[0] ) ):
		if( not res_fully_aligned_without_error( ll_elts[1] ) ):
			log( "{0}\tonly one fully aligned".format( scaff_id ) )
			return l_best, 0
		else:
			log( "{0}\tundecidable, at least 2 full alignements".format( scaff_id ) )
			return None, 0

	if( res_fully_aligned( ll_elts[0] ) ):
		if( not res_fully_aligned( ll_elts[1] ) ):
			log( "{0}\tonly one fully aligned (with mismatches)".format( scaff_id ) )
			return l_best, 0
		elif( ll_elts[0][13] < ll_elts[1][13] ):
			log( "{0}\tat least 2 fully aligned (with mismatches), kept the one with less errors.".format( scaff_id ) )
			return l_best, 1
		else:
			log( "{0}\tundecidable, at least 2 full alignements with equal mismatches.".format( scaff_id ) )
			return None, 0

	# compare % and e-value
	if( ll_elts[0][10] == ll_elts[1][10] ):
		#~ if( ll_elts[0][11] == ll_elts[1][11] ):
			#~ log( "{0}\tundecidable, at least 2 not fully aligned.".format( scaff_id ) )
			#~ return None, 0
		#~ else:
			#~ # usually seen v = 0.0
			log( "{0}\tundecidable, both e-value equals.".format( scaff_id ) )
			return None, 2

	log( "{0}\tundecidable, too fuzzy.".format( scaff_id ) )
	return None,9


def convert_type( o_gen ):
	ll_elts = []
	for l_ in o_gen:
		# convert int
		for i in ( 7,12,13 ):
			l_[i] = int( l_[i] )
		# convert float
		for i in ( 10,11 ):
			l_[i] = float( l_[i] )
		ll_elts.append( l_ )

	return ll_elts

def res_fully_aligned_without_error( l_ ):
	'''
	$F[7]	qlen
	$F[12]	nident
	'''
	return l_[7] == l_[12]

def res_fully_aligned( l_ ):
	'''
	$F[7]	qlen
	$F[12]	nident
	$F[13]	mismatch
	'''
	return l_[7] == ( l_[12] + l_[13] )

# ----------------------------------------
def defineOptions():
	descr = "filter blast result to get only the best solution."
	o_parser = argparse.ArgumentParser( description=descr )

	# special
	o_parser.add_argument( '--version', action='version', version='%(prog)s ' + VERSION )

	# positionnal / mandatory param
	o_parser.add_argument( 'infile', nargs='?',
							type=argparse.FileType( 'r' ),
							default=sys.stdin,
							help='The tsv blast result (produced by only_QTL.sh for the moment).' )

	# optionnal
	o_parser.add_argument( '--outfile', '-o',
						type=argparse.FileType( 'w' ),
						default=sys.stdout,
		               help='outfile, = entry + one score column.' )

	o_parser.add_argument( '--log_file', '-l',
		               help='log filepath.' )

	o_parser.add_argument( '--verbose', '-v', action='store_true',
		               help='If set give info on what happen.' )

	return o_parser

def analyseOptions( o_args ):

	global VERBOSE
	if( o_args.verbose ):
		VERBOSE = True

	global O_LOG
	if( o_args.log_file ):
		O_LOG = open( o_args.log_file, 'w' )
		verbose( 'Logging into {}'.format( o_args.log_file ) )

	return

def log( msg ):
	if( O_LOG ):
		O_LOG.write( msg + "\n" )

def verbose( msg ):
	if( VERBOSE ):
		sys.stderr.write( msg + "\n" )

# ----------------------------------------

if __name__ == '__main__':
	o_parser = defineOptions()
	o_args   = o_parser.parse_args()
	analyseOptions( o_args )
	# PP.pprint( o_args )

	main( o_args )

	sys.exit( 0 )

