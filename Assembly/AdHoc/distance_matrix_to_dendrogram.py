#!/usr/bin/env python
# -*- coding: utf-8 -*-

## AIMS  : PROTOTYPE !
## USAGE : 
## AUTHORS : sebastien.letort@irisa.fr
## SOURCE : https://joernhees.de/blog/2015/08/26/scipy-hierarchical-clustering-and-dendrogram-tutorial/


import sys
import pdb	# debug
import pprint # debug
import argparse	# option management, close to C++/boost library (?)
import os	# for local libraries
import csv	# manage tsv files also.
from math import log
from collections import defaultdict

import numpy as np
from scipy.cluster.hierarchy import linkage, dendrogram, fcluster
import scipy.cluster.hierarchy as sch
import matplotlib.pyplot as plt

# local libraries
f_path = os.path.dirname( os.path.realpath( __file__ ) )
sys.path.append( f_path + "/../lib" )

VERSION = "1.0"
PP = pprint.PrettyPrinter(indent=4)
VERBOSE   = False

# ========================================
def main( o_args ):
	verbose( "En mode verbeux." )

	( l_names, a_distances ) = load_matrix( o_args.infile )
	#~ PP.pprint( l_names )
	#~ PP.pprint( { 'distances': a_distances } )

	a_linkage = linkage( a_distances, "single")

	init_figure()

	ds_idx = build_cluster( a_linkage, 0.1 )
	#~ PP.pprint( ds_idx )
	#~ a_clusters = fcluster( a_linkage, 5, criterion='distance' )
	#~ PP.pprint( a_clusters )
	#~ x = a_clusters.argmax()

	for s in ds_idx.values():
		l_ = sorted( l_names[ list( s ) ] )
		#~ PP.pprint( l_ )
		print "\t".join( l_ )

	draw_dendrogram( a_linkage, l_names )
	plt.show()
#	plt.savefig( "toto.png" )

	return 0

def load_matrix( fe ):
	parser = csv.reader( fe, delimiter="\t" )
	matrix = np.array( list( parser ) )
	fe.close()

	# 1st col + extraction of the cultivar name (or ref)
	l_names = np.array([ x.split( "|", 1 )[0] for x in matrix[:,0] ])

	a_distances = matrix[:,1:]

	return l_names, a_distances

def init_figure():
	fig = plt.figure()
	plt.clf()
	plt.axis()
	#~ plt.grid(True)

def draw_dendrogram( a_linkage, l_names ):

	x_max = None
	for x in np.nditer( a_linkage[:,2] ):
		if( x_max < x ):
			x_max = x
	# PP.pprint( { 'linkage': a_linkage } )
	# PP.pprint( { 'x_max': x_max } )

	try:
		base = log( x_max ) / 10
	except ValueError as e:
		base = 0
	if( base <= 0 ): base = 0.01

	for x in np.nditer( a_linkage[:,2], op_flags=['readwrite'] ):
		x[...] = log( 1 + x ) + base

	#~ PP.pprint( { 'linkage': a_linkage } )
	base = 0

	
	ddata = dendrogram( a_linkage, labels=l_names )
	#~ ddata = dendrogram( a_linkage, labels=l_names, truncate_mode='lastp', p=4 )
	#~ PP.pprint( { 'dendro_out': ddata } )

	for i,d,c in zip( ddata['icoord'], ddata['dcoord'], ddata['color_list'] ):
		x = 0.5 * sum(i[1:3])
		y = d[1]
		#~ color = 'r' if( y == base ) else 'b'
		#~ plt.plot( x, y, 'o', c=color)
		plt.plot( x, y, 'o', c=c)
		plt.annotate("%.3g" % y, (x, y), xytext=(0, -8),
                         textcoords='offset points',
                         va='top', ha='center')


	return ddata

def build_cluster( a_linkage, max_d ):

	#~ PP.pprint( { 'linkage': a_linkage } )
	a_clusters = fcluster( a_linkage, max_d, criterion='distance' )
	d = defaultdict( set )
	for i in range( len( a_clusters ) ):
		d[ a_clusters[i] ].add( i )

	return d


# ----------------------------------------
def defineOptions():
	o_parser = argparse.ArgumentParser( description='Prog descr.' )

	# special
	o_parser.add_argument( '--version', action='version', version='%(prog)s ' + VERSION )

	# positionnal / mandatory param
	#	type = int, file
	o_parser.add_argument( 'infile', type=file,
		               help='The distance matrix to represent as a dendrogram (tsv file).' )

	# optionnal
	o_parser.add_argument( '--verbose', '-v', action='store_true',
		               help='If set give info on what happen.' )
	#~ o_parser.add_argument( '-f', dest='opt1',
							#~ default='toto',
							#~ help='a flag with a value.' )

	#~ # mandatory option
	#~ #	destination is opt2 by default
	#~ o_parser.add_argument( '--opt2', '-o',
						#~ action='store_true', # store the value as boolean
						#~ help='a flag opt, long and short.')

	return o_parser

def analyseOptions( o_args ):

	global VERBOSE
	if( o_args.verbose ):
		VERBOSE = True


def verbose( msg ):
	if( VERBOSE ):
		sys.stderr.write( msg + "\n" )

# ----------------------------------------

if __name__ == '__main__':
	o_parser = defineOptions()
	o_args   = o_parser.parse_args()
	analyseOptions( o_args )
	#~ PP.pprint( o_args )

	main( o_args )

	sys.exit( 0 )

