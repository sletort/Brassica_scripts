#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

## AIMS  : Merge VCF produced for discoSNP and assembly. Mainly check genotypes.
## USAGE : ./merge_asm_disco_vcf.py disco.vcf asm.vcf
## AUTHORS : sletort@irisa.fr
## NOTE  : disco.vcf asm.vcf is equal to asm.vcf disco.vcf
## NOTE  : VCF files have to be sorted, with samples in the same order.

import sys
import pprint # debug
import argparse	# option management, close to C++/boost library (?)
import gzip
import csv
#~ import pysam	# generation des VCF ... too complex to create a vcf from scratch !
from datetime import date
import re


import os	# for local libraries

# local libraries
f_path = os.path.dirname( os.path.realpath( __file__ ) )
sys.path.append( f_path + "/../../lib" )

import Vcf # as pysam is too complex
from Misc import open_infile,verbose,set_verbose

VERSION   = "0.1"
PP = pprint.PrettyPrinter( indent=4, stream=sys.stderr )

REFERENCE    = 'Darmor-bzh_v4-1' # should be read from the infile, i think

# ========================================
def manage_meta_lines_and_guess_src( o_in1, o_in2 ):
	""" replace lines with fileformat,source,fileDate
		hard coded, no time/envy to make things better
	"""
	def __update_meta_and_guess_src( l_meta_out, l_meta_in ):

		src = None
		for line in l_meta_in:
			if line.startswith( '##fileformat=' ) \
					or line.startswith( '##fileDate=' ):
				continue

			if line.startswith( '##source=' ) \
					and 'disco' in line:
				src = 'disco'

			l_meta_out.append( line )

		#Note: contig meta will appear duplicated
		return src

	l_meta = [
			'##fileformat=VCFv4.2\n',
			'##source="merge_asm_disco_vcf.py"\n',
			'##fileDate={}\n'.format( date.today().strftime( '%Y%m%d' ) ),
		]
	src1 = __update_meta_and_guess_src( l_meta, o_in1.getRawMeta() )
	src2 = __update_meta_and_guess_src( l_meta, o_in2.getRawMeta() )
	# PP.pprint( l_meta )

	if 'disco' == src1:
		o_in_disco = o_in1
		o_in_asm   = o_in2
	elif 'disco' == src2:
		o_in_disco = o_in2
		o_in_asm   = o_in1
	else:
		sys.stderr.write( "Warn: Cannot guess which file is from disco or asm. I'll suppose the first one is from disco." )
		o_in_disco = o_in1
		o_in_asm   = o_in2


	o_fmt_gt_disco = Vcf.Format({
					'ID'     : 'GT_disco',
					'Number' : '1',
					'Type'   : 'String',
					'Description': '"Genotype from disco_file (0:Ref, 1: alt1, 2, alt2, ...)."',
				})
	o_fmt_gt_asm   = Vcf.Format({
					'ID'     : 'GT_asm',
					'Number' : '1',
					'Type'   : 'String',
					'Description': '"Genotype from asm_file (0:Ref, 1: alt1, 2, alt2, ...)."',
				})
	do_new_meta = { 'FORMAT': [ o_fmt_gt_disco, o_fmt_gt_asm ] }

	return l_meta, do_new_meta, o_in_disco, o_in_asm

def load_buff( l_buff, o_in ):
	for o_rec in o_in:
		l_buff.append( o_rec )

		if 2 <= len( l_buff ) \
				and not o_rec.simple_equals( l_buff[0] ):
			return True
	else:
		return False

def consume_rec( o_out, l_buff, src ):
	
	def __write_record( o_out, o_rec, src ):
		for d_sample in o_rec.l_sample_data:
			d_sample['GT_'+src] = d_sample[ 'GT' ]
			if '0/0' == d_sample['GT'][0] or '0|0' == d_sample['GT'][0]:
				d_sample['GT'] = '0'

		o_rec.write( o_out )
		verbose( str(o_rec) )
		return

	o_ref = l_buff.pop( 0 )

	while( l_buff and o_ref.simple_equals( l_buff[0] ) ):
		o_rec = l_buff.pop( 0 )
		if '.' == o_rec.l_alleles[1]:
			continue

		# bouillasse pour ne pas propager une erreur apparue plus tôt
		if '.' == o_ref.l_alleles[1]:
			o_ref = o_rec
		else:
			skip = False # for disco
			if 'asm' == src:
				skip = True
				for i,d_sample in enumerate( o_ref.l_sample_data ):
					if d_sample['HAP'] != o_rec.l_sample_data[i]['HAP']:
						skip = False
						break
			if skip:
				# still need to find if I skip ref or rec. -> the one with 0/0
				# encore plus grosse bouillasse ...
				skip_rec = True
				for i,d_sample in enumerate( o_ref.l_sample_data ):
					if ( '.' == d_sample['GT'] and '.' != o_rec.l_sample_data[i]['GT'] ) \
							or ( '0/0' == d_sample['GT'] \
								and ( '1' == o_rec.l_sample_data[i]['GT'] or '1/1' == o_rec.l_sample_data[i]['GT'] ) ):
						o_ref = o_rec
						break
			else:
				# no fusion
				__write_record( o_out, o_ref, src )
				o_ref = o_rec

	__write_record( o_out, o_ref, src )

def manage_variant_record_buffer( o_out, l_buff_asm, l_buff_disco ):

	def __merge_records( l_buff_asm, l_buff_disco ):
		def __determine_GT( gt1, gt2 ):
			'''too deep !'''
			if gt1 == gt2:
				return gt1

			else:
				s1 = set( re.split( "[|/]", gt1 ) )
				s2 = set( re.split( "[|/]", gt2 ) )
				if 1 != len( s1 ) or 1 != len( s2 ):
					raise Exception( 'Heterozygotie is not managed yet.\n' )

				gt1 = s1.pop()
				gt2 = s2.pop()
				#~ PP.pprint({ 's1': gt1, 's2': gt2 })
				if gt1 == gt2:
					return gt1
				elif '.' == gt1:
					return gt2
				elif '.' == gt2:
					return gt1

			return None

		# simple case
		o_asm   = l_buff_asm.pop( 0 )
		o_disco = l_buff_disco.pop( 0 )
		if o_asm.l_alleles == o_disco.l_alleles:
			verbose( "\tFUSION | {}:{}\n".format( o_asm.seq_id, o_asm.pos ) )
			o_rec = Vcf.VariantRecord( o_asm.seq_id, o_asm.pos,
							o_asm.l_alleles, o_asm.var_id )
			d_infos = o_asm.d_infos
			d_infos.update( o_disco.d_infos )
			d_infos.pop( 'AC' )
			d_infos.pop( 'AN' )
			o_rec.updateInfos( d_infos )

			l_asm_samples   = o_asm.l_sample_data
			l_disco_samples = o_disco.l_sample_data
			ld_samples = []
			for i, d_asm in enumerate( l_asm_samples ):
				d_sample = d_asm.copy()
				d_sample['GT_asm'] = d_asm.pop( 'GT' )

				d_disco = l_disco_samples[i]
				d_sample.update( d_disco )
				d_sample['GT_disco'] = d_disco.pop( 'GT' )

				# GT are lists of 1 string.
				d_sample['GT'] = __determine_GT( d_sample['GT_asm'][0], d_sample['GT_disco'][0] )

				o_rec.addSampleData( d_sample )

		else:
			# HERE: we can have error on allele with 0/0 on one record and 1 on the other
			#	I failed managed this easily for not been generated.
			#	It should be managed here.
			msg  = "Records don't have the same allele, not managed yet."
			msg += "\n\t{}:{}\n".format( o_asm.seq_id, o_asm.pos )
			raise Exception( msg )

		o_rec.write( o_out )

	if l_buff_asm[0] < l_buff_disco[0]:
		consume_rec( o_out, l_buff_asm, 'asm' )
		return 'asm'

	if l_buff_asm[0].simple_equals( l_buff_disco[0] ):
		__merge_records( l_buff_asm, l_buff_disco )
		return 'both'

	else: # l_buff_asm[0] > l_buff_disco[0]
		consume_rec( o_out, l_buff_disco, 'disco' )
		return 'disco'

def init_outfile( outfile, l_meta, l_samples, do_new_meta ):
	o_out = Vcf.Writer.build_from_rawMeta_lines( outfile, l_meta, l_samples )
	o_out.appendMeta( do_new_meta )
	o_out.writeHeader()

	return o_out

# ----------------------------------------
def main( o_args ):

	verbose( "Let's get begin !" )
	o_in1   = Vcf.Reader( o_args.in_1 )
	o_in2   = Vcf.Reader( o_args.in_2 )

	if o_in1.l_samples != o_in2.l_samples:
		raise Exception( "Your 2 files don't have the same samples." )

	l_meta,do_new_meta,o_disco,o_asm = manage_meta_lines_and_guess_src( o_in1, o_in2 )

	o_out = init_outfile( o_args.vcf_out, l_meta, o_in1.l_samples, do_new_meta )
	#~ o_err = init_outfile( o_args.vcf_err, l_meta, o_in1.l_samples, do_new_meta )

	# I'll keep in mind that many record can represent the same position
	# -> I'll do not manage indel yet (more complicated)
	l_buff_disco = []
	l_buff_asm   = []

	verbose( "Load buffs\n" )
	load_buff( l_buff_asm, o_asm )
	load_buff( l_buff_disco, o_disco )

	while l_buff_asm and l_buff_disco:
		len_asm   = str( len( l_buff_asm ) )
		len_disco = str( len( l_buff_disco ) )
		verbose( "buff_size | ASM : DISCO = {}:{}".format( len_asm, len_disco ) )

		flag = manage_variant_record_buffer( o_out, l_buff_asm, l_buff_disco )
		if 'asm' == flag:
			load_buff( l_buff_asm, o_asm )
		elif 'disco' == flag:
			load_buff( l_buff_disco, o_disco )
		elif 'both' == flag:
			load_buff( l_buff_asm, o_asm )
			load_buff( l_buff_disco, o_disco )

		verbose( "next loop\n" )
	verbose( "end loop\n" )
	len_asm   = str( len( l_buff_asm ) )
	len_disco = str( len( l_buff_disco ) )
	verbose( "after ASM : DISCO = {} :{}".format( len_asm, len_disco ) + "\n" )

	# one or another loop, not both.
	while( l_buff_asm ):
		consume_rec( o_out, l_buff_asm, 'asm' )

	while( l_buff_disco ):
		consume_rec( o_out, l_buff_disco, 'disco' )

	return 0

# ----------------------------------------
def defineOptions():
	descr = "Merge VCF produced for discoSNP and assembly. Mainly check genotypes."
	o_parser = argparse.ArgumentParser( description=descr )

	# special
	o_parser.add_argument( '--version', action='version', version='%(prog)s ' + VERSION )

	# positionnal / mandatory param
	o_parser.add_argument( 'vcf_1',
							#~ type=argparse.FileType( 'r' ),
							#~ default=sys.stdin,
							help='first vcf file to merge.' )

	o_parser.add_argument( 'vcf_2',
							#~ type=argparse.FileType( 'r' ),
							#~ default=sys.stdin,
							help='second vcf file to merge.' )

	# optionnal
	o_parser.add_argument( '--vcf_out', '-o',
						type=argparse.FileType( 'w' ),
						default=sys.stdout,
		               help='vcf outfile.' )

	#~ o_parser.add_argument( '--vcf_err', '-e',
						#~ type=argparse.FileType( 'w' ),
						#~ default=sys.stdout,
		               #~ help='vcf errfile (records that cannot be merged.' )

	#~ o_parser.add_argument( '--output-type', '-O',
						#~ #type=argparse.FileType( 'w' ),
						#~ choices=[ 'b', 'u', 'z', 'v' ]
						#~ default='u',
		               #~ help='Output compressed BCF (b), uncompressed BCF (u), compressed VCF (z), uncompressed VCF (v).' )

	o_parser.add_argument( '--verbose', '-v', action='store_true',
		               help='If set give info on what happen.' )

	return o_parser

def analyseOptions( o_args ):

	if( o_args.verbose ):
		set_verbose( True )

	if "-" == o_args.vcf_1:
		o_args.in_1 = sys.stdin
		if "-" == o_args.vcf_2:
			raise Exception( "You cannot have the 2 infile on stdin." )
	else:
		o_args.in_1 = open_infile( o_args.vcf_1 )

	if "-" == o_args.vcf_2:
		o_args.in_2 = sys.stdin
	else:
		o_args.in_2 = open_infile( o_args.vcf_2 )

	return


# ----------------------------------------

if __name__ == '__main__':
	o_parser = defineOptions()
	o_args   = o_parser.parse_args()
	analyseOptions( o_args )
	# PP.pprint( o_args )

	sys.exit( main( o_args ) )
