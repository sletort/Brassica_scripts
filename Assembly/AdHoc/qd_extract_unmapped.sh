#! /bin/bash

source /omaha-beach/sletort/rapsodynDB.sh
source /omaha-beach/sletort/brassicaDB.sh

source /softs/local/env/envpython-2.7.sh
source /softs/local/env/envsamtools-1.3.sh

ROOT=/groups/genscale/sletort/depots/gitlab.inria/Brassica_scripts

# first try.
function first_try ()
{
	for cul in $( loopOverCultivars bnapus echo )
	do
		perl -lane 'print $F[0] if( $F[1]>=500 && 2==@F )' gs_bnapus_$cul/scaff_chrom_size.tsv \
			> gs_bnapus_$cul/unmapped_scaff.lst
		python $ROOT/generic/mfasta_extract_seqs.py gs_bnapus_$cul/unmapped_scaff.lst /groups/rapsodyn/Assembly/$cul/$cul.fa.gz \
				| bgzip -c \
			> gs_bnapus_$cul/$cul.unmapped.fa.gz &
	done
	wait
}


function extract_seqs ()
{
	local cul="$1"
	local id_lst="$2"

	local mfasta=$( get_assembly_path $cul )/$cul.fa.gz

	local tmp_file=$( mktemp --tmpdir=gs_bnapus_$cul XXXXX.mfasta )
	python $ROOT/generic/mfasta_extract_seqs.py "$mfasta" "$id_lst" \
		> $tmp_file
	# pysam cannot read on stdin
	python $ROOT/generic/mfasta_prefix_seq_ids.py "${cul}|" $tmp_file \
		| bgzip -c

	rm -f "${tmp_file}"
	rm -f "${tmp_file}.fai"
}

# second try, make 2 files : big seq (>10k) and others
function second_try ()
{
	for cul in $( loopOverCultivars bnapus echo )
	do
		# will be used as perl -lane "$perle \$F[1] >= SIZE $perle_end"
		perle='$max=0; for( $i=3; $i<@F; $i+=2 ){'
		perle=$perle' $max = $F[$i] if( $F[$i]>$max ); }'
		perle=$perle' $l = $F[1]-$max;'
		perle=$perle' print $F[0] if( '
		perle_end=' && $l >= 500 )'

		# big seqs
		zcat gs_bnapus_$cul/against_ref/scaff_chrom_size.tsv.gz \
				| perl -lane "$perle \$F[1] >= 10_000 $perle_end" \
			> gs_bnapus_$cul/unmapped_scaff.big.lst
		extract_seqs "$cul" "gs_bnapus_$cul/unmapped_scaff.big.lst" \
			> gs_bnapus_$cul/$cul.unmapped.big.fa.gz &

		zcat gs_bnapus_$cul/against_ref/scaff_chrom_size.tsv.gz \
				| perl -lane "$perle \$F[1]<10_000 $perle_end" \
			> gs_bnapus_$cul/unmapped_scaff.not_big.lst
		extract_seqs "$cul" "gs_bnapus_$cul/unmapped_scaff.not_big.lst" \
			> gs_bnapus_$cul/$cul.unmapped.not_big.fa.gz &
	done
	wait
	printf "Waiting for extraction to terminate.\n"
}

second_try
exit 0

