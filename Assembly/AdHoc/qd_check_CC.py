#!/usr/bin/env python
# -*- coding: utf-8 -*-

## AIMS  : detect strech of N in fasta file and output as bed file.
## USAGE : detectOcean.py fasta_file > out.bed
## AUTHORS : sebastien.letort@irisa.fr


import sys
import pdb	# debug
import pprint # debug
import argparse	# option management, close to C++/boost library (?)
import os	# for local libraries
import errno
import csv	# manage tsv files also.
import networkx as nx
import matplotlib.pyplot as plt
from collections import defaultdict
import json
from operator import attrgetter
import itertools

# local libraries
f_path = os.path.dirname( os.path.realpath( __file__ ) )
sys.path.append( f_path + "/../../lib" )
import RegionLink as rl

VERSION = "1.0"
PP = pprint.PrettyPrinter(indent=4)
VERBOSE   = False

# ========================================
class BlastResult( rl.RegionLink ):
	def load_mblast_file( cls, o_file ):
		'''Return a list of BlastResult. (based on RegionLink.loadSimpleLinkFile)'''
		o_parser = csv.reader( o_file, delimiter="\t" )

		h_scaffs = {}
		lo_br = []
		for a_elts in o_parser:
			o_ref  = rl.Region( *a_elts[0:3] )
			o_tgt  = rl.Region( *a_elts[3:6] )

			if( o_ref.reversed == o_tgt.reversed ):
				strand = 1
			else:
				strand = -1

			o_br = BlastResult( o_ref, o_tgt, strand )
			lo_br.append( o_br )

			for t_ in [ (o_ref,7), (o_tgt,8) ]:
				if( t_[0].chrom not in h_scaffs ):
					h_scaffs[t_[0].chrom] = int( a_elts[t_[1]] )

		return lo_br,h_scaffs
	load_mblast_file = classmethod( load_mblast_file )

class InfilesLoader( object ):
	def __init__( self, lo_br, d_scaffs, do_link_ref, do_link_TE ):
		self.__lo_mblast = lo_br
		self.__d_scaffs  = d_scaffs

		# dictionnary of o_link objects
		self.__do_link_ref = do_link_ref
		self.__do_link_TE  = do_link_TE

	@property
	def lo_mblast( self ):
		return self.__lo_mblast

	@property
	def d_scaffs( self ):
		return self.__d_scaffs

	@property
	def do_link_ref( self ):
		return self.__do_link_ref

	@property
	def do_link_TE( self ):
		return self.__do_link_TE

	def load( cls, o_mblast_file, o_align_ref_file, o_TE_file ):

		# --- file1
		( lo_br, h_scaffs ) = BlastResult.load_mblast_file( o_args.align_file )
		verbose( str( len( lo_br ) ) + " links have been loaded from mblast file." )

		# --- file2
		of_align = csv.reader( o_align_ref_file, delimiter='\t' )
		lo_links = rl.RegionLink.loadSimpleLinkFile( of_align )

		# in those links ref is the true Reference sequence
		do_links = defaultdict( list )
		for o_link in lo_links:
			do_links[ o_link.o_tgt.chrom ].append( o_link )

		# --- file3
		do_te = None
		if( o_TE_file ):
			do_te = defaultdict( list )
			of_te = csv.reader( o_TE_file, delimiter='\t' )
			for a_elts in of_te:
				if( a_elts[0].startswith( '#' ) ):
					continue
				do_te[a_elts[0]].append( rl.Region( a_elts[0],a_elts[3],a_elts[4] ) )

		return InfilesLoader( lo_br, h_scaffs, do_links, do_te )
	load = classmethod( load )

class Output( object ):
	def __init__( self, outfile ):
		self.__outfile = outfile
		self.__fo = None

	def __enter__( self ):
		self.__fo = open( self.__outfile, 'w' )

	def __exit__( self ):
		self.__fo.close()

class Node( object ):
	def __init__( self, o_region ):
		self.__lo_regions = [ o_region ]

	def append( self, x ):
		self.__lo_regions.append( x )

	# some accessor to the first region in the node
	@property
	def chrom( self ):
		return self.__lo_regions[0].chrom
	@property
	def start( self ):
		return self.__lo_regions[0].start
	@property
	def end( self ):
		return self.__lo_regions[0].end
	@property
	def reversed( self ):
		return self.__lo_regions[0].reversed

	def __str__( self, short = False ):
		if short:
			return str( self.__lo_regions[0] )
		return "\n".join( [ str( x ) for x in self.__lo_regions ] )

	@property
	def regions( self ):
		return self.__lo_regions

	def intersect( self, o_node ):
		for o_reg1 in o_node.regions:
			for o_reg2 in self.regions:
				if( o_reg1.chrom == o_reg2.chrom ):
					return True
		return False

	def subRegion( self, start=None, end=None ):
		return self.__lo_regions[0].subRegion( start, end )

# ========================================
def main( o_args ):
	verbose( "En mode verbeux." )

	verbose( "Load infiles." )
	o_files = InfilesLoader.load( o_args.align_file, o_args.align_ref_file, o_args.te_gff )
	#~ PP.pprint( o_files.d_scaffs )

	# here a node is an exact region, same chrom/start/end
	dG_region = build_graph_from_refAlign( o_files.do_link_ref )
	verbose( "# edges : " + str( dG_region.number_of_edges() ) )
	verbose( "# nodes : " + str( dG_region.number_of_nodes() ) )
	#~ draw( dG_region )

	dG_scaffs = build_graph_from_mblast( o_files.lo_mblast )
	verbose( "# edges : " + str( dG_scaffs.number_of_edges() ) )
	verbose( "# nodes : " + str( dG_scaffs.number_of_nodes() ) )
	#~ draw( dG_scaffs )
	#~ sys.exit( 42 )

	# work by CC
	G = nx.Graph( dG_region )
	n_cc = nx.number_connected_components( G )
	verbose( "G has {} connected components.".format( ( n_cc ) ) )

	dl_trees = defaultdict( dict )
	for l_nodes in nx.connected_components(G):
		verbose( "\n--CC--{}".format( [ str( x ) for x in l_nodes ] ) )

		( o_ref, dG_amont, dG_aval ) = study_CC( dG_region, dG_scaffs, o_files.d_scaffs, l_nodes )
		#~ draw( dG_amont )
		#~ draw( dG_aval )

		t_trees = ( dG_amont, dG_aval )
		dl_trees[o_ref.chrom].setdefault( 'trees', [] ).append( t_trees )
		dl_trees[o_ref.chrom].setdefault( 'regions', [] ).append( o_ref )

	sort_trees_regarding_ref( dl_trees )
	merge_CC_trees( dl_trees )

	true_ref = get_probable_ref( dl_trees )

	if( 1 < len( dl_trees.keys() ) ):
		print( "Your data refer to more than 1 reference, there is probably an ref-alignment pb." )
		print( "The one with the biggest coverage is " + str( true_ref ) )
		check_for_probable_chimere( dl_trees )

	for ref_chrom in dl_trees.keys():
		lt_trees = dl_trees[ref_chrom]['trees']
		for ( up_tree, down_tree ) in lt_trees:
			draw( up_tree )
			draw( down_tree )

	sys.exit( 42 )
#end main

def check_for_probable_chimere( dl_trees ):
	verbose( "Checking for chimere.\n" )

def get_probable_ref( dl_trees ):
	max_ref  = 0
	true_ref = None
	for k,dl_ in dl_trees.items():
		for t_x in dl_['trees']:
			for x in t_x:
				for o_node in x:
					if( o_node.chrom == k ):
						n = len( o_node.regions )
						if( max_ref < n ):
							max_ref  = n
							true_ref = o_node
	return true_ref

def sort_trees_regarding_ref( dl_trees ):
	for ref_chrom in dl_trees.keys():
		lt_trees = dl_trees[ref_chrom]['trees']
		l_refs   = dl_trees[ref_chrom]['regions']

		# I need to sort list tree regarding l_refs
		l_refs2 = []
		for i in range( len(l_refs) ):
			l_refs2.append( (l_refs[i],i) )
		l_refs2.sort( key=lambda x: x[0] ) # we sort on Region object

		# then for each tree
		lt_trees_new = []
		l_idx = [ i for (x,i) in l_refs2 ]
		for i in range( 0, len( l_idx ) ):
			lt_trees_new.append( lt_trees[ l_idx[i] ] )

		dl_trees[ref_chrom]['trees'] = lt_trees_new

	return

def merge_CC_trees( dl_trees ):
	for ref_chrom in dl_trees.keys():
		lt_trees = dl_trees[ref_chrom]['trees']
		for i in range( 1, len( lt_trees ) ):
			o_tree_up   = lt_trees[i][0]
			o_tree_down = lt_trees[i-1][1]

			lt_to_merge = []
			for o_up in o_tree_up.nodes():
				if( o_up.chrom == ref_chrom ):
					continue

				for o_down in o_tree_down.nodes():
					if( o_down.chrom == ref_chrom ):
						continue

					# nodes up and down can make reference to several regions
					#~ if( o_up.chrom == o_down.chrom ):
					if( o_up.intersect( o_down ) ):
						#~ lt_to_merge.append( ( o_up, o_down ) )
						verbose( "Intersection found between {0} and {1}.".format( str(o_up), str(o_down) ) )

						if( o_up.reversed == 0 ):
							t_coords = ( o_down.start, o_up.end )
						else:
							t_coords = ( o_down.end, o_up.start )
						o_intersect = Node( rl.Region( o_up.chrom, *t_coords ) )
						#~ PP.pprint( str( o_intersect ) )

						safely_replace_node( o_tree_up, o_up, o_intersect )
						safely_replace_node( o_tree_down, o_down, o_intersect )

	return

def safely_replace_node( o_tree, o_node, o_replace ):
	for o_pred in o_tree.predecessors_iter( o_node ):
		o_tree.add_path([ o_pred, o_replace ])

	for o_succ in o_tree.successors_iter( o_node ):
		o_tree.add_path([ o_replace, o_succ ])

	o_tree.remove_node( o_node )
	verbose( "node {0} replaced by {1}".format( str( o_node ), str( o_replace ) ) )

	return

def study_CC( dG_ref, dG_scaffs, d_scaffs, l_nodes ):

	dG_regions_up   = nx.DiGraph()
	dG_regions_down = nx.DiGraph()
	o_root = None
	for o_ref,o_tgt, d_data in dG_ref.edges_iter( l_nodes, data='link' ):
		verbose( "* integrate {}".format( str( o_tgt ) ) )

		o_link = d_data['link']
		if( not o_root ):
			o_root = Node( o_ref )
		#~ draw( dG_regions_aval )

		
		if( 0 == len( dG_regions_up ) ):
			verbose( "\tInitiate tree with ref.\n" )
			dG_regions_up.add_node( o_root )
			dG_regions_down.add_node( o_root )

		o_root.append( o_link.o_tgt )
		( o_up, o_down ) = get_amont_aval( o_link.o_tgt, d_scaffs )
		verbose( "\t" + str( { "amont": str( o_up ) } ) )
		verbose( "\t" + str( { "aval": str( o_down ) } ) )

		if( o_up ):
			add_node_in_tree( 'amont', dG_regions_up, o_root, dG_scaffs, o_up, d_scaffs )

		if( o_down ):
			add_node_in_tree( 'aval', dG_regions_down, o_root, dG_scaffs, o_down, d_scaffs )

		verbose( "\n" )

	return o_ref, dG_regions_up, dG_regions_down


# cracra tout ça !
def add_node_in_tree( key_search, dG_regions, o_ref, dG_scaffs, o_scaff, d_scaffs ):
	t_anchor = search_anchor_node( key_search, dG_regions, o_ref, dG_scaffs, o_scaff )
	verbose( "\t\t" + str({ "anchor": [ str(x) for x in t_anchor ] }) )

	if( not t_anchor[0] ):
		verbose( "\t" + "Pas d'ancre" )
		verbose( "\t" + str({ "create link ref-scaff": str( o_scaff ) }) )
		dG_regions.add_path([ o_ref, Node( o_scaff ) ])
	else:
		integrate_node_in_graph( key_search, dG_regions, t_anchor[0], o_scaff, t_anchor[1], d_scaffs )

	return

# pour l'aval - pour le moment = keyword
def integrate_node_in_graph( keyword, o_tree, o_anchor, o_node, o_link, d_scaffs ):
	''' o_link est le lien entre le scaff de l'ancre et le scaff du noeud'''

	verbose( "\n\t*integrate_node_in_graph" )

	def extend_tree_with_node_end( o_tree, o_anchor, o_node, o_link, d_scaffs ):
		s  = o_link.o_tgt.end-1
		e  = d_scaffs[ o_link.o_tgt.chrom ]

		o_chunk = Node( o_node.subRegion( start=s, end=e ) )
		o_tree.add_path([ o_anchor, o_chunk ])
		verbose( "\tlien {0} -> {1}".format( str( o_anchor ), str( o_chunk ) ) )

	def extend_tree_with_node_start( o_tree, o_anchor, o_node, o_link, d_scaffs ):
		s  = 1
		e  = o_link.o_tgt.start - 1

		o_chunk = Node( o_node.subRegion( start=s, end=e ) )
		o_tree.add_path([ o_anchor, o_chunk ])
		verbose( "\tlien {0} -> {1}".format( str( o_anchor ), str( o_chunk ) ) )

	def split_anchor( o_tree, o_anchor, cut_point ):
		''' n(p) -> anchor -> m(s) become n(p) -> n1 -> n2 -> m(s)'''
		
		( o_n1, o_n2 ) = split_node( o_anchor, cut_point )

		for o_pred in o_tree.predecessors_iter( o_anchor ):
			o_tree.add_path([ o_pred, o_n1 ])

		for o_succ in o_tree.successors_iter( o_anchor ):
			o_tree.add_path([ o_n2, o_succ ])

		o_tree.remove_node( o_anchor )
		o_tree.add_path([ o_n1, o_n2 ])
		verbose( "\t\tlien {0} -> {1}".format( str( o_n1 ), str( o_n2 ) ) )

		return o_n1

	def split_node( o_node, cut_point ):
		if( o_node.reversed ):
			o_n1 = o_node.subRegion( start=cut_point+1 )
			o_n2 = o_node.subRegion( end=cut_point )
		else:
			o_n1 = o_node.subRegion( end=cut_point )
			o_n2 = o_node.subRegion( start=cut_point+1 )

		verbose( "\t\tdivision of Node {0} on {1}".format( str( o_node ), cut_point ) )

		return( Node( o_n1 ), Node( o_n2 ) )


	# o_node will be the target
	if( o_node.chrom == o_link.o_ref.chrom ):
		o_link.revert()

	if( o_link.o_ref.end < o_anchor.end ):
		o_n1 = split_anchor( o_tree, o_anchor, o_link.o_ref.end )
		if( 1 == o_link.strand ):
			( o_node1, o_node2 ) = split_node( o_node, o_link.o_tgt.end )
		else:
			( o_node1, o_node2 ) = split_node( o_node, o_link.o_tgt.start )
		o_n1.append( o_node1 )
		if( o_node2 ):
			o_tree.add_path([ o_n1, o_node2 ])
			verbose( "\t\tlien {0} -> {1}".format( str( o_n1 ), str( o_node2 ) ) )

	elif( o_link.o_ref.end == o_anchor.end ):
		if( 1 == o_link.strand ):
			o_shared = o_node.subRegion( end=o_link.o_tgt.end )
			extend_tree_with_node_end( o_tree, o_anchor, o_node, o_link, d_scaffs )
		else:
			o_shared = o_node.subRegion( start=o_link.o_tgt.start )
			extend_tree_with_node_start( o_tree, o_anchor, o_node, o_link, d_scaffs )
		o_anchor.append( o_shared )

	# those choices should be mutually exclusive if anchor has been well chosen.
	elif( o_anchor.start < o_link.o_ref.start ):
		o_n1 = split_anchor( o_tree, o_anchor, o_link.o_ref.start )
		if( 1 == o_link.strand ):
			( o_node1, o_node2 ) = split_node( o_node, o_link.o_tgt.start )
		else:
			( o_node1, o_node2 ) = split_node( o_node, o_link.o_tgt.end )
		o_n1.append( o_node1 )
		if( o_node2 ):
			o_tree.add_path([ o_n1, o_node2 ])
	elif( o_link.o_ref.start == o_anchor.start ):
		if( 1 == o_link.strand ):
			o_shared = o_node.subRegion( start=o_link.o_tgt.start )
			extend_tree_with_node_start( o_tree, o_anchor, o_node, o_link, d_scaffs )
		else:
			o_shared = o_node.subRegion( end=o_link.o_tgt.end )
			extend_tree_with_node_end( o_tree, o_anchor, o_node, o_link, d_scaffs )
		o_anchor.append( o_shared )

	else:
		PP.pprint( "END   : {0} <? {1}".format( str( o_link.o_ref.end ), str( o_anchor.end ) ) )
		PP.pprint( "START : {0} <? {1}".format( str( o_anchor.start ), str( o_link.o_ref.start ) ) )
		raise Exception( "unexpected case." )

	return

# code for aval
def search_anchor_node( key_search, o_tree, o_root, dG_scaffs, o_aval ):
	verbose( "\n\t" + "*Search anchor node in " + key_search + " tree." )

	#~ draw( o_tree )
	#~ PP.pprint({ "aval" : str( o_aval ) })
	if( 1 == len( o_tree.nodes() ) ):
		verbose( "\t\tNo leaf" )
		return ( None, None )

	l_leafs = [ x for x,d in o_tree.out_degree().items() if( d == 0 ) ]
	#~ PP.pprint({ "feuilles dispo": [ str(x) for x in l_leafs ] })

	o_best = None
	d_candidates = dict()

	# by construction each leaf refer to a different scaff.
	while( True ):
		try:
			o_node = l_leafs.pop( 0 )
		except IndexError:
			break

		ld_data = []
		for o_reg in o_node.regions:
			d_data = dG_scaffs.get_edge_data( o_reg.chrom, o_aval.chrom )
			if( d_data ):
				ld_data.append( d_data )

		if( 0 == ld_data ):
			verbose( "\t\tNo link found between {0} and {1}".format( o_node.chrom, o_aval.chrom ) )
			verbose( "\t\tWill try upper nodes" )
			for o_pred in o_tree.predecessors_iter( o_node ): # only one in aval
				l_leafs.append( o_pred ) # o_root case ?

			continue # next leaf

		for d_data in ld_data:
			o_link = d_data.get( 'link' )

			# make o_node the ref.
			if( o_aval.chrom == o_link.o_ref.chrom ):
				o_link.revert()
			verbose( "\t\t" + str( { "o_link": str( o_link ) } ) )

			if( 'aval' == key_search ):
				if(( 1 == o_link.strand )
					and ( o_link.o_ref.end <= o_node.start ) ):
						o_predecessor = o_tree.predecessors( o_node )[0]
						l_leafs.append( o_predecessor ) # o_root case ?
				else:
					d_candidates[ o_node ] = o_link
			else:
				if(( 1 == o_link.strand ) # really enough ?
					and ( o_node.end < o_link.o_ref.start ) ):
						o_predecessor = o_tree.predecessors( o_node )[0]
						l_leafs.append( o_predecessor ) # o_root case ?
				else:
					d_candidates[ o_node ] = o_link
	#/while
			

	# choose one candidate
	t_best = ( None, None )
	for o_node,o_link in d_candidates.items():
		#~ PP.pprint({ "t_best.link": t_best[1] })

		if( not t_best[1]
			or t_best[1].o_tgt.end > o_link.o_tgt.end ):
			t_best = ( o_node, o_link )

	#~ verbose( "FIN Search anchor node in aval tree." )
	return t_best


def get_amont_aval( o_scaff, d_scaffs ):

	if( 1 == o_scaff.start ):
		o_amont = None
	else:
		o_amont = o_scaff.subRegion( 1, o_scaff.start - 1 )

	if( d_scaffs[o_scaff.chrom] == o_scaff.end ):
		o_aval  = None
	else:
		o_aval  = o_scaff.subRegion( o_scaff.end + 1, d_scaffs[o_scaff.chrom] )

	if( o_scaff.reversed ):
		return [ o_aval, o_amont ]

	return [ o_amont, o_aval ]


def draw( o_graph ):

	nx.draw_networkx( o_graph )
	plt.show() # ne sais pas pk, peut pas être sauvé et vu en même temps.
	plt.close()

	nx.write_graphml( o_graph, "tmp.graphml" )

def build_graph_from_mblast( lo_br ):
	'''lo_br is a list of BlastResult object.'''

	G = nx.Graph()
	for o_br in lo_br:
		data = G.get_edge_data( o_br.o_ref.chrom, o_br.o_tgt.chrom )
		if( data and ( data['link'].revert() != o_br ) ):
			PP.pprint({ "already_in": str( data['link'] ) })
			PP.pprint({ "new_link"  : str( o_br ) })
			err_msg = "There are reciproqual edges, with different links.\n"
			raise Exception( err_msg )

		G.add_path( [ o_br.o_ref.chrom, o_br.o_tgt.chrom ], link=o_br )

	return G

def build_graph_from_refAlign( do_links ):
	'''do_links is a dictionnary of RegionLink object.'''

	def __get_best_ref( lo_regions, o_ref ):
		for o_reg in lo_regions:
			if( o_ref.intersect( o_reg ) ):
				return o_reg

	# compute contig-ref regions
	lo_ref_regions = []
	for lo_links in do_links.values():
		lo_ref_regions += [ x.o_ref for x in lo_links ]
	lo_ref_regions.sort( key = lambda x: ( x.chrom, x.start, x.end ) )

	i = 0
	while( i < len( lo_ref_regions )-1 ):
		if( lo_ref_regions[i].intersect( lo_ref_regions[i+1] ) ):
			o_region = rl.Region.merge([ lo_ref_regions[i], lo_ref_regions[i+1] ])[0]
			del lo_ref_regions[i:i+2]
			lo_ref_regions.insert( i, o_region )
		else:
			i += 1
	
	# now build the graph, ref will be one the contig-ref region previously computed.
	G = nx.DiGraph()
	for lo_links in do_links.values():
		for o_link in lo_links:

			o_ref = __get_best_ref( lo_ref_regions, o_link.o_ref )
			G.add_path( [ o_ref, o_link.o_tgt ], link=o_link )

	return G

# ----------------------------------------
def defineOptions():
	o_parser = argparse.ArgumentParser( description='Prog descr.' )

	# special
	o_parser.add_argument( '--version', action='version', version='%(prog)s ' + VERSION )

	# positionnal / mandatory param
	#	type = int, file
	o_parser.add_argument( 'align_file', nargs='?',
	                   type=file,
		               help='a tsv file with at least 7 cols (chr,s,e)x2,sign. default stdin.' )

	o_parser.add_argument( 'align_ref_file', nargs='?',
	                   type=file,
		               help='a tsv file with at least 7 cols (chr,s,e)x2,sign. default stdin.' )

	# optionnal
	o_parser.add_argument( '--verbose', '-v', action='store_true',
		               help='If set give info on what happen.' )

	o_parser.add_argument( '--te_gff', type=file,
		               help='a gff file with transposable element.' )
	#~ o_parser.add_argument( '-f', dest='opt1',
							#~ default='toto',
							#~ help='a flag with a value.' )

	#~ # mandatory option
	#~ #	destination is opt2 by default
	#~ o_parser.add_argument( '--opt2', '-o',
						#~ action='store_true', # store the value as boolean
						#~ help='a flag opt, long and short.')

	return o_parser

def analyseOptions( o_args ):

	global VERBOSE
	if( o_args.verbose ):
		VERBOSE = True


def verbose( msg ):
	if( VERBOSE ):
		sys.stderr.write( msg + "\n" )

# ----------------------------------------

if __name__ == '__main__':
	o_parser = defineOptions()
	o_args   = o_parser.parse_args()
	analyseOptions( o_args )
	#~ PP.pprint( o_args )

	main( o_args )
	# I lose stack and line number when try/except
	#~ try:
		#~ main( o_args )
	#~ except Exception as e:
		#~ sys.stderr.write( str( e ) )

	sys.exit( 0 )

