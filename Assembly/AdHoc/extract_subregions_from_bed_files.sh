#! /bin/bash

## AIMS  : 
## USAGE :
## ALGO  : 
#
## OUTPUT :
## OUTFILE:
#
## NOTE  : sequence file are compressed using bgzip, but bgzip is not able to extract *.bgz file, it needs .gz, clustalo is the same.
#
## BUG   : None found yet.

set -o nounset
set -o errexit

# =========================================================
declare -a A_BED_FILES=();
declare -r SCRIPT_PATH=$( dirname $( readlink -e $0 ) )

source $SCRIPT_PATH/../../rapsodynDB.sh
source $SCRIPT_PATH/../../brassicaDB.sh

# Cluster sourcing ## nounset detect PYTHONPATH does not exist

# ---------------------------------------------------------
declare -r ERR_NO_INPUT=1

declare VERBOSE=0
declare WITH_CONTIGS=0

# EXTRACT_PROG will take arguments as tsv file, human reading : first 2 bases = "seq_id\t1\t2\n"
declare -r EXTRACT_PROG="$DB__ROOT/generic/mfasta_extract_subseqs.py"

# =========================================================
function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT      = $0\n"
	printf "╟ - A_BED_FILES = ${A_BED_FILES[*]}\n"
	printf "╟\n"
	printf "╟ - WITH_CONTIGS= ${WITH_CONTIGS}\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

# =============================================
function manageOptions ()
{
# getopts reminder:
#	FLAG, x will look for -x
#	x: will look for -x stg
#	if the option string start with ':', getopts turns to silent error reporting mode.
	while getopts "hvc" opt
	do
		case $opt in
			c) WITH_CONTIGS=1	;;
			v)	VERBOSE=1	;;
			h)
				usage && exit 0 ;;
			\?)	usage && exit 0 ;;
		esac
	done
	shift $((OPTIND-1))

	A_BED_FILES+=( "$@" )

	if [[ 0 -eq ${#A_BED_FILES[*]} ]]
	then
		printf "/!\\ You have to give at least one bed filename.\n" >&2
		exit $ERR_NO_INPUT
	fi
}

function usage ()
{
		cat <<EOF
USAGE : $0 [options] bed_file [bed_file ...]

	bed_filepath  : the bed files to use to extract sequences.

Options :
	-v : verbose mode, see parameter values.
	-h : print this message and exit.
EOF
}

# =========================================================
function treat_bed_file ()
{
	local -r bed_file="$1"
	local cul

	while read id start end
	do
		#printf "$id\n" >&2

		# id is like $cul|$seq unless if ref where only chrom name is written
		if [[ "$id" == *"|"* ]]
		then
			cul=${id%|*}
			local seq=${id#*|}

			# bgz file does not have the _uid_* part in the scaff name.
			if [[ 0 -eq $WITH_CONTIGS ]]
			then
				seq_file=$( get_assembly_path $cul )/$cul.fa.bgz
			else
				seq_file=$( get_assembly_path $cul )/${cul}_k101.contigs.fa.bgz
			fi
		else
			# printf "Extract from ref.\n" >&2
			cul="ref"
			wait # a way not to mess the pysam index generation.
			seq=$id
			seq_file=$( get_ref_fasta Bnapus )
		fi

		printf "Extract from $cul ($seq: $start-$end).\n" >&2
#		cat <<EOL
		printf "$seq\t$start\t$end\n" \
				| "$EXTRACT_PROG" --tsv - --id_prefix "$cul" "$seq_file"
#EOL
	done < "$bed_file"
}

function main ()
{
	for bed in "${A_BED_FILES[@]}"
	do
		outfile=${bed/%bed/fa}
		rm -f "$outfile"

		treat_bed_file "$bed" \
				| bgzip -c \
			> "$outfile.gz" &
	done
	printf "Waiting for backgrounded job to terminante.\n" >&2
	wait
}

# =========================================================
manageOptions "$@"

if [[ 1 -eq $VERBOSE ]]
then
	printVars
fi


# your work.
main

printf "End of %s.\n\n" $( basename $0 )
exit 0
