#! /bin/bash

## AIMS  : Build p/a matrices about QTL.
## USAGE : ./qd_build_matrices_pa.sh
## NOTE  : 1 presence, -1 abcense, 0 no data
## NOTE  : This is just the script of what have been ran. -> Makefile ?
#
## OUTPUT : generate several files

set -o nounset
set -o errexit

declare -r cwd=$( dirname $( readlink -e $0 ) )
source "$cwd/lib.analyse_QTL.sh"

# =========================================================

declare -r QTL_FILE="QTL.bed"

# ---------------------------------------------------------
declare -r TE_GFF=$( jq_extract_from_json '.genomes.bnapus."v4.1".annot.te' )
declare GENE_GFF=$(  jq_extract_from_json '.genomes.bnapus."v4.1".annot.genes')
declare -r ALIGN_DIR="/omaha-beach/sletort/assemblage/Finishing/AlignOnRef"

# =========================================================
function init ()
{
	# generate a bed file containing the QTLs
	for qtl_id in "${!A_QTLs[@]}"
	do
		printf "${A_QTLs[$qtl_id]}\t$qtl_id\n"
	done > "$QTL_FILE"
}

function rename_matrices ()
{
	local -r suffix="$1"

	for qtl_id in "${!A_QTLs[@]}"
	do
		mv "$qtl_id.matrix.tsv" "$qtl_id.${suffix}.matrix.tsv"
		mv "$qtl_id.elt.bed" "$qtl_id.$suffix.bed"
	done
}

function main ()
{
	printf "Build TE matrices.\n"
	$__ROOT/Assembly/build_matrix_pa.sh \
			-b "$QTL_FILE" "$TE_GFF" "$ALIGN_DIR"
	rename_matrices "TE"
	mv elts.starch TE.starch

	printf "Build Gene matrices.\n"
	printf "Annotation file does not respect gff3 specification, score column is missing.\n"
	printf "\tI have to reformat it.\n"
	local -r gene_gff="genes.gff"
	perl -lane 'print join( "\t", @F[0..4], ".", @F[5..7] ) if( $F[2] eq "gene" );' "$GENE_GFF" \
		> $gene_gff
	$__ROOT/Assembly/build_matrix_pa.sh \
			-b "$QTL_FILE" "$gene_gff" "$ALIGN_DIR"
	rename_matrices "Gene"
	mv elts.starch Gene.starch
}

# =========================================================
# your work.
init

main

printf "End of %s.\n\n" $( basename $0 ) >&2
exit 0
