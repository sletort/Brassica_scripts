#! /bin/bash

INFILE=${1?"which infile ?"}

LINES=$( mktemp lines.XXXX )
grep -n duplication $INFILE \
		| perl -lane '($line,$size)= /(\d+):.+\((\d+) bases/; print( join( "\n", ($line-2),($line-1),$line ) ) if( $size > 500 );' \
	> $LINES
perl -lane 'BEGIN{ open( $f_, "<", "'$LINES'" ); @a_lines = <$f_>; } exit if( 0 == @a_lines ); if( $.== $a_lines[0] ){ print; shift @a_lines; }' $INFILE

rm $LINES
