#!/usr/bin/env python
# -*- coding: utf-8 -*-

## AIMS  : parse scaff_ocean file (generated in fillOceans.sh) to determine the sequence to extract from scaffolds.
## USAGE : generate_bedfile_for_scaff_ocean.py --scaff_len scaff_len.tsv scaff_ocean.tsv
## AUTHORS : sletort@irisa.fr


import sys
import pdb	# debug
import pprint # debug
import argparse	# option management, close to C++/boost library (?)
import os	# for local libraries
import csv	# manage tsv files also.
import itertools as it

# local libraries
f_path = os.path.dirname( os.path.realpath( __file__ ) )
sys.path.append( f_path + "/../../lib" )

import misc_assembly as ma
import RegionLink as rl

VERSION = "0.1"
PP = pprint.PrettyPrinter(indent=4)
VERBOSE   = False

# ========================================
# from itertools' doc
def grouper(iterable, n, fillvalue=None ):
	"Collect data into fixed-length chunks or blocks"
	# grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx
	args = [iter(iterable)] * n
	return it.izip_longest(fillvalue=fillvalue, *args)

# ========================================
def main( o_args ):
	with open( o_args.scaff_len, 'r' ) as o_fe:
		d_scaff_len = ma.load_seq_len( o_fe )

	o_file = csv.reader( o_args.infile, delimiter='\t' )
	for l_row in o_file:
		l_out = l_row[0:4]

		( cul, scaff_id ) = l_row[7].split( "|", 1 )
		o_scaff = rl.Region( *l_row[7:10] )
		
		if( int( l_row[1] )+1 == int( l_row[6] ) ):
			if( not o_scaff.reversed ):
				#print( "print scaff 3' part" )
				# manage error ?
				scaff_len = d_scaff_len[scaff_id]
				start = int( l_row[9] ) + 1
				l_out += [ l_row[7], str( start ), str( scaff_len ) ]
					
			else:
				# print( "print scaff 5' part" )
				start = int( l_row[9] ) - 1
				l_out += [ l_row[7], str( start ), "1" ]
			print( "\t".join( l_out ) )
		else:
			rec = "\t".join( l_out + l_row[7:8] )
			verbose( "look for fully hold (?) ocean [{0}].".format( rec ) )
			sys.stderr.write( "\t".join( l_row ) )


		# print( cul + " -- " + scaff_id + " == " + str( scaff_len ) )

		#~ o_scaff = rl.Region( l_row[4:3] )
		#~ if( o_ocean == cur_ocean ):
			#~ l_scaffs.add( l_row

	return 0

# ----------------------------------------
def defineOptions():
	descr = "parse scaff_ocean file (generated in fillOceans.sh) to determine the sequence to extract from scaffolds."
	o_parser = argparse.ArgumentParser( description=descr )

	# special
	o_parser.add_argument( '--version', action='version', version='%(prog)s ' + VERSION )

	# positionnal / mandatory param
	o_parser.add_argument( 'infile', nargs='?',
							type=argparse.FileType( 'r' ),
							default=sys.stdin,
							help='The scaff_ocean tsv file (ocean_region, id, ref_region, cul_region).' )

	# mandatory param
	o_parser.add_argument( '--scaff_len', '-S', required=True,
		               help='scaffold length file (id,len _tsv).' )

	# optionnal
	o_parser.add_argument( '--outdir', '-O',
							default='.',
		               help='Output directory.' )

	o_parser.add_argument( '--verbose', '-v', action='store_true',
		               help='If set give info on what happen.' )

	return o_parser

def analyseOptions( o_args ):

	global VERBOSE
	if( o_args.verbose ):
		VERBOSE = True


def verbose( msg ):
	if( VERBOSE ):
		sys.stderr.write( msg + "\n" )

# ----------------------------------------

if __name__ == '__main__':
	o_parser = defineOptions()
	o_args   = o_parser.parse_args()
	analyseOptions( o_args )
	# PP.pprint( o_args )

	main( o_args )

	sys.exit( 0 )

