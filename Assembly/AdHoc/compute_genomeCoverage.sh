#! /bin/bash

## AIMS  : compute different genome coverage by Rapsodyn cultivars
## USAGE : compute_genomeCoverage.sh FinishingDirectory
## NOTE  : FinishingDirectory is where have been generated the gs_bnapus_* dirs.
## AUTHORS : sletort@irisa.fr

set -o nounset
set -o errexit

source $__ROOT/brassicaDB.sh

# =========================================================
declare -r INDIR="${1?What is the Finishing directory ?}"
declare -r TYPE="${2?Working with scaffolds or contigs ?}"

# ---------------------------------------------------------
declare -r TEMPLATE_DIR="gs_bnapus_"
declare -r GENOME=$( get_ref_seq_len bnapus )

# =========================================================
function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT     = $0\n"
	printf "╟ - INDIR      = $INDIR\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

# =========================================================

function cultivar_coverage ()
{
	local keyword="$1"

	for f in $INDIR/${TEMPLATE_DIR}*
	do
		local cul=${f#$INDIR/$TEMPLATE_DIR}

		zgrep -w $keyword $f/against_ref/$TYPE/$cul.gff.gz \
			| bedtools merge -i -
	done \
	| sort -k 1,1 \
	| bedtools genomecov -g $GENOME -bga -i -
}

function scaffold_coverage ()
{
	local keyword="$1"

	for f in $INDIR/${TEMPLATE_DIR}*
	do
		local cul=${f#$INDIR/$TEMPLATE_DIR}

		zgrep -w $keyword $f/against_ref/$TYPE/$cul.gff.gz
	done \
	| sort -k 1,1 \
	| bedtools genomecov -g $GENOME -bga -i -
}

function conversion_message ()
{
	printf "to convert bedGraph to bigWig for JBrowse, do as follow on genouest server.\n"

	printf "source /softs/local/env/envucsc.sh\n"

	printf " bedGraphToBigWig infile.bed REF.seq_len.tsv outfile\n"
}

function main ()
{
	local outdir=$INDIR/genomeCoverage/$TYPE

	mkdir -p $outdir

	printf "Cultivar coverage (Overlapping scaffold count 1, N count 1).\n"
	cultivar_coverage "match" \
		> "$outdir/genomeCov.cultivars.withN.bed" &

	printf "Cultivar coverage (Overlapping scaffold count 1, N count 0).\n"
	cultivar_coverage "match_part"\
		> "$outdir/genomeCov.cultivars.withoutN.bed" &

	printf "Scaffold coverage (Overlapping scaffold count >1, N count 1).\n"
	scaffold_coverage "match" \
		> "$outdir/genomeCov.$TYPE.withN.bed" &

	printf "Scaffold coverage (Overlapping scaffold count >1, N count 0).\n"
	scaffold_coverage "match_part"\
		> "$outdir/genomeCov.$TYPE.withoutN.bed" &

	printf "Waiting for background job to terminate.\n"
	wait

	conversion_message | tee "$outdir"/conversion_to_bigwig
}

# =========================================================
printVars

# your work.
main

printf "End of %s.\n\n" $( basename $0 )

exit 0
