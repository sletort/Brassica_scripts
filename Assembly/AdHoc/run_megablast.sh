#! /bin/bash

# shell to use
#$ -S /bin/bash

#$ -M sletort@irisa.fr
# send mail before b, at the end e, on stop a, on suspend s, or never n
#$ -m bea

#$ -N run_megablast

# if the script should be run in the current directory
#$ -cwd

# merge stdout and stderr
#$ -j no


# ne pas utiliser, ça ne sert qu'à afficher ce qui va être envoyer à la queue ?
# check submitted task ( -> ça veut dire quoi ?)
##$ -verify


## AIMS  : run a job on the cluster biogenouest
## USAGE : run_cluster.tmpl.sh

# init env Mummer
source /local/env/envsamtools-1.3.sh
source /local/env/envblast-2.6.0.sh

PATH=$HOME/logiciels/bin:$PATH
export PATH

source /omaha-beach/sletort/rapsodynDB.sh
source /omaha-beach/sletort/brassicaDB.sh

#declare -r INFILE_TMPL="unmapped_scaff.fa"
#declare -r INFILE_TMPL="unmapped.not_big.fa"
declare -r INFILE_TMPL=${1?What is the template name ?}

function get_blast_dir ()
{
	# NOTE : still directory pb if depth change, many relative path need to be changed.
	local cul="$1"

	printf "/groups/rapsodyn/Assembly/AlignOnRef/gs_bnapus_$cul/scaff_analysis/mblast"
}

function make_intermediate_blastDB ()
{
	local cul="$1"

	local old_dir=$( pwd )
	local outdir=$( get_blast_dir $cul )

	mkdir -p $outdir
	cd $outdir

	local infile=../$cul.$INFILE_TMPL.gz
	local dbname=$( basename $infile .gz )

	bgzip -dc $infile > $dbname
	makeblastdb -in $dbname -dbtype nucl -parse_seqids &

	cd $old_dir
}

function all_but ()
{
	local cul="$1";
	local unwanted="$2";

	if [ $cul != $unwanted ];
	then
		local b_dir=$( get_blast_dir $cul )
		printf "../../$b_dir/$cul.$INFILE_TMPL "
	fi
}


function make_blast ()
{
	local old_dir=$( pwd )
	local outdir=$( get_blast_dir $cul )
	cd $outdir

	# make all-otherDB
	local DB_list=$( loopOverCultivars bnapus all_but $cul )
	blastdb_aliastool -dblist "$DB_list" -dbtype nucl \
		-out all_but_$cul -title all_but_$cul

	local headers='qseqid qstart qend sseqid sstart send'
	headers=$headers' sstrand qlen slen length'
	headers=$headers' score evalue pident nident mismatch'
	blastn -task megablast \
		-db all_but_$cul -query $cul.$INFILE_TMPL \
		-word_size 32 -perc_identity 95 -qcov_hsp_perc 80 \
		-outfmt "6 $headers" -out $cul.mblast.tsv \
		-num_threads 8

	cd $old_dir
}

# ---------------------------------------------
loopOverCultivars bnapus make_intermediate_blastDB
wait

loopOverCultivars bnapus make_blast

exit 0
