#! /bin/bash

## AIMS  : Compress several file after alignment of scaffolds on ref.
## USAGE : ./qd_finalize_alignment.sh
## NOTE  : starch format is bed+bzip, cf bedops

source /softs/local/env/envbedops-2.4.15.sh

set -o nounset
set -o errexit

source $__ROOT/brassicaDB.sh

# =========================================================
declare -r INDIR=${1?What is the directory containing the alignments ?}
declare -r TYPE=${2?What is the type of alignment (contigs or scaffs) ?}

# ---------------------------------------------------------

# =========================================================
function starch_alignment_file ()
{
	local cul="$1"
	local indir="$2"
	local type="$3"

	local data_dir=$indir/gs_bnapus_$cul/against_ref/$type
	{
		sort-bed $data_dir/$cul.tsv \
				| starch - \
			> $data_dir/$cul.starch
		rm -f $data_dir/$cul.tsv
	}&

}

function main ()
{
	printf "Starch final alignment file.\n"
	loopOverCultivars bnapus starch_alignment_file "$INDIR" "$TYPE"
	wait

	# compress gff files ?
}

# =========================================================
# your work.
main

printf "End of %s.\n\n" $( basename $0 ) >&2
exit 0
