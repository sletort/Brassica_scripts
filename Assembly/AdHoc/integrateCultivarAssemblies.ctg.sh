#! /bin/bash

## AIMS  : Extract information from the diversity of the cultivars/indiv. studied.
## USAGE : integrateCultivarAssemblies.sh FinishingDirectory
## NOTE  : This script is to ran after FinishingAssembly.sh
## NOTE  : Too AdHoc ! does not work with whatever directory => against_ref !
## TODO  : All in the Snakefile ?
## AUTHORS : sletort@irisa.fr

# needed by m_fasta* scripts to use pysam
# before set * because PYTHON_PATH unbound.
source /softs/local/env/envpython-2.7.sh
source /softs/local/env/envsamtools-1.3.sh
source /softs/local/env/envbedtools-2.25.0.sh

set -o nounset
set -o errexit

# =========================================================
declare -r INDIR="${1?What is the Finishing directory ?}"
declare -r TYPE="${2?Working with scaffolds or contigs ?}"

declare -r SCRIPT_PATH=$( dirname $( readlink -e $0 ) )

source $SCRIPT_PATH/../../brassicaDB.sh

# No more snakemake, can't install pysam with python3
# snakemake need python3 (installed via pip ? ie only for me !)
#source /softs/local/env/envpython-3.3.2.sh

# ---------------------------------------------------------

# =========================================================
function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT     = $0\n"
	printf "╟ - INDIR      = $INDIR\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

# =========================================================
function extractUnmapScaffolds ()
{
	printf "NOT TESTED YET\n" >&2

	local cul="$1"

  {
	#extract scaff id : len > 500 and at least 500 bases unmapped
	local perle='next if( $F[1]<500 ); if( 2 == @F ){ print $F[0]; }'
	perle=$perle' else{ for( $i=3; $i<@F; $i+=2 ){  $delta = $F[1] - $F[$i];'
	perle=$perle' if( 500 < $delta ){ print "$F[0]"; last; } } }'
	local infile="$INDIR/gs_bnapus_${cul}/against_ref/scaff_chrom_size.tsv"
	local outfile="${INDIR}/gs_bnapus_{cul}/unmapped_scaff.lst"
	perl -lane "$perle" $infile > $outfile

	# extract scaff seq
	local infile_id=$outfile
	local infile_seq=$( get_assembly_path $cul )/$cul.fa.gz
	outfile="${INDIR}/gs_bnapus_${cul}/${cul}.unmapped_scaff.sm.fa"
	$DB__ROOT/generic/mfasta_extract_seqs.py ${infile_id} ${infile_seq} \
	                | $DB__ROOT/generic/mfasta_prefix_seq_ids.py - "$cul|" \
	                            > ${outfile}
  }&
}

function extract_scaff_seq ()
{
	local cul="$1"
	local infile_id="$2"

	local outdir=$( dirname $infile_id )

	# extract scaff seq
	local infile_seq=$( get_assembly_path $cul )/$cul.fa.gz
	outfile="$outdir/${cul}.part_unmapped.fa"
	local tmp_file=$( mktemp XXXXX.mfasta )
	$DB__ROOT/generic/mfasta_extract_seqs.py ${infile_seq} ${infile_id} \
		> $tmp_file
	$DB__ROOT/generic/mfasta_prefix_seq_ids.py $tmp_file "$cul|" \
			| bgzip -c \
		> ${outfile}.gz
	rm "$tmp_file"
}

function extract_ref_alignment ()
{
	local cul="$1"
	local ref_align_file="$2"
	local outbase="$3"
	local scaff_id_file="$outbase.lst"


	local tmp_src=$( mktemp XXXXX.src.lst )
	local tmp_ids=$( mktemp XXXXX.scaff_ids.lst )
	local tmp_scaff_len=$( mktemp XXXXX.scaff_ids.lst )

	perl -p -e 's/_uid_\d+$//' "$scaff_id_file" \
			| sort -k 1b,1 \
		> "$tmp_ids"

	zcat "$ref_align_file" \
			| sort -k 4b,4 \
		> "$tmp_src"
	join -2 4 "$tmp_ids" "$tmp_src" \
			| perl -lane '$F[0] = "'$cul'|$F[0]"; print join( "\t", @F[1..3,0,4..$#F] );' \
			| sort-bed - | starch  - \
		> "$outbase.ref_align.starch"

	# scaff_len => revoir avec $TYPE
	zcat $( get_assembly_path $cul )/ctg.seq_len.tsv.gz \
			| perl -pe "s/_uid_\d+//; " \
			| sort -k 1b,1 \
		> "$tmp_scaff_len"
	# I did not rerun it since the -t addition
	join -t $'\t' "$tmp_ids" "$tmp_scaff_len" \
		> "$outbase.ctg_len.tsv"

	rm -f "$tmp_src" "$tmp_ids" "$tmp_scaff_len"
}

function extractPartiallyUnmappedContigs ()
{
	local cul="$1"
	local dir="$2"

	local work_dir="$INDIR/gs_bnapus_${cul}"

	# Note: scaff_chrom_size.tsv is not a well named file anymore, it contains contigs length.
	local infile="$work_dir/against_ref/$TYPE/scaff_chrom_size.tsv"
	if [[ ! -e "$infile.gz" ]]
	then
		printf "$infile does not exist.\n" >&2
		return 1
	fi

	local outdir="$work_dir/$dir"
	mkdir -p "$outdir"
	local outbase="$outdir/part_unmapped"

  {
	#extract scaff id : len > 500 and at least 500 bases unmapped and at least one chrom
	# also do not keep scaff that already align all along a chrom.
	local perle='next if( @F < 6 || $F[1]<500 ); '
	perle=$perle' $go = 0; for( $i=3; $i<@F; $i+=2 ){ '
	perle=$perle' if( $F[$i] == $F[1] ){ $go = 0; last; }'
	perle=$perle' $delta = $F[1] - $F[$i];'
	perle=$perle' if( 500 < $delta ){ $go = 1; } }'
	perle=$perle' print "$F[0]" if( 1 == $go );'
#	perl -lane "$perle" $infile > $outfile
	zcat $infile.gz | perl -lane "$perle" > $outbase.lst

	# no use since blast is abandonned
	# extract_scaff_seq $cul "$outbase.lst"

	local ref_align_file="$work_dir/against_ref/$TYPE/$cul.tsv.gz"
	extract_ref_alignment $cul "$ref_align_file" "$outbase"
  }&
}

function main ()
{
	printf "1- Reference coverage.\n"
#	$SCRIPT_PATH/compute_genomeCoverage.sh "$INDIR" "$TYPE"

	# directory under ${INDIR}/gs_bnapus_${cul}/
	local outdir="ctg_analysis"

	printf "2.1- Extraction of partially unmapped contigs.\n"
#	loopOverCultivars bnapus extractPartiallyUnmappedContigs "$outdir"
	extractPartiallyUnmappedContigs montego "$outdir"
	extractPartiallyUnmappedContigs tenor "$outdir"
	wait

#	printf "2.2- Extraction of unmapped scaffolds.\n"
#	loopOverCultivars bnapus extractUnmapScaffolds
#	wait

#	CUL=$( loopOverCultivars bnapus echo ) snakemake -s $SCRIPT_PATH/extract_unmapped.Snakefile all

}

# =========================================================
printVars

# your work.
main

printf "End of %s.\n\n" $( basename $0 )

exit 0
