#! /bin/bash

## AIMS  : run clustal-omega on each mfasta file of the 'part unaligned' data.
## USAGE : ./run_clustalO.sh fasta_dir
## NOTE  : launch qsub on each file.
## AUTHORS : sletort@irisa.fr

set -o nounset
set -o errexit

# =========================================================
declare -r INDIR="${1?What is your fasta dir ?}"

# ---------------------------------------------------------
declare -r ROOT_=$( dirname $( readlink -e $0 ) )/../../
declare -r PROG=$ROOT_/misc/run_cluster.clustalO.sh

# =========================================================
function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT   = $0\n"
	printf "╟ - INDIR    = $INDIR\n"
	printf "╟ - PROG     = $PROG\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

# =========================================================
function main ()
{
	for d in $INDIR/*
	do
		printf "$d\n"
		for f in $d/*.fa.gz
		do
			qsub $PROG $f
		done
	done

}

# =========================================================
printVars

# your work.
main

printf "End of %s.\n\n" $( basename $0 )

exit 0
