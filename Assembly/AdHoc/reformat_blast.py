#!/usr/bin/env python
# -*- coding: utf-8 -*-

## AIMS  : remove _uid_* from scaff id and sort file on chrom-start-end reorder link with ref on strand +.
## USAGE : reformat_blast.py mblast.tsv > reformated.blast
## AUTHORS : sletort@irisa.fr
## NOTE  : scaff id is expected on column 4
## NOTE  : chrom is expected on column 1
## NOTE  : start & end are columns 2,3 (can be start > end)

import sys
import pprint # debug
import argparse	# option management, close to C++/boost library (?)

import csv	# manage tsv files also.

import os	# for local libraries

# local libraries
f_path = os.path.dirname( os.path.realpath( __file__ ) )
sys.path.append( f_path + "/../../lib" )

from MiscSeq import MiscSeq
from lib_only_QTL import *

VERSION   = "0.1"
PP = pprint.PrettyPrinter(indent=4)
VERBOSE   = False

# ========================================
class BlastResultReformatorIterator( BlastResultIterator ):
	def next( self ):
		return BlastResultReformator( next( self._it ) )

class BlastResultReformator( BlastResult ):
	'''
		ref_id, start, end
		scaff_id, start, end
		strand
		scaff_len length
		score evalue pident nident mismatch
		sseq qseq
		filter_score
	'''
	def __init__( self, l_elts ):
		self.__l_elts = l_elts # to simplify the réécriture
		super( self.__class__, self ).__init__( l_elts )

		# complicated things
#		o_ref = rl.Region( l_elts[0], *[ int(x) for x in l_elts[1:3] ] )

#		scaff_id = self.__reformat_scaff_id( l_elts[3] )
#		o_tgt = rl.Region( scaff_id, *[ int(x) for x in l_elts[4:6] ] )

#		strand = self.d_strand[ l_elts[6] ]

#		self.__o_link = rl.RegionLink( o_ref, o_tgt, strand )

	def recString( self ):
		# back to the original for chrom id
		#~ self.o_link.o_ref.chrom = self.__l_elts[0]
		str_link = self.o_link.recString( mode='ref' )

		if( -1 == self.o_link.strand ):
			seq_ref = MiscSeq.complement( self.seq_ref )
			seq_tgt = MiscSeq.complement( self.seq_tgt )
		else:
			seq_ref = self.seq_ref
			seq_tgt = self.seq_tgt

		l_elts   = [ str_link ]
		l_elts  += self.__l_elts[6:14]
		l_elts  += [ seq_ref, seq_tgt, self.__l_elts[-1] ]
		return "\t".join( l_elts )

# ========================================
def main( o_args ):

	verbose( "Reading Blast results" )
	o_tsv = csv.reader( o_args.infile, delimiter='\t' )

	lo_br = []
	for o_reformator in BlastResultReformatorIterator( o_tsv ):
		lo_br.append( o_reformator )

	for o_br in sorted( lo_br, key=lambda x: x.o_link.o_ref ):
		o_args.outfile.write( o_br.recString() + "\n" )


	return 0


# ----------------------------------------
def defineOptions():
	descr = "reformat blast result (part of the only_QTL.sh pipeline)."
	o_parser = argparse.ArgumentParser( description=descr )

	# special
	o_parser.add_argument( '--version', action='version', version='%(prog)s ' + VERSION )

	# positionnal / mandatory param
	o_parser.add_argument( 'infile', nargs='?',
							type=argparse.FileType( 'r' ),
							default=sys.stdin,
							help='The tsv blast result (produced by only_QTL.sh for the moment).' )

	# optionnal
	o_parser.add_argument( '--outfile', '-o',
						type=argparse.FileType( 'w' ),
						default=sys.stdout,
		               help='outfile, = entry + one score column.' )

	o_parser.add_argument( '--verbose', '-v', action='store_true',
		               help='If set give info on what happen.' )

	return o_parser

def analyseOptions( o_args ):

	global VERBOSE
	if( o_args.verbose ):
		VERBOSE = True

	return

def verbose( msg ):
	if( VERBOSE ):
		sys.stderr.write( msg + "\n" )

# ----------------------------------------

if __name__ == '__main__':
	o_parser = defineOptions()
	o_args   = o_parser.parse_args()
	analyseOptions( o_args )
	# PP.pprint( o_args )

	main( o_args )

	sys.exit( 0 )

