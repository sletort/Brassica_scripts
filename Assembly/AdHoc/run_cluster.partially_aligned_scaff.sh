#! /bin/bash

# shell to use
#$ -S /bin/bash

#$ -M sletort@irisa.fr
# send mail before b, at the end e, on stop a, on suspend s, or never n
#$ -m bea

#$ -N partially_aligned_scaff.sh

# if the script should be run in the current directory
#$ -cwd

# merge stdout and stderr
#$ -j no


# ne pas utiliser, ça ne sert qu'à afficher ce qui va être envoyer à la queue ?
# check submitted task ( -> ça veut dire quoi ?)
##$ -verify

# nb CPU to use.
#$ -pe make 29

## AIMS  : run a job on the cluster biogenouest
## USAGE : run_cluster.tmpl.sh

# to access software I installed locally (jq,pigz)
source $HOME/.bashrc

set -o nounset
set -o errexit


# Rapsodyn environnement
declare -r WORKDIR=/omaha-beach/sletort/

source $WORKDIR/rapsodynDB.sh

# work script
$__ROOT/Assembly/AdHoc/qd_analyse_ctg_from_scaff_part_unmapped.sh "$@"

exit 0

