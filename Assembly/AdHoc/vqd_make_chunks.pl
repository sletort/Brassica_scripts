#! /usr/bin/env perl

## AIMS  : make chunk list of sequences given the basename of the 2bit and chom.sizes file.
## NOTES : to use for blat alignment + liftUp (liftUp aims to merge the results

use strict;
use warnings;

use IO::File;
use File::Slurp;

my $BNAME     = $ARGV[0];

print "Parsing $BNAME\n";

# lst = list of seq. chunks
# lft = 
my $o_lst = new IO::File( "$BNAME.lst", "w" );
my $o_lft = new IO::File( "$BNAME.lft", "w" );

my $seq_file  = "../$BNAME.2bit";
my $size_file = "../$BNAME.chrom.sizes";


# use globals : $seq_file, $o_lst, $o_lft
sub make_chunks
{
	my( $seq_id, $seq_size ) = @_;
	
	my $chunk_start = 0;
	my $chunk_end   = 0;
	while( $chunk_end < $seq_size )
	{
		$chunk_end = $chunk_start + 5000;
		if( $seq_size < $chunk_end )
		{	$chunk_end = $seq_size;	}

		my $range = "$chunk_start-$chunk_end";
		print $o_lst "$seq_file:$seq_id:$range\n";

		my @a_elts = ( $chunk_start, $seq_id );
		if( 0 == $chunk_start && $chunk_end == $seq_size )
		{
			push( @a_elts, $seq_size );
		}
		else
		{
			$a_elts[-1] .= ":$range";

			my $chunk_size = $chunk_end - $chunk_start;
			push( @a_elts, $chunk_size );
		}
		print $o_lft join( "\t", @a_elts, $seq_id, $seq_size ), "\n";

		$chunk_start = $chunk_end;
	}

	return;
}

for my $ra_ ( map{ [ split /\t/ ] } read_file( $size_file, { chomp => 1 } ) )
{
	my( $seq_id, $seq_size ) = @$ra_;

	#~ if( $seq_size < 5000 )
	{	make_chunks( $seq_id, $seq_size );	}
}

map{ $_->close() } ( $o_lft, $o_lst );

__END__
