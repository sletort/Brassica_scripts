INDIR=/omaha-beach/sletort/assemblage/Finishing
SHARED_IN=shared_seqs.in.bed
SHARED_OUT=shared_seqs.out.bed
SHARED_ALL=shared_seqs.all.bed

function appendNodes ()
{
	local cul="$1"
	local in="$2"
	local out="$3"

	local infile="$INDIR/gs_bnapus_$cul/mblast/$cul.mblast.tsv"

	cut -f 1-3 $infile \
		>> $in

	perl -lane '($s,$e) = ($F[4]<$F[5])? @F[4,5] : @F[5,4]; print join( "\t", $F[3],$s,$e); ' $infile \
		>> $out
}

tmp_in=$( mktemp shared_in.XXXX.bed )
tmp_out=$( mktemp shared_out.XXXX.bed )

loopOverCultivars bnapus appendNodes $tmp_in $tmp_out

# convertir ici en starch ?
sort -V $tmp_in  > $SHARED_IN &
sort -V $tmp_out > $SHARED_OUT &
wait

sort -m $SHARED_IN $SHARED_OUT > $SHARED_ALL

# les SHARED_* contiennent des lignes duppliqués : un indice
for f in $SHARED_IN $SHARED_OUT $SHARED_ALL
do
	local bname=$( basename $f .bed )
	bedops --partition $f > $bname.chunks.bed &
done
wait

# les partitions ne contiennent plus de lignes dupliquées.

