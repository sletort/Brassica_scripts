#!/usr/bin/env python
# -*- coding: utf-8 -*-

## AIMS  : .
## USAGE : 
## AUTHORS : KooK.dev@free.fr


import sys
import pdb	# debug
import pprint # debug
import argparse	# option management, close to C++/boost library (?)
import os	# for local libraries
import csv	# manage tsv files also.

from collections import defaultdict

# local libraries
f_path = os.path.dirname( os.path.realpath( __file__ ) )
sys.path.append( f_path + "/../lib" )

VERSION = "0.1"
PP = pprint.PrettyPrinter(indent=4)
VERBOSE   = False

# ========================================
def load_infile( o_fe ):
	o_file = csv.reader( o_fe, delimiter='\t' )

	d_aligns = defaultdict( list )
	for l_row in o_file:
		ocean = ".".join( l_row[0:3] )

		# start end on ref and target
		for i in ( 4,5, 7,8 ):
			l_row[i] = int( l_row[i] )

		d_aligns[ocean].append( l_row[3:] )

	return d_aligns

def analyse_alignments( outdir, d_aligns ):

	for ocean, l_aligns in d_aligns.iteritems():
		if( 1 == len( l_aligns ) ):
			continue

		# determine the longuest shared sequence region [ max_start, min_end ]
		min_end   = None
		max_start = None
		for l_row in l_aligns:
			if( None == min_end or l_row[2] < min_end ):
				min_end = l_row[2]
			if( None == max_start or max_start < l_row[1] ):
				max_start = l_row[1]

		l_elts  = ocean.split( "." )
		l_elts += [ str( max_start ), str( min_end ) ]
		outfile = outdir + '/' + ocean + '.regions.tsv'
		with open( outfile, 'w' ) as o_fs:
			o_fs.write( "\t".join( l_elts ) + "\n" )

		# print a bed file for those region
		outfile = outdir + '/' + ocean + '.bed'
		with open( outfile, 'w' ) as o_fs:
			for l_row in l_aligns:
				delta_start = max_start - l_row[1]
				delta_end   = l_row[2]  - min_end
				if( l_row[5] < l_row[4] ):
					start = l_row[4] - delta_start
					end   = l_row[5] + delta_end
				else:
					start = l_row[4] + delta_start
					end   = l_row[5] - delta_end

				l_elts = [ str( x ) for x in ( l_row[3], start, end ) ]
				o_fs.write( "\t".join( l_elts ) + "\n" )


def main( o_args ):
	d_aligns = load_infile( o_args.infile )
	# PP.pprint( d_aligns )

	analyse_alignments( o_args.outdir, d_aligns )

	return 0

# ----------------------------------------
def defineOptions():
	o_parser = argparse.ArgumentParser( description='Prog descr.' )

	# special
	o_parser.add_argument( '--version', action='version', version='%(prog)s ' + VERSION )

	# positionnal / mandatory param
	o_parser.add_argument( 'infile', nargs='?',
							type=argparse.FileType( 'r' ),
							default=sys.stdin,
							help='The infile.' )

	# optionnal
	o_parser.add_argument( '--outdir', '-O',
							default='.',
		               help='Output directory.' )

	o_parser.add_argument( '--verbose', '-v', action='store_true',
		               help='If set give info on what happen.' )

	return o_parser

def analyseOptions( o_args ):

	global VERBOSE
	if( o_args.verbose ):
		VERBOSE = True


def verbose( msg ):
	if( VERBOSE ):
		sys.stderr.write( msg + "\n" )

# ----------------------------------------

if __name__ == '__main__':
	o_parser = defineOptions()
	o_args   = o_parser.parse_args()
	analyseOptions( o_args )
	# PP.pprint( o_args )

	main( o_args )

	sys.exit( 0 )

