#!/usr/bin/env python
# -*- coding: utf-8 -*-

## AIMS  : detect strech of N in fasta file and output as bed file.
## USAGE : detectOcean.py fasta_file > out.bed
## AUTHORS : sebastien.letort@irisa.fr


import sys
import pdb	# debug
import pprint # debug
import argparse	# option management, close to C++/boost library (?)
import os	# for local libraries
import csv	# manage tsv files also.
from collections import defaultdict
from collections import Counter
import pysam
from Bio.Seq import Seq
import copy
import re

# local libraries
f_path = os.path.dirname( os.path.realpath( __file__ ) )
sys.path.append( f_path + "/../../lib" )

import RegionLink as rl
import FeatureOutputWriter as fow
import Rapsodyn
import MFasta

VERSION = "1.0"
PP = pprint.PrettyPrinter( indent=4, stream=sys.stderr )
VERBOSE = False
DEBUG   = False

MIN_UNALIGNED_LEN = 500

# ========================================
class myRegion( rl.Region ):
	def __sub__( o_a, o_b ):
		if( not isinstance( o_a, rl.Region ) ):
			raise Exception( "First param is not a Region instance." )
		if( not isinstance( o_b, rl.Region ) ):
			raise Exception( "Second param is not a Region instance." )

		if( not o_a.includes( o_b ) ):
			raise Exception( "Second parameter is not included in the first one." )

		lo_regions = []
		if( o_a.start != o_b.start ):
			lo_regions.append( rl.Region( o_a.start, o_b.start-1 ) )

		if( o_b.end != o_a.end ):
			lo_regions.append( rl.Region( o_b.end+1, o_a.end ) )

		return lo_regions

class InfilesLoader( object ):
	def __init__( self, do_links, d_scaff_len ):
		self.__do_links    = do_links
		self.__d_scaff_len = d_scaff_len
		self.__id_pattern  = re.compile( '\.\d+$' )

	@property
	def d_scaffs( self ):
		return self.__d_scaff_len

	@property
	def do_links( self ):
		return self.__do_links

	def getScaffLen( self, scaff_id ):
		#if( '.' in scaff_id ):
		scaff_id = self.__id_pattern.split( scaff_id )[0]

		return self.d_scaffs[scaff_id]

	def removeDubiousLinks( self ):
		'''If a scaff contains a big gap between 2 alignments
		 the scaff will be split, with 2 ids.'''
		def __are_dubious_links( o_l1, o_l2, scaff_len ):
			s = o_l2.o_ref.start
			e = o_l1.o_ref.end
			delta  = s - e
			# print( "{0} - {1} = {2}".format( str( s ), str( e ), str( delta ) ) )
			# distribution of final sequence shows that Q3 seq len ~8000 on chrom A10
			# scaff len max = 79783
			return ( delta > scaff_len
					and delta > 100000 )

		for scaff_id,lo_links in self.do_links.items():
			#if( 'aviso|scaffold_16789' != scaff_id ):
			#	continue
			if( 1 == len(lo_links) ):
				continue

			scaff_len = self.getScaffLen( scaff_id )

			last_id = len( lo_links )
			j = 1
			for i in reversed( range( len( lo_links ) - 1 ) ):
				# print( "L1\t" + lo_links[i].recString() )
				# print( "L2\t" + lo_links[i+1].recString() )
				if( __are_dubious_links( lo_links[i], lo_links[i+1], scaff_len ) ):
					# print( "DEL" )
					# del lo_links[i+1]
					new_id = scaff_id + "." + str( j )
					self.do_links[new_id] = lo_links[i+1:last_id]
					j += 1
					last_id = i+1
			if( j != 1 ):
				new_id = scaff_id + "." + str( j )
				self.do_links[new_id] = lo_links[0:last_id]
				del self.do_links[scaff_id]

		return

	def __load_ref_align( cls, o_ref_align ):
		# o_ref_align => RegionLinks
		of_align = csv.reader( o_ref_align, delimiter='\t' )
		lo_links = rl.RegionLink.loadSimpleLinkFile( of_align )

		# in those links ref is the true Reference sequence
		do_links = defaultdict( list )
		for o_link in lo_links:
			do_links[ o_link.o_tgt.chrom ].append( o_link )

		return do_links
	__load_ref_align = classmethod( __load_ref_align )

	def __load_seq_len( cls, o_scaff_len ):
		of_seq = csv.reader( o_scaff_len, delimiter='\t' )
		d_scaff_len = {}
		for seq_id,seq_len in of_seq:
			d_scaff_len[seq_id] = int( seq_len )

		return d_scaff_len
	__load_seq_len = classmethod( __load_seq_len )

	def load( cls, o_ref_align, o_scaff_len ):
		'''Objects are file handles on tsv files.'''
		do_links = cls.__load_ref_align( o_ref_align )
		d_scaff_len = cls.__load_seq_len( o_scaff_len )

		return InfilesLoader( do_links, d_scaff_len )
	load = classmethod( load )	

class GroupExtractor( object ):
	ANCHOR_SIZE = 100 # keep 100 bases that align the ref.
	MIN_UNALIGNED_LEN = MIN_UNALIGNED_LEN

	def __init__( self, lo_links ):

		#sys.stderr.write( str( type( self ) ) + "\n" )
		self.__lo_links = lo_links
		# TODO stocker la liste triée sur ref_start/end pour faciliter la determination des ref_regions

		self.__o_ref     = None
		self.__o_infiles = None
		#for o_ in self.__lo_links:
		#	print "===\t" + o_.recString()

	@property
	def lo_links( self ):
		return self.__lo_links
	@property
	def g_start( self ):
		return self.__o_ref.start
	@property
	def g_end( self ):
		return self.__o_ref.end
	@property
	def o_infiles( self ):
		return self.__o_infiles
	@o_infiles.setter
	def o_infiles( self, o_infiles ):
		self.__o_infiles = o_infiles

	#~ def __setGroupLimit( self ):
		#~ ( g_start, g_end ) = self._getGroupLimits()
		#~ self.__g_start  = g_start
		#~ self.__g_end    = g_end

	def ref_region( self ):
		return self.__o_ref

	def getGroupsTargetRegions( self ):
		i=0
		while( 1 < len( self.lo_links ) ):
			self.__o_ref = self._getGroupRegion() # pas vraiment top ça une méthode protégée qui va initialiser un attribut privé.
			if( DEBUG ):
				sys.stderr.write( "\tnx groupe\n" )
				sys.stderr.write( "\tref : " + str( self.__o_ref ) + "\n" )
				sys.stderr.write( "\tnb links : " + str( len( self.lo_links ) ) + "\n" )

			lo_regions = list( self.__getGroupTargetRegions() )
			if( 2 <= len( lo_regions ) ):
				yield lo_regions
			else:
				verbose( "\t*There are less than 2 scaffolds that share valuable sequences, region is skipped." )
			i += 1

		if( DEBUG ):
			sys.stderr.write( "FIN- getGroupsTargetRegions\n" )
	# getGroupsTargetRegions

	def __getGroupTargetRegions( self ):
		lo_next_links = []
		ref_len = self.ref_region().length()
		ref_len_thres = 0.9 * ref_len

		for o_link in self.lo_links:
			verbose( o_link.recString() )

			if( o_link.o_ref.end < self.g_start # condition for upstream and downstream ?
				or self.g_end < o_link.o_ref.start ):
				lo_next_links.append( o_link )
				continue

			( s,e, next_turn ) = self._getGroupTargetRegion( o_link )

			# there is probably a better place to test this
			if( ref_len >= self.MIN_UNALIGNED_LEN ):
				# if( o_link.o_tgt.seq_id.startswith( "amber|scaffold_10659" ) ):
				# PP.pprint({ 's': s, 'e': e })

				if( s < 1 ): s = 1
				if( e < 1 ): e = 1
				scaff_len = self.o_infiles.getScaffLen( o_link.o_tgt.seq_id )
				if( scaff_len < e ): e = scaff_len
				if( scaff_len < s ): s = scaff_len

				#if( o_link.o_tgt.seq_id.startswith( "amber|scaffold_10659" ) ):
				# PP.pprint({ 's': s, 'e': e })

				if( o_link.o_tgt.reversed ): # <- dans quel cas l'utiliser ? cas reversed sur une analyse downstream => ça ne fonctionne pas, cf aviso|scaffold_16789
					length = s - e
				else:
					length = e - s
				# e/s > scaff end ?
				#~ length = max( e, s ) - min( e, s )
				if( length < self.MIN_UNALIGNED_LEN ):
					verbose( "\t" + o_link.o_tgt.seq_id + " doesn't align with enough bases, skipped." )
				elif( length < ref_len_thres ):
					verbose( "\t" + o_link.o_tgt.seq_id + " too short compared to the region, skipped." )
				else:
					verbose( "\tadded" )
					yield rl.Region( o_link.o_tgt.chrom, s,e )
			# too small soi cannot be yield

			if( next_turn ):
				lo_next_links.append( o_link )
			elif( DEBUG ):
				sys.stderr.write( "\t\tlink excluded from next turn.\n" )
				sys.stderr.write( "\t\t" + o_link.recString() + "\n\n" )

		# passer par un autre objet ? Là on redéfini completement celui ci.
		self.__lo_links = lo_next_links
	# getGroupTargetRegions
# GroupExtractor

class GroupExtractorUpstream( GroupExtractor ):
	def __my_max( self, l, previous, lower_limit=None ):
		'''return the highest value lower than or equal to previous-MIN
			unless this value is lt lower_limit in which case the next highest value will be return.'''
		thres = previous - self.MIN_UNALIGNED_LEN
		#sys.stderr.write( "thres = " + str( thres ) + "\n" )

		l.sort( reverse = True )
		for i in range( len(l) ):
			#sys.stderr.write( "Li = " + str( l[i] ) + "\n" )
			if( l[i] <= thres ):
				if( lower_limit and l[i] < lower_limit ):
					return l[i-1]
				return l[i]
		return l[-1]
	# __my_max
			
	def _getGroupRegion( self ):
		if( 0 == len( self.lo_links ) ):
			return None

		if( self.ref_region() ):
			g_start  = self.__my_max([ x.o_ref.start for x in self.lo_links ], self.g_start )
			g_end    = self.__my_max([ x.o_ref.end   for x in self.lo_links ], self.g_end, g_start )
		else:
			g_start = max([ x.o_ref.start for x in self.lo_links ])
			g_end   = max([ x.o_ref.end   for x in self.lo_links ])
		g_end  += self.ANCHOR_SIZE
		return self.lo_links[0].o_ref.subRegion( start = g_start, end = g_end )
		#~ return( g_start, g_end + self.ANCHOR_SIZE )

	def _getGroupTargetRegion( self, o_link ):
		delta_start = self.g_start - o_link.o_ref.start
		delta_end   = self.g_end   - o_link.o_ref.end
		# PP.pprint({ 'link': o_link.recString(), 'ds': delta_start, 'de': delta_end })
		if( not o_link.o_tgt.reversed ):
			start = o_link.o_tgt.start + delta_start
			end   = o_link.o_tgt.end   + delta_end
		else:
			start = o_link.o_tgt.end   - delta_start
			end   = o_link.o_tgt.start - delta_end

		next_turn = self.MIN_UNALIGNED_LEN < delta_start

		return( start, end, next_turn )

class GroupExtractorDownstream( GroupExtractor ):
	def __my_min( self, l, previous, upper_limit=None ):
		'''return the lowest value greater than or equal to previous+MIN
			unless this value is gt upper_limit in which case the next lowest value will be return.'''
		thres = previous + self.MIN_UNALIGNED_LEN
		#sys.stderr.write( "thres = " + str( thres ) + "\n" )

		l.sort()
		for i in range( len(l) ):
			# sys.stderr.write( "Li = " + str( l[i] ) + "\n" )
			if( thres <= l[i] ):
				if( upper_limit and upper_limit < l[i] ):
					return l[i-1]
				return l[i]
		return l[-1]
	# __my_min
			
	def _getGroupRegion( self ):
		if( 0 == len( self.lo_links ) ):
			return None

		if( self.ref_region() ):
			g_end    = self.__my_min([ x.o_ref.end   for x in self.lo_links ], self.g_end )
			g_start  = self.__my_min([ x.o_ref.start for x in self.lo_links ], self.g_start, g_end )
		else:
			g_end    = min([ x.o_ref.end   for x in self.lo_links ])
			g_start  = min([ x.o_ref.start for x in self.lo_links ])

		g_start -= self.ANCHOR_SIZE

		return self.lo_links[0].o_ref.subRegion( start = g_start, end = g_end )
		#~ return( g_start - self.ANCHOR_SIZE, g_end )

	def _getGroupTargetRegion( self, o_link ):
		delta_start = o_link.o_ref.start - self.g_start
		delta_end   = o_link.o_ref.end   - self.g_end
		# PP.pprint({ 'Dlink': o_link.recString(), 'ds': delta_start, 'de': delta_end })
		if( not o_link.o_tgt.reversed ):
			start  = o_link.o_tgt.start - delta_start
			end    = o_link.o_tgt.end   - delta_end
			#~ length = end - start
		else:
			start  = o_link.o_tgt.end   + delta_start
			end    = o_link.o_tgt.start + delta_end
			#~ length = start - end

		next_turn = self.MIN_UNALIGNED_LEN < delta_end

		#~ # length already computed -> l'extraire du test ci dessus pour le remettre plus bas ?
		#~ if( s < 1 ): s = 1
		#~ if( e < 1 ): e = 1

		#~ # e/s > scaff end ?
		#~ if( length < self.MIN_UNALIGNED_LEN ):
			#~ verbose( "\t" + o_link.o_tgt.seq_id + " doesn't align with enough bases, skipped." )

		return( start, end, next_turn )

class GroupExtractorFactory( object ):
	def makeIt( cls, lt_link_keys ):
		# check that all keys are the same.
		c = Counter([ x[1] for x in lt_link_keys ])
		#~ if( 1 != len( c.keys() ) ):
			#~ msg = "The group has different keys, not yet managed !\n"
			#~ for o_link,key in lt_link_keys:
				#~ msg += str( o_link ) + " --- " + key + "\n"
			#~ raise Exception( msg )

		lo_links = [ x[0] for x in lt_link_keys ]
		#~ if( 'up' in c ):
			#~ return GroupExtractorUpstream( lo_links )
		#~ if( 'down' in c ):
			#~ return GroupExtractorDownstream( lo_links )
		if( 1 == len( c ) ):
			if( 'up' in c ):
				return GroupExtractorUpstream( lo_links )
			if( 'down' in c ):
				return GroupExtractorDownstream( lo_links )
		else:
			msg  = "key links are not unique.\n"
			msg += str( c ) + "\n"
			msg += "Take the most present."
			key = c.most_common( 1 )[0][0] # 1st tuple element of the uniq element in the list
			if( 'up' == key ):
				return GroupExtractorUpstream( lo_links )
			if( 'down' == key ):
				return GroupExtractorDownstream( lo_links )

			err_msg  = "One key is not 'up' nor 'down'.\n"
			err_msg += str( c ) + "\n"
			raise Exception( err_msg )
		
		raise Exception( "error in lt_link_keys : no key\n" )
	makeIt = classmethod( makeIt )

#~ class SequenceRetriever( object ):
	
# ========================================
def main( o_args ):
	verbose( "Load infiles." )
	o_files = InfilesLoader.load( o_args.align_ref_file, o_args.scaff_len )
	'''
	for k,lo_links in o_files.do_links.items():
		print( "id = " + k )
		for o_link in lo_links:
			print( o_link.recString() )
		print( "---" )
	'''

	o_files.removeDubiousLinks()
	'''
	for k,lo_links in o_files.do_links.items():
		print( "id = " + k )
		for o_link in lo_links:
			print( o_link.recString() )
		print( "###" )
	'''

	if( DEBUG ):
		generate_align_gff( o_files.do_links )

	verbose( "extraction of the unmatched sequences." )
	# do_ = [ref_chrom][(region,key),(region,key)]
	do_regions = turn_align_intervals_to_unalign_intervals( o_files )
	if( DEBUG ):
		generate_unalign_gff( do_regions )

	verbose( "identify regions of interest." )
	dlt_ROI = identify_regions_of_interest( do_regions )
	if( DEBUG ):
		generate_ROI_gff( dlt_ROI )

	verbose( "determine sequences of interest." )
	llo_SOI = determine_sequences_of_interest( dlt_ROI, o_files )
	if( DEBUG ):
		generate_SOI_gff( llo_SOI )

	verbose( "generate bed files." )
	generateBedfiles( o_args.outdir, llo_SOI )

	return 0
# main

# ===========DEBUG FUNC=======================
def generate_align_gff( do_links ):
	with fow.OutGff( "align.dbg.gff" ) as o_gff:
		o_gff.comment( "Alignment input." )

		for scaff_id,lo_links in do_links.items():
			
			g_start = min( x.o_ref.start for x in lo_links )
			g_end   = max( x.o_ref.end   for x in lo_links )
			o_r = rl.Region( lo_links[0].o_ref.seq_id, g_start, g_end )
			o_gff.writeMatch( o_r, scaff_id, strand=lo_links[0].strand_symbol )
			for i in range( len( lo_links ) ):
				o_gff.writeMatchPart( lo_links[i],
									ID="{0}_{1}".format( scaff_id, str( i+1 ) ),
									Parent=scaff_id )
# generate_align_gff	

def generate_unalign_gff( dlt_links ):
	with fow.OutGff( "unalign.dbg.gff" ) as o_gff:
		o_gff.comment( "unAlignment input." )

		for ref_chrom,lt_links in dlt_links.items():
			o_gff.comment( ref_chrom )

			# make one match per tgt scaff
			dlt_ = defaultdict( list )
			for o_link,key in lt_links:
				dlt_[o_link.o_tgt.seq_id].append( o_link )

			for scaff_id,lo_links in dlt_.items():
				lo_links.sort( key=lambda x:x.o_ref )
				s = lo_links[0].o_ref.start
				e = lo_links[-1].o_ref.end
				o_scaff = rl.Region( ref_chrom, s,e )
				o_gff.writeMatch( o_scaff, scaff_id )

				i=1
				for o_link in lo_links:
					part_id = scaff_id + "_" + str(i)
					i += 1
					o_gff.writeMatchPart( o_link, ID=part_id, Parent=scaff_id )
					
			o_gff.writeSep()
# generate_unalign_gff

def generate_ROI_gff( dlt_ROI ):
	with fow.OutGff( "roi.dbg.gff" ) as o_gff:
		o_gff.comment( "Regions of interest." )

		# pb with scaff_id suffix.
		# several scaff link can be in the same ROI.
		i = 1
		for o_ref in sorted( dlt_ROI.keys() ):
			lt_links = dlt_ROI[o_ref]
			region_id = 'R' + str( i )
			o_gff.writeMatch( o_ref, region_id )

			for o_link,key in lt_links:
				seq_id = o_link.o_tgt.seq_id + "_" + str(i)
				o_gff.writeMatchPart( o_link, ID=seq_id, \
										Parent = region_id )
			o_gff.writeSep()
			i += 1
# generate_ROI_gff

def generate_SOI_gff( llo_SOI ):
	with fow.OutGff( "soi.dbg.gff" ) as o_gff:
		o_gff.comment( "Sequences of interest." )

		llo_SOI.sort( key= lambda l_x:l_x[0])

		i = 1
		for lo_soi in llo_SOI:
			o_ref = lo_soi[0]
			id = ".".join([ o_ref.seq_id, str( i ) ])
			o_gff.writeMatch( o_ref, id )
			
			j = 1
			for o_soi in lo_soi[1:]:
				# I cheat, id will contain all the attributes
				id  = ".".join([ o_soi.seq_id, str(i), str(j) ])
				tgt = "Target={0} {1} {2}".format( o_soi.seq_id, str( o_soi.start ), str( o_soi.end ) )
				attr= ";".join([ id, tgt ])
				j  += 1
				o_gff.writeMatch( o_ref, attr )

			#for o_soi in lo_soi:
			#	id = ".".join([ o_soi.seq_id, str( i ) ])
			#	i += 1
			#	o_gff.writeMatch( o_soi, id )
			i += 1
			o_gff.writeSep()
# generate_SOI_gff

# /===========DEBUG FUNC=======================

def generateBedfiles( outdir, llo_SOI ):

	for lo_SOI in llo_SOI:
		o_ref = lo_SOI[0]

		local_outdir = "/".join([ o_args.outdir, o_ref.chrom ])
		if( not os.path.exists( local_outdir ) ):
			os.makedirs( local_outdir )

		filename = "_".join( map( str, [ o_ref.chrom, o_ref.start, o_ref.end ]) )
		outbase  = "/".join([ local_outdir, filename ])
		bed_outfile = outbase + ".bed"

		with open( bed_outfile, "w" ) as o_bed:
			for o_region in lo_SOI:
				if( o_region == o_ref
					and o_region.start <= 0 ):
					o_region.start = 1

				o_bed.write( o_region.recString() + "\n" )
				#~ continue # I do not extract here.

				#~ l_elts = o_region.chrom.split( "|", 1 )
				#~ if( 1 == len( l_elts ) ):
					#~ ass_path = Rapsodyn.Paths.get_ref_fasta( 'bnapus' )
					#~ cultivar = 'Bnapus'
					#~ seq_id = l_elts[0]
				#~ else:
					#~ cultivar = l_elts[0]
					#~ seq_id   = l_elts[1]
					#~ ass_path = Rapsodyn.Paths.get_assembly_path( cultivar )
					#~ ass_path += "/{}.fa.bgz".format( cultivar )

				#~ o_r = rl.Region( seq_id, *o_region.feature() )
				#~ o_r.reversed = o_region.reversed
				#~ # PP.pprint( str( o_r ) )
				#~ o_es = MFasta.ExtractSeq( ass_path )
				#~ for seq_id, o_seq in o_es.extractRegions( [ o_r ], MFasta.build_seqId, cultivar ):
					#~ o_fs.write( ">" + seq_id + "\n" + str( o_seq ) + "\n" )

	return
# generateBedfiles

def determine_sequences_of_interest( d_roi, o_infiles ):
	llo_SOI = []

	for o_ref_region, lt_ in d_roi.items():
		# print( o_ref_region.recString() )
		o_gse = GroupExtractorFactory.makeIt( lt_ )
		o_gse.o_infiles = o_infiles
		for lo_regions in o_gse.getGroupsTargetRegions():
			lo_SOI = [ o_gse.ref_region() ]

			for o_region in lo_regions:
				lo_SOI.append( o_region )
			llo_SOI.append( lo_SOI )

	return llo_SOI
# determine_sequences_of_interest

def identify_regions_of_interest( do_regions ):
	"""Defines which ref regions are of interest."""
	d_roi    = dict()

	for ref_chrom,lt_ in do_regions.items():
		# I first need to sort all chunks by ref position
		lt_.sort( key=lambda x: x[0].o_ref )

		# the region will be altered, so i need a copy of the object
		o_region = copy.copy( lt_[0][0].o_ref )
		lt_roi   = [ lt_[0] ]
		# print str( lt_[0][0] ) + " -- " + lt_[0][1]
		for o_link,key in lt_[1:]:
			# print "Region (1) : " + str( o_region )
			# print str( o_link ) + " -- " + key
			if( o_region.intersect( o_link.o_ref ) ):
				if( o_link.o_ref.start < o_region.start ):
					o_region.start = o_link.o_ref.start
				if( o_region.end < o_link.o_ref.end ):
					o_region.end = o_link.o_ref.end
				lt_roi.append(( o_link, key ))
			else:
				if( 1 < len( lt_roi ) and MIN_UNALIGNED_LEN < o_region.length() ):
					d_roi[ o_region ] = lt_roi
					# print( "Stored" )
				o_region = copy.copy( o_link.o_ref )
				lt_roi   = [( o_link, key )]
				# print "Region (n) : " + str( o_region )
		# print( "=========" )

		if( 1 < len( lt_roi ) and MIN_UNALIGNED_LEN < o_region.length() ):
			d_roi[ o_region ] = lt_roi
			# print( "Stored" )

	return d_roi
# identify_regions_of_interest

def turn_align_intervals_to_unalign_intervals( o_infiles ):
	# une autre façon de faire en partant d'un intervalle = scaff et en enlevant les parties qui s'alignent.

	def __getExtremity( strand, o_link, scaff_len ):
		if( strand == o_link.strand ):
			bout     = 1
			tgt_bout = o_link.o_tgt.start
			offset   = -1
		else:
			bout    = scaff_len
			tgt_bout= o_link.o_tgt.end
			offset  = 1
			
		return bout, tgt_bout, offset
	# __getExtremity

	def __links_overlap( o_link1, o_link2 ):
		if( o_link1.o_tgt.reversed ):
			if( o_link1.o_tgt.start <= o_link2.o_tgt.end ):
				return 1, o_link2.o_tgt.end - o_link1.o_tgt.start + 1
		else:
			if( o_link2.o_tgt.start <= o_link1.o_tgt.end ):
				return 1, o_link1.o_tgt.end - o_link2.o_tgt.start + 1

		if( o_link2.o_ref.start <= o_link1.o_ref.end ):
			return 2, o_link1.o_ref.end - o_link2.o_ref.start + 1

		return 0,None
	# __links_overlap

	def _init( o_link, scaff_id, scaff_len ):
		"""Build the top left/upstream unaligned region"""
		( bout, tgt_bout, offset ) \
				= __getExtremity( 1, o_link, scaff_len )
		# PP.pprint({ 'first': { 'end': tgt_bout, 'bout':bout, 'offset': offset } })

		if( tgt_bout == bout ):
			return # First 

		# the new target region starts at the end of the scaff until the base preceding the first to align the ref.
		o_tgt = rl.Region( scaff_id, bout, tgt_bout+offset )

		ref_end   = o_link.o_ref.start - 1 #+ offset
		ref_start = ref_end - o_tgt.length()
		o_ref     = o_link.o_ref.subRegion( ref_start, ref_end )

		strand = -1 if( o_tgt.reversed ) else 1
		o_link = rl.RegionLink( o_ref, o_tgt, strand )
		
		return ( o_link, 'up' )
	# _init

	def _build_gap_link( o_link1, o_link2 ):
		# tgt region
		if( 1 == o_link2.strand ):
			tgt_start = o_link1.o_tgt.end + 1
			tgt_end   = o_link2.o_tgt.start - 1
			o_tgt     = rl.Region( scaff_id, tgt_start, tgt_end )
		else:
			tgt_start = o_link1.o_tgt.start - 1
			tgt_end   = o_link2.o_tgt.end + 1
			o_tgt     = rl.Region( scaff_id, tgt_start, tgt_end )

		# ref region
		ref_start = o_link1.o_ref.end + 1
		ref_end   = o_link2.o_ref.start - 1
		o_ref = o_link2.o_ref.subRegion( ref_start, ref_end )

		# link
		strand = -1 if( o_tgt.reversed ) else 1
		o_link = rl.RegionLink( o_ref, o_tgt, strand )

		return ( o_link, 'up' )
	# _build_gap_link

	def _terminate( o_link, scaff_id, scaff_len ):
		( bout, tgt_bout, offset ) \
			= __getExtremity( -1, o_link, scaff_len )
		PP.pprint({ 'ultimate': { 'end': tgt_bout, 'bout':bout } })
			
		if( tgt_bout == bout ):
			return

		# this last gap starts the first base after the last that aligns the ref, until the end of the scaff
		o_tgt = rl.Region( scaff_id, tgt_bout+offset, bout )

		ref_start = o_link.o_ref.end + 1
		ref_end   = ref_start + o_tgt.length()
		o_ref     = o_link.o_ref.subRegion( ref_start, ref_end )
		
		strand = -1 if( o_tgt.reversed ) else 1
		o_link = rl.RegionLink( o_ref, o_tgt, strand )

		return ( o_link, 'down' )
	# _terminate


	d_ = defaultdict( list )
	for scaff_id,lo_links in o_infiles.do_links.items():
		#~ if( not scaff_id.startswith( 'montego' ) ):
			#~ continue
		scaff_len = o_infiles.getScaffLen( scaff_id )
		N = len( lo_links )

		t_ = _init( lo_links[0], scaff_id, scaff_len )
		if( t_ ):
			ref_chr = t_[0].o_ref.chrom
			d_[ref_chr].append( t_ )
			PP.pprint({ 'First': str( t_[0] ) })

		for i in range( 1, N ):
			o_ref = None
			PP.pprint({ 'link': str( lo_links[i] ) })

			# we do not consider local reorientation.
			# all segments in a scaff have the same orientation.
			( code, length ) = __links_overlap( lo_links[i-1], lo_links[i] )
			if( 0 != code ):
				msg  = "Links are overlapping, no gap !"
				msg += "\n\t" + str( lo_links[i-1] )
				msg += "\n\t" + str( lo_links[i] )
				if( 1 == code ):
					msg += "\n\t" + "sequence duplication deleted on target ({} bases).".format( str( length ) )
				else:
					msg += "\n\t" + "sequence duplication on target ({} bases).".format( str( length ) )
				sys.stderr.write( msg + "\n" )
				continue

			t_ = _build_gap_link( lo_links[i-1], lo_links[i] )
			ref_chr = t_[0].o_ref.chrom
			d_[ref_chr].append( t_ )
			PP.pprint({ 'Middle': str( t_[0] ) })

		t_ = _terminate( lo_links[-1], scaff_id, scaff_len )
		if( t_ ):
			ref_chr = t_[0].o_ref.chrom
			d_[ref_chr].append( t_ )
			PP.pprint({ 'Last': str( t_[0] ) })

	return d_

def turn_align_intervals_to_unalign_intervals2( o_infiles ):
	d_ = defaultdict( list )
	for k,lo_links in o_infiles.do_links.items():
		if( not k.startswith( 'boston' ) ):
			continue
		scaff_len = o_infiles.d_scaffs[k]
		o_region  = myRegion( k, 1, scaff_len )
		o_region.reversed = lo_links[0].o_tgt.reversed

		lo_regions = [ o_region ]
		for o_link in lo_links:
			lo_new = []
			#~ for o_region in lo_regions:
				#~ try:
					#~ lo_ = o_region - o_link
###

# ----------------------------------------
def defineOptions():
	o_parser = argparse.ArgumentParser( description='Prog descr.' )

	# special
	o_parser.add_argument( '--version', action='version', version='%(prog)s ' + VERSION )

	# positionnal / mandatory param
	#	type = int, file
	o_parser.add_argument( 'align_ref_file',
	                   type=file,
		               help='a tsv file with at least 6 cols (chr,s,e)x2.' )

	o_parser.add_argument( 'scaff_len',
	                   type=file,
		               help='a tsv file with 2 cols: sscaff_id, length.' )


	# optionnal
	o_parser.add_argument( '--verbose', '-v', action='store_true',
		               help='If set give info on what happen.' )
	o_parser.add_argument( '--debug', action='store_true',
		               help='If set output much more information and generate some files.' )

	o_parser.add_argument( '--outdir', '-O', default="bed_files",
		               help='Output directory, where to store generated bedfiles.' )
	#~ o_parser.add_argument( '--bedfile', '-B', action='store_true',
		               #~ help='If set generate one bedfile per fasta outfile.' )


	return o_parser

def analyseOptions( o_args ):

	global VERBOSE
	if( o_args.verbose ):
		VERBOSE = True

	global DEBUG
	if( o_args.debug ):
		DEBUG = True

	if( not os.path.isdir( o_args.outdir ) ):
		os.makedirs( o_args.outdir )

def verbose( msg ):
	if( VERBOSE ):
		sys.stderr.write( msg + "\n" )

# ----------------------------------------

if __name__ == '__main__':
	o_parser = defineOptions()
	o_args   = o_parser.parse_args()
	analyseOptions( o_args )
#	PP.pprint( o_args )

	main( o_args )

	sys.exit( 0 )

