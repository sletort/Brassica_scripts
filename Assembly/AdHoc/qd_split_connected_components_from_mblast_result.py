#!/usr/bin/env python
# -*- coding: utf-8 -*-

## AIMS  : detect strech of N in fasta file and output as bed file.
## USAGE : detectOcean.py fasta_file > out.bed
## AUTHORS : sebastien.letort@irisa.fr


import sys
import pdb	# debug
import pprint # debug
import argparse	# option management, close to C++/boost library (?)
import os	# for local libraries
import errno
import csv	# manage tsv files also.
import networkx as nx
import matplotlib.pyplot as plt
from collections import defaultdict

# local libraries
f_path = os.path.dirname( os.path.realpath( __file__ ) )
sys.path.append( f_path + "/../../lib" )
import RegionLink as rl

VERSION = "1.0"
PP = pprint.PrettyPrinter(indent=4)
VERBOSE   = False

class Node( rl.Region ):
	'''Singleton from Region class'''
	#~ h_regions = defaultdict( lambda: defaultdict(dict) )
	h_regions = defaultdict(dict) # only seq id/chrom stored

	def builder( cls, o_region ):
		o_r = Node.h_regions.get( o_region.chrom )
		if( not o_r ):
			Node.h_regions[o_region.chrom] = o_region

		return Node.h_regions[o_region.chrom]
	builder = classmethod( builder )


class BlastResult( rl.RegionLink ):

#	def __init__( self, o_ref, o_tgt, strand, len1, len2 ):
#		super( BlastResult, self ).__init__( o_ref, o_tgt, strand )
#		self.o_ref_len = len1
#		self.o_tgt_len = len2

	def load_mblast_file( cls, o_file ):
		'''Return a list of BlastResult. (based on RegionLink.loadSimpleLinkFile)'''
		o_parser = csv.reader( o_file, delimiter="\t" )

		l_res = []
		for a_elts in o_parser:
			o_ref  = rl.Region( *a_elts[0:3] )
			o_tgt  = rl.Region( *a_elts[3:6] )

			if( o_ref.reversed == o_tgt.reversed ):
				strand = 1
			else:
				strand = -1

			#~ (l1,l2) = ( *a_elts[8:9] )
			#~ print( o_link, l1,l2 )
			#o_br = BlastResult( o_ref, o_tgt, strand, *a_elts[8:10] )
			o_br = BlastResult( o_ref, o_tgt, strand )
			l_res.append( o_br )

		return l_res
	load_mblast_file = classmethod( load_mblast_file )


class Misc( object ):
	__nb_cc = 0

	def cc_outfile( cls, o_graph ):
		n = len( o_graph.nodes() )
		if( 2 == n ):
			outdir = "{}_nodes".format( str( n ) )
		elif( n <= 5 ):
			outdir = "5_nodes"
		elif( n <= 10 ):
			outdir = "10_nodes"
		else:
			outdir = "nodes"

		try:
			os.makedirs( outdir )
		except OSError as e: # python >2.5
			if( e.errno == errno.EEXIST and os.path.isdir( outdir ) ):
				pass
			else:
				raise
	
		cls.__nb_cc += 1
		outfile = "CC_{}".format( str( cls.__nb_cc ) )

		return outdir + '/' + outfile
	cc_outfile = classmethod( cc_outfile )

# ========================================
def main( o_args ):
	verbose( "En mode verbeux." )

	l_file = BlastResult.load_mblast_file( o_args.align_file )
	verbose( str( len( l_file ) ) + " links have been loaded." )

	G = build_graph_from_alignment( l_file )

	verbose( "# edges : " + str( G.number_of_edges() ) )
	verbose( "# nodes : " + str( G.number_of_nodes() ) )
	#~ draw( G, "raw_simple.svg" )

	# besoin de raffiner.
	# si A inclus B qui inclus C
	#	on ajoute A-B, A-C et B-C.
	# probablement que B-C est inutile.
	##
	# sur un graphe ne regardant pas les positions, on n'ajoute rien !
	#~ add_inclusion_edges( G )
	#~ verbose( "after addition of inclusion edges." )
	#~ verbose( "# edges : " + str( G.number_of_edges() ) )
	#~ verbose( "# nodes : " + str( G.number_of_nodes() ) )


	if( nx.is_connected( G ) ):
		verbose( "G is connected." )
		study_CC( G )
	else:
		n_cc = nx.number_connected_components( G )
		verbose( "G has {} connected components.".format( ( n_cc ) ) )
		for l_nodes in nx.connected_components(G):
			study_CC( G.subgraph( l_nodes ) )



	#~ nx.draw( G )
	#~ nx.draw_networkx( G )
	#~ plt.show()
	#~ plt.savefig( "joli_graph.png" )

	return 0

def add_inclusion_edges( G ):
	# v1
	for o_node1 in nx.nodes_iter( G ):
		for o_node2 in nx.nodes_iter( G ):
			if( o_node1.o_r.includes( o_node2.o_r, strict=True ) ):
				print( str( o_node1 ) + '-->' + str( o_node2 ) )
				G.add_edge( o_node1, o_node2 )


def study_CC( o_cc ):

	#verbose( "\t# edges : " + str( o_cc.number_of_edges() ) )
	#verbose( "\t# nodes : " + str( o_cc.number_of_nodes() ) )

	outfile = Misc.cc_outfile( o_cc )
	#draw( o_cc, outfile+".svg" )

	#verbose( "CC : longuest clique has " + str( nx.graph_clique_number( o_cc ) ) + " nodes." )
	save_tgf( outfile+".tgf", o_cc )

	#~ max_len = 0
	#~ for o_node in o_cc.nodes_iter():
		#~ verbose( "node len = " + str( o_node.o_r.length() ) )
		#~ if( max_len < o_node.o_r.length() ):
			#~ max_len = o_node.o_r.length
	#~ verbose( "Max seq len = " + str( max_len ) )

	#~ sys.exit( 42 )

def save_tgf( outfile, o_g ):

	h_nodes = {}

	i = 1
	with open( outfile, "w" ) as o_fs:
		for o_node in o_g.nodes_iter():
			h_nodes[o_node] = i

			line = str( i ) + "\t" + o_node.recString()
			o_fs.write( line + "\n")
			i += 1

		o_fs.write( "#\n" )

		for l_nodes in o_g.edges_iter():
			n1 = h_nodes[ l_nodes[0] ]
			n2 = h_nodes[ l_nodes[1] ]
			line = str( n1 ) + " " + str( n2 )
			o_fs.write( line + "\n" )
		

def draw( o_graph, outfile=None ):
	#~ nx.drawing.nx_pydot.write_dot( o_cc, "graph.dot" ) 

	#~ nx.draw( o_graph )
	#~ plt.show()
	#~ if( outfile != None ):
		#~ plt.savefig( outfile )

	nx.draw_networkx( o_graph )
	if( outfile != None ):
		plt.savefig( outfile, dpi=1000 )
	else:
		plt.show() # ne sais pas pk, peut pas être sauvé et vu en même temps.
	plt.close()



def build_graph_from_alignment( l_br ):
	'''l_br is a list of BlastResult.'''

	G = nx.Graph()
	for o_br in l_br:
		o_node1 = Node.builder( o_br.o_ref )
		o_node2 = Node.builder( o_br.o_tgt )
		#~ o_reg   = rl.Region( o_link.o_ref.chrom, 1,2 )
		#~ o_node1 = Node.builder( o_reg )
		#~ o_reg   = rl.Region( o_link.o_tgt.chrom, 1,2 )
		#~ o_node2 = Node.builder( o_reg )

		G.add_nodes_from([ o_node1, o_node2 ])
		G.add_edge( o_node1, o_node2, len = o_node1.length() )

	return G

# ----------------------------------------
def defineOptions():
	o_parser = argparse.ArgumentParser( description='Prog descr.' )

	# special
	o_parser.add_argument( '--version', action='version', version='%(prog)s ' + VERSION )

	# positionnal / mandatory param
	#	type = int, file
	o_parser.add_argument( 'align_file', nargs='?',
	                   type=file, default=sys.stdin,
		               help='a tsv file with at least 7 cols (chr,s,e)x2,sign. default stdin.' )

	# optionnal
	o_parser.add_argument( '--verbose', '-v', action='store_true',
		               help='If set give info on what happen.' )
	#~ o_parser.add_argument( '-f', dest='opt1',
							#~ default='toto',
							#~ help='a flag with a value.' )

	#~ # mandatory option
	#~ #	destination is opt2 by default
	#~ o_parser.add_argument( '--opt2', '-o',
						#~ action='store_true', # store the value as boolean
						#~ help='a flag opt, long and short.')

	return o_parser

def analyseOptions( o_args ):

	global VERBOSE
	if( o_args.verbose ):
		VERBOSE = True


def verbose( msg ):
	if( VERBOSE ):
		sys.stderr.write( msg + "\n" )

# ----------------------------------------

if __name__ == '__main__':
	o_parser = defineOptions()
	o_args   = o_parser.parse_args()
	analyseOptions( o_args )
#	pp.pprint( o_args )

	main( o_args )

	sys.exit( 0 )

