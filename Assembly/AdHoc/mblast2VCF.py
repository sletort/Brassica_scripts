#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

## AIMS  : generate a VCF file from a blast result (obtain in only_QTL.sh).
## USAGE : mblast2VCF.py -R ref -s sample_name -o variants.vcf mblast.tsv
## USAGE : mblast2VCF.py --Reference ref --sample sample_name --outfile variants.vcf mblast.tsv
## AUTHORS : sletort@irisa.fr
## NOTE  : blast should be run with -outfmt "6 sseqid sstart send qseqid qstart qend sstrand qlen lengthscore evalue pident nident mismatch sseq qseq

import sys
import pprint # debug
import argparse	# option management, close to C++/boost library (?)

import csv	# manage tsv files also.

import os	# for local libraries

# local libraries
f_path = os.path.dirname( os.path.realpath( __file__ ) )
sys.path.append( f_path + "/../../lib" )

import Vcf
from MiscSeq import MiscSeq
import RegionLink as rl
from lib_only_QTL import *

VERSION   = "0.1"
PP = pprint.PrettyPrinter(indent=4)
VERBOSE   = False

class BlastVariant( object ):
	'''a class just to hide some complexity'''
	def __init__( self, s,e, ref_start, tgt_start ):
		self.__start   = s
		self.__end     = e
		self.__ref_pos = ref_start
		self.__tgt_pos = tgt_start

	@property
	def ref_pos( self ):
		return self.__ref_pos
	@property
	def tgt_pos( self ):
		return self.__tgt_pos
	@property
	def feature( self ):
		return( self.__start, self.__end )

	def size( self ):
		return self.__end - self.__start + 1

	def __str__( self ):
		t_attr = ( self.__start,self.__end,self.__ref_pos,self.__tgt_pos )
		x = [ str( x ) for x in t_attr ]
		x.append( self.__class__.__name__ )
		return '{4}: [{0},{1}],ref {2}, tgt {3}'.format( *x )

class Snp( BlastVariant ):
	pass
class Ins( BlastVariant ):
	pass
class Del( BlastVariant ):
	pass

class BlastResultVariantDetectorIterator( BlastResultIterator ):
	def next( self ):
		return BlastResultVariantDetector( next( self._it ) )

class BlastResultVariantDetector( BlastResult_mini ):
	def __init__( self, l_elts ):
		super( self.__class__, self ).__init__( l_elts )

		o_ref = rl.Region( l_elts[0], *[ int(x) for x in l_elts[1:3] ] )
		o_tgt = rl.Region( l_elts[3], *[ int(x) for x in l_elts[4:6] ] )
		strand = self.d_strand[ l_elts[6] ]
		self._build_o_link( o_ref, o_tgt, strand )

		self.__l_vars = []

		# __variant_builder is a method that return a variant given information found in the object
		#	It will be called for each variant found in the object
		#	used to export the variants
		self.__variant_builder = self.__variant_builder_factory()


	def build_variations( self, debug=None ):
		if( debug ):
			msg  = "# Region coordinates are 1-based.\n"
			msg += "# variations descriptions are 0-based, relative to the alignment.\n"
			msg += "#\t[s,e in the alignment], start in the ref coordinnate of the alignment, start in the tgt coordinnate of the alignment.\n"
			debug.write( msg )
			debug.write( str( self.o_link ) + "\n" )

		self.__l_vars = []
		#~ self.__compute_extremity_variations()
		self.__diff_seq( debug ) # build variant objects

		self.__check_variations() # assert variant founds are coherent with mismatches reported.

		if( debug ):
			debug.write( "\n" )

		return

	def __compute_extremity_variations( self ):
		q_start,q_end = self.o_link.o_tgt.feature()
		q_len = self.tgt_len

		if( q_start != 1 ):
			#segment is before the alignment
			start = -(q_start-1) # -1 to turn qstart to 0-based, -1 to get the previous base
			self.__l_vars.append({ 'Ins': [start,0] })

		if( q_end != q_len ):
			self.__l_vars.append({ 'Ins': [q_end+1,q_len] })

		return

	def __diff_seq( self, o_debug ):
		def __get_indel_end( i, seq ):
			for j in range( i, len( seq ) ):
				if( seq[j] != '-' ):
					return j-1

		ref   = self.seq_ref
		query = self.seq_tgt

		if( len( ref ) != len( query ) ):
			raise NotImplementedError( "I don't manage seq with different length." )

		( i, n_ins, n_del ) = ( 0, 0, 0 )
		while( i< len( ref ) ):
			if( ref[i] != query[i] ):
				if( ref[i] == '-' ):
					j = __get_indel_end( i, ref )
					o_var = Ins( i,j, i-n_ins, i-n_del )
					n_ins += ( j - i + 1 )
					i=j
				elif( query[i] == '-' ):
					j = __get_indel_end( i, query )
					o_var = Del( i,j, i-n_ins, i-n_del )
					n_del += ( j - i + 1 )
					i=j
				else:
					o_var = Snp( i,i, i-n_ins, i-n_del )

				self.__l_vars.append( o_var )
				if( o_debug ):
					o_debug.write( "\t" + str( o_var ) + "\n" )
			i+=1

		return

	def __check_variations( self ):

		def __assert_count( l_vars, var_type, count, expected ):
			if( count != expected ):
				msg  = "Count error with the {}.".format( var_type )
				msg += "\n\tcount {}, expected {}.".format( str( count ), str( expected ) )
				l_str_vars = [ str( o_ ) for o_ in l_vars ]
				raise AssertionError( msg, l_str_vars )

		def __count_variations( lo_vars ):
			'''return a tuple with count of bases in snp, insertion, deletion variations.'''
			n_snp = 0
			n_ins = 0 # number of bases in insertions
			n_del = 0 # number of bases in deletions

			for o_ in lo_vars:
				
				if( isinstance( o_, Snp ) ):
					n_snp += o_.size()
				elif( isinstance( o_, Ins ) ):
					n_ins += o_.size()
				elif( isinstance( o_, Del ) ):
					n_del += o_.size()

			return n_snp,n_ins,n_del
		# __count_variations

		n_snp,n_ins,n_del = __count_variations( self.__l_vars )

		__assert_count( self.__l_vars, 'SNPs', n_snp, self.n_mismatch )

		delta = self.o_link.o_ref.length() - self.n_ident - self.n_mismatch
		__assert_count( self.__l_vars, 'deletions', n_del, delta )

		delta = self.o_link.o_tgt.length() - self.n_ident - self.n_mismatch
		__assert_count( self.__l_vars, 'insertions', n_ins, delta )

		return

	def __variant_builder_factory( self ):
		o_ref = self.o_link.o_ref # to ease reading
		scaff_id = self.o_link.o_tgt.chrom

		def variant_builder( o_var ):
			'''o_var is a BlastVariant
				Reminder o_ref, o_tgt are 1-based and o_var is 0-based
				Output is 1-based
			'''
			#~ PP.pprint( str( o_var ) )

			#~ PP.pprint({ 'scaff_start': [ self.__o_link.o_tgt.start, o_var.tgt_pos ] })
			scaff_start = self.o_link.o_tgt.start + o_var.tgt_pos

			if( o_ref.reversed ):
				#~ PP.pprint([ o_ref.end, o_var.ref_pos, o_var.size() ])
				chrom_pos  = o_ref.end - o_var.ref_pos
				if( isinstance( o_var, Del ) ):
					chrom_pos -= o_var.size()
			else:
				chrom_pos  = o_ref.start + o_var.ref_pos

			#~ PP.pprint({ 'i': chrom_pos })
			( s,e ) = o_var.feature

			if( not isinstance( o_var, Snp ) ):
				# anchor management
				chrom_pos   -= 1
				scaff_start -= 1	# only checked for Del
				if( o_ref.reversed ):
					chrom_pos += 1
					e += 1
				else:
					s -= 1
			#~ PP.pprint({ 'ii': chrom_pos })

			# strip formats indels
			ref_allele = self.seq_ref[s:e+1].strip( '-' )
			tgt_allele = self.seq_tgt[s:e+1].strip( '-' )
			if( o_ref.reversed ):
				ref_allele = ''.join( MiscSeq.complement( ref_allele ) )
				tgt_allele = ''.join( MiscSeq.complement( tgt_allele ) )

			if( tgt_allele == ref_allele ):
				tgt_allele = '.'

			chrom = o_ref.chrom.lstrip( 'chr' )
			return ( chrom, chrom_pos, ref_allele, tgt_allele, scaff_id, scaff_start )

		return variant_builder

	def export_variations( self, fn_format=lambda x:x ):
		'''make variation usable outsite the BlastResult class.
			In the object, variations are placed with the alignment coordinates (ref and tgt).
			generate a tuple with
				chrom,pos, ref_all, tgt_all
			I made the assumption that first and last base of the alignment are not a variant.
				because if so, they should be excluded from the alignment
		'''
		#~ PP.pprint({ 'avant': [ str( x ) for x in self.__l_vars ] })
		l_vars = []

		# make a block record following gVCF recommandation
		# I am not sure that I can remove my fake variants.
		#	-> I would miss the scaff end position
		# Not used for the moment
		# t_snp = self.__variant_builder( Snp( 0,0, 0,0 ) )
		# l_vars.append( fn_format( *t_snp, chrom_end=self.o_link.o_ref.end ) )

		# fake SNP to define haplotype start
		t_snp = self.__variant_builder( Snp( 0,0, 0,0 ) )
		l_vars.append( fn_format( *t_snp ) )

		for o_var in self.__l_vars:
			t_snp = self.__variant_builder( o_var )
			l_vars.append( fn_format( *t_snp ) )
			
		# fake SNP to define haplotype end
		 # -1 to turn 0-base
		last  = self.align_len - 1
		last_ref   = self.o_link.o_ref.length() - 1
		last_scaff = self.o_link.o_tgt.length() - 1
		t_snp = self.__variant_builder( Snp( last,last, last_ref, last_scaff ) )
		l_vars.append( fn_format( *t_snp ) )

		if( self.o_link.o_ref.reversed ):
			l_vars.reverse()

		#~ PP.pprint({ "apres": l_vars })
		return l_vars

	def __get_alleles( self, start, end=None ):
		'''position is relative (0-based) to alignment seqs.'''
		if( None == end ):
			end = start

		ref_allele = self.seq_ref[start:end+1]
		tgt_allele = self.seq_tgt[start:end+1]
		#~ PP.pprint({ 'pos':[start, end+1], 'ref': ref_allele, 'tgt': tgt_allele })
		if( self.o_link.o_ref.reversed ):
			ref_allele = ''.join( MiscSeq.complement( ref_allele ) )
			tgt_allele = ''.join( MiscSeq.complement( tgt_allele ) )

		return ref_allele,tgt_allele

# ========================================
def getVcfBuilder( sample ):
	def vcf_builder( chrom, pos, ref, alt, scaff_id, scaff_pos, chrom_end=None ):
		'''static method to turn tuple generated by export_variations to Vcf record as expected by Vcf lib.'''
		if( alt == '.' ):
			gt = '0'
		else:
			gt = '1'

		o_var = Vcf.VariantRecord( chrom, str(pos), [ref,alt] )
		o_var.addSampleData({ 'GT': gt, 'HAP': scaff_id, 'HAP_POS': scaff_pos })

		if( chrom_end ):
			o_var.updateInfo( 'END', str( chrom_end ) )

		return o_var

	return vcf_builder

def main( o_args ):

	l_formats = Vcf.Format.standard([ 'GT' ])
	# I do not use standard HAP to allow NODE_xxx
	o_hap     = Vcf.Format( { 'ID': "HAP", 'Number':1, 'Type':'String', 'Description':'"Unique haplotype identifier."' })
	l_formats.append( o_hap )
	o_hap_pos = Vcf.Format( { 'ID': "HAP_POS", 'Number':1, 'Type':'Integer', 'Description':'"position of the variation in the haplotype."' })
	l_formats.append( o_hap_pos )
	end_info  = Vcf.Info( { 'ID': 'END', 'Number':1, 'Type':'Integer', 'Description':'"End position of the variant described in this record."' } )
	d_meta = {
		'reference': o_args.Reference,
		'FORMAT'   : l_formats
	}
	# Not use for the moment
	# d_meta['INFO'] = [ end_info ]

	l_samples = [ o_args.sample ]
	vcf_builder = getVcfBuilder( o_args.sample )

	verbose( "Reading Blast results" )
	o_vcf = Vcf.Writer( o_args.outfile, d_meta, l_samples )
	o_vcf.writeHeader()
	o_tsv = csv.reader( o_args.infile, delimiter='\t' )
	for o_detector in BlastResultVariantDetectorIterator( o_tsv ):
		# o_detector is the object manipulating one alignment row
		#	which can contain many variants.
		o_detector.build_variations( debug=o_args.debug )
		lo_vars = o_detector.export_variations( fn_format=vcf_builder )

		for o_var in lo_vars:
			# o_var is a VariantRecord object
			o_var.write( o_vcf )

	return 0
# ----------------------------------------
def defineOptions():
	descr = "generate a VCF file from a blast result (obtain in only_QTL.sh)."
	o_parser = argparse.ArgumentParser( description=descr )

	# special
	o_parser.add_argument( '--version', action='version', version='%(prog)s ' + VERSION )

	# positionnal / mandatory param
	o_parser.add_argument( 'infile', nargs='?',
							type=argparse.FileType( 'r' ),
							default=sys.stdin,
							help='The tsv blast result (produced by filter_blast for the moment).' )

	# optionnal
	o_parser.add_argument( '--outfile', '-o',
						type=argparse.FileType( 'w' ),
						default=sys.stdout,
		               help='vcf outfile.' )

	o_parser.add_argument( '--Reference', '-R',
	                   required=True,
		               help='filepath to the reference used to perform the balst.' )
	o_parser.add_argument( '--sample', '-s',
						default='X',
		               help='sample name.' )

	o_parser.add_argument( '--debug', '-d',
	                       type=argparse.FileType( 'w' ),
	                       help='filename for debugging information (variation descr).' )

	o_parser.add_argument( '--verbose', '-v', action='store_true',
		               help='If set give info on what happen.' )

	return o_parser

def analyseOptions( o_args ):

	global VERBOSE
	if( o_args.verbose ):
		VERBOSE = True

	return

def verbose( msg ):
	if( VERBOSE ):
		sys.stderr.write( msg + "\n" )


# ----------------------------------------

if __name__ == '__main__':
	o_parser = defineOptions()
	o_args   = o_parser.parse_args()
	analyseOptions( o_args )
	# PP.pprint( o_args )

	sys.exit( main( o_args ) )

