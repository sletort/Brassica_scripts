#! /bin/bash

## AIMS  : Analyse the QTLs based on partially unmapped scaffolds.
## USAGE : ./analyse_QTL.sh [-h] [-v] -t [scaffs|contigs] Finishing_dir
#
## OUTPUT :
## OUTFILE:
#
## NOTE  :
#
## BUG   : None found yet.

declare _CWD=$( dirname $( readlink -e $0 ) )
source "$_CWD"/lib.analyse_QTL.sh

# =========================================================
# filename, not path.
#	files will be generated in the current directory.
declare -r STARCH_ALIGNMENT="all_ref_align.starch"
declare -r ALL_SCAFF_LEN="all_scaff_len.tsv"

# progs
declare -r DETERMINE_SOI="$CWD_/check_partially_unaligned_seqs.py"

# =========================================================
function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT    = $0\n"
	printf "╟ - INDIR     = $INDIR\n"
	printf "╟ - TYPE      = $TYPE\n"
	printf "╟ - BED_DIR   = $BED_DIR\n"
	printf "╟ -------------\n"
	printf "╟ - STARCH_ALIGNMENT = $STARCH_ALIGNMENT\n"
	printf "╟ - ALL_SCAFF_LEN    = $ALL_SCAFF_LEN\n"
	printf "╟ - REPORT           = $REPORT\n"
	printf "╟ - DETERMINE_SOI    = $DETERMINE_SOI\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

# =========================================================
function extract_alignment ()
{
	local bed_region="$1"

	printf "$bed_region" \
			| bedops -e 1 ../../"$STARCH_ALIGNMENT" -
}


function __look_for_duplications ()
{
	local log_file="$1"

	# first, I get which lines are valuable
	printf "Look for duplications : \n"
	local LINES=$( mktemp lines.XXXX )

	local perle

	perle='($line,$size)= /(\d+):.+\((\d+) bases/;'
	perle=$perle'print( join( "\n", ($line-2),($line-1),$line ) ) if( $size > 500 );'
	grep -n duplication "$log_file" \
			| perl -lane "$perle" \
		> $LINES

	# then I extract those lines
	local p_begin='BEGIN{ open( $f_, "<", "'$LINES'" ); @a_lines = <$f_>; close( $f_ ); }'
	perle='exit if( 0 == @a_lines );'
	perle=$perle' if( $.== $a_lines[0] ){ print; shift @a_lines; }'
	perl -lane "$p_begin $perle" "$log_file"

	rm $LINES
}

function determine_soi ()
{
	local bname="$1"
	local align_file="$2"

	local err_file=$( basename "$align_file" .tsv ).err
	local outdir="$BED_DIR"

	$DETERMINE_SOI "$align_file" ../../"$ALL_SCAFF_LEN" --debug --verbose \
			--outdir "$outdir" \
		2> "$err_file"

	# print on stdout (copied into REPORT file)
	__look_for_duplications "$err_file"
	__analyse_determine_soi_gff_file "$bname"
}

# summarize the information from all chroms/QTL
# summarize per scaff doesn't make sense, scaff are not shared across chroms.
function summarize_info_groups ()
{
	local -r bname="$1"

	# threshold values are hard coded in run_clustalO_from_bedfile.sh
	for thres in 0.10 0.25
	do
		"$CWD_/summarize_groups.py" -v "$BED_DIR"/*/*.cul_group.$thres.tsv \
			> $bname.similarity_group.$thres.txt
	done
}

# ---------------------------------------------------------
function init ()
{
	printf "Load QTLs.\n"
	while read chrom start end id
	do
		A_QTLs[$id]="$chrom\t$start\t$end"
	done < "$QTL_FILE"

	printf "Aggregate scaffold alignments.\n"
	starchcat "$INDIR"/gs_bnapus_*/scaff_analysis/part_unmapped.ref_align.starch \
		> "$STARCH_ALIGNMENT" &

	for cul in $( loopOverCultivars Bnapus echo )
	do
		perl -pe "s/^/$cul|/;" "$INDIR"/gs_bnapus_$cul/scaff_analysis/part_unmapped.scaff_len.tsv
	done > "$ALL_SCAFF_LEN"

	wait
}

function analyse_QTL ()
{
	local qtl="$1"

	local bname="rapsodyn_${qtl}"

	printf "$qtl :\n" | tee -a $REPORT

	printf "\tExtract alignments present on QTLs.\n"
	local align_file="$bname.align.tsv"

	extract_alignment "${A_QTLs[$qtl]}\n" > "$align_file"
	describe_QTL "$align_file" | tee -a $REPORT

	# ===============================
#	if [[ 0 -eq 1 ]]
#	then
		printf "Determine sequence of interest for each cultivars over the QTL.\n"
		determine_soi "$bname" "$align_file" | tee -a $REPORT

		build_similarity_matrices

		# ===============================
		#~ summarize_info_groups $bname
		local summary_file="$bname.matrices.sum_up.tsv"
		summarize_info_groups_for_dendrogram $bname \
			> "$summary_file"
		$__ROOT/generic/drawHeatmapDendrowFromMatrix.py \
			--title "Sequence comparison for partially mapped scaffolds on QTL $qtl." \
			--outfile "$qtl.HM_dendrow.png" \
			"$summary_file"
#	fi
}


# =========================================================
manageOptions "$@"

if [[ 1 -eq $VERBOSE ]]
then
	printVars >&2
fi

# your work.
main

printf "End of %s.\n\n" $( basename $0 ) >&2
exit 0
