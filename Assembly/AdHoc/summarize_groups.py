#!/usr/bin/env python
# -*- coding: utf-8 -*-

## AIMS  : determine cultivars proximity from different region groups.
## USAGE : summarize_groups.py [--verbose] 
## AUTHORS : sebastien.letort@irisa.fr


import sys
import pprint # debug
import argparse	# option management, close to C++/boost library (?)
import os	# for local libraries
import glob
import networkx as nx
import itertools as it

# local libraries
f_path = os.path.dirname( os.path.realpath( __file__ ) )
sys.path.append( f_path + "/../../lib" )

import Misc

VERSION = "1.0"
PP = pprint.PrettyPrinter( indent=4, stream=sys.stderr )
VERBOSE = False
DEBUG   = False

# weight of graph
LINK   = 2
UNLINK = -4

# ========================================
# ========================================
def main( o_args ):
	verbose( "Resolve the file pattern." )
	msg = "{} files have been found and will be parsed.\n"
	verbose( msg.format( len( o_args.l_filepaths ) ) )

	o_graph = nx.Graph()
	for fp in o_args.l_filepaths:
		ll_groups = load_groupfile( fp )
		update_graph( o_graph, ll_groups )

	#~ print( o_graph.edges( 'jet_neuf', data=True ) )
	Misc.save_tgf( o_graph, "groups.tgf" )

	#do stg with graph
	#~ import matplotlib.pyplot as plt
	#~ plt.subplot(121)
	#~ nx.draw( o_graph, with_labels=True, font_weight='bold' )
	#~ plt.subplot(122)

	#~ plt.show()
	#~ for s in nx.connected_components( o_graph ):
		#~ print( s )
	#~ print( o_graph.edges( data=True ) )

	# t_similar is a generator
	lt_final_groups = build_group_of_similarity( o_graph )

	for s_group,score in sorted( lt_final_groups, key=lambda x: x[1], reverse = True ):
		print( "### score = " + str( score ) )
		print( "\n".join( s_group ) + "\n" )

	# fin 1 ici
	# je peux aussi faire un graphe avec ces ensembles et avoir des liens de 'différences'

	return 0
# main


def load_groupfile( fp ):
	ll_groups = [] # list of list=group
	l_group = []

	verbose( "Parsing {}.".format( fp ) )

	with open( fp, 'r' ) as o_fe:
		for line in o_fe:
			line = line.rstrip()
			if( line == '###' ):
				if( 0 != len( l_group ) ):
					ll_groups.append( l_group )
				l_group = []
			else:
				l_group.append( line )
		else:
			ll_groups.append( l_group )

	return ll_groups

def update_graph( o_graph, ll_groups ):

	#~ print( str( ll_groups ) )
	l_prev_nodes = []
	for l_group in ll_groups:

		#~ print( "working with group " + str( l_group ) )

		# add 'bad' link to show difference
		#~ print( "prev_nodes = " + str( l_prev_nodes ) )
		for a in l_group:
			for b in l_prev_nodes:
				if( o_graph.has_edge( a,b ) ):
					o_graph[a][b]['weight'] += UNLINK
				else:
					o_graph.add_edge( a,b, weight = UNLINK )

		#~ print( "Graph after bad nodes" )
		#~ print( o_graph.edges( data=True ) )
		l_prev_nodes += l_group

		# add group as a clique
		for i in range( len( l_group ) ):
			a = l_group[i]
			o_graph.add_node( a )
			for j in range( i+1, len( l_group ) ):
				b = l_group[j]
				if( o_graph.has_edge( a,b ) ):
					o_graph[a][b]['weight'] += LINK
				else:
					o_graph.add_edge( a,b, weight = LINK )

		#~ print( "Graph after good nodes" )
		#~ print( o_graph.edges( data=True ) )

	return

def build_group_of_similarity( o_graph ):
	o_g = nx.Graph()
	for a, b, data in sorted( o_graph.edges(data=True), key=lambda x: x[2]['weight'], reverse = True ):
		if( 0 < data['weight'] ):
			#~ print('{a} {b} {w}'.format(a=a, b=b, w=data['weight']))
			o_g.add_edge( a,b, weight = data['weight'] )

	# I cannot do that because those nodes can be similar to groups
	#	we just do not have the informatio here.
	#s_node_alones = set( o_graph.nodes() ) - set( o_g.nodes() )
	#o_g.add_nodes_from( s_node_alones )

	lt_final_groups = []
	for s in nx.connected_components( o_g ):
		t_edges = it.combinations( s, 2 )
		weight_sum = sum([ o_g[a][b]['weight'] for a,b in t_edges if( o_g.has_edge( a,b ) ) ])
		lt_final_groups.append( ( s, weight_sum ) )

	return lt_final_groups

# ----------------------------------------
def defineOptions():
	o_parser = argparse.ArgumentParser( description='Prog descr.' )

	# special
	o_parser.add_argument( '--version', action='version', version='%(prog)s ' + VERSION )

	# positionnal / mandatory param
	#	type = int, file
	o_parser.add_argument( 'l_filepaths',
	                   type=str,
	                   nargs='+',
		               help='paths to the group file' )


	# optionnal
	o_parser.add_argument( '--verbose', '-v', action='store_true',
		               help='If set give info on what happen.' )

	return o_parser

def analyseOptions( o_args ):

	global VERBOSE
	if( o_args.verbose ):
		VERBOSE = True

def verbose( msg ):
	if( VERBOSE ):
		sys.stderr.write( msg + "\n" )

def debug( *t_msg ):
	if( DEBUG ):
		for msg in t_msg:
			sys.stderr.write( "\t" + msg + "\n" )

# ----------------------------------------

if __name__ == '__main__':
	o_parser = defineOptions()
	o_args   = o_parser.parse_args()
	analyseOptions( o_args )
	# PP.pprint( o_args )

	main( o_args )

	sys.exit( 0 )

