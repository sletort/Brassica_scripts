#! /bin/bash

## AIMS  : 
## USAGE :
## NOTE  : J'ai oublié de sélectionner une région QTL seulement !
## AUTHORS : sletort@irisa.fr

# before nounset because PYTHONPATH: unbound variable
source /softs/local/env/envpython-2.7.sh

set -o nounset
set -o errexit

# rapsosyn.sh must have been sourced.
source $__ROOT/brassicaDB.sh


# =========================================================
declare -r INDIR="${1?What is the Finishing directory ?}"

# ---------------------------------------------------------
declare -r AGP_EXTRACT_SCAFF_LINES="$__ROOT/generic/agp_extract_scaff_lines.py"
declare -r EXTRACT_MFASTA="$__ROOT/generic/mfasta_extract_seqs.py"
declare -r PREFIX_MFASTA="$__ROOT/generic/mfasta_prefix_seq_ids.py"

trap before_exiting EXIT

# =========================================================
function before_exiting ()
{
	printf "TRAP : $0 exit with code $?.\n" >&2
}

function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT    = $0\n"
	printf "╟ - INDIR     = $INDIR\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

# =========================================================
# needed by blast functions
function get_mfasta ()
{
	local cul="$1"

	printf "$INDIR/gs_bnapus_${cul}/scaff_analysis/part_unmapped.ctg.fasta"
}

function extract_ctg_fasta ()
{
	local cul="$1"

	local work_dir="$INDIR/gs_bnapus_${cul}/scaff_analysis"

	local scaff_file="$work_dir/part_unmapped.lst"
	local agp_file=$( get_assembly_path $cul )/${cul}_besst/BESST_output/pass1/info-pass1.agp.gz
	local fasta_file=$( get_assembly_path $cul )/${cul}_k101.contigs.fa.bgz

	# redefine tham from get_fasta ?
	local scaff_ctg="$work_dir/part_unmapped.scaff_ctg.tsv"
	local ctg_outfile="$work_dir/part_unmapped.ctg.lst"
	local ctg_fasta="$work_dir/part_unmapped.ctg.fasta" # $( get_mfasta $cul )

	# for paralellism
	{

		# extract contig ids
		{
			grep ^NODE "$scaff_file" | sort -V

			grep -v ^NODE "$scaff_file" | sort -V \
				| "$AGP_EXTRACT_SCAFF_LINES" --agp "$agp_file" --id - -w \
				| grep -v ^# \
				| cut -f 1,6 | tee "$scaff_ctg" \
				| cut -f 2
		} > "$ctg_outfile"

		# extract contig sequences
		local tmp_file=$( mktemp XXXXX.mfasta )
		$EXTRACT_MFASTA "$fasta_file" "$ctg_outfile" \
			> "$tmp_file"
		$PREFIX_MFASTA "$tmp_file" "$cul|" \
			> "$ctg_fasta"
		rm -f "$tmp_file"*


		# once blast sourced, remove python source !
		source $__ROOT/lib/lib_pop_analysis.sh
		#~ python --version

		# note: this DB is not local at all.
		#	I still do this to get shared transposon and reduce generated files.
		#	but local DB (one per QTL,per scaffold, per window ?) is to test.
		F__make_blastDB $( get_mfasta "$cul" )
	} &

}

# just to make run them in parallel
function run_megablast ()
{
	local cul="$1"

	F__run_megablast $cul &
}

function main ()
{
	printf "1- extract contig sequence from partially unmapped scaffold.\n"
	# extract_ctg_fasta es-venus
	loopOverCultivars bnapus extract_ctg_fasta
	printf "Waiting for backgrounded job to terminate.\n"
	wait

	printf "2- compare each cultivar against all others.\n"
	source $__ROOT/lib/lib_pop_analysis.sh
	# F__run_megablast es-venus
	loopOverCultivars bnapus run_megablast
	printf "Waiting for backgrounded job to terminate.\n"
	wait
}

# =========================================================

printVars >&2

# your work.
main

printf "End of %s.\n\n" $( basename $0 ) >&2

exit 0
