#! /bin/bash

## AIMS  : Align an assembly to reference with Blast.
## USAGE : AlignAssemblyToRef.Blast.sh ref_name ass_name outdir
## AUTHORS : sebastien.letort@irisa.fr
## NOTE  : file generated is 1-based included [s;e].

source /softs/local/env/envblast-2.6.0.sh

source $__ROOT/lib/lib_AlignAssemblyToRef.sh

declare DB_REF=${REF_NAME}.blastDB # name of the ref database

# =========================================================
function align()
{
	local fasta_file="$1"

	Misc__log "Aligning $fasta_file on $DB_REF."

	local headers='sseqid sstart send qseqid qstart qend'
	headers=$headers' sstrand slen qlen length'
	headers=$headers' score evalue pident nident mismatch'
	headers=$headers' sseq qseq'

	# Note I used 97% of identity because it seems to solve lots of ambiguities
	#	seen on amber (first 10 duplets), the 2d alignments are
	#		95.190, 96.770, 95.111, 96.679, 100.000, 99.883, 98.204, 95.413, 99.907, 96.944
	#~ local cmd='blastn -task megablast'
	#~ cmd=$cmd' -word_size 32 -perc_identity 97 -qcov_hsp_perc 80'
	#~ cmd="$cmd -db $SEQ_REF -query $fasta_file"
	#~ cmd="$cmd -outfmt '6 $headers' -out $CUL.mblast.tsv"
	#~ cmd=$cmd' -num_threads 8'
	# Path to DB_REF is not a good solution, I don't be supposed to know the depth directory.
	blastn -task megablast \
		-word_size 32 -perc_identity 97 \
		-db ../../../../$DB_REF -query $fasta_file \
		-outfmt "6 $headers" -out $OUTFILE \
		-num_threads 8

	# I cannot make $cmd work with blast, seems like outfmt is badly interpreted
	#~ Misc__log "$cmd"
	#~ $cmd
}

function make_blastDB ()
{
	local -a DB_list=()

	for ref in $REF_NAME ${REF_NAME}_mito ${REF_NAME}_chloro
	do
		local seq_ref=$( get_ref_fasta $ref )
		local bname=$( basename "$seq_ref" )

		if [[ ! -e "$bname.nsq" ]]
		then
			ln -s "$seq_ref" "$bname"

			makeblastdb -in "$bname" -dbtype nucl -parse_seqids
		fi
		DB_list+=( $bname )
	done

	# build the full database
	#	I cannot use relative path to the DB, which would has been better.
	blastdb_aliastool -dblist "${DB_list[*]}" -dbtype nucl \
		-out $DB_REF -title $DB_REF

}


# =========================================================
printVars >&2

# only if not exist
make_blastDB
OUTFILE=$CUL.mblast.tsv

# your work.
main

printf "End of %s.\n\n" $( basename $0 )
exit 0

