#! /bin/bash

## AIMS  : parse the scaff.seq_len.tsv.gz to generate a json file of len distribution.
## USAGE : ./scaff_len2json.sh [-h] [-v] Assembly_dir > scaff_len.json
## NOTE  : the output file can be use as input do generate plots.
#
## OUTPUT : a json file containing hash with key=cultivar, value = lengths
#
## BUG   : None found yet.

set -o nounset
set -o errexit

source $__ROOT/brassicaDB.sh

# =========================================================
declare INDIR

# ---------------------------------------------------------
declare VERBOSE=0

# =========================================================
function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT    = $0\n"
	printf "╟ - INDIR     = $INDIR\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

# =============================================
function manageOptions ()
{
# getopts reminder:
#	FLAG, x will look for -x
#	x: will look for -x stg
#	if the option string start with ':', getopts turns to silent error reporting mode.
	while getopts "hv" opt
	do
		case $opt in
			v)	VERBOSE=1	;;
			h)
				usage && exit 0 ;;
			\?)	usage && exit 0 ;;
		esac
	done
	shift $((OPTIND-1))

	INDIR="${1?What is the directory containing the assemblies ?}"
}

function usage ()
{
		cat <<EOF
USAGE : $0 [options] INDIR

Options :

	-v : verbose mode, see parameter values.
	-h : print this message and exit.
EOF
}

# =========================================================
declare is_first=1

function extract_scaff_len ()
{
	local -r cul="$1"

	printf "Working with $cul\n" >&2

	if [[ 1 -eq $is_first ]]
	then
		if [[ 1 == $VERBOSE ]]
		then
			printf "\tit is the first cultivar.\n" >&2
		fi
		is_first=0
	else
		printf ","
	fi

	local perle='@a_ = map{ $_ + 0 } <>; print encode_json( {'$cul' => \@a_ } );'
	zcat "$INDIR"/$cul/scaff.seq_len.tsv.gz \
		| cut -f 2 \
		| perl -MJSON -le "$perle"
}

function main ()
{
	printf "["
	loopOverCultivars bnapus extract_scaff_len
	printf "]\n"
}

# =========================================================
manageOptions "$@"

if [[ 1 -eq $VERBOSE ]]
then
	printVars >&2
fi

# your work.
main

printf "End of %s.\n\n" $( basename $0 ) >&2
exit 0
