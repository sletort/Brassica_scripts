#!/usr/bin/env python
#-*- coding:utf-8 -*-

## AIMS  : generate an AGP file reflecting a corrected alignment of a new assembly onto the ref.
## USAGE : produceCleanAgpAssembly.py alignment.tsv [-o out.agp]
## ALGO  : Sort alignment by tgt to determinate "true" chrom ref
## NOTE  : franchement pas terrible. Je pense que le GFF n'est pas dans "l'esprit" de la version 3
##			Le détails de l'alignement devrait être mis dans l'attribut Gap au lieu de juste la taille.
##				=> JBrowse ne comprend pas cet "esprit", donc on laisse comme ça.
##			/!\ The alignement can include indel which makes alignment length on ref and target different.
##				The consequence is that it is impossible to place exactly 
##					the contig onto the ref without having the indel position.
## TODO  : Manage multiprocess.
## TODO  : integrate scaff size here could simplify QTL exploitation (gff parse only)

import sys
import pprint
import argparse	# option management, close to C++/boost library (?)
import csv
import operator
from collections import defaultdict
import gzip

PP = pprint.PrettyPrinter( indent=4, stream=sys.stderr )

# from http://stackoverflow.com/questions/2632199/how-do-i-get-the-path-of-the-current-executed-file-in-python#
from inspect import getsourcefile
import os.path
cur_file = os.path.realpath( getsourcefile( lambda:0 ) )
sys.path.insert( 0, os.path.dirname( cur_file ) + '/../lib' )

from RegionLink import *
import Misc

VERSION = '0.1'
VERBOSE = False
DEBUG   = False
GFF_SOURCE = 'finishing_'+VERSION
GFF_OUTPUT = "alignments.gff"
THRESHOLD_PC = 0.2 # the lowest, the higher distance between alignment authorized

# ========================================
def main( o_args ):

	verbose( "Init outfiles ...\n" )
	initOutfilesGFF( o_args )

	verbose( "Loading infiles ...\n" )
	o_csv = csv.reader( o_args.link_file, delimiter='\t' )
	lo_links = RegionLink.loadSimpleLinkFile( o_csv )
	verbose( "nb links loaded : " + str( len( lo_links ) ) + "\n" )
	debug( [ "First link :", "\t" + str( lo_links[0] ) ] )
	debug( [ "Last link :",  "\t" + str( lo_links[-1] ) ] )

	verbose( "Alter the data structure.\n" )
	do_links = hash_byTgt_byRef( lo_links )

	verbose( "Build GFF3 file.\n" )
	genereGff3( o_args, do_links )

	return


def initOutfilesGFF( o_args ):
	'''Note : o_args.out_ctg has been turn to None to prevent writing metadata.
		It is really a bad way to manage this outfile, but I have no time to think of another.'''
	def __initGff3( o_fs, o_json, species ):
		# TODO faire avec un try / exception
		o_fs.write( "##gff-version 3\n" )

		if( o_json ):
			d_species = o_json.getReference( species )

			URI = d_species.get( 'species_URI' )
			if( URI ):
				o_fs.write( "##species " + URI +"\n" )

			d_infos = d_species.get( "genome-build" )
			if( d_infos ):
				SRC = " ".join([ d_infos.get(k) for k in ( "source", "name" ) ])
				o_fs.write( "##genome-build " + SRC +"\n" )

			karyo_file = d_species.get( "sequence_descr" )
			if( karyo_file ):
				with open( karyo_file, 'r' ) as fe:
					d_seqs = Misc.loadTsvAsDict( fe )
					for k in sorted( d_seqs.keys() ):
						region = " ".join([ d_seqs[k][1], '1', d_seqs[k][2] ])
						o_fs.write( "##sequence-region " + region  + "\n" )
	# __initGff3

	for o_fs in ( o_args.output, o_args.out_ctg ):
		if( o_fs ):
			__initGff3( o_fs, o_args.o_json, o_args.species )

	if( None == o_args.out_ctg ):
		o_args.out_ctg = sys.stdout

	if( o_args.d_agp_links ):
		msg = "contig coordinates (id with .c*) are not exact due to indel in the alignment"
		o_args.out_ctg.write( "# WARNING : %s.\n" % msg )
		msg = "Contig are not really all in strand +. No time to correct this."
		o_args.out_ctg.write( "# WARNING : %s.\n" % msg )


def hash_byTgt_byRef( lo_links ):

	# fill the dictionnary
	d_tgt_ref_olinks = defaultdict( lambda: defaultdict( list ) )
	for o_link in lo_links:
		o_ref = o_link.o_ref
		o_tgt = o_link.o_tgt

		d_tgt_ref_olinks[o_tgt.chrom][o_ref.chrom].append( o_link )

	return d_tgt_ref_olinks

def getScaffoldSize( d_agp_links ):
	'''Return the scaffold size as a dictionnary.'''

	d_scaff_sizes = {}
	for ref_chrom in d_agp_links.keys():
		scaff_size = 0
		for o_link in d_agp_links[ref_chrom]:
			if( scaff_size < o_link.o_ref.end ):
				scaff_size = o_link.o_ref.end
		d_scaff_sizes[ref_chrom] = scaff_size

	return d_scaff_sizes

def genereGff3( o_args, do_links ):
	'''generate a GFF3 file
	IN : do_links = h{scaff}{ref_chrom} = lo_links
		TODO : separate alignment and contig match
	'''

	def __writeGffRows( o_fs, d_gff_rows ):
		
		for ref_chrom in sorted( d_gff_rows.keys() ):
			# sort by ? star/end ?
			l_rows = sorted( d_gff_rows[ref_chrom], key=lambda x: (x[3],x[2]) )
			for i in range( len( l_rows ) ):
				l_rows[i] = [ str(e) for e in l_rows[i] ]

			l_rows.append([ "###" ])

			o_fs.writelines([ "\t".join(l)+"\n" for l in l_rows ])
	# __writeGffRows

	def __generateOneScaffoldMatch_scaff( o_bloc, ID, l_elts, d_scaff_size ):
		l_out = []

		if( d_scaff_size ):
			scaff_size = d_scaff_size.get( o_bloc.o_tgt.chrom, None )
		else:
			scaff_size = None

		l_row = l_elts + __generateGff3Match( o_bloc, 'match', id=ID, scaff_size=scaff_size )
		l_out.append( l_row )
		#~ PP.pprint( l_row )

		i = 1
		l_algt_sizes = []

		for o_link in o_bloc.l_links:
			algt_id  = ID + '.p' + str(i)
			l_row = l_elts + __generateGff3Match( o_link, 'match_part', algt_id, pid=ID )
			l_out.append( l_row )

			i += 1

		return l_out
	# __generateOneScaffoldMatch

	def __generateOneScaffoldMatch_ctg( o_bloc, ID, lo_agp_links, l_elts ):
		agp_ID = ID + '.c'
		l_out  = []
		if( not lo_agp_links ):
			return l_out

		i = 1
		l_algt_sizes = []
		for o_link in o_bloc.l_links:
			if( 1 < len( o_bloc.l_links ) ):
				agp_id = agp_ID + str(i)
			else:
				agp_id = agp_ID
			l_algt_sizes.append( o_link.o_ref.length() )

			c = 1 # alignment-ctg id
			for o_agp_link in lo_agp_links:
				if( o_link.o_tgt.intersect( o_agp_link.o_ref ) ):
					ctg_id = agp_id + '.' + str( c )

					l_row = l_elts + __generateGff3Contig__2( o_agp_link, o_link, ctg_id, pid=agp_ID )
					#PP.pprint( l_row )
					l_out.append( l_row )
					c += 1
			i += 1

		if( lo_agp_links ):
			#~ #[ PP.pprint( x.recString() ) for x in lo_agp_links ]
			l_row = l_elts + __generateGff3CtgMatch( o_bloc, 'match', agp_ID, l_algt_sizes )
			l_out.append( l_row )

		return l_out
	# __generateOneScaffoldMatch

	# I use a dictionnay to write output sorted by ref_chrom
	d_gff_scaff_rows = defaultdict( list ) # keys will be ref_chrom
	d_gff_ctg_rows   = defaultdict( list ) # keys will be ref_chrom

	# patch to add % of scaffold in id
	if( o_args.d_agp_links ):
		d_scaff_size = getScaffoldSize( o_args.d_agp_links )
	else:
		d_scaff_size = None

	# for each scaffold
	for tgt_chrom in sorted( do_links.keys() ):
		if( o_args.d_agp_links ):
			lo_agp_links = o_args.d_agp_links[tgt_chrom]
		else:
			lo_agp_links = None

		for ref_chrom in sorted( do_links[tgt_chrom].keys() ):
			l_elts   = [ ref_chrom, o_args.gff_source ]
			lo_links = do_links[tgt_chrom][ref_chrom]

			o_bloc   = Bloc( lo_links )
			l_blocs  = o_bloc.checkCohesion( THRESHOLD_PC )
			k=1
			for o_bloc in l_blocs :
				if( 1 < len( l_blocs ) ):
					ID = '.'.join([ tgt_chrom, str(k) ])
				else:
					ID = tgt_chrom

				# scaff
				l_scaff_rows = __generateOneScaffoldMatch_scaff( o_bloc, ID, l_elts, d_scaff_size )
				d_gff_scaff_rows[ref_chrom] += l_scaff_rows

				# ctg
				l_ctg_rows = __generateOneScaffoldMatch_ctg( o_bloc, ID, lo_agp_links, l_elts )
				d_gff_ctg_rows[ref_chrom] += l_ctg_rows
				k+=1

	__writeGffRows( o_args.output,  d_gff_scaff_rows )
	__writeGffRows( o_args.out_ctg, d_gff_ctg_rows )
	
# genereGff3

def __buildAttrColumn( id, pid, l_target, l_gap=None, name=None ):
	l_attrs = [ 'ID=' + id ]

	if( name ):
		l_attrs.append( 'Name=' + name )

	if( pid ):
		l_attrs.append( 'Parent=' + pid )
	l_attrs.append( 'Target=' + ' '.join( l_target ) )

	if( l_gap ):
		l_attrs.append( 'Gap=' + ' '.join( l_gap ) )

	return l_attrs
# __buildAttrColumn

# Note: o_link can be a Bloc
def __generateGff3Match( o_link, feat_type, id='X', pid=None, scaff_size=None ):
	( chrom_start, chrom_end ) = o_link.o_ref.feature()

	if( 1 == o_link.strand ):
		strand = '+'
	else:
		strand = '-'
	l_match = [ feat_type, chrom_start, chrom_end, '.', strand, '.' ]

	if( scaff_size and type( o_link ) is Bloc ):
		align_len = 0.0 + o_link.getTgtAlignLength()
		pc   = 100 * ( align_len / scaff_size )

		name = "%s (%.2f %%)" % ( id,pc )
	else:
		name = None


	( tgt_start, tgt_end ) = o_link.o_tgt.feature()
	l_target = [ o_link.o_tgt.chrom, str(tgt_start), str(tgt_end) ]
	l_gap = o_link.getCigar()

	l_attrs = __buildAttrColumn( id, pid, l_target, l_gap, name )
	l_match.append( ';'.join( l_attrs ) )

	return l_match

def __generateGff3CtgMatch( o_link, feat_type, id, l_algt_sizes ):
	'''cracra : just need to alter gap element from Gff3Match'''

	#~ # build Gap string
	#~ ref_size = o_link.o_ref.length()
	#~ algt_sum_size = sum( l_algt_sizes )
	#~ delta = ref_size - algt_sum_size
	#~ delta_gap = int( round( ( 0.0 + delta ) / len( l_algt_sizes ) ) )
	#~ l_gaps = [ l_algt_sizes[0] ]
	#~ for size in l_algt_sizes[1:]:
		#~ l_gaps += [ delta_gap, size ]

	#~ if( 2 < len( l_gaps ) ):
		#~ # last correction due to round function
		#~ gap_sum_size = sum( l_gaps )
		#~ delta = ref_size - gap_sum_size
		#~ l_gaps[-2] += delta # correction on the last gap size


	l_match = __generateGff3Match( o_link, feat_type, id )

	#~ # decompose
	#~ l_attrs = []
	#~ for attr in l_match[-1].split( ';' ):
		#~ if attr.startswith( 'Gap=' ):
			#~ attr = 'Gap=M' + ' '.join( [ str( x ) for x in l_gaps ] )
		#~ l_attrs.append( attr )

	#~ #recompose
	#~ l_match[-1] = ';'.join( l_attrs )

	return l_match

# Note: o_link can be a Bloc
# Note: a more precise output can be generated with an ad-hoc parsing of delta file.
#	Here I just warn that aligned region can have a different size due to indel.
#	Target start/end attribute is not exact when contig does not align all along the alignment (too much work to correct, wait for a cleaner code)
def __generateGff3Contig( o_agp_link, o_algt_link, id='X', pid=None ):
	'''NOT USED ANYMORE'''
# JE NE ME SERS PAS DE CTG_START NI DE CTG_END
	#~ if( o_agp_link.o_tgt.chrom == 'NODE_422963_length_343_cov_3.300_ID_422962' ):
		#~ PP.pprint( o_agp_link.recString() )
		#~ PP.pprint( o_algt_link.recString() )

	if( o_algt_link.o_tgt.includes( o_agp_link.o_ref, strict=True ) ):
		if( 1 == o_algt_link.strand ):
			# ok scaffold_38793
			chrom_start = o_algt_link.o_ref.start - o_algt_link.o_tgt.start + o_agp_link.o_ref.start #+ 1 - 1
			chrom_end   = o_algt_link.o_ref.start - o_algt_link.o_tgt.start + o_agp_link.o_ref.end #+ 1 - 1
		else:
			chrom_start = o_algt_link.o_ref.start + o_algt_link.o_tgt.end - o_agp_link.o_ref.end + 1
			chrom_end   = o_algt_link.o_ref.start + o_algt_link.o_tgt.end - o_agp_link.o_ref.start + 1

		ctg_start   = o_agp_link.o_tgt.start
		ctg_end     = o_agp_link.o_tgt.end

	elif( o_algt_link.o_tgt < o_agp_link.o_ref ):
		align_len = o_algt_link.o_tgt.end - o_agp_link.o_ref.start + 1

		if( 1 == o_algt_link.strand ):
			chrom_start = o_algt_link.o_ref.end - align_len + 1
			chrom_end   = o_algt_link.o_ref.end
		else:
			chrom_start = o_algt_link.o_ref.start
			chrom_end   = o_algt_link.o_ref.start + align_len - 1

		if( 1 == o_agp_link.strand ):
			ctg_start   = o_agp_link.o_tgt.start
			ctg_end     = o_agp_link.o_tgt.start + align_len - 1
		else:
			ctg_start   = o_agp_link.o_tgt.end - align_len + 1
			ctg_end     = o_agp_link.o_tgt.end

	else:
		align_len = o_agp_link.o_ref.end - o_algt_link.o_tgt.start + 1
		#~ if( o_agp_link.o_tgt.chrom == 'NODE_783396_length_4489_cov_3.300_ID_783395' ):
			#~ PP.pprint( str( align_len ) )

		if( 1 == o_algt_link.strand ):
			chrom_start = o_algt_link.o_ref.start
			chrom_end   = o_algt_link.o_ref.start + align_len - 1
		else:
			chrom_start = o_algt_link.o_ref.end - align_len + 1
			chrom_end   = o_algt_link.o_ref.end

		if( 1 == o_agp_link.strand ):
			ctg_start   = o_agp_link.o_tgt.end - align_len + 1
			ctg_end     = o_agp_link.o_tgt.end
		else:
			ctg_start   = o_agp_link.o_tgt.start
			ctg_end     = o_agp_link.o_tgt.start + align_len - 1


	if( 1 == o_agp_link.strand * o_algt_link.strand ):
		strand = '+'
	else:
		strand = '-'
	l_match = [ 'match_part', chrom_start, chrom_end, '.', strand, '.' ]

	l_target = [ o_agp_link.o_tgt.chrom, str(ctg_start), str(ctg_end) ]

	l_attrs = __buildAttrColumn( id, pid, l_target )
	l_match.append( ';'.join( l_attrs ) )

	return l_match


def __generateGff3Contig__2( o_agp, o_algt, id='X', pid=None ):

	#~ if( o_agp.o_tgt.chrom == 'NODE_46695_length_1700_cov_3.300_ID_46694' ):
		#~ PP.pprint( o_agp.recString() )
		#~ PP.pprint( o_algt.recString() )

	def _X( len1, len2, ref_start, ref_end, strand ):
		#~ PP.pprint( { 'len1': len1, 'len2':len2, 'ref_start':ref_start, 'ref_end':ref_end, 'strand':strand } )
		if( len1 <= len2 ):
			return ref_start + len1 - 1
		return ref_end - len2 + 1

	if( o_agp.o_ref.start <= o_algt.o_tgt.start ):
		chrom_start = o_algt.o_ref.start
		len1        = ( o_agp.o_ref.end - o_algt.o_tgt.start )
		ctg_1       = o_agp.o_tgt.end - len1 if( 1 == o_agp.strand ) else o_agp.o_tgt.start + len1
#sys.stderr.write( "cgt = " + str( ctg_1 ) + "\n" )
	else:
		len1 = o_agp.o_ref.start - o_algt.o_tgt.start + 1
		len2 = o_algt.o_tgt.end - o_agp.o_ref.start + 1
		chrom_start = _X( len1, len2, o_algt.o_ref.start, o_algt.o_ref.end, o_algt.strand )
		ctg_1       = o_agp.o_tgt.start if( 1 == o_agp.strand ) else o_agp.o_tgt.end


	if( o_agp.o_ref.end < o_algt.o_tgt.end ):
		len1 = o_agp.o_ref.end - o_algt.o_tgt.start + 1
		len2 = o_algt.o_tgt.end - o_agp.o_ref.end + 1
		chrom_end = _X( len1, len2, o_algt.o_ref.start, o_algt.o_ref.end, o_algt.strand )
		ctg_2     = o_agp.o_tgt.end if( 1 == o_agp.strand ) else o_agp.o_tgt.start
	else:
		chrom_end = o_algt.o_ref.end
		ctg_2     = o_agp.o_tgt.start + ( o_agp.o_ref.end - o_algt.o_tgt.end )


	( ctg_start, ctg_end ) = ( ctg_1,ctg_2 ) if( ctg_1 <= ctg_2 ) else( ctg_2,ctg_1 )

	if( 1 == o_agp.strand * o_algt.strand ):
		strand = '+'
	else:
		strand = '-'
	l_match = [ 'match_part', chrom_start, chrom_end, '.', strand, '.' ]
	l_target = [ o_agp.o_tgt.chrom, str(ctg_start), str(ctg_end) ]
	#~ PP.pprint( { 'chrom' : l_match } )
	#~ PP.pprint( { 'ctg': l_target } )

	l_attrs = __buildAttrColumn( id, pid, l_target )
	l_match.append( ';'.join( l_attrs ) )

	return l_match

# ----------------------------------------
def verbose( msg ):
	if( VERBOSE ):
		sys.stderr.write( msg )

def debug( l_msg ):
	if( DEBUG ):
		for msg in l_msg:
			sys.stderr.write( "\t**" + msg + "\n" )

def analyseOptions( o_args ):
	global VERBOSE
	global DEBUG

	if( True == o_args.verbose ):
		VERBOSE = True

	if( True == o_args.debug ):
		DEBUG = True

	if( o_args.json_data ):
		from json_tool import JsonReference
		o_args.o_json = JsonReference( o_args.json_data )
		if( None == o_args.species ):
			raise ValueError( "--json_data args need --species args." )
	else:
		o_args.o_json = None

	d_ref = defaultdict( list )
	if( o_args.agp_ctg ):
		if( Misc.is_compressed( o_args.agp_ctg ) ):
			o_fe = gzip.open( o_args.agp_ctg )
		else:
			o_fe = open( o_args.agp_ctg )

		o_csv = csv.reader( o_fe, delimiter='\t' )
		verbose( "Loading agp file.\n" )
		for o_row in RegionLink.loadAgpFile( o_csv ):
			#~ ref_chrom = o_row.o_ref.chrom
			# bad way to remove _uid_\d+ extension on scaffold id.
			ref_chrom = '_'.join( o_row.o_ref.chrom.split( '_', 2 )[0:2] )
			o_ref = Region( ref_chrom, o_row.o_ref.start, o_row.o_ref.end )
			o_row.o_ref = o_ref
			d_ref[ref_chrom].append( o_row )
		o_fe.close()
	o_args.d_agp_links = d_ref

	if( o_args.gff_source ):
		o_args.gff_source = ".".join([ o_args.gff_source, GFF_SOURCE ])
	else:
		o_args.gff_source = GFF_SOURCE

	if( o_args.out_ctg == sys.stdout and o_args.output == sys.stdout ):
		o_args.out_ctg = None

	o_args.species = o_args.species.lower()

def defineOptions():
	descr = 'generate an AGP file '
	descr += 'reflecting a corrected alignment of an assembly to a ref.'
	o_parser = argparse.ArgumentParser( description=descr )

	# special
	o_parser.add_argument( '--version', action='version', version='%(prog)s '+VERSION )

	# positionnal / mandatory param
	#	type = int, file, str
	o_parser.add_argument( 'link_file', type=argparse.FileType('r'),
		               help='The link file : tsv -> r_chr,r_start,r_end, t_chr,t_start,t_end, [anything else].' )

	# optionnal
	#~ o_parser.add_argument( '--report', '-r',
							#~ action='store_true',
							#~ help='If set, will generate files with details.' )

	o_parser.add_argument( '--gff_source',
							help='specify a string to prepend to "'+GFF_SOURCE+'" used as source in gff output.' )

	o_parser.add_argument( '--json_data',
							type=argparse.FileType('r'),
							help='json file to enrich output.' )
	o_parser.add_argument( '--agp_ctg',
							help='agp file to enrich output (add contig lines).' )
	o_parser.add_argument( '--out_ctg',
							default=sys.stdout, type=argparse.FileType( 'w' ),
							help='gff containing contig alignment.' )
	o_parser.add_argument( '--species',
							help='The species to look at in json file (json_data needed !).' )

	o_parser.add_argument( '--verbose', '-v',
							action='store_true',
							help='If set, display some outputs.' )

	o_parser.add_argument( '--debug', '-d',
							action='store_true',
							help='If set, display debug informations.' )

	o_parser.add_argument( '--output', '-o',
							default=sys.stdout, type=argparse.FileType( 'w' ),
							help='Gff output filepath.' )

	return o_parser


# ----------------------------------------

if __name__ == '__main__':
	o_parser = defineOptions()
	o_args   = o_parser.parse_args()
	analyseOptions( o_args )

	if( True == o_args.verbose ):
		VERBOSE = True

	main( o_args )

	sys.exit( 0 )

