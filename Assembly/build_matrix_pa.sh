#! /bin/bash

## AIMS  : build a matrix presence/absence for all cultivars.
## USAGE : ./build_matrix_pa.sh [-h] [-v] -b QTL.bed -O outdir elts.gff indir
## NOTE  : 1 presence, -1 abcense, 0 no data
#
## OUTPUT : a tsv file, matrix, cultivars in column, elts in line
#
## BUG   : None found yet.

source /softs/local/env/envbedops-2.4.15.sh

set -o nounset
set -o errexit

source $__ROOT/brassicaDB.sh
source $__ROOT/lib/lib_misc.sh

# =========================================================
declare GFF_FILE
declare QTL_FILE=""
declare INDIR # directory containing alignments gs_bnapus_*
declare OUTDIR='.'

# ---------------------------------------------------------
declare VERBOSE=0

# =========================================================
function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT    = $0\n"
	printf "╟ - GFF_FILE  = $GFF_FILE\n"
	printf "╟ - QTL_FILE  = $QTL_FILE\n"
	printf "╟ - INDIR     = $INDIR\n"
	printf "╟ - OUTDIR    = $OUTDIR\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

# =============================================
function manageOptions ()
{
# getopts reminder:
#	FLAG, x will look for -x
#	x: will look for -x stg
#	if the option string start with ':', getopts turns to silent error reporting mode.
	while getopts "hvb:O:" opt
	do
		case $opt in
			b)  QTL_FILE=$OPTARG  ;;
			O)  OUTDIR=$OPTARG  ;;
			v)	VERBOSE=1	;;
			h)
				usage && exit 0 ;;
			\?)	usage && exit 0 ;;
		esac
	done
	shift $((OPTIND-1))

	GFF_FILE="${1?What is the annotation file (gff) containing element to detect ?}"
	INDIR="${2?What is the directory containing the alignments gs_bnapus_*?}"

	if [[ "" == "$QTL_FILE" ]]
	then
		printf "Region file is mandatory.\n" >&2
		exit 1
	fi
}

function usage ()
{
		cat <<EOF
USAGE : $0 [options] GFF_FILE INDIR

	GFF_FILE : the annotation file (gff) containing elements to detect
	INDIR    : the directory containing the alignments gs_bnapus_*

Options :

	[mandatory]
	-b bed_file : region to analyse.

	-v : verbose mode, see parameter values.
	-h : print this message and exit.
	-O outdir : directory where to put the results, default is current directory.
EOF
}

# =========================================================
function get_cultivar_data ()
{
	local cul=$1

	printf "$INDIR/gs_bnapus_$cul/against_ref/scaffs/$cul.starch"
}

function build_vector ()
{
	local cul=$1
	local template="$2"
	local elts_on_region="$3"

	# extract TE's QTL scaffolds 
	#~ bedops --element-of 1 QTL.$id.TE.bed $cul.tsv

	bedmap --count --fraction-ref 0.5 "$elts_on_region" $( get_cultivar_data $cul ) \
		> "$cul.$template" &
}

function get_vector_file_list ()
{
	local template="$1"
	local -a a_files=()

	for cul in $( loopOverCultivars bnapus echo | sort )
	do
		a_files+=( "$cul.$template" )
	done

	printf "${a_files[*]}"
}

# return a tsv string
function get_cultivar_list ()
{
	loopOverCultivars bnapus echo \
		| sort \
		| perl -le '@a_ = map{ chomp; $_ } <>; print join( "\t", @a_ );'
}

function main ()
{
	Misc__log "1) turn gff file to starch."
	local annot_file=elts.starch
	if [[ ! -e "$annot_file" ]]
	then
		gff2starch < $GFF_FILE > $annot_file
	fi

	Misc__log "2) Working with regions"
	while read chr s e id
	do
		Misc__log "	$id"

		local elts_on_region="$id.elt.bed"
		Misc__verbose "\tExtract elements present on the region."
		# extract elts from the QTL.
		printf "$chr\t$s\t$e\n" \
				| bedops --element-of 1 "$annot_file" - \
			> "$elts_on_region"

		# generate *.elts.$id.lst files
		local template="elts.$id.lst"
		loopOverCultivars bnapus build_vector "$template" "$elts_on_region"
		wait

		local list=$( get_vector_file_list $template )

		# gff2 format is not clean on column 10 (imo).
		{
			local cul_list=$( get_cultivar_list )
			printf "chrom\tstart\tend\tannot\t$cul_list\n"
			cut -f 1-3,10 "$elts_on_region" \
					| paste - $list
		} > $id.matrix.tsv

		rm -f $list
	done < "$QTL_FILE"
}

# =========================================================
manageOptions "$@"

if [[ 1 -eq $VERBOSE ]]
then
	printVars >&2
fi

# your work.
main

printf "End of %s.\n\n" $( basename $0 ) >&2
exit 0
