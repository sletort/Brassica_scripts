#!/usr/bin/env python
#-*- coding:utf-8 -*-

## AIMS  : generate link file with only validated alignment (reduced noise).
## USAGE : purgeAlignmentLinks.py [--report] [--keep x] [--scaff_sizes file] [--output out.tsv] alignment.tsv
## ALGO  : Sort alignment by tgt to determinate "true" chrom ref
## NOTE  : Alignment producted are not guaranteed to be unique.
## NOTE  : file generated is 1-based included [s;e].

import sys
import pprint
import argparse	# option management, close to C++/boost library (?)
import gzip
import csv
import operator
from collections import defaultdict


PP = pprint.PrettyPrinter( indent=4, stream=sys.stderr )

# from http://stackoverflow.com/questions/2632199/how-do-i-get-the-path-of-the-current-executed-file-in-python#
from inspect import getsourcefile
import os.path
cur_file = os.path.realpath( getsourcefile( lambda:0 ) )
sys.path.insert( 0, os.path.dirname( cur_file ) + '/../lib' )

from RegionLink import *
import Misc

VERSION = '0.1'
VERBOSE = False
DEBUG   = False
SCAFF_CHROM_SIZE_REPORT = "scaff_chrom_size.tsv"
EXCLUDED_LINK_REPORT    = "links_excluded.tsv"

# ========================================
def main( o_args ):

	verbose( "Loading infiles ...\n" )
	o_csv = csv.reader( o_args.link_file, delimiter='\t' )
	lo_links = RegionLink.loadSimpleLinkFile( o_csv )

	verbose( "nb links loaded : " + str( len( lo_links ) ) + "\n" )
	debug( [ "First link :", "\t" + str( lo_links[0] ) ] )
	debug( [ "Last link :",  "\t" + str( lo_links[-1] ) ] )

	# return for each scaff its best chrom ref (as list)
	verbose( "Look for inconsistent links.\n" )
#	PP.pprint( o_args.d_scaff_sizes )
	h_scaffs = lookForInconsistentLinks( lo_links, o_args.keep, o_args.report, o_args.d_scaff_sizes )

	verbose( "Then remove 'short chroms' from the alignment list (noise).\n" )
	lo_links = purgeLinks( lo_links, h_scaffs, o_args.report )

	RegionLink.writeSimpleLinkFile( o_args.output, lo_links )

	return

def lookForInconsistentLinks( lo_links, pc_min, flg_report, d_scaff_sizes ):
	'''
		IN  : lo_links = list of RegionLink
		IN  : pc_min   = we will keep alignment with pc_min (%) alignment max length 
		IN  : flg_report    = if true generate a report file with ref chrom bases per scaffold.
		IN  : d_scaff_sizes = a dictionnary with scaffold size, to add the information in the report.
		OUT : h_consistent_scaff[scaff] = l_chroms # list of chroms where they align.
	'''
	def __sum_by_ref_chrom( lo_links, h_scaffs ):
		'''for each tgt, sums the # of bases from each refchrom
			IN  = lo_links: sequence of RegionLink
			IN  = h_scaffs{ scaff } = dict
			OUT = h_scaffs will be updated like h_{ scaff }{ ref_chrom } = sum_size
		'''

		for o_link in lo_links:
			ref_chrom = o_link.o_ref.chrom
			scaff     = o_link.o_tgt.chrom

			h_sum_sizes = h_scaffs[ scaff ]
			sum_size    = h_sum_sizes.get( ref_chrom, 0 )

			sum_size   += o_link.o_tgt.length()
			h_sum_sizes[ref_chrom] = sum_size

		return h_scaffs

	def __get_longuest_chroms( h_sum_sizes, pc_min, scaff_size ):
		'''	simply return the chrom(s) with the max #_bases
			most of the time it will be a one element list.
			IN  = h_sum_sizes{ ref_chrom } = #_bases
			OUT = l_best_chrom, list of ref_chrom
			ALGO : sort the dictionnay on values, determine size threshold and filter.
			NOTE : I add scaff_size because an alignment can be repeated in the same chrom, making the sum unrelevant.
		'''
		l_best_chrom = []
		lt_chrom_sorted_by_nb_bases = \
					sorted( h_sum_sizes.items(),
							key=operator.itemgetter(1),
							reverse=True )

		t_best_chrom  = lt_chrom_sorted_by_nb_bases[0] # ( chrom, align_sum )
		if( t_best_chrom[1] < scaff_size ):
			max_size  = t_best_chrom[1]
		else:
			max_size  = scaff_size
		len_threshold = max_size * pc_min

		for t_chrom in lt_chrom_sorted_by_nb_bases:
			if( len_threshold <= t_chrom[1] ):
				l_best_chrom.append( t_chrom[0] )
			else:
				break

		return l_best_chrom
	# __get_longuest_chrom

	def __printScaffChromSums( h_scaffs, d_scaff_sizes=None ):
		'''Simply print scaff with their chrom'''
		with open( SCAFF_CHROM_SIZE_REPORT, "w" ) as o_fs:
			for scaff,size in d_scaff_sizes.items():		# I use keys and not iteritems to be compatible with python3
				l_out = [ scaff, str( size ) ]

				for chrom,size in h_scaffs[scaff].items():
					l_out += [chrom, str( size ) ]

				o_fs.write( "\t".join( l_out ) + "\n" )
		sys.stderr.write( SCAFF_CHROM_SIZE_REPORT + " has been generated.\n" )

		return
	# __printScaffChromSums

	# scaff => ref_chrom => size sum
	verbose( "\tFor each scaff, sums bases by ref_chrom.\n" )
	h_scaffs = { x:dict() for x in d_scaff_sizes.keys() }	# I use keys and not iterkeys to be compatible with python3
	__sum_by_ref_chrom( lo_links, h_scaffs )
	#~ PP.pprint( h_scaffs )

	if( flg_report ):
		verbose( "\t\tprint a report on chrom size by scaff.\n" )
		__printScaffChromSums( h_scaffs, d_scaff_sizes )

	# now for each scaff, I will keep only the chrom with the longuest scaff size on it.
	# scaff => ref_chrom
	verbose( "\tFor each scaff, determine the longuest chrom(s).\n" )
	h_consistent_scaffs = {}
	for scaff,h_chroms in h_scaffs.items():	# I use keys and not iteritems to be compatible with python3
		if( 0 < len( h_chroms ) ):
			l_best_chrom = __get_longuest_chroms( h_chroms, pc_min, d_scaff_sizes[scaff] )
			h_consistent_scaffs[scaff] = l_best_chrom

	return h_consistent_scaffs

def purgeLinks( lo_links, h_scaffs, flg_report ):
	'''Remove links with "short chroms", which are probably unspecific alignment.
		IN  = lo_links: a list of RegionLink
			= h_scaffs{ scaff } = list of longuest ref_chroms.
	'''
	lo_excluded = []
	lo_kept     = []
	for o_link in lo_links:
		l_best_chr = h_scaffs[ o_link.o_tgt.chrom ]
		if( o_link.o_ref.chrom not in l_best_chr ):
			lo_excluded.append( o_link )
		else:
			lo_kept.append( o_link )

	if( flg_report ):
		with open( EXCLUDED_LINK_REPORT, "w" ) as o_fs:
			RegionLink.writeSimpleLinkFile( o_fs, lo_excluded )
		sys.stderr.write( EXCLUDED_LINK_REPORT + " has been generated.\n" )

	return lo_kept


# ----------------------------------------
def verbose( msg ):
	if( VERBOSE ):
		sys.stderr.write( msg )

def debug( l_msg ):
	if( DEBUG ):
		for msg in l_msg:
			sys.stderr.write( "\t**" + msg + "\n" )

def analyseOptions( o_args ):
	global VERBOSE
	global DEBUG
	if( True == o_args.verbose ):
		VERBOSE = True
	if( True == o_args.debug ):
		DEBUG = True

	if( o_args.scaff_sizes ):
		if( Misc.is_compressed( o_args.scaff_sizes ) ):
			# python3 => mode='rt'
			o_fe = gzip.open( o_args.scaff_sizes, mode='rt' )
		else:
			o_fe = open( o_args.scaff_sizes )

		o_args.d_scaff_sizes = { x[0]: int(x[1]) for x in Misc.load2ColsAsDict( o_fe ).items() }
		o_fe.close()

	global EXCLUDED_LINK_REPORT
	global SCAFF_CHROM_SIZE_REPORT
	outdir = os.path.dirname( o_args.output.name ) or "."
	EXCLUDED_LINK_REPORT    = outdir + "/" + EXCLUDED_LINK_REPORT
	SCAFF_CHROM_SIZE_REPORT = outdir + "/" + SCAFF_CHROM_SIZE_REPORT

def defineOptions():
	descr = 'excluded unspecific links. '
	descr += 'Only alignments on the chroms that hold the maximum number of bases are kept.'
	o_parser = argparse.ArgumentParser( description=descr )

	# special
	o_parser.add_argument( '--version', action='version', version='%(prog)s '+VERSION )

	# positionnal / mandatory param
	#	type = int, file, str
	o_parser.add_argument( 'link_file', type=argparse.FileType('r'),
		               help='The link file : tsv -> r_chr,r_start,r_end, t_chr,t_start,t_end, [anything else].' )

	# optionnal
	o_parser.add_argument( '--keep', '-k',
							default='1', type=float,
							help='Percentage of alignment max length to keep.' )

	o_parser.add_argument( '--report', '-r',
							action='store_true',
							help='If set, will generate files with details.' )

	o_parser.add_argument( '--scaff_sizes',
							help='tsv file with scaffold size to enrich output (add scaff size in report).' )

	o_parser.add_argument( '--verbose', '-v',
							action='store_true',
							help='If set, display some outputs.' )

	o_parser.add_argument( '--debug', '-d',
							action='store_true',
							help='If set, display debug informations.' )

	o_parser.add_argument( '--output', '-o',
							default=sys.stdout, type=argparse.FileType( 'w' ),
							help='output filepath.' )

	return o_parser

# ----------------------------------------

if __name__ == '__main__':
	o_parser = defineOptions()
	o_args   = o_parser.parse_args()
	analyseOptions( o_args )

	main( o_args )

	sys.exit( 0 )

