#! /bin/bash

## AIMS  : Align an assembly to reference with nucmer.
## USAGE : AlignAssemblyToRef.Nucmer.sh ref_name ass_name
## AUTHORS : sebastien.letort@irisa.fr
## NOTE  : Not tested since refactorisation.

source /softs/local/env/envMUMmer-3.22.sh

source $__ROOT/lib/lib_AlignAssemblyToRef.sh

# =========================================================
# ---------------------------------------------------------
# minimum alignment length
# this is minimum exact match,for nucmer
#	value too high, 250 is better, without major speed degradation
#	100 is even better, but run 10 times slower !
declare -r ALIGN_MINI=500

# =========================================================
function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT     = $0\n"
	printf "╟ - ALIGN_MINI = $ALIGN_MINI\n"
	printf "╟ "
	printf "╟ - REF_NAME   = $REF_NAME\n"
	printf "╟ - CULTIVAR   = $CUL\n"
	printf "╟ - OUTDIR     = $OUTDIR\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

function align()
{
	local fasta_file="$1"

	local ref_fasta=$( get_ref_fasta $REF_NAME )
	Misc__log "Aligning $fasta_file on $ref_fasta."

	local cmd="nucmer --mum --minmatch $ALIGN_MINI $ref_fasta $fasta_file"
	Misc__log "$cmd"
	$cmd
	show-coords -g -r -T out.delta -H \
		> coords.tsv
}

function reformat_output ()
{
	local seq_ref_path=$( dirname $( get_ref_fasta $REF_NAME ) )

	# show-coords output is ass1=r ass2=t: r_start r_end r_len t_start t_end t_len t_% r_id t_id
	# With first perl line, I turn it to region-file [id,start,len]x2, and keep r_len,t_len
	#
	# second perl split NCBI id, and outputs
	#       gi      919454328       ref     NC_027760.1     18      156     NODE_1228352_length_139_cov_10.661_ID_1228351   139     1
	#
	printf "nucmer outputs will be concatenated, with column reordered to proper define Regions.\n"
	printf "Rows will then be sorted and NCBI ids converted to more understandable chrom name.\n"
	tmp_file=$( mktemp coords.XXXX.tsv )
	cat coords.tsv \
			| perl -lane 'print join( "\t", @F[7,0,1, 8,2,3, 4,5] )' \
		> $tmp_file


# code used when ref was NCBI, to extract chrom name and turn them into "our" labels.

#			| perl -F'[\|\t]+' -lane 'print join( "\t", $F[3], @F[4..$#F] );' \
#			| sort -V \
#		> $tmp_file

	if [[ -e $seq_ref_path/*.ids_chrom.tsv ]]
	then
		$__ROOT/generic/dictionnary_based_conversion.py -d $seq_ref_path/*.ids_chrom.tsv -i $tmp_file \
			> coords.tsv
	else
		mv $tmp_file coords.tsv
	fi
}


# =========================================================
printVars >&2

# your work.
main

printf "End of %s.\n\n" $( basename $0 )
exit 0

