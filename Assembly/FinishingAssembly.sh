#! /bin/bash

## AIMS  : Finishing Genome assembly by aligning scaffold on a reference, and produce files like GFF3.
## USAGE : FinishingAssembly.sh -R reference -O outdir cultivar
#
## OUTPUT :
## OUTFILE:
#
## NOTE  : Environnement have to be sourced before.
## NOTE  : Alignments generated are not guaranteed to be uniq.
## NOTE  : We only remove alignments that represents less than $PC_KEEP of the biggest alignment.
#
## BUG   : None found yet.

source /softs/local/env/envpython-2.7.sh
source /softs/local/env/envbedops-2.4.15.sh

set -o nounset
set -o errexit

source $__ROOT/brassicaDB.sh

source $__ROOT/lib/lib_misc.sh

# =========================================================
declare REF_NAME
declare CUL_NAME	# name of the cultivar
declare OUTDIR

# ---------------------------------------------------------
declare VERBOSE=0
declare -r CWD_=$( dirname $( readlink -e $0 ) )
declare -rl PIPELINE_NAME="Gatb-minia"
declare ON_CHUNKS=0
declare -r PC_KEEP=0.9 # % of max alignment to keep in output.
declare -r JSON_FILE=$__ROOT/References.json

# =========================================================
function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT     = $0\n"
	printf "╟ - REF_NAME   = $REF_NAME\n"
	printf "╟ - CUL_NAME   = $CUL_NAME\n"
	printf "╟ - OUTDIR     = $OUTDIR\n"
	printf "╟ - ON_CHUNKS  = $ON_CHUNKS\n"
	printf "╟ - PC_KEEP    = $PC_KEEP\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

# =============================================
function manageOptions ()
{
# getopts reminder:
#	FLAG, x will look for -x
#	x: will look for -x stg
#	if the option string start with ':', getopts turns to silent error reporting mode.
	while getopts "R:O:hvc" opt
	do
		case $opt in
			R)	REF_NAME=$OPTARG  ;;
			O)	OUTDIR=$OPTARG ;;

			c)	ON_CHUNKS=1	;;
			v)	VERBOSE=1	;;
			h)
				usage && exit 0 ;;
			\?)	usage && exit 0 ;;
		esac
	done
	shift $((OPTIND-1))

	CUL_NAME="${1?Please name the cultivar}"

	if [[ -z $REF_NAME ]]; then printf "pb with REF_NAME\n" >&2; fi
	if [[ -z $OUTDIR ]]; then printf "pb with OUTDIR\n" >&2; fi
}

function usage ()
{
		cat <<EOF
USAGE : $0 [options] cultivar

Options :
	-R String   : reference name. Bnapus, Brapa or Boleracea.
	-O dirpath  : directory path where data will be generated.

	-c : if set, perform alignment on chunks of reference file.
	-v : verbose mode, see parameter values.
	-h : print this message and exit.
EOF
}


# =========================================================
function extract_agp_info ()
{
	local ass_path=$( get_assembly_path $CUL_NAME )
	local tmp_fp=${CUL_NAME}_besst/BESST_output/pass1/info-pass1.agp.gz
	local agp_fp=$ass_path/$tmp_fp

	printf "$agp_fp"
}

function get_scaff_len ()
{
	local ass_path=$( get_assembly_path $CUL_NAME )
	local scaff_len=$ass_path/scaff.seq_len.tsv.gz

	printf "$scaff_len"
}


function tsv_to_starch ()
{
	local infile="$1"
	local sort_opt=${2-0}

	# to test : outfile=${infile#.tsv}.starch
	local outfile=$( dirname $infile )/$( basename $infile .tsv ).starch

	# turn 1-based [s,e] to 0-based [s,e[
	local perle='$F[1] -= 1;'
	perle=$perle' if( $F[4] < $F[5] ){ $F[4] -= 1; }else{ $F[5] -= 1; }'
	perle=$perle' print join( "\t", @F );'
	local cmd=""
	if [[ 1 -eq $sort_opt ]]
	then
		cmd="$__ROOT/misc/sort_regionlinks.py"
	else
		cmd="cat"
	fi

	$cmd "$infile" \
			| perl -lane "$perle" \
			| sort-bed - | starch - \
		> $outfile
}

function main ()
{
	local cmd

	# generate a coords.tsv in $OUTDIR
	printf "1- align assemblies to Reference.\n"
	# if [[ 1 -eq $ON_CHUNKS ]]
	# then
		# cmd="$CWD_/Align2Assemblies_byChunks.sh $REF_NAME $CUL_NAME $OUTDIR"
	# else
		cmd="$CWD_/AlignAssemblyToRef.Blast.sh $REF_NAME $CUL_NAME $OUTDIR"
	# fi
	Misc__log "$cmd"
	$cmd


	# OUTDIR has to contain only the alignment file (with tsv extension).
	printf "2- purge link => remove small alignment and noise.\n"
	local workdir="$OUTDIR/against_ref/scaffs/"
	local  infile="$workdir/$CUL_NAME.mblast.tsv"
	local outfile="$workdir/$CUL_NAME.purged.tsv"
	local options="--report --keep $PC_KEEP"
	options+=" --scaff_sizes "$( get_scaff_len )
	cmd="$CWD_/purgeAlignmentLinks.py $options --output $outfile $infile"
	Misc__log "$cmd"
	$cmd

	printf "3- additionnal removal, remove trully unspecific alignment.\n"
	infile="$outfile"
	outfile="$workdir/$CUL_NAME.tsv"
	# cmd="$CWD_/AdHoc/qd_filter_links.py "$( get_scaff_len )" $infile"
	# cmd=$cmd" | $__ROOT/misc/sort_regionlinks.py -"
	# Misc__log "$cmd"
	# $cmd > "$outfile" # seems that redirection and pipe cannot be used in variable.
	$CWD_/AdHoc/qd_filter_links.py "$( get_scaff_len )" $infile \
			| $__ROOT/misc/sort_regionlinks.py - \
		> $outfile

	# generate a $CUL_NAME.gff in $OUTDIR
	printf "4- generate gff file for each alignment (to integrate in JBrowse).\n"
	options="--json_data $JSON_FILE --species $REF_NAME"
	options+=" --gff_source $PIPELINE_NAME"
	options+=" --agp_ctg "$( extract_agp_info )
	local outputs="-o $workdir/$CUL_NAME.gff --out_ctg $workdir/$CUL_NAME.ctg.gff"

	if [[ 1 -eq $VERBOSE ]]
	then
		options+=" -v"
	fi
	infile="$outfile"
	cmd="$CWD_/genereCleanGffAssembly.py $options $outputs $infile"
	Misc__log "$cmd"
	$cmd

	# I lost what is not a region in the sort, not what I want.
	#~ tsv_to_starch "$OUTDIR/$CUL_NAME.mblast.tsv" 1
	tsv_to_starch "$infile" 0
	Misc__log "Waiting for starch to terminate."
	wait
}

# =========================================================
manageOptions "$@"

if [[ 1 -eq $VERBOSE ]]
then
	printVars
fi

# your work.
main

printf "End of %s.\n\n" $( basename $0 )
exit 0
