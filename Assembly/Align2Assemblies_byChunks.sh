#! /bin/bash

## AIMS  : Align 2 assemblies by ref chunks/chrom with nucmer.
## USAGE : Align2Assemblies.sh ref_name ass_name
## TODO  : gérer où est généré le fichier out.delta et où on exécute le chainage.
## AUTHORS : sebastien.letort@irisa.fr

set -o nounset
set -o errexit

declare -r BASEPATH=$( dirname $( readlink -e $0 ) )/..

source $BASEPATH/brassicaDB.mine.sh
source $BASEPATH/lib/lib_misc.sh

# =========================================================
declare -r REF_NAME="${1?What is the reference name to use ?}"
declare -r ASSEMBLY="${2?What is the assembly to align onto the reference ?}"
declare -r OUTDIR="${3?What is the output directory ?}"

# ---------------------------------------------------------
# minimum alignment length
declare -r ALIGN_MINI=500

# =========================================================
function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT     = $0\n"
	printf "╟ - REF_NAME   = $REF_NAME\n"
	printf "╟ - ASSEMBLY   = $ASSEMBLY\n"
	printf "╟ - ALIGN_MINI = $ALIGN_MINI\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

function align_by_chunks()
{
	local wd="$1"
	local fasta_file="$2"

	local ref_chunk
	for ref_chunk in $( myDB__get_ref_chunk_fasta $REF_NAME )
	do
	{
		Misc__log "Aligning $fasta_file on $ref_chunk."

		local chunk_dir=$( basename $ref_chunk .fa )
		mkdir -p $wd/$chunk_dir
		cd $wd/$chunk_dir

		local cmd="nucmer --mum --minmatch $ALIGN_MINI $ref_chunk $fasta_file"
		Misc__log "$cmd"
		$cmd
		show-coords -g -r -T out.delta -H \
			> coords.tsv
	}&
	done

	printf "Waiting for alignments to terminate.\n"
	wait
}

function get_assembly_filepath ()
{
	local cultivar="$1"
	local found=0

	local path=$( myDB__get_assembly_path $ASSEMBLY )
	for f in "$path"/${cultivar}*.fasta "$path"/${cultivar}*.fa "$path"/${cultivar}*.gz
	do
		if [[ -e $f ]]
		then
			if Misc__is_gzip $f
			then
				local tmp_file=$( mktemp --tmpdir=$PWD ${cultivar}.XXXXX.fa )
				gzip -dc $f > $tmp_file
				printf "$tmp_file"
			else
				printf "$f"
			fi
			found=1
			break
		fi
	done

	if [[ 0 -eq $found ]]
	then
		printf "No suitable file could be found in $path.\n" >&2
		exit 1
	fi
}

function main ()
{
	Misc__log "Align with nucmer (followed by show-coords), by chunk\n"

	local assembly_filepath=$( get_assembly_filepath "$ASSEMBLY" )
	printf "Will assembly file : %s\n" "$assembly_filepath"

	mkdir -p ${OUTDIR}
	local wd=$( readlink -e $PWD/${OUTDIR} )
	align_by_chunks "$wd" "$assembly_filepath"
	cd "$wd"

	local seq_ref_path=$( dirname $( myDB__get_ref_fasta $REF_NAME ) )

	# show-coords output is ass1=r ass2=t: r_start r_end r_len t_start t_end t_len t_% r_id t_id
	# With first perl line, I turn it to region-file [id,start,len]x2, and keep r_len,t_len
	#
	# second perl split NCBI id, and outputs
	#       gi      919454328       ref     NC_027760.1     18      156     NODE_1228352_length_139_cov_10.661_ID_1228351   139     1
	#
	printf "nucmer outputs will be concatenated, with column reordered to proper define Regions.\n"
	printf "Rows will then be sorted and NCBI ids converted to more understandable chrom name.\n"
	cat */coords.tsv \
			| perl -lane 'print join( "\t", @F[7,0,1, 8,2,3, 4,5] )' \
			| perl -F'[\|\t]+' -lane 'print join( "\t", $F[3], @F[4..$#F] );' \
			| sort -V \
			| $BASEPATH/generic/dictionnary_based_conversion.py -d $seq_ref_path/*.ids_chrom.tsv \
		> coords.tsv
}

# =========================================================
printVars >&2

# your work.
main

printf "End of %s.\n\n" $( basename $0 )
exit 0

