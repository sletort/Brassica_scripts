#! /bin/bash

## AIMS  : generate a file with all sequence length and compute a N(g)50 score, from a multi fasta file
## USAGE : NG50.sh file.fasta [genome_length]
## NOTE  : Output is raw : N50, L50 and assembly length on 3 line, once for all seqs, and another one for seqs > 5kb
## TODO  : Make a version with options, like json output and full report output

declare -r ROOT__=$( dirname $( readlink -e $0 ) )/..

set -o nounset
set -o errexit

source $ROOT__/lib/lib_assembly.sh
source $ROOT__/lib/lib_misc.sh

# =========================================================
declare -r INFILE="${1?What is your input file (mfasta) ?}"
declare -r G_LENGTH="${2-0}"

# ---------------------------------------------------------
# constante definition

# dependancies
Misc__assert_cmd python

# =========================================================
function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT     = $0\n"
	printf "╟ - INFILE     = $INFILE\n"
	printf "╟ - G_LENGTH   = $G_LENGTH\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

# =========================================================
function main ()
{
	local tmp_file=$( mktemp XXXX.seq_len.tsv )

	# dependency on biopython, not that good ...
	$ROOT__/generic/mfasta_print_seq_length.py "$INFILE" \
			| sort -nrk2,2 \
		> $tmp_file

	Ass__print_NG50 $tmp_file $G_LENGTH 1

#	rm -f $tmp_file
}

# =========================================================
printVars >&2

# your work.
main

printf "End of %s.\n\n" $( basename $0 ) >&2

exit 0
