#! /bin/bash

## AIMS  : Generate a gff file representing the Ocean annotation on B napus ref.
## USAGE : ./qd_genere_Bnapus_ocean.sh
## NOTE  : Very dirty.
#
## OUTPUT : generate several files

source /softs/local/env/envbedops-2.4.15.sh
source /softs/local/env/envpython-2.7.sh
source /softs/local/env/envsamtools-1.6.sh

set -o nounset
set -o errexit

source $__ROOT/brassicaDB.sh

# =========================================================

function loop ()
{
	for chrom in $( cut -f 1 $( get_ref_seq_len bnapus ) )
	do
		$__ROOT/generic/detectOcean.py --chr $chrom -f gff $ref_path \
			> tmp.$chrom.gff &
	done
	wait
}

# to test gnu parallel
function paral ()
{
	cut -f 1 $( get_ref_seq_len bnapus ) \
			| parallel -a - "$DB__ROOT/generic/detectOcean.py --chr {} -f gff $ref_path > tmp.{}.gff"
}


# =========================================================
# Note : Both method run at same speed.
#	to reduce dependency I'll use the loop function.
function compute_with_loop ()
{
	local ref_path="$1"

	printf "Loop\n"
	time loop "$ref_path"

	cat tmp.*.gff \
		> Bnapus.ocean.loop.gff
	rm -f tmp.*.gff
}

function compute_with_paralel ()
{
	local ref_path="$1"

	printf "paral\n"
	time paral "$ref_path"
	cat tmp.*.gff \
		> Bnapus.ocean.paral.gff
	rm -f tmp.*.gff
}


function main ()
{
	local ref_path=$( get_ref_fasta bnapus )

	# uniquement pour générer localement le fichier d'index
	#	(pas les droits d'écriture sur la ref)
	ln -s $ref_path
	ref_path=$( basename $ref_path )
	samtools faidx "$ref_path"

	# compute_with_paralel "$ref_path"
	compute_with_loop "$ref_path"

	#diff -q Bnapus.ocean.*.gff

	{
		printf "##gff-version 3\n"
		cat Bnapus.ocean.loop.gff
	} >  Bnapus.ocean.gff
	rm -f Bnapus.ocean.loop.gff

	gff2starch < Bnapus.ocean.gff \
		> Bnapus.starch
	#~ cut -f 1,4,5 Bnapus.ocean.gff \
			#~ | sort-bed - \
		#~ > Ocean.bed
}

# =========================================================
# your work.
main

printf "End of %s.\n\n" $( basename $0 ) >&2
exit 0
