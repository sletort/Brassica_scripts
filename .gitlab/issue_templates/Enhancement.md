**Part** : Milestone/project

**File** : the_file_concerned

**Why** : the purpose of the enhancement.

**Proposition** :
* what should be enhanced ?  
//example  
* make it use argparse with the signature  
`prog [-o out] [-b bed_file] infile`,  
and make it support `-h` to get help.

