#! /bin/bash

## AIMS  : provides personal paths for Rapsodyn project, specific to a machine.
## USAGE : source rapso_paths.sh
## NOTE  : This is a requirement of rapsodynDB.sh

# =========================================================
# where	valid assembly file are stored
declare -r DB_ASSEMBLY_ROOT=''
declare -r DB_RAWDATA_PATH=''
