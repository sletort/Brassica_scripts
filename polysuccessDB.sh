#! /bin/bash

## AIMS  : provides functions/vars to access easily to PolySuccess files
## USAGE : source polysuccess.sh (through brassicaDB.mine.sh)

set -o nounset
#set -o errexit # kill bash if sourced

declare -r __envDB__=1

# =========================================================
declare __ROOT=$( dirname $( readlink -e $BASH_SOURCE ) )
declare -r __META_FILE="$__ROOT/PolySuccess/metadata/cultivar.descr.tsv"

source $__ROOT/poly_paths.sh

# ---------------------------------------------------------
# =========================================================
declare -rA __H_FUNCTIONS=(
		#~ [DB__printVars]="DB__printVars\n\tFor debugging purpose, prints variables (like constant sourced)."
		#~ [DB__help]="DB__help [function_name]\n\tPrints a tiny help/usage for the function.\n\tIf no function_name, all functions help are printed."
		#~ [__DB__assertFile]="__DB__assertFile filepath\n\tPrints a message on stderr if the file does not exist."

		#~ # end-user functions
		#~ [rDB__loopOverCultivars]="rDB__loopOverCultivars func \"arg1 arg2 ...\"\n\trun run on each cultivars, first arg of func is cultivar."
	)

# =========================================================
source $__ROOT/var_export.sh
