#! /bin/bash

# shell to use
#$ -S /bin/bash

#$ -M sletort@irisa.fr
# send mail before b, at the end e, on stop a, on suspend s, or never n
#$ -m bea

#$ -N script_name

# if the script should be run in the current directory
#$ -cwd
##$ -i input_file
##$ -o output_file

# merge stdout and stderr
#$ -j no


# ne pas utiliser, ça ne sert qu'à afficher ce qui va être envoyer à la queue ?
# check submitted task ( -> ça veut dire quoi ?)
##$ -verify


## AIMS  : run a job on the cluster biogenouest
## USAGE : run_cluster.tmpl.sh

# Rapsodyn environnement
declare -r WORKDIR=/omaha-beach/sletort/

source $WORKDIR/rapsodynDB.sh
source $WORKDIR/brassicaDB.sh

# work script
source $__ROOT/test.sh "$@"

exit 0

