#!/usr/bin/env python
# -*- coding: utf-8 -*-

## AIMS  : a template for all mfasta analyse that can be done chrom by chrom or region by region.
## USAGE : X.py [--chr X] [--bed bed_file] fasta_file
## NOTE  : Bed_file can be stdin if '-' is used. fasta_file must be a filename.
## NOTE  : bed_file is 0-based, half-open intervals, end is excluded. It like start is 0-based and end 1-based
## AUTHORS : sebastien.letort@irisa.fr


import sys
import pdb	# debug
import pprint # debug
import argparse	# option management, close to C++/boost library (?)
import os	# for local libraries
import csv	# manage tsv files also.
import pysam


# local libraries
f_path = os.path.dirname( os.path.realpath( __file__ ) )
sys.path.append( f_path + "/../lib" )

VERSION = "0.1.0"
PP = pprint.PrettyPrinter(indent=4)
VERBOSE   = False

# ========================================
class Region( object ):
	def __init__( self, chrom, start, end ):
		self.__chrom = chrom
		self.__start = int( start )
		self.__end   = int( end )

	def samtools_string( self ):
		'''samtools string have start one based.
		
		samtools defines region 1-based, with end included in the interval.
		Pysam does not include then end.
		So to have the correct behaviour, I add 1 to start only.'''
		return "{0}:{1}:{2}".format( self.__chrom, 	str(self.__start+1), self.__end )

class BedFile( list ):
	def __init__( self, o_fe ):
		o_csv = csv.reader( o_fe, delimiter='\t' )
		for a_elts in o_csv:
			print( a_elts )
			self.append( Region( *a_elts[0:3] ) )

# ========================================
def action( seq_id, seq ):
	'''The action to perform on the wanted sequence.'''
	print( "{0} : {1}".format( seq_id, str( len( seq ) ) ) )
	print( seq )

def main( o_args ):
	print( "La fonction principale" )
	verbose( "En mode verbeux." )

	with pysam.FastaFile( o_args.fasta_file ) as fe:
		verbose( "Working with file : " + fe.filename )

		if( o_args.chr ):
			seq = fe.fetch( reference=o_args.chr )
			action( o_args.chr, seq )
		elif( o_args.f_bed ):
			o_bed = BedFile( o_args.f_bed )
			for o_rec in o_bed:
				reg = o_rec.samtools_string()
				seq = fe.fetch( region=reg )
				action( reg, seq )
		else:
			for ref in fe.references:
				seq = fe.fetch( reference=ref )
				action( ref, seq )

	if( o_args.f_bed ):
		o_args.f_bed.close()

	return 0

# ----------------------------------------
def defineOptions():
	o_parser = argparse.ArgumentParser( description='Prog descr.' )

	# special
	o_parser.add_argument( '--version', action='version', version='%(prog)s ' + VERSION )

	# positionnal / mandatory param
	#	type = int, file
	o_parser.add_argument( 'fasta_file',
		               help='The fasta filename to parse, can be bgzipped.' )

	# optionnal
	o_parser.add_argument( '--verbose', '-v', action='store_true',
		               help='If set give info on what happen.' )
	o_parser.add_argument( '--chr',
							help='chrom (seq id) to use.' )
	o_parser.add_argument( '--bed',
							help='filename in bed format, containing regions to use.' )

	# mandatory option
	#	destination is opt2 by default
	#~ o_parser.add_argument( '--opt2', '-o',
						#~ action='store_true', # store the value as boolean
						#~ help='a flag opt, long and short.')

	return o_parser

def analyseOptions( o_args ):

	global VERBOSE
	if( o_args.verbose ):
		VERBOSE = True

	if( o_args.bed ):
		if( o_args.bed == '-' ):
			f_bed = sys.stdin
		else:
			f_bed = open( o_args.bed, "r" )
		o_args.f_bed = f_bed


def verbose( msg ):
	if( VERBOSE ):
		sys.stderr.write( msg + "\n" )

# ----------------------------------------

if __name__ == '__main__':
	o_parser = defineOptions()
	o_args   = o_parser.parse_args()
	analyseOptions( o_args )
	PP.pprint( o_args )

	main( o_args )

	sys.exit( 0 )

