#! /bin/bash

# shell to use
#$ -S /bin/bash

#$ -M sletort@irisa.fr
# send mail before b, at the end e, on stop a, on suspend s, or never n
#$ -m bea

#$ -N script name

# if the script should be run in the current directory
#$ -cwd
#$ -i input_file
#$ -o output_file

# merge stdout and stderr
#$ -j yes|no


# ne pas utiliser, ça ne sert qu'à afficher ce qui va être envoyer à la queue ?
# check submitted task ( -> ça veut dire quoi ?)
##$ -verify


## AIMS  : run a job on the cluster biogenouest
## USAGE : run_cluster.tmpl.sh

# init env X
source /local/env/envX.sh

# le script

exit 0

