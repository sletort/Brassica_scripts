#! /bin/bash

## AIMS  : provides functions to access easily to Rapsodyn/PolySuccess files.
## USAGE : source brassicaDB.sh [RAPSO|POLY]
## NOTE  : code do not allowed space in names in meta files.
## AUTHORS : sletort@irisa.fr

set -o nounset
set +o errexit # kill bash if sourced

# =========================================================
# assert prog exit.
jq --version > /dev/null \
	|| { printf "run bash first.\n" >&2 && exit 1; }

# assert ( really ?) that an environnement file has been sourced
echo $__envDB__ > /dev/null

# ---------------------------------------------------------

# =========================================================
declare -r __brassicaDB__=1

declare -A __H_SPECIES=( )
for sp in $( grep -v ^# $__META_FILE | cut -f 2 | sort -u )
do
	perle='print $F[0] if( $F[1] eq "'$sp'" );'
	__H_SPECIES[$sp]=$( perl -lane "$perle" $__META_FILE )
done

declare -A __H_CULTIVARS=( )
for sp in ${!__H_SPECIES[@]}
do
	for cul in ${__H_SPECIES[$sp]}
	do
		__H_CULTIVARS["$cul"]=$sp
	done
done


# ---------------------------------------------------------
declare -rA __BRASSICA_H_FUNCTIONS=(
	[DB__printVars]="DB__printVars\n\tFor debugging purpose, prints variables (like constant sourced)."
	[DB__help]="DB__help [function_name]\n\tPrints a tiny help/usage for the function.\n\tIf no function_name, all functions help are printed."

	# end-user functions
	[get_ref_fasta]='get_ref_fasta species\n\treturn the value found in json file for the reference fasta filepath.'
	[get_ref_seq_len]='get_ref_seq_len species\n\treturn the value found in json file for the reference seq_len. This is the size of each chrom.'
	[get_ref_annot]='get_ref_annot species element\n\treturn the value found in json file for the reference annotation element.'
	[get_assembly_path]='get_assembly_path cultivar\n\treturn the path to the directory containing our official assembly.'

	[loopOverCultivars]="loopOverCultivars species func \"arg1 arg2 ...\"\n\trun on each cultivars, first arg of func is cultivar."
	[loopOverSpecies]="loopOverSpecies func \"arg1 arg2 ...\"\n\trun on each species, first arg of func is species."
)
# =========================================================
function DB__printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT = $0\n"
	printf "╟ - __ROOT  = $__ROOT\n"
	printf "╟ - __H_SPECIES = ${__H_SPECIES[*]}\n"
#	printf "╟ - __H_CULTIVARS = ${__H_CULTIVARS[*]}\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

function DB__help ()
{
	local func="${1-NULL}"

	if [[ "NULL" == "$func" ]]
	then
		func="${!__BRASSICA_H_FUNCTIONS[@]}"
		func=$func" ${!__H_FUNCTIONS[@]}"
	fi

set +o nounset
	for f in $func
	do
		if [[ ${__BRASSICA_H_FUNCTIONS[$f]} ]]
		then
			printf "${__BRASSICA_H_FUNCTIONS[$f]}\n"
		elif [[ ${__H_FUNCTIONS[$f]} ]]
		then
			printf "${__H_FUNCTIONS[$f]}\n"
		else
			printf "No function name '$f' is known.\n" >&2
		fi
	done
set -o nounset

	printf "\nYou can have access to the code with\n\tdeclare -f function_name\n"
}

# =========================================================
# a set of function that look into json file
function get_ref_fasta ()
{
	local -rl species="$1"

	local json_file=$__ROOT/References.json
	local path=$( jq '.References.'$species'.fasta_filepath' $json_file | tr -d '"' )
	if [[ "null" == "$path" ]]
	then
		printf "Error, species $species has not been found in the reference json file.\n" >&2
		exit 1
	fi

	printf "$path"
}

function get_ref_seq_len ()
{
	local -rl species="$1"

	local json_file=$__ROOT/References.json
	local path=$( jq '.References.'$species'.seq_len' $json_file | tr -d '"' )
	if [[ "null" == "$path" ]]
	then
		printf "Error, species $species has not been found in the reference json file.\n" >&2
		exit 1
	fi

	printf "$path"
}

function get_ref_annot ()
{
	local -rl species="$1"
	local -rl element="$2"

	local json_file=$__ROOT/References.json
	local path=$( jq '.References.'$species'.annot.'$element $json_file | tr -d '"' )
	if [[ "null" == "$path" ]]
	then
		printf "Error, no annotation '$element' has been found for species '$species' in the reference json file.\n" >&2
		exit 1
	fi

	printf "$path"
}

# generic function to extract one value.
# maybe a simple function giving acces to json filepath is suffisant ?
function jq_extract_from_json ()
{
	local -r jq_path="$1"

	local json_file=$__ROOT/References.json
	local value=$( jq $jq_path $json_file | tr -d '"' )
	if [[ "null" == "$value" ]]
	then
		printf "Error, jq path '$jq_path' has not been found in the reference json file.\n" >&2
		exit 1
	fi

	printf "$value"
}

# =========================================================
function get_assembly_path ()
{
	local -r cultivar=${1?"You have to specify a cultivar.\n"}

	local -r species=${__H_CULTIVARS[$cultivar]}
	if [[ -z $species ]]
	then
		printf "$cultivar is not refered.\n" >&2
	else
		# ${species^} = capitalization
		printf "%s" "${DB_ASSEMBLY_ROOT}/${species^}/$cultivar"
	fi
}

function get_raw_seqs ()
{
	local -rl cultivar=${1?"You have to specify a cultivar.\n"}

	local -r sub_path=$( grep -w "^$cul" $__META_FILE | cut -f 3 )

	ls $DB_RAWDATA_PATH/$sub_path*
}

# =========================================================
function loopOverCultivars ()
{
	local -l species=$1 && shift
	local func=$1 && shift

	if [ -z ${__H_SPECIES[$species]+'x'} ]
	then
		printf "*$species* is not a known species.\n"
		printf "\t-%s\n" "${!__H_SPECIES[@]}"
	else
		for cul in ${__H_SPECIES[$species]}
		do
			$func $cul $*
		done
	fi
}

function loopOverSpecies ()
{
	local func=$1 && shift

	for sp in ${!__H_SPECIES[@]}
	do
		$func $sp $*
	done
}

