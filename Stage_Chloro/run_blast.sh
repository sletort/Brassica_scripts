#! /bin/bash

## AIMS  : 
## USAGE :
## NOTE  :
## AUTHORS : sebastien.letort@irisa.fr

source /softs/local/env/envblast-2.6.0.sh


set -o nounset
set -o errexit

# =========================================================
declare -r CHLORO_GENEDIR=${1?What is the input directory (dir. containing chloro genes) ?}
declare -r OUTDIR=${1?What is the output directory ?}

# ---------------------------------------------------------
declare -r CHLORO_GENEDIR=/home/genouest/cnrs_umr6074/vepain/data/gene_seq_chlo/
declare -r BLASTDB=/omaha-beach/sletort/tmp_blast/all_napus

# =========================================================

function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT     = $0\n"
	printf "╟ - CHLORO_GENEDIR = $CHLORO_GENEDIR\n"
	printf "╟ - OUTDIR     = $OUTDIR\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

# =========================================================
function run_blast ()
{
	local -r fasta="$1"
	local -r headers="sseqid sstart send qseqid qstart qend sstrand slen qlen length score evalue pident nident mismatch"

	local bname=$( basename $fasta .fasta )

	blastn -task megablast \
			-word_size 32 -perc_identity 90 \
			-db "$BLASTDB" -query "$fasta" \
			-outfmt "6 $headers" -out $bname.napus.blast
}

function main ()
{
	printf "Blast des genes de chloro.\n"
	for f in $CHLORO_GENEDIR/*
	do
		run_blast "$f"
	done
}

# =========================================================
printVars >&2

# your work.
main

printf "End of %s.\n\n" $( basename $0 ) >&2

exit 0

