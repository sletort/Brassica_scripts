#! /bin/bash

# have to be defined
# loopOverCultivars
# get_mfasta $cul

source /local/env/envblast-2.6.0.sh

# private functions
function __F__all_but ()
{
	local cul="$1";
	local unwanted="$2";

	if [ $cul != $unwanted ];
	then
		printf "%s " $( get_mfasta "$cul" )
	fi
}



function F__run_megablast ()
{
	local cul="$1";

	# local db : a small txt file (*.nal)
	local DB_all_but=$( mktemp all_but_${cul}.XXXX )

	# make all-otherDB
	local DB_list=$( loopOverCultivars bnapus __F__all_but $cul )
	blastdb_aliastool -dblist "$DB_list" -dbtype nucl \
		-out "$DB_all_but" -title all_but_$cul

	local headers='qseqid qstart qend sseqid sstart send'
	headers=$headers' sstrand qlen slen length'
	headers=$headers' score evalue pident nident mismatch'
	local query_file=$( get_mfasta "$cul" )
	local outfile=$( dirname $query_file )"/$cul.mblast.tsv"

	blastn -task megablast \
		-db $DB_all_but -query "$query_file" \
		-word_size 32 -perc_identity 95 -qcov_hsp_perc 80 \
		-outfmt "6 $headers" -out "$outfile" \
		-num_threads 8

	rm -f "$DB_all_but"*
}

function F__make_blastDB ()
{
	local mfasta="$1"
	local type="${2-nucl}"

	makeblastdb -in $mfasta -dbtype $type -parse_seqids
}

