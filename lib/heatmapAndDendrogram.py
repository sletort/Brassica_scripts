'''
	code provided by Jean Coquet.
'''

import brewer2mpl
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import scipy.spatial.distance as distance
import scipy.cluster.hierarchy as sch
import numpy as np
from pylab import cm
from numpy import arange

# helper for cleaning up axes by removing ticks, tick labels, frame, etc.
def clean_axis(ax):
	"""Remove ticks, tick labels, and frame from axis"""
	ax.get_xaxis().set_ticks([])
	ax.get_yaxis().set_ticks([])
	for sp in ax.spines.values():
		sp.set_visible(False)

def saveHeatmapAndDendrogram(data, title, labelRows, labelColumns, pathFile) :
	dataDF = pd.DataFrame(data)
	dataDF.index = labelRows
	dataDF.columns = labelColumns

	# CLUSTERING
	col_pairwise_dists = distance.squareform(distance.pdist(dataDF.T)) # calculate pairwise distances for columns
	col_clusters = sch.linkage(col_pairwise_dists,method='complete') # cluster
	pairwise_dists = distance.squareform(distance.pdist(dataDF))
	row_clusters = sch.linkage(pairwise_dists,method='complete')

	# make norm
	vmin = np.floor(dataDF.min().min())
	vmax = np.ceil(dataDF.max().max())
	vmax = max([vmax,abs(vmin)]) # choose larger of vmin and vmax
	vmin = vmax * -1
	my_norm = mpl.colors.Normalize(vmin, vmax)

	width = min(300,max(20,int(0.5*len(labelColumns))))
	height = min(300,max(10,int(0.2*len(labelRows))))
	
	# heatmap with row names	
	fig = plt.figure(figsize=(width,height))
	fig.suptitle(title, fontsize=18, fontweight='bold')
	
	heatmapGS = gridspec.GridSpec(2,2,wspace=0.0,hspace=0.0,width_ratios=[1,0.25],height_ratios=[0.25,1])
	
	### col dendrogram ###
	col_denAX = fig.add_subplot(heatmapGS[0,0])
	col_denD = sch.dendrogram(col_clusters,color_threshold=np.inf)
	clean_axis(col_denAX)

	### row dendrogram ###
	row_denAX = fig.add_subplot(heatmapGS[1,1])
	row_denD = sch.dendrogram(row_clusters,color_threshold=np.inf,orientation='left')
	clean_axis(row_denAX)
	
	### heatmap ####
	heatmapAX = fig.add_subplot(heatmapGS[1,0])
	axi = heatmapAX.imshow(dataDF.ix[row_denD['leaves'],col_denD['leaves']],interpolation='nearest',aspect='auto',origin='lower',norm=my_norm,cmap=cm.RdBu)
	clean_axis(heatmapAX)
	
	## row labels ##
	heatmapAX.set_yticks(arange(dataDF.shape[0]))
	heatmapAX.yaxis.set_ticks_position('right')
	heatmapAX.set_yticklabels(dataDF.index[row_denD['leaves']])

	## col labels ##
	heatmapAX.set_xticks(arange(dataDF.shape[1]))
	xlabelsL = heatmapAX.set_xticklabels(dataDF.columns[col_denD['leaves']])
	
	# remove the tick lines
	for l in heatmapAX.get_xticklines() + heatmapAX.get_yticklines(): l.set_markersize(0)

	### scale colorbar ###
	#scale_cbAX = fig.add_subplot(heatmapGS[0:2,0]) # colorbar for scale in upper left corner
	#cb = fig.colorbar(axi,scale_cbAX) # note that we could pass the norm explicitly with norm=my_norm
	#cb.set_label('Measurements')
	#cb.ax.yaxis.set_ticks_position('left') # move ticks to left side of colorbar to avoid problems with tight_layout
	#cb.ax.yaxis.set_label_position('left') # move label to left side of colorbar to avoid problems with tight_layout
	#cb.outline.set_linewidth(0)
	## make colorbar labels smaller
	#tickL = cb.ax.yaxis.get_ticklabels()
	#for t in tickL: t.set_fontsize(t.get_fontsize() - 3)


	
	heatmapGS.tight_layout(fig, rect=[0, 0, 1, 0.97])

	fig.savefig(pathFile)

