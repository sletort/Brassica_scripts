#!/usr/bin/env python
#-*- coding:utf-8 -*-

import sys
import csv
import gzip


# =====================================
# better to use a logging module ...
VERBOSE = False

def set_verbose( flag ):
	global VERBOSE
	VERBOSE = bool( flag )

def verbose( msg ):
	if( VERBOSE ):
		sys.stderr.write( msg + "\n" )

# =====================================
def open_infile( infile ):
	# from https://stackoverflow.com/questions/13044562/python-mechanism-to-identify-compressed-file-type-and-uncompress
	magic_dict = {
			"\x1f\x8b\x08": "gz",
			"\x42\x5a\x68": "bz2",
			"\x50\x4b\x03\x04": "zip"
		}
	max_len = max( len(x) for x in magic_dict )

	def file_type( filename ):
		with open( filename ) as f:
			file_start = f.read(max_len)
		for magic, filetype in magic_dict.items():
			if file_start.startswith(magic):
				return filetype
		return "no_zip"

	if( 'no_zip' == file_type( infile ) ):
		return open( infile, 'r' )

	return gzip.open( infile, 'rt' )

# =====================================
def load2ColsAsDict( fh_in ):
	"""Read a 2 cols TSV file and return it as a dictionnay"""
	h_dict = {}

	o_csv = csv.reader( fh_in, delimiter='\t' )

	for a_elts in o_csv:
		h_dict[a_elts[0]] = a_elts[1]

	return h_dict


def loadTsvAsDict( fh_in, id_col=0 ):
	"""Read a TSV file and return it as a dictionnay.
		OUT : a dictionnay whose keys are column @id_col, and value are the entire row.
	"""
	h_dict = {}

	o_csv = csv.reader( fh_in, delimiter='\t' )

	for a_elts in o_csv:
		h_dict[a_elts[id_col]] = a_elts

	return h_dict


# difference avec import mimetypes + mimetypes.guess_type( infile, strict=0 ) ?
def is_compressed( filename ):
	# based on https://stackoverflow.com/questions/13044562/python-mechanism-to-identify-compressed-file-type-and-uncompress
	# python3 -> b prefix
	h_magic = {
			b"\x1f\x8b\x08": "gz",
			b"\x42\x5a\x68": "bz2",
			b"\x50\x4b\x03\x04": "zip"
		}
	max_len = max(len(x) for x in h_magic )

	# python3 -> open in binary mode
	with open( filename, 'rb' ) as f:
		magic_bytes = f.read( max_len )
		for magic in h_magic.keys():
			if( magic_bytes.startswith( magic ) ):
				return True
	return False

# =====================================
# ==== Graph functions ====

def save_tgf( o_g, outfile ):
	h_nodes = {} # to store ids

	i = 1
	with open( outfile, "w" ) as o_fs:
		for o_node in o_g.nodes_iter():
			h_nodes[o_node] = i

			line = "\t".join([ str( x ) for x in ( i, o_node ) ])
			o_fs.write( line + "\n")
			i += 1

		o_fs.write( "#\n" )

		#~ for a,b,data in o_g.edges_iter( data = True ):
		for a,b,data in sorted( o_g.edges( data = True ), key=lambda x: x[2]['weight'], reverse = True ):
			n1 = h_nodes[ a ]
			n2 = h_nodes[ b ]
			#~ line = str( n1 ) + " " + str( n2 )
			line = " ".join([ str( x ) for x in ( n1,n2,data['weight'] ) ])
			o_fs.write( line + "\n" )

	return

def draw( o_graph, outfile=None ):
	#~ nx.drawing.nx_pydot.write_dot( o_cc, "graph.dot" ) 

	#~ nx.draw( o_graph )
	#~ plt.show()
	#~ if( outfile != None ):
		#~ plt.savefig( outfile )

	nx.draw_networkx( o_graph )
	if( outfile != None ):
		plt.savefig( outfile, dpi=1000 )
	else:
		plt.show() # ne sais pas pk, peut pas être sauvé et vu en même temps.
	plt.close()

	return
