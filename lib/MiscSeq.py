#!/usr/bin/env python
#-*- coding:utf-8 -*-

# ========================================
class MiscSeq( object ):
	d_cmpl = {
		'a': 't', 'A':'T',
		'c': 'g', 'C': 'G',
		'g': 'c', 'G': 'C',
		't': 'a', 'T': 'A',
		'n': 'n', 'N': 'N',
		'*': '*', '-': '-'
	}
	@classmethod
	def complement( cls, seq ):
		'''return the reverse complement of the given sequence.'''
		cmpl = ""
		for b in seq:
			cmpl += cls.d_cmpl[b]

		return cmpl[::-1]
