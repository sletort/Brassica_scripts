#!/usr/bin/env python
#-*- coding:utf-8 -*-

## AIMS  : Facilitate the use of References.json file.
## USAGE : ./json_tool.py ?

import json

class JsonReference( object ):
	'''Does this class really facilitate the use of our json file ?'''

	def __init__( self, o_fs ):
		self.__d_json = json.load( o_fs )

	def getReference( self, species, key=None ):
		if( not key ):
			return self.__d_json[ 'References' ][species]
		return self.__d_json[ 'References' ][species][key]
