#!/usr/bin/env python
#-*- coding:utf-8 -*-

import csv

# class Files ?
def load_seq_len( o_scaff_len ):
	of_seq = csv.reader( o_scaff_len, delimiter='\t' )
	d_scaff_len = {}
	for seq_id,seq_len in of_seq:
		d_scaff_len[seq_id] = int( seq_len )

	return d_scaff_len
