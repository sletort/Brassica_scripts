#! /bin/bash

## AIMS  : provide some miscellaneous functions.
## USAGE : source lib_misc.sh
## NOTE  : All function names are prefixed with Misc__

# =========================================================
# @filepath: filepath, the path to the file to check.
# print a message on stderrif file does not exist.
function Misc__assertFile ()
{
	local -r filepath="${1}"

	if [[ ! -e "$filepath" ]]
	then
		printf "%s does not exist." "$filepath" >&2
	fi
}

# @s: string, the string to capitalize.
# print the string with the first charcapitalized, and other char lowered.
# NOTE : Already obsolete. bash propose ${s^} = capitalization
function Misc__capitalize ()
{
	local s="${1}"

	local -u First=${s:0:1}
	local -l reste=${s:1}

	printf "%s" "${First}${reste}"
}



# @msg     : string, the message to log
# @outfile : filepath, the file where to log.
# print $msg in outfile or by default on stderr
# @bug     : "\t" in msg are not interpreted.
function Misc__log ()
{
	local msg="$1"
	local outfile=${2-""}

	if [[ ""=="$outfile" ]]
	then
		printf "%s\n" "$msg" >& 2
	else
		printf "%s\n" "$msg" > $outfile
	fi
}

# @msg     : string, the message to output
# @output  : 1 for stdout (default), 2 for stderr or any filepath
# print $msg on output if VERBOSE is set to 1
function Misc__verbose ()
{
	local msg="$1"
	local output=${2-1}

	#~ local output=/dev/stdout
	#~ if [[ 1 -ne $stderr ]]
	#~ then
		#~ output=/dev/stderr
	#~ fi

	if [[ 1 -eq $VERBOSE ]]
	then
		printf "%s\n" "$msg" >&$output
	fi
}

# @str_size	: string, the string to turn to integer, can be like 1K,1M,1G
# print $str_size as integer.
function Misc__string_size_to_integer ()
{
	local str_size="$1"

	local perle='%h_ = ( K => 10**3, k => 10**3, M => 10**6, G => 10**9, "" => 1 );'
	perle=$perle" if( \"$str_size\" =~ /^(\d+)([kKMG]?)$/ )"
	perle=$perle'{	$d = $1; $l = $2; print $d * $h_{ $l }; }'
	perle=$perle'else{	warn "Unrecognized string_size \"'$str_size'\""; }'

	perl -MData::Dumper -e "$perle"
}

# @filepath : filepath of the file to test
# test if the given filepath is a gzip file.
# @usage : if Misc__is_gzip $file; then echo "$file is a gzip file."; fi
function Misc__is_gzip ()
{
	local filepath="$1"

	local type=$( file --brief --dereference --mime-type $filepath )
	test 'application/x-gzip' = "$type"
}

# @cmd : command to test
# exit if the given command doesn't exist.
# @usage : Misc__assert_cmd $cmd
# @note : if errexit is set, use only 'which' and ignore this function.
function Misc__assert_cmd ()
{
	local cmd="$1"

	which "$cmd" 2>&1 > /dev/null
	if [[ $? -ne 0 ]]
	then
		printf "'%s' return 1, which probably means that the command is unknown.\n" "$cmd" >&2
		exit 1
	fi
}
