#!/usr/bin/env python
# -*- coding: utf-8 -*-

## AIMS  : Managed sequence feature outputs, to generate BED or GFF.
## USAGE : import FeatureOutputWriter
## NOTE  : 

import sys
import pprint

VERSION = "0.1.0"
PP = pprint.PrettyPrinter(indent=4)

# ========================================
class FeatureOutputWriter( object ):
	def __init__( self, o_fs ):
		self.__filename = o_fs
		self.__fs = None

	def _write( self, string ):
		self.__fs.write( string + "\n" )

	def __enter__( self ):
		if( hasattr( self.__fs, 'read' ) ):
			self.__fs = self.__filename
			self.__filename = 'filehandle'
		else:
			self.__fs = open( self.__filename, 'w' )

		return self

	def __exit__( self, *arg ):
		self.__fs.close()

	#~ def __del__( self ):
		#~ sys.stderr.write( "fn __del__\n" )

	#~ def write( self, seq_id, o_ocean ):
		#~ l_fields = self._getFields( seq_id, o_ocean )
		#~ self.__fs.write( "\t".join( l_fields ) + "\n" )

	#~ def _getFields( seq_id, o_ocean ):
		#~ raise Exception( "Not implemented yet !" )

#~ class OutBed( OutputWriter ):
	#~ ''' Manage bed output.'''
	#~ def _getFields( self, seq_id, o_ocean ):
		#~ s = o_ocean.start()
		#~ e = o_ocean.end()
		#~ return [ str(x) for x in [ seq_id, s, e ] ]
		

class OutGff( FeatureOutputWriter ):
	''' Manage gff output.'''
	#~ def __init__( self, o_fs ):
		#~ super( OutGff, self ).__init__( o_fs )

	def __enter__( self ):
		super( OutGff, self ).__enter__()
		self._write( "##gff-version 3" )
		#~ self.__i = 1
		return self

	#~ def _getFields( self, seq_id, o_ocean ):
		#~ l_fields  = [ seq_id, 'detectOcean.py', 'gap' ]
		#~ l_fields += [ o_ocean.start(), o_ocean.end() ]
		#~ l_fields += [ '.', '.', '.' ]
		#~ l_fields.append( ";".join([ 'ID={}'.format( self.__i )]) )

		#~ self.__i += 1

		#~ return [ str( x ) for x in l_fields ]

	def comment( self, line ):
		self._write( "#" + line )

	def writeSep( self ):
		self._write( "###" )

	def writeMatch( self, o_region, r_id, src=".", strand='.' ):
		"""o_region is an Region object."""
		l_fields  = [ o_region.seq_id, src, 'match' ]
		l_fields += list( o_region.feature() )
		l_fields += [ '.', strand, '.' ]
		l_fields.append( ";".join([ 'ID={}'.format( r_id )]) )

		l_ = [ str( x ) for x in l_fields ]
		self._write( "\t".join( l_ ) )

	def writeMatchPart( self, o_link, src=".", ID="X", Parent=None ):
		"""o_region is an Region object."""
		l_fields  = [ o_link.o_ref.seq_id, src, 'match_part' ]
		l_fields += list( o_link.o_ref.feature() )
		l_fields += [ '.', o_link.strand_symbol, '.' ]
		l_attrs   = [ 'ID={}'.format( ID ), \
						'Target={0} {1} {2}'.format( o_link.o_tgt.seq_id, *o_link.o_tgt.feature() )]
		if( Parent ):
			l_attrs.append( 'Parent=' + str( Parent ) )
		l_fields.append( ";".join( l_attrs ) )

		l_ = [ str( x ) for x in l_fields ]
		self._write( "\t".join( l_ ) )

# ========================================
if __name__ == '__main__':

	sys.exit( 0 )
