#! /bin/bash

## AIMS  : provide some functions used in assembly analysis.
## USAGE : source lib_assembly.sh
## NOTE  : All function names are prefixed with Ass__

set +o nounset
if [[ ! $ROOT__ ]]
then
	declare -r ROOT__=$( dirname $( readlink -e $0 ) )/..
fi

#if [[ ! $__brassicaDB__ ]]
#then
#        source $ROOT__/brassicaDB.mine.sh
#fi
set -o nounset

# =========================================================
# @infile : filepath, 2 col file : scaff_name scaff_size
# @expected_size : int, genome size (default=0 return N50, not NG50), want_header (default=0, no header).
# print 3 cols N(g)50, L(g)50, and assembly size
function Ass__print_NG50 ()
{
	local infile="$1"
	local expected_size=${2-0}
	local want_header=${3-0}

	local half
	local header
	if [[ 0 -eq $expected_size ]]
	then
		half=$( perl -lane '$s += $F[1]; END{ print $s/2; }' $infile )
		header="N50\tL50\tAss_size\n"
	else
		half=$(( expected_size / 2 ))
		header="NG50\tLG50\tAss_size\n"
	fi

	if [[ 0 != want_header ]]
	then
		printf $header
	fi

#    perl -lane '$s += $F[1]; die "N50 = $F[1]\nL50 = $.\n" if( $s >= '$half' );' $infile
#    perl -lane '$s += $F[1]; END{ print "Genome length = $s"; }' $infile
    perl -ane '$s += $F[1]; print "$F[1]\t$." and exit(0) if( $s >= '$half' );' $infile
    perl -lane '$s += $F[1]; END{ print "\t$s"; }' $infile
}


# @cultivar : string, cultivar's name
# Get the assembly filepath and extract it if needed
#	If no filepath is found, prog ends with "exit 1"
function Ass__get_assembly_filepath ()
{
	local cultivar="$1"

	local found=0
	local path=$( get_assembly_path $cultivar )
	for f in "$path"/${cultivar}.fa "$path"/${cultivar}.fa.gz
	do
		if [[ -e $f ]]
		then
			if Misc__is_gzip $f
			then
				local tmp_file=$( mktemp --tmpdir=$PWD ${cultivar}.XXXXX.fa )
				printf "\tUnzipping file.\n" >&2
				gzip -dc $f > $tmp_file
				printf "$tmp_file"
			else
				printf "$f"
			fi
			found=1
			break
		fi
	done

	if [[ 0 -eq $found ]]
	then
		printf "No suitable file could be found in $path.\n" >&2
		exit 1
	fi
}



