#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import pprint # debug

# my lib
import Vcf # as pysam is too complex
from Misc import verbose

PP = pprint.PrettyPrinter( indent=4, stream=sys.stderr )

# ========================================
class RecordsBufferException( Exception ):
	def __init__( self, o_buff, msg ):
		descr = "\n"
		for o_rec in o_buff.lo_records:
			descr += "\t{}\n".format( str( o_rec ) )
		Exception.__init__( self, msg+descr )

class NotSameRefException( RecordsBufferException ):
	def __init__( self, o_buff, ref_1, ref_2 ):
		msg = "Not the same reference. {} != {}".format( ref_1, ref_2 )
		RecordsBufferException.__init__( self, o_buff, msg )

# ----------------------------------------
class RecordsBuffer( object ):
	def __init__( self, o_vcf_in ):
		self.__o_curr_record = o_vcf_in.next() # no need for iter ?
		self.__lo_records = []
		self.__o_vcf_in   = o_vcf_in
		#~ PP.pprint({ 'in cstr': str( self.__lo_records[0] ) })

		# Combinators
		self.__o_info   = None
		self.__o_format = None

		self.__stacked_record = None

		# some counters
		self.__compressed_lines = 0
		self.__counter = 0

	@property
	def counter( self ):
		return self.__counter

	@property
	def compressed_lines( self ):
		return self.__compressed_lines

	@property
	def lo_records( self ):
		return self.__lo_records

	@property
	def __o_record( self ):
		return self.__o_curr_record

	@property
	def o_info( self ):
		return self.__o_info
	@o_info.setter
	def o_info( self, o_ ):
		assert isinstance( o_, Combinator )
		self.__o_info = o_

	@property
	def o_format( self ):
		return self.__o_format
	@o_format.setter
	def o_format( self, o_ ):
		assert isinstance( o_, Combinator )
		self.__o_format = o_

	def fill( self ):
		self.__lo_records = []
		for o_rec in self.__o_vcf_in:
			#~ PP.pprint({ 'in fill': str( o_rec ) })

			self.__lo_records.append( self.__o_curr_record )
			self.__o_curr_record = o_rec

			if not self.__lo_records[0].simple_equals( o_rec ):
				return True

		verbose( "dbg: End Fill" )
		self.__lo_records.append( self.__o_curr_record )
		return False

	def __init_new_record( self ):
		# determine alleles
		s_alleles = set()
		ld_allele_indexes = []
		ref = None
		for o_rec in self.__lo_records:
			if ref is None:
				ref = o_rec.l_alleles[0]
			elif ref != o_rec.l_alleles[0]:
				raise NotSameRefException( self, ref, o_rec.l_alleles[0] )

			d_indexes = {}
			for i,allele in enumerate( o_rec.l_alleles[1:] ):
				if '.' == allele:
					continue # break ?
				d_indexes[allele] = i+1 # +1 because i starts at 0 while the alt allele starts at 1
				s_alleles.add( allele )
			ld_allele_indexes.append( d_indexes )
		l_alleles = [ ref ] + sorted( s_alleles )

		# il faudra faire qqc avec ld_alleles !
		self.__tmp_ld_allele_indexes = ld_allele_indexes
		o_rec = self.__lo_records[0]
		self.__stacked_record = Vcf.VariantRecord( o_rec.seq_id, o_rec.pos, l_alleles )
		return

	def __stack_infos( self, fn_stack ):
		for o_rec in self.lo_records:
			fn_stack( o_rec )
		self.__stacked_record.updateInfos( self.o_info.d_infos )

	def __stack_samples( self, fn_stack ):
		# build ad-hoc data struct
		lld_samples = [ [] for _ in self.lo_records[0].l_sample_data ]
		s_format_keys = set()
		for o_rec in self.lo_records:
			for i,d_sample in enumerate( o_rec.l_sample_data ):
				lld_samples[i].append( d_sample )

				if 0 == i:
					s_format_keys.update( d_sample.keys() )
		self.o_format.setKeys( s_format_keys )

		# we need to keep a trace of the alt allele indices
		#	we will pass their new indices as argument
		l_new_alleles = self.__stacked_record.l_alleles
		ll_indices = []
		for o_rec in self.lo_records:
			l_ = []
			for allele in o_rec.l_alleles:
				if allele is not '.':
					l_.append( l_new_alleles.index( allele ) )
			ll_indices.append( l_ )

		# now loop over samples
		for i_sample, ld_samples in enumerate( lld_samples ):
			# PP.pprint( "loop over sample " + str( i_sample ) )
			d_sample = fn_stack( i_sample, ld_samples, ll_indices )
			# PP.pprint({ 'RecBuff.__stack_samples': d_sample })
			# PP.pprint({ 'RecBuff.__stack_samples': self.__stacked_record })
			self.__stacked_record.addSampleData( d_sample )

	def stack( self, b_delete_duplication ):
		# il serait pertinent de changer la struct des données pour ne pas boucler sur les rec encore et encore.
		if len( self.__lo_records ) < 2:
			return self.__lo_records

		try:
			self.__init_new_record()
			if self.__o_info:
				self.__stack_infos( self.__o_info.stack )
			if self.__o_format:
				self.__stack_samples( self.__o_format.stack )

		except NotSameRefException as e:
			# sometime NotSameRefException can be reported as an error
			#	or can really be combine (here for example a T/A SNP followed by a TT/A deletion
			#		could be replaced by the ATT/AA (TT/A ?) only. (-> but not time and no real need for this now.)
			return self.__lo_records
		except CombinatorException as e:
			msg = self.lo_records[0].pos_str() + " => Records not stacked !\n"
			sys.stderr.write( msg )
			msg = "" # donner le nom du ou des échantillons serait un plus !
			for o_rec in self.lo_records:
				msg += "\t{}\n".format( o_rec.string( l_i_samples=[e.i_sample] ) )
			verbose( str( e ) + msg )
			if b_delete_duplication:
				return [ None ]
			return self.__lo_records
		except ( InfosKeyException,FormatKeyException ) as e:
			raise RecordsBufferException( self, str( e ) )
		#~ except Exception as e:
			#~ PP.pprint( e )
			#~ sys.stderr.write( str(e) )

		# verbose( "Fin Compression !\n" + str( self.__stacked_record ) )
		self.__counter += 1
		self.__compressed_lines += len( self.__lo_records )
		return [ self.__stacked_record ]

# ========================================
from collections import defaultdict
class CombinatorException( Exception ):
	'''This exception should lead to the output of all records unstacked.'''
	def __init__( self, msg, i_sample=None ):
		self.__i_sample = i_sample
		Exception.__init__( self, msg )

	@property
	def i_sample( self ):
		return self.__i_sample

class Combinator( object ):
	def stack( self, o_rec ):
		raise NotImplementedError()

class InfosKeyException( Exception ):
	def __init__( self, key ):
		msg = "Infos {} not managed yet".format( str( key ) )
		Exception.__init__( self, msg )

class InfosCombinator( Combinator ):
	def __init__( self, d_actions={} ):
		self.__d_infos   = None
		self.__d_actions = {
			# fn_pass = skip this.
			'AN': fn_pass,
			'AC': fn_pass,
		}
		self.__d_actions.update( d_actions )

	@property
	def d_infos( self ):
		return self.__d_infos

	def stack( self, o_rec ):
		self.__d_infos   = {}
		for k,v in o_rec.d_infos.items():
			func = self.__d_actions.get( k, None )
			if None is func:
				raise InfosKeyException( self, k )
			else:
				func( self, v )


class FormatKeyException( RecordsBufferException ):
	def __init__( self, key ):
		msg = "Format {} not managed yet".format( str( key ) )
		Exception.__init__( self, msg )

class FormatCombinator( Combinator ):
	# peut-être que ça aurait plus de sens d'avoir une instance juste pour le paquet à compacter.
	# instance générée par cet objet, donc une classe de plus.
	def __init__( self, d_actions={} ):
		self.__l_keys    = []
		self.__d_actions = {
		}
		self.__d_actions.update( d_actions )

	def setKeys( self, s_keys ):
		self.__l_keys = sorted( s_keys )

	def stack( self, i_sample, ld_samples, ll_indices ):
		# garder la même signature pour toutes les fonctions stacks ?
		# ici on combine par échantillon, la seule chose partagée est la liste des clefs.
		# ld_samples est la liste des d_sample pour tous les enregistrements, pour un même échantillon
		__d_sample = {}
		# PP.pprint({ 'stack': ld_samples })
		# PP.pprint({ 'indices': ll_indices })

		# is it really cleaver to loop on key first ?
		for k in self.__l_keys:
			# PP.pprint({ "F_key": k })
			func = self.__d_actions.get( k, None )
			if None is func:
				raise FormatKeyException( k )

			# PP.pprint({ 'ld_samples': ld_samples })
			l_v = [] # d_sample[k] for d_sample in ld_samples ]
			for d_sample in ld_samples: # <=> for each record_sample
				# PP.pprint({ 'd_sample': d_sample })
				# rechercher mieux les données vides ?
				# Note : some keys are not present in all the records.
				if None is not d_sample.get( k, None ) \
						and None is not d_sample.get( 'DP', None ) \
						and d_sample['DP'].count('0') != len( d_sample['DP'] ): # rechercher mieux les données vides ?
					l_v.append( d_sample[k] )
				else:
					l_v.append( '.' )

			__d_sample[k] = func( i_sample, l_v, ll_indices )
			# PP.pprint({ "New key value": __d_sample[k] })

		# PP.pprint({ "__d_sample": __d_sample })
		return __d_sample

# -----------------------------------------------------
# functions that could be "standard", used with many files.
#	don't know if the best should be to have a class function ...
def fn_pass( *args ):
	'''This null function can be used as a template.'''
	pass

def fn_FORMAT_GT( i_sample, l_v, ll_indices ):
	'''i_sample is the sample index, l_v the list of values associated to the key one per record
		and ll_indices is the list of new allele indices.'''
	# idealement il faudrait avoir un lien vers la nature des valeurs, savoir si c'est un entier, une liste par alleles, par alt/alleles, autres.
	# PP.pprint({ 'i_sample': i_sample, 'l_v': l_v, 'll_i': ll_indices })

	gt_i = None
	for i,v in enumerate( l_v ):
		# Note: GT value is a list of 1 value, should not be ... I'll managed that here
		#	also it is an indice of allele, so turn it to an int
		if '.' == v:
			continue

		v = int( v[0] )

		if None is gt_i:
			gt_i = ll_indices[i][ v ]
		elif gt_i != ll_indices[i][ v ]:
			# there could be only one !
			l_gt = [ gt_i, ll_indices[i][ v ] ]
			msg  = "GTs are not compatible.\n"
			msg += "\twe found those {} for sample #{}\n" \
				.format( str( l_gt ), str( i_sample ) )
			raise CombinatorException( msg, i_sample )

	return str( gt_i )

