#!/usr/bin/env python
#-*- coding:utf-8 -*-

## AIMS  : Facilitate Region manipulation (cf bed file) and Link between them (alignment).
## USAGE : ./RegionLink ?

import RegionLink as rl
import pprint # debug
import sys
PP = pprint.PrettyPrinter(indent=4, stream=sys.stderr )

class BlastResultIterator( object ):
	# generator management
	def __init__( self, o_tsv ):
		self._it = o_tsv

	def __iter__( self ):
		return self

	def next( self ):
		return BlastResult( next( self._it ) )

class BlastResult_mini( object ):
	'''class to try to managed conception error in BlastResult: __build_ref_region
		WARN does not set o_link yet.
	'''
	d_strand = { 'plus':1, 'minus':-1 }

	def __init__( self, l_elts ):
		self.__o_link     = None

		self.__align_len  = int( l_elts[8] )
		self.__n_ident    = int( l_elts[12] )
		self.__n_mismatch = int( l_elts[13] )

		self.__seq_ref    = l_elts[14]
		self.__seq_tgt    = l_elts[15]

	def _build_o_link( self, o_ref, o_tgt, strand ):
		self.__o_link     = rl.RegionLink( o_ref, o_tgt, strand )

	@property
	def o_link( self ):  return self.__o_link
	@property
	def seq_ref( self ): return self.__seq_ref
	@property
	def seq_tgt( self ): return self.__seq_tgt
	@property
	def tgt_len( self ): return self.__tgt_len
	@property
	def n_mismatch( self ): return self.__n_mismatch
	@property
	def n_ident( self ): return self.__n_ident
	@property
	def align_len( self ): return self.__align_len

class BlastResult( BlastResult_mini ):
	'''
		an instance holds one blast result.
		It can look for variations in the alignment and export them elsewhere.
	'''
	def __init__( self, l_elts ):
		'''
		ref_id, start, end
		scaff_id, start, end
		strand
		scaff_len length
		score evalue pident nident mismatch
		sseq qseq
		filter_score
		'''
		#~ PP.pprint( l_elts )
		# super( self.__class__, self ).__init__( l_elts )
		super( BlastResult, self ).__init__( l_elts )

		#~ self.__l_elts = l_elts # I do not do this here for the moment (don't want to check everything), but in case of need cf reformat_blast.py
		#~ self.__ref_id = l_elts[0] # only to keep a trace of the original
		( qtl_id, o_ref ) = self._build_ref_region( *l_elts[0:3] )
		scaff_id = self.reformat_scaff_id( l_elts[3] )
		o_tgt = rl.Region( scaff_id, *[ int(x) for x in l_elts[4:6] ] )
		strand = self.d_strand[ l_elts[6] ]

		self._build_o_link( o_ref, o_tgt, strand )
		#~ self.__tgt_len    = int( l_elts[7] ) # not used any more since extrenity are not considered as insertions.

	def _build_ref_region( self, ref_id, start, end ):
		'''ID = QTL_id|chrom|qtl_start|qtl_stop
		start, end are relative to QTL
		region output is 1-based
		'''
		( qtl_id, chrom, qtl_region, strand ) = ref_id.split( '|' )
		if( strand != '+' ):
			raise NotImplementedError( 'only strand "+" are considered for the moment.' )

		( qtl_s, qtl_e ) = ( int( x ) for x in qtl_region.split( '-' ) )
		start = qtl_s + int( start ) - 1
		end   = qtl_s + int( end ) - 1

		return qtl_id, rl.Region( chrom, start, end )

	#~ @property
	#~ def ref_id( self ): return self.__ref_id

	@staticmethod
	def reformat_scaff_id( scaff_id ):
		uid_pos = scaff_id.find( '_uid_' )

		if( -1 != uid_pos ):
			return scaff_id[0:uid_pos]
		return scaff_id

# ==========================================================
#~ def test__partition():
	#~ o_r1 = Region( 'c1', 11, 22 )
	#~ o_r2 = Region( 'c2', 30, 5 )
	#~ lo_regions = o_r1.partition( o_r2 )
	#~ lo_expected = [ o_r1, o_r2 ]
	#~ assert( lo_expected == lo_regions )

	#~ o_r1 = Region( 'c1', 11, 22 )
	#~ o_r2 = Region( 'c1', 30, 5 )
	#~ lo_regions = o_r1.partition( o_r2 )

	#~ lo_expected = [
			#~ Region( 'c1',  5, 11 ),
			#~ Region( 'c1', 11, 22 ),
			#~ Region( 'c1', 22, 30 )
		#~ ]
	#~ assert( lo_expected == lo_regions )

# test__partition
def main( l_args ):
	'''build a Region'''
	#~ o_region1 = Region( 'c1', 11, 22 )
	#~ print( "region1 : " + str( o_region1 ) )
	#~ o_region2 = Region( 'c1', 30, 5 )
	#~ print( "region2 : " + o_region2.recString() )
	#~ o_region3 = Region( 'c2', 13, 2050 )
	#~ print( "region3 : " + str( o_region3 ) )

	#~ (o_r1,o_r2) = Region.sort( o_region2, o_region1 )
	#~ print( "the first region between region2 and region1 is : " + str( o_r1 ) )
	#~ (o_r1,o_r2) = Region.sort( o_region3, o_region1 )
	#~ print( "the first region between region3 and region1 is : " + str( o_r1 ) )

	#~ o_link = RegionLink( o_region1, o_region2 )
	#~ print( "Link 1-2 = " + str( o_link ) )
	#~ o_link = RegionLink( o_region1, o_region3 )
	#~ print( "Link 1-3 = " + o_link.recString() )

	return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
