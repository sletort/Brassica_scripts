#! /bin/bash

## AIMS  : Align an assembly to reference with nucmer.
## USAGE : source lib_AlignAssemblyToRef.sh
## AUTHORS : sebastien.letort@irisa.fr

set -o nounset
set -o errexit

source $__ROOT/brassicaDB.sh
source $__ROOT/lib/lib_misc.sh

# =========================================================
declare -r REF_NAME="${1?What is the reference name to use ?}"
declare -r CUL="${2?What is the assembly to align onto the reference ?}"
declare -r OUTDIRNAME="${3?What is the output directory ?}"

# ---------------------------------------------------------
declare -r TYPE='scaffs'	# one day maybe a contig mode.
declare OUTFILE # filename of the region file generated

declare -r OUTDIR=${OUTDIRNAME}/against_ref/$TYPE

# =========================================================
function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT     = $0\n"
	#~ printf "╟ - ALIGN_MINI = $ALIGN_MINI\n"
	printf "╟ "
	printf "╟ - REF_NAME   = $REF_NAME\n"
	printf "╟ - CULTIVAR   = $CUL\n"
	printf "╟ - OUTDIR     = $OUTDIR\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

# =========================================================
function get_assembly_filepath ()
{
	local cultivar="$1"

	local found=0
	local path=$( get_assembly_path $cultivar )
	for f in "$path"/${cultivar}.fa "$path"/${cultivar}.fa.gz
	do
		if [[ -e $f ]]
		then
			if Misc__is_gzip $f
			then
				local tmp_file=$( mktemp --tmpdir=$PWD ${cultivar}.XXXXX.fa )
				printf "\tUnzipping file.\n" >&2
				gzip -dc $f > $tmp_file
				printf "$tmp_file"
			else
				printf "$f"
			fi
			found=1
			break
		fi
	done

	if [[ 0 -eq $found ]]
	then
		printf "No suitable file could be found in $path.\n" >&2
		exit 1
	fi
}


# this allow post-processing
function reformat_output ()
{	return 0;	}

function rename_scaffolds ()
{
	Misc__log "Alter besst scaff id in alignment file.\n"
	# $cmd # does not work :(
	perl -pi -e 's/_uid_\d+//;' $OUTFILE

}

function main ()
{
	Misc__log "Align sequence to ref.\n"

	mkdir -p ${OUTDIR}
	#? local wd=$( readlink -e $PWD/${OUTDIR} )
	#? cd "$wd"
	cd ${OUTDIR}

	local assembly_filepath=$( get_assembly_filepath "$CUL" )
#	local assembly_filepath="amber.50.fa"
	printf "Will align assembly file : %s\n" "$assembly_filepath"

	align "$assembly_filepath"

	reformat_output

	rename_scaffolds
}
