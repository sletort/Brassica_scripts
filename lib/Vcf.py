#!/usr/bin/env python
# -*- coding: utf-8 -*-

## AIMS  : Vcf file management, as PyVCF cannot write correctly files (doesn't deal with metadata).
## USAGE : x
## AUTHORS : sletort@irisa.fr
## TODO  : redo everything based on Pysam or even better, only use pysam !

import sys
import os
from datetime import date
import pprint # debug
PP = pprint.PrettyPrinter( indent=4, stream=sys.stderr )

class Vcf( object ):
	def __init__( self ):
		self.__l_samples = []
		self.__d_meta    = {}

	@property
	def l_samples( self ):
		return self.__l_samples
	@l_samples.setter
	def _l_samples( self, l_samples ):
		self.__l_samples = l_samples

	@property
	def _d_meta( self ):
		return self.__d_meta
	@_d_meta.setter
	def _d_meta( self, d_meta ):
		self.__d_meta = d_meta

# ----------------------------------------------------------
class Writer( Vcf ):
	def __init__( self, o_file, d_meta={}, l_samples=[] ):
		Vcf.__init__( self )
		self.__o_file   = o_file
		self._l_samples = l_samples

		if d_meta is not None:
			self._d_meta    = d_meta
			self.__writeMeta()

	def build_from_another( cls, o_file, o_vcf_in ):
		return Writer( o_file,
						o_vcf_in._d_meta,
						o_vcf_in.l_samples
					)
	build_from_another = classmethod( build_from_another )

	def build_from_rawMeta_lines( cls, o_file, l_raw_meta, l_samples ):
		for line in l_raw_meta:
			o_file.write( line )
		return Writer( o_file, None, l_samples )
	build_from_rawMeta_lines = classmethod( build_from_rawMeta_lines )

	def __writeMeta( self ):
		d_meta = {
			'fileformat': 'VCFv4.2',
			'source': '"VcfWriter"',
			'fileDate': date.today().strftime( '%Y%m%d' )
		}
		d_meta.update( self._d_meta )

		# order meta - those data will be written key=value
		l_keys = [ 'fileformat',
			'fileDate', 'source', 'reference' ]

		# value linked to those keys have to be list of X object <-- TODO when needed !
		# will be written key=<value>
		l_spe_keys = [
			'INFO', 'FILTER', 'FORMAT'
		]

		for k in l_keys:
			if( k in d_meta ):
				self.__o_file.write( "##{0}={1}\n".format( k, d_meta[k] ) )

		for k in l_spe_keys:
			if( k in d_meta ):
				for o_ in d_meta[k]:
					self.__o_file.write( "##{0}=<{1}>\n".format( k, str( o_ ) ) )

	def appendMeta( self, d_meta ):
		for k in d_meta:
			for o_ in d_meta[k]:
				self.__o_file.write( "##{0}=<{1}>\n".format( k, str( o_ ) ) )

	def writeHeader( self ):
		l_headers = [ 'CHROM', 'POS', 'ID',
				'REF', 'ALT',
				'QUAL', 'FILTER',
				'INFO', 'FORMAT' ]
		head = "\t".join( l_headers + self.l_samples )
		self.__o_file.write( '#' + head + "\n" )


	def writeVariant( self, chrom, pos, ref, d_properties={} ):
		'''minimal SNP is chr, pos, ref
			Output is chrom, pos, ID, ref, alt, qual, filter, info, format, sample1,...,samplei
			Tab separated
		'''
		l_out   = [ chrom, str( pos ) ]
		#~ snp_id  = d_properties.get( 'ID', '_'.join([ chrom, str( pos ) ]) )
		snp_id = '.'
		l_out += [ snp_id, ref ]
		for k in ( 'ALT', 'QUAL', 'FILTER', 'INFO', 'FORMAT' ):
			l_out.append( str( d_properties.get( k, '.' ) ) )

		#~ d_samples = d_properties.get( 'SAMPLES', {} )
		#~ for k in self.l_samples:
			#~ l_out.append( str( d_samples.get( k, '.' ) ) )
		l_samples = d_properties.get( 'SAMPLES', [] )
		for sample_str in l_samples:
			if sample_str is None:
				sample_str = '.'
			l_out.append( str( sample_str ) )

		self.__o_file.write( "\t".join( l_out ) + "\n" )


class MetaField( object ):
	l_keys_required = [ 'ID' ]
	

	def __init__( self ):
		'''_d_descr is the dict that describe one element.'''
		self._d_descr = {}

		self.__l_keys = self.__class__.l_keys_required
		for k in self._d_descr:
			if( k not in self.__class__.l_keys_required ):
				self._l_keys.append( k )

	@property
	def _l_keys( self ):
		'''return the list of d_descr keys in the writing order.'''
		return self.__l_keys

	def _assert_keys( self, d_descr ):
		for k in self.__class__.l_keys_required:
			if( k not in d_descr ):
				msg = "'{}' not defined in description dictionnary.".format( k )
				raise AssertionError( msg )

	def __str__( self ):
		l_ = [ '{0}={1}'.format( k, self._d_descr[k] ) for k in self._l_keys ]
		return ','.join( l_ )


class Format( MetaField ):
	l_keys_required = [ 'ID', 'Number', 'Type', 'Description' ]

	def __init__( self, d_descr ):
		super( self.__class__, self ).__init__()

		self._assert_keys( d_descr )
		self._d_descr = d_descr

	@classmethod
	def standard( cls, l_keys ):
		# commented have not been finished
		d_standard = {
				'GT' : { 'ID': 'GT',  'Number': '1', 'Type': 'String',  'Description':'"Genotype"' },
				'DP' : { 'ID': 'DP',  'Number': '1', 'Type': 'Integer',  'Description':'"read deapth"' },
				#~ 'FT' : { 'ID': 'FT',  'Number': '1', 'Type': '?String',  'Description':'"?Genotype"' },
				#~ 'GL' : { 'ID': 'GL',  'Number': '?1', 'Type': 'Float',  'Description':'"Genotype likelihoods, log10"' },
				#~ 'GLE' : { 'ID': 'GLE',  'Number': '1', 'Type': 'String',  'Description':'"Genotype"' },
				'PL' : { 'ID': 'PL',  'Number': 'G', 'Type': 'Integer',  'Description':'"Phred-scaled Genotype Likelihoods"' },
				#~ ?'GP' : { 'ID': 'GP',  'Number': '1', 'Type': 'String',  'Description':'"Genotype"' },
				#~ ?'GQ': { 'ID': 'GQ', 'Number': '1', 'Type': 'Integer', 'Description':'"Unique haplotype identifier"' },
				'HQ': { 'ID': 'HQ', 'Number': '2', 'Type': 'Integer', 'Description':'"Haplotype Quality"' },
				#~ ?'PS': { 'ID': 'PS', 'Number': '1', 'Type': 'Integer', 'Description':'"Unique haplotype identifier"' },
				#~ ?'PQ': { 'ID': 'PQ', 'Number': '1', 'Type': 'Integer', 'Description':'"Unique haplotype identifier"' },
				#~ ?'EC': { 'ID': 'EC', 'Number': '1', 'Type': 'Integer', 'Description':'"Unique haplotype identifier"' },
				#~ ?'MQ': { 'ID': 'MQ', 'Number': '1', 'Type': 'Integer', 'Description':'"Unique haplotype identifier"' },
				'HAP': { 'ID': 'HAP', 'Number': '1', 'Type': 'Integer', 'Description':'"Unique haplotype identifier"' },
				'AD': { 'ID': 'AD', 'Number': '2', 'Type': 'Integer', 'Description':'"Depth of each allele by sample"' },
			}
		return [ cls( d_standard[k] ) for k in l_keys ]

class Info( MetaField ):
	l_keys_required = [ 'ID', 'Number', 'Type', 'Description' ]

	def __init__( self, d_descr ):
		super( self.__class__, self ).__init__()

		self._assert_keys( d_descr )
		self._d_descr = d_descr

	@classmethod
	def standard( cls, l_keys ):
		d_standard = {
				# none yet
			}
		return [ cls( d_standard[k] ) for k in l_keys ]


# ----------------------------------------------------------
class Reader( Vcf ):
	def __init__( self, o_file ):
		Vcf.__init__( self )
		self.__l_meta_lines = None

		self.__o_file  = o_file
		try:
			self.__o_file.seek( 0, os.SEEK_SET )
		except IOError as e:
			if( 29 == e.errno ):
				msg = "IOError :'{}' encountered,\n" \
					+ "let's consider you are on a stream and ignore this." \
					+ "\n\tIf you are not on a stream, there is a problem !\n"
				sys.stderr.write( msg.format( e ) )
		self.__cur_line = None

		self.__readMeta()
		#~ self.__l_samples = self.__readHeader()
		self.__readHeader()

	def __iter__( self ):
		return self

	def next( self ):
		self.__cur_line = self.__o_file.next()
		return VariantRecord.build_from_vcf_line( self.__cur_line )

	def __readMeta( self ):
		'''For the moment skip line beginning with ##.'''
		self.__l_meta_lines = []
		for line in self.__o_file:
			self.__cur_line = line
			if not line.startswith( '##' ):
				break
			self.__l_meta_lines.append( line )

	def getRawMeta( self ):
		return self.__l_meta_lines

	def __readHeader( self ):
		'''cur_line should contain the header, does nothing !'''
		self.__cur_line.rstrip( "\n" )
		self._l_samples = self.__cur_line.rstrip( "\n" )\
										 .split( '\t' )[9:]


class VariantRecord( object ):
	def __init__( self, seq_id, pos, l_alleles, var_id=None, i_ref_allele=0 ):
		'''i_ref_allele = index of ref allele in l_alleles => dans quel cas on en a besoin ?
				=> en attendant de savoir, je fixe cet indice à 0.

			pos is supposed to be int
			TODO: get field format from header to cast them here in build_from_vcf_line (for example)'''
		self.__seq_id  = seq_id
		self.__pos     = pos
		self.__l_alleles  = l_alleles
		self.__var_id  = var_id
		#self.__i_ref_allele  = i_ref_allele
		self.__l_sample_data = []
		self.__d_infos = {}

	@property
	def seq_id( self ):
		return self.__seq_id
	@property
	def pos( self ):
		return self.__pos
	@property
	def l_alleles( self ):
		return self.__l_alleles
	@property
	def var_id( self ):
		return self.__var_id
	@property
	def d_infos( self ):
		return self.__d_infos
	@property
	def l_sample_data( self ):
		return self.__l_sample_data

	def __lt__( self, o_rec ):
		if self.seq_id < o_rec.seq_id:
			return True
		if self.pos < o_rec.pos:
			return True

		return False

	def __str__( self ):
		return '{}:{}\t{}\t{}\t{}'.format(
						str( self.__seq_id ),
						str( self.__pos ),
						":".join( self.__l_alleles ),
						str( self.__var_id ),
						", ".join([ str(d_) for d_ in self.__l_sample_data ])
					)

	def pos_str( self ):
		return '{}:{}'.format( str( self.__seq_id ), str( self.__pos ) )

	def string( self, l_i_samples=[] ):
		'''Like __str__ but do not output all information (usefull in case of lot of samples).
			l_samples is the list of samples to display.'''
		# PP.pprint( self.__l_sample_data )
		# PP.pprint( l_i_samples )
		return '{}\t{}\t{}\t{}'.format(
						self.pos_str(),
						":".join( self.__l_alleles ),
						str( self.__var_id ),
						", ".join([ str( self.__l_sample_data[i] ) for i in l_i_samples ])
					)

	def addSampleData( self, d_sample ):
		'''Append a sample dictionnary in the list.
			maybe should also give the sample index.
			No time for that yet.'''
		self.__l_sample_data.append( d_sample )

	def updateInfo( self, t_ ):
		'''Append a key-value couple in the info field.
			deprecated use updateInfos.'''
		self.__d_infos.update([ t_ ])

	def updateInfos( self, d_ ):
		'''Append a key-value couple in the info field.'''
		self.__d_infos.update( d_ )

	def build_from_vcf_line( cls, line ):
		l_elts    = line.rstrip().split( "\t" )
		all_ref   = l_elts[3] if( l_elts[3] != "." ) else None
		l_alleles = [ all_ref ] + l_elts[4].split( "," )

		o_var = VariantRecord( l_elts[0], l_elts[1], l_alleles, var_id = l_elts[2] )
		#qual
		#filter
		#info
		for info_str in l_elts[7].split( ";" ):
			if '.' != info_str:
				l_ = info_str.split( '=' )
				o_var.updateInfo( l_ )

		# format
		l_fmt_keys = l_elts[8].split( ":" )
		for format_sample in l_elts[9:]:
			d_sample = {}

			l_fmt = format_sample.split( ":" )
			for i in range( len(l_fmt_keys) ):
				k  = l_fmt_keys[i]
				d_sample[k] = l_fmt[i].split( "," )
			o_var.addSampleData( d_sample )

		return o_var
	build_from_vcf_line = classmethod( build_from_vcf_line )

	def loopOverSamples( self, func ):
		for d_sample in self.__l_sample_data:
			func( d_sample )
		return

	def simple_equals( self, o_rec  ):
		'''simple comparison: id and pos.
			Does not really manage indel.'''
		if self.seq_id != o_rec.seq_id:
			return False
		if self.pos != o_rec.pos:
			return False

		return True

	def write( self, o_writer ):
		def __get_fmt_keys( ld_samples ):
			#~ PP.pprint( ld_samples )
			s_keys = set()
			for d_samples in ld_samples:
				s_keys.update( set( d_samples.keys() ) )
			return sorted( s_keys )


		#~ l_alts  = self.__l_alleles[0:self.__i_ref_allele] \
				#~ + self.__l_alleles[self.__i_ref_allele:]
		# l_alleles[0] is the ref allele
		alt_str = ",".join( self.__l_alleles[1:] )

		if self.__d_infos:
			info_str = ";".join([ "=".join([k,v]) for k,v in self.__d_infos.iteritems() ])
		else:
			info_str = "."

		# /!\ no check is done that all sample have the same keys.
		l_fmt_keys = __get_fmt_keys( self.__l_sample_data )
		fmt_str  = ":".join( l_fmt_keys )

		l_sample_str = []
		for d_sample in self.__l_sample_data:
			l_fmt = []
			for k in l_fmt_keys:
				value = d_sample.get( k, '.' )
				if value is None:
					value = '.'
				if isinstance( value, list ):
					l_ = [ str(x) for x in value ]
				else:
					l_ = [ str( value ) ]
				l_fmt.append( ",".join( l_ ) )
			l_sample_str.append( ":".join( l_fmt ) )

			#~ sample_str = ":".join([ ",".join([ str(x) for x in d_sample[k] ]) for k in l_fmt_keys ])
			#~ l_sample_str.append( sample_str )

		o_writer.writeVariant(
				str( self.seq_id ),
				str( self.pos ),
				#self.__l_alleles[self.__i_ref_allele],
				self.__l_alleles[0],
				{
					'ALT': alt_str, 'INFO': info_str,
					'FORMAT' : fmt_str,
					'SAMPLES': l_sample_str
				}
			)

# ---------------------------
if __name__ == '__main__':

	print( "En construction" )

	# simple SNP : chr, pos, ref, alt
	l_samples = [ 'I', 'J' ]
	t_snp = (
			[ 'chrC04', 125442, 'T',
				{ 'ALT': 'C', 'FORMAT':'GT',
					'SAMPLES': dict( zip( l_samples, ( '1|1', '0|1' ) ) )
				}
			],
		[ 'chrC05', 1254, 'A',
				{ 'ALT': 'C', 'FORMAT':'GT:HAP',
					'SAMPLES': dict( zip( l_samples, ( '1|1:9', '0|1:9' ) ) )
				}
			]
	)
	
	filename = 'test.vcf'
	with open( filename, 'w' ) as o_fs:
		d_meta = {
			'reference': 'ref.fa',
			'FORMAT': Format.standard([ 'GT', 'HAP' ])
		}

		o_vcf = Writer( o_fs, d_meta, l_samples )
		for x in t_snp:
			#~ PP.pprint( x )
			l_alleles = [ x[2], x[3]['ALT'] ]
			o_rec = VariantRecord( x[0], x[1], l_alleles )
			for sample in l_samples:
				sample_data_str = x[3]['SAMPLES'][sample]
				d_sample = dict( zip( x[3]['FORMAT'].split( ":" ), sample_data_str.split( ":" ) ) )

				o_rec.addSampleData( d_sample )
			o_rec.write( o_vcf )

	print( "a file named {} has been generated.".format( filename ) )

	sys.exit( 0 )

