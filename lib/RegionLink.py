#!/usr/bin/env python
#-*- coding:utf-8 -*-

## AIMS  : Facilitate Region manipulation (cf bed file) and Link between them (alignment).
## USAGE : ./RegionLink ?

#~ from collections import defaultdict

class Region( object ):
	"""A region is something with a start and end, related to a chrom (or anything uniq)."""

	def __init__( self, chrom, start, end ):
		'''chrom should be a string, start & end integers.
			If end < start, it will be reversed.
			In the object, start <= end.
		'''
		self.__chrom  = chrom

		start = int( start )
		end   = int( end )

		if( start <= end ):
			self.__start  = start
			self.__end    = end
			self.__reversed = False

		else:
			self.__start  = end
			self.__end    = start
			self.__reversed = True

	@property
	def start( self ):
		return self.__start
	@start.setter
	def start( self, start ):
		self.__start = start
	@property
	def end( self ):
		return self.__end
	@end.setter
	def end( self, end ):
		self.__end = end
	@property
	def chrom( self ):
		return self.__chrom
	@property
	def seq_id( self ):
		return self.__chrom
	@property
	def reversed( self ):
		return self.__reversed
	@reversed.setter
	def reversed( self, bool_flg ):
		self.__reversed = bool_flg

	def getall( self ):
		return self.seq_id, self.start, self.end, self.reversed


	def __str__( self ):
		#~ if( self.reversed ):
			#~ return "%s:%d-%d" % (self.chrom, self.end, self.start )
		return "%s:%d-%d (%s)" % (self.chrom, self.start, self.end, self.reversed )

	def __lt__( self, o_region ):
		if( self.chrom < o_region.chrom ):
			return True
		if( self.chrom > o_region.chrom ):
			return False

		if( self.start < o_region.start ):
			return True
		return( self.start == o_region.start and self.end < o_region.end )

	def __eq__( self, o_region ):
		if( self.chrom == o_region.chrom 
				and self.start == o_region.start
				and self.end   == o_region.end ):
			return True
		return False
	def __ne__( self, o_region ):
		return not self == o_region

	def __len__( self ):
		'''self.end - self.start + 1'''
		return self.end - self.start + 1

	def length( self ):
		'''self.end - self.start + 1
		deprecated, use len(x)'''
		return self.end - self.start + 1

	def feature( self ):
		'''( start,end )'''
		return ( self.start, self.end )

	def recString( self ):
		'''return a string tsv based : chrom, start,end'''
		l_elts = [ self.chrom ]

		if( True == self.reversed ):
			l_elts.append( str(self.end) )
			l_elts.append( str(self.start) )
		else:
			l_elts.append( str(self.start) )
			l_elts.append( str(self.end) )

		return "\t".join( l_elts )

	def intersect( self, o_region, strict=True ):
		'''Return True if self & o_region share at least 1 base.'''
		if( self.chrom != o_region.chrom ):
			return False
		(o_r1,o_r2) = Region.sort( self, o_region )
		if( strict ):
			return( o_r2.start < o_r1.end )
		return( o_r2.start <= o_r1.end )

	def overlap( self, o_region ):
		'''return a new region representing the overlap between the 2 regions'''
		if( not self.intersect( o_region ) ):
			raise Exception( "Regions are not overlapping." ) # faire une exception adHoc

		(o_r1,o_r2) = Region.sort( self, o_region )
		start = o_r2.start
		end   = min( o_r1.end, o_r2.end )
		return self.subRegion( start, end )

	def includes( self, o_region, strict=False ):
		if( self.chrom != o_region.chrom ):
			return False

		if( strict ):
			return ( self.start < o_region.start and o_region.end < self.end )
		return ( self.start <= o_region.start and o_region.end <= self.end )

	def subRegion( self, start=None, end=None ):
		if( start == None ):
			start = self.start
		if( end == None ):
			end = self.end

		# pourquoi ? quelle est la justification de ce test ?
		if( self.reversed ):
			return Region( self.chrom, end, start )
		return Region( self.chrom, start, end )

	def partition( self, o_region ):
		'''almost like bedops partition tool.'''
		if( o_region.seq_id != self.seq_id ):
			return [ self, o_region ]

		l_bounds = sorted( self.feature() + o_region.feature() )
		lo_parts = []
		for j in range( 1, len( l_bounds ) ):
			start = l_bounds[j-1]
			end   = l_bounds[j]
			lo_parts.append( self.subRegion( start, end) )
		
		return lo_parts
	# partition

	#~ def isCloseTo( self, o_region, pc_limit=0.1 ):
		#~ # distance between the 2 consecutive regions
		#~ d = o_region.start - self.end
		#~ D = o_region.end   - self.start
		#~ if( 0 == D ):
			#~ print self
			#~ print o_region
			#~ print "The length of the merged region is 0. This is clearly an error !"
			#~ sys.exit( 2 )

		#~ r = float( d ) / D
#~ #		print "%d / %d = %f" % ( d, D, r )
		#~ return ( r <= pc_limit )

#	#
#	def orderedBorders( self ):
#		m = min( self.start, self.end )
#		M = max( self.start, self.end )
#		return (m,M)



# class methods
	# 
	def sort( cls, o_region1, o_region2 ):
#		print str( o_region1 ) + " VS " + str( o_region2 )

		if( o_region1.chrom != o_region2.chrom ):
			return (o_region1, o_region2 )

		if( o_region1.start < o_region2.start ):
			return ( o_region1, o_region2 )

		if( o_region2.start < o_region1.start ):
			return ( o_region2, o_region1 )

		# else s1 == s2, no rule, dont change order.
		return ( o_region1, o_region2 )
	sort = classmethod( sort )

	def merge( cls, lo_regions ):
		'''merge regions when they share at least 1 base.'''
		if( 0 == len( lo_regions ) ):
			return []

		lo_regions.sort( key = lambda x: ( x.chrom, x.start, x.end ) )
		l_out  = []
		o_reg1 = lo_regions[0]
		for o_reg in lo_regions[1:]:
			if( o_reg1.chrom == o_reg.chrom 
					and o_reg.start <= o_reg1.end+1 ):
				if( o_reg1.end < o_reg.end ):
					o_reg1.end = o_reg.end
			else:
				l_out.append( o_reg1 )
				o_reg1 = o_reg

		l_out.append( o_reg1 )

		return l_out
	merge = classmethod( merge )

class RegionLink( object ):
	"""A RegionLink aims to manage Link between 2 regions.
		It is especially what you have when you align 2 sequences.
		
		strand = 1|-1"""

	def __init__( self, o_ref, o_tgt, strand=None ):
		self.o_ref = o_ref
		self.o_tgt = o_tgt
		self.__strand = strand

	def __str__( self ):
		l_elts =  ( self.o_ref, self.o_tgt, self.strand )
		s = "%s\t%s\t%s" % tuple( [ str(x) for x in l_elts ] )
		#~ ( lambda x: str(x) str( self.o_ref ), str( self.o_tgt ) + "\t" + 
		return s

	def __eq__( self, o_rl ):
		if( self.strand == o_rl.strand ):
			if( self.o_ref == o_rl.o_ref 
					and self.o_tgt == o_rl.o_tgt ):
				return True
			if( self.o_ref == o_rl.o_tgt 
					and self.o_tgt == o_rl.o_ref ):
				return True
		return False

	def __ne__( self, o_rl ):
		return not self == o_rl

	def recString( self, mode=None ):
		if( None == mode ):
			s = "\t".join([ self.o_ref.recString(), self.o_tgt.recString() ])
		elif( 'ref' == mode ):
			if( self.o_ref.reversed ):
				self.o_ref.reversed = False
				self.o_tgt.reversed = not self.o_tgt.reversed
			s = "\t".join([ self.o_ref.recString(), self.o_tgt.recString() ])
		else:
			msg  = "Bad mode {} used in recString, ".format( mode )
			msg += "authorized value is '{}'".format( 'ref' )
			raise RegionLinkError( msg )

		return s

	@property
	def strand( self ):
		return self.__strand
	@property
	def strand_symbol( self ):
		if( 1 == self.__strand ):
			return '+'
		return '-'
	@strand.setter
	def strand( self, value ):
		self.__strand = value

	def getCigar( self ):
		'''return Mref_length as list'''
		return [ 'M' + str( self.o_ref.length() ) ]

	def revert( self ):
		o_x = self.o_ref
		self.o_ref = self.o_tgt
		self.o_tgt = o_x
		if( -1 == self.strand ):
			self.o_ref.reversed = False
			self.o_tgt.reversed = True
		return self

	def upstream_nbases( self, d_chrom_sizes ):
		'''return the number of bases in the ref_upstream part of seqs = before link.'''

		up_ref = self.o_ref.start - 1

		if( 1 == self.strand ):
			stream_tgt = self.o_tgt.start - 1
		else:
			stream_tgt = d_chrom_sizes[self.o_tgt.chrom] - self.o_tgt.end

		return ( up_ref, stream_tgt )

	def downstream_nbases( self, d_chrom_sizes ):
		'''return the number of bases in the ref_downstream part of seqs = after link.'''

		down_ref = d_chrom_sizes[self.o_ref.chrom] - self.o_ref.end

		if( 1 == self.strand ):
			stream_tgt = d_chrom_sizes[self.o_tgt.chrom] - self.o_tgt.end
		else:
			stream_tgt = self.o_tgt.start - 1

		return ( down_ref, stream_tgt )


	#~ def getTgtStart( self ):
		#~ '''Different from self.o_tgt.start, this one regard the strand.'''
		#~ if( 1 == self.strand ):
			#~ return self.o_tgt.start
		#~ return self.o_tgt.end


	#~ def update( self, o_link ):
		#~ '''No check on chrom.'''
		#~ # row are sorted on ref.
		#~ # 	so self.start <= o_rec.start + strand +
		#~ if( self.o_ref.end < o_link.o_ref.end ):
			#~ self.o_ref.end = o_link.o_ref.end

		#~ # tgt are not sorted.
		#~ self.o_tgt.start = min( self.o_tgt.start, o_record.o_tgt.start )
		#~ self.o_tgt.end   = max( self.o_tgt.end, o_record.o_tgt.end )
		#~ self.o_tgt.strand = 0 # undefined.

	#
	#~ def isCloseTo( self, o_record, pc_limit=0.1 ):
		#~ if( not self.o_ref.isCloseTo( o_record.o_ref, pc_limit ) ):
			#~ return False

		#~ # target are not ordered
#~ #		print "Target is not sorted on start."
		#~ ( o_tgt1, o_tgt2 ) = Region.sort( self.o_tgt, o_record.o_tgt )
		#~ if( not o_tgt1.isCloseTo( o_tgt2, pc_limit  ) ):
			#~ return False

		#~ return True

# class methods
	#~ def loadLinkFile_asHash_refKey( cls, o_csv ):
		#~ '''This method load a region link file not necessarily sorted,
			#~ the returned structure will be dict[ref_chrom][tgt_chrom] = lo_links
		#~ o_csv is what return csv.reader,
			#~ but can be anything that can loop on line.'''

		#~ d_ref_tgt_olinks = defaultdict(lambda: defaultdict(list))

		#~ n_links = 0
		#~ for a_elts in o_csv:
			#~ o_ref  = Region( *a_elts[0:3] )
			#~ o_tgt  = Region( *a_elts[3:6] )
			#~ o_link = RegionLink( o_ref, o_tgt )

			#~ d_ref_tgt_olinks[o_ref.chrom][o_tgt.chrom].append(o_link)
			#~ n_links += 1

		#~ return d_ref_tgt_olinks, n_links
	#~ loadLinkFile_asHash_refKey = classmethod( loadLinkFile_asHash_refKey )

	def loadSimpleLinkFile( cls, o_csv ):
		'''o_csv is what return csv.reader,
			but can be anything that can loop on line'''
		l_links = []

		for a_elts in o_csv:
			o_ref  = Region( *a_elts[0:3] )
			o_tgt  = Region( *a_elts[3:6] )

			if( o_ref.reversed == o_tgt.reversed ):
				strand = 1
			else:
				strand = -1
			o_link = RegionLink( o_ref, o_tgt, strand )
			l_links.append( o_link )

		return l_links
	loadSimpleLinkFile = classmethod( loadSimpleLinkFile )

	def writeSimpleLinkFile( cls, o_fs, lo_links ):
		'''print lo_links in a Region file, one per row.
			lo_links is a Sequence of RegionLink.'''
		for o_link in lo_links:
			o_fs.write( o_link.recString() + "\n" )

		return
	writeSimpleLinkFile = classmethod( writeSimpleLinkFile )

	def writeRefLinkFile( cls, o_fs, lo_links ):
		'''print lo_links in a Region file, one per row.
			lo_links is a Sequence of RegionLink.
			All ref region will be + stranded'''
		for o_link in lo_links:
			o_fs.write( o_link.recString( mode='ref' ) + "\n" )

		return
	writeRefLinkFile = classmethod( writeRefLinkFile )

	def loadAgpFile( cls, o_csv ):
		'''Until I rewrite my AGP class ...'''

		l_links = []

		for a_elts in o_csv:
			if( a_elts[0].startswith( "#" ) or 'N' == a_elts[4] ):
				continue

			o_ref  = Region( *a_elts[0:3] )
			if( 'N' != a_elts[4] ):
				o_tgt  = Region( *a_elts[5:8] )

			if( a_elts[8] == '+' ):
				strand = 1
			else:
				strand = -1
			o_link = RegionLink( o_ref, o_tgt, strand )
			l_links.append( o_link )

		return l_links
	loadAgpFile = classmethod( loadAgpFile )


	def getRefGap( cls, o_link1, o_link2 ):
		# -1 suppose [start;end] data
		if( o_link1.o_ref < o_link2.o_ref ):
			return o_link2.o_ref.start - o_link1.o_ref.end - 1
		return o_link1.o_ref.start - o_link2.o_ref.end - 1
	getRefGap = classmethod( getRefGap )

	def getTgtGap( cls, o_link1, o_link2 ):
		if( o_link1.o_tgt < o_link2.o_tgt ):
			return o_link2.o_tgt.start - o_link1.o_tgt.end
		return o_link1.o_tgt.start - o_link2.o_tgt.end
	getTgtGap = classmethod( getTgtGap )

	def getRefSurface( cls, o_link1, o_link2 ):
		if( o_link1.o_ref < o_link2.o_ref ):
			return o_link2.o_ref.end - o_link1.o_ref.start
		return o_link1.o_ref.end - o_link2.o_ref.start
	getRefSurface = classmethod( getRefSurface )

	def getTgtSurface( cls, o_link1, o_link2 ):
		if( o_link1.o_tgt < o_link2.o_tgt ):
			return o_link2.o_tgt.end - o_link1.o_tgt.start
		return o_link1.o_tgt.end - o_link2.o_tgt.start
	getTgtSurface = classmethod( getTgtSurface )


class Bloc( RegionLink ):
	"""A Bloc is a list of RegionLink that share the same ref_chrom and the same tgt_chrom.
	A Bloc is a special RegionLink.
	# For now no check is done !
	"""
	# I note l_links, but let consider for the moment that it is a tuple

	def __init__( self, l_links, name='' ):
		def __getStrand( l_links ):
			'''return the strand, based on the number of tgt bases'''

			if( 1 < len( self.l_links ) ):
				n_bases = 0
				for o_link in self.l_links:
					n_bases += o_link.strand * o_link.o_tgt.length()
			else:
				n_bases = self.l_links[0].strand # shortcut

			if( 0 < n_bases ):
				strand = 1
			else:
				strand = -1

			return strand
		# __getStrand

		self.__l_links = l_links
		self.__name = name

		chrom = l_links[0].o_ref.chrom
		start = min( [ o.o_ref.start for o in self.l_links ] )
		end   = max( [ o.o_ref.end   for o in self.l_links ] )
		o_ref = Region( chrom, start, end )

		chrom = l_links[0].o_tgt.chrom
		start = str( min( [ o.o_tgt.start for o in self.l_links ] ) )
		end   = str( max( [ o.o_tgt.end   for o in self.l_links ] ) )
		o_tgt = Region( chrom, start, end )

		strand = __getStrand( l_links )
		super( Bloc, self ).__init__( o_ref, o_tgt, strand )
	#__init__

	@property
	def l_links( self ):
		return self.__l_links

	@property
	def name( self ):
		return self.__name
	@name.setter
	def name( self, value ):
		self.__name = value

	def __str__( self ):
		s = "\n".join([ str(o) for o in self.l_links ])
		return s

	def getCigar( self ):
		'''Not complete/in dev
		based on ref.
		return the cigar string as a list M2 D3 M3'''
		l_cigar = self.__l_links[0].getCigar()
		for i in range( 1, len( self.__l_links ) ): # I use range to be compatible py2 & 3
			o_link = self.__l_links[i]

			gap_size = RegionLink.getRefGap( self.__l_links[i-1], o_link )
			l_cigar.append( 'D' + str( gap_size ) )
			l_cigar += o_link.getCigar()

		return l_cigar

	def getRefAlignLength( self ):
		l = 0
		for o_link in self.__l_links:
			l += o_link.o_ref.length()
		return l

	def getTgtAlignLength( self ):
		l = 0
		for o_link in self.__l_links:
			l += o_link.o_tgt.length()
		return l

	def checkCohesion( self, threshold_pc=0.2 ):

		l_blocs  = []
		lo_links = [ self.l_links[0] ]
		#import sys
		#RegionLink.writeSimpleLinkFile( sys.stdout, self.l_links )
		for i in range( 1, len( self.l_links ) ):
			ref_S = abs( RegionLink.getRefSurface( self.l_links[i-1], self.l_links[i] ) )
			tgt_S = abs( RegionLink.getTgtSurface( self.l_links[i-1], self.l_links[i] ) )
			#print( "ref_S = " + str( ref_S ) )
			#print( "tgt_S = " + str( tgt_S ) )

			# developpement de score = (A-B)/(A+B) avec A > B
			#	score = (A-B) }/ ( A + B ) +1-1
			#	score = { (A+B) + (A-B) }/ ( A + B ) - 1
			#	score = 2*A / ( A + B ) - 1
			sum = ref_S + tgt_S
			#print( "sum = " + str( sum ) )

			if( 0 == sum ): # dans quel cas on a ça ?
				score = 100
			else:
				score = 2.0 * max( ref_S,tgt_S ) / sum - 1
			#print( "score = " + str( score ) )

			if( score > threshold_pc ):
				l_blocs.append( Bloc( lo_links ) )
				lo_links = [ self.l_links[i] ]
			else:
				lo_links.append( self.l_links[i] )
		l_blocs.append( Bloc( lo_links ) )

		return l_blocs

	def check4Gaps( self, threshold_pc=0.8 ):

		l_blocs  = []
		lo_links = [ self.l_links[0] ]
		#import sys
		#RegionLink.writeSimpleLinkFile( sys.stdout, self.l_links )
		for i in range( 1, len( self.l_links ) ):
			ref_gap = abs( RegionLink.getRefSurface( self.l_links[i-1], self.l_links[i] ) )
			tgt_gap = abs( RegionLink.getTgtSurface( self.l_links[i-1], self.l_links[i] ) )
			#print( "ref_gap = " + str( ref_gap ) )
			#print( "tgt_gap = " + str( tgt_gap ) )

			# developpement de (A-B)/(A+B) avec A > B
			S = ref_gap + tgt_gap
			#print( "S = " + str( S ) )

			if( 0 == S ):
				score = 0
			else:
				score = 2.0 * max( ref_gap,tgt_gap ) / S - 1
			#print( "score = " + str( score ) )

			if( score < threshold_pc ):
				l_blocs.append( Bloc( lo_links ) )
				lo_links = [ self.l_links[i] ]
			else:
				lo_links.append( self.l_links[i] )
		l_blocs.append( Bloc( lo_links ) )

		return l_blocs
	
	#~ def getRefFeature( self ):
		#~ '''return ref.start, ref.end'''

		#~ # For now I do not consider links sorted
		#~ start   = str( min( [ o.o_ref.start for o in self.l_links ] ) )
		#~ end     = str( max( [ o.o_ref.end   for o in self.l_links ] ) )

		#~ return start,end

	#~ def getTgtFeature( self ):
		#~ '''return tgt.start, tgt.end'''

		#~ # For now I do not consider links sorted
		#~ start   = str( min( [ o.o_tgt.start for o in self.l_links ] ) )
		#~ end     = str( max( [ o.o_tgt.end   for o in self.l_links ] ) )

		#~ return start,end

class Links( object ):
	def __init__( self, lo_links ):
		self.__lo_links = sorted( lo_links, key=lambda x: x.o_ref.feature() )
		self.__i = None

	@property
	def lo_links( self ):
		return self.__lo_links

	def __iter__( self ):
		self.__i = 0
		return self

	def next( self ):
		if( self.__i > len( self.lo_links ) - 1 ):
			raise StopIteration
		else:
			self.__i += 1
			return self.lo_links[self.__i - 1]

	def __len__( self ):
		return len( self.lo_links )

	def mergeOnRef( self ):
		'''Return a list of target regions under the key merged ref region.
		
			**NOT YET TESTED**
		'''
		o_region = self.lo_links[0].o_ref
		lo_tgts  = [ self.lo_links[0].o_tgt ]
		d_roi    = dict()
		for o_link in self.lo_links[1:]:
			if( o_region.intersect( o_link.o_ref ) ):
				if( o_link.o_ref.start < o_region.start ):
					o_region.start = o_link.o_ref.start
				if( o_region.end < o_link.o_ref.end ):
					o_region.end = o_link.o_ref.end
				lo_tgts.append( o_link.o_tgt )
			else:
				if( 1 < len( lo_tgts ) ):
					d_roi[ o_region ] = lo_tgts
				o_region = o_link.o_ref
				lo_tgts  = [ o_link.o_tgt ]
		
		return d_roi
# Links

class ChromLinks( Links ):
	'''a ChromLinks is a list of RegionLink that have the same ref.chrom.
	
	WARN : no check is done.'''

	# franchement pas sur de l'efficacité du truc !
	# parcourir 2 fois la liste, me semble une fois de trop.
	def getCoverage( self, o_ref ):
		'''return the regions shared with the number of sharing.
		
		This function is exactly what you could have with bedtools genomecov -bga.
		o_ref is the "genome" region, start-end is the entire region.'''
		s_pos = set( o_ref.feature() )
		for o_link in self.lo_links:
			s_pos.update({ o_link.o_ref.start, o_link.o_ref.end })
		l_bounds = sorted( list( s_pos ) )


		lo_coverage = []
		for i in range( len( l_bounds )-1 ):
			lo_coverage.append( Region( o_ref.chrom, l_bounds[i], l_bounds[i+1] ) )

		ho_coverage = { x:1 for x in lo_coverage }
		for o_link in self.lo_links:
			( start, end ) = o_link.o_ref.feature()
			for o_region in lo_coverage:
				#~ if( start < o_region[1] ):
				if( o_link.o_ref.includes( o_region ) ):
					ho_coverage[o_region] += 1

		return ho_coverage

	def __str__( self ):
		s = ""
		for o_link in self.lo_links:
			s += str( o_link ) + "\n"
		return s

# ==========================================================
def test__partition():
	o_r1 = Region( 'c1', 11, 22 )
	o_r2 = Region( 'c2', 30, 5 )
	lo_regions = o_r1.partition( o_r2 )
	lo_expected = [ o_r1, o_r2 ]
	assert( lo_expected == lo_regions )

	o_r1 = Region( 'c1', 11, 22 )
	o_r2 = Region( 'c1', 30, 5 )
	lo_regions = o_r1.partition( o_r2 )

	lo_expected = [
			Region( 'c1',  5, 11 ),
			Region( 'c1', 11, 22 ),
			Region( 'c1', 22, 30 )
		]
	assert( lo_expected == lo_regions )

# test__partition
def main( l_args ):
	'''build a Region'''
	o_region1 = Region( 'c1', 11, 22 )
	print( "region1 : " + str( o_region1 ) )
	o_region2 = Region( 'c1', 30, 5 )
	print( "region2 : " + o_region2.recString() )
	o_region3 = Region( 'c2', 13, 2050 )
	print( "region3 : " + str( o_region3 ) )

	(o_r1,o_r2) = Region.sort( o_region2, o_region1 )
	print( "the first region between region2 and region1 is : " + str( o_r1 ) )
	(o_r1,o_r2) = Region.sort( o_region3, o_region1 )
	print( "the first region between region3 and region1 is : " + str( o_r1 ) )

	o_link = RegionLink( o_region1, o_region2 )
	print( "Link 1-2 = " + str( o_link ) )
	o_link = RegionLink( o_region1, o_region3 )
	print( "Link 1-3 = " + o_link.recString() )

	return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
