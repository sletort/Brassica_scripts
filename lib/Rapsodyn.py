#!/usr/bin/env python
# -*- coding: utf-8 -*-

## AIMS  : Package Rapsodyn, provide some functions to work with Rapsodyn data.
## USAGE : import Rapsodyn
## AUTHORS : sebastien.letort@irisa.fr


import sys
import os
import subprocess
import pprint # debug

PP = pprint.PrettyPrinter(indent=4)

# ========================================
# Some variables that can be modified
#	should be the same values as rapso_paths.sh
#	should be rationnalized, key val file ?
DB_ASSEMBLY_ROOT=''
DB__ROOT = os.path.dirname( os.path.realpath( __file__ ) ) + "/../"

# ========================================
# a kind of namespace ?
class Paths():
	def get_assembly_path( cls, cultivar ):
		return "{0}/Bnapus/{1}".format( DB_ASSEMBLY_ROOT, cultivar )
	get_assembly_path = classmethod( get_assembly_path )

	def get_ref_fasta( cls, species ):
		json_file=DB__ROOT + "/References.json"
		cmd = "jq .References.{0}.fasta_filepath {1}" \
			.format( species.lower(), json_file )
		process = subprocess.Popen( cmd.split(), stdout=subprocess.PIPE )
		out,err = process.communicate()

		return eval( out )
	get_ref_fasta = classmethod( get_ref_fasta )

# ========================================
# pytest fonctions
def test_constant():
	#~ assert os.path.isdir( DB_ASSEMBLY_ROOT )
	assert os.path.isdir( DB__ROOT )

def test__Paths():
	assert Paths.get_assembly_path( "X" )
	assert Paths.get_ref_fasta( "Bnapus" )

# ========================================
if __name__ == '__main__':

	Paths.get_ref_fasta( "Bnapus" )

	sys.exit( 0 )
