#!/usr/bin/env python
# -*- coding: utf-8 -*-

## AIMS  : Parse a (bgzipped) fasta file and extract a sub-sequence regarding arguments
## USAGE : import MFasta
## NOTE  : 

import sys
import pprint

import pysam
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord

VERSION = "0.1.0"
PP = pprint.PrettyPrinter(indent=4)

# ========================================
NO_ID = '...'

class ExtractSeq( object ):
	def __init__( self, filepath ):
		#~ if( not isinstance( o_fe, file ) ):
			#~ o_fe = open( o_fe, 'r' )
		#~ self.__o_fe = o_fe
		self.__filepath = filepath

	def extractRegions( self, ldo_regions, fn_build_subseq_id=None, **fn_args ):
		'''given a list of regions, yield fasta sequences.

		lo_regions has to be a list of Region objects.
		fn_build_subseq_id is a callback function to define seq_id, by default return region.chrom
		'''
		with pysam.FastaFile( self.__filepath ) as fe:
			#~ verbose( "Working with file : " + fe.filename )

			for do_region in ldo_regions:
				( subseq_id, o_region ) = do_region.popitem()
				( seq_id,start,end,rev ) = o_region.getall()

				seq_str = fe.fetch( reference=seq_id,
									start=start-1, # turn fn_build_seq_idto 0-based
									end=end )      # 1-based included == 0-based excluded
				o_seq   = Seq( seq_str )
				if( rev ):
					o_seq = o_seq.reverse_complement()

				if( fn_build_subseq_id ):
					if( subseq_id == NO_ID ):
						subseq_id = None
					# not sur = signature not guaranted.
					full_subseq_id = fn_build_subseq_id( o_region, o_seq, subseq_id, **fn_args )
					#~ seq_id = fn_build_seq_id( o_region, o_seq, new_seq_id, *fn_args )

				yield( full_subseq_id, o_seq )

	def extractCoords( self, lt_coords, fn_build_seq_id=None, *fn_args ):
		'''given a list of tuples (seq_id,start,end,strand), yield fasta sequences.

		fn_build_seq_id is a callback function to define seq_id, by default return region.chrom
		'''
		with pysam.FastaFile( self.__filepath ) as fe:
			#~ verbose( "Working with file : " + fe.filename )

			for seq_id, start,end, strand in lt_coords:
				seq_str = fe.fetch( reference=seq_id,
									start=start-1, # turn to 0-based
									end=end )      # 1-based included == 0-based excluded
				o_seq   = Seq( seq_str )
				if( '-' == strand or '-1' == strand or False == strand ):
					o_seq = o_seq.reverse_complement()

				if( fn_build_seq_id ):
					seq_id = fn_build_seq_id( o_region, o_seq, *fn_args )

				yield( seq_id, o_seq )

def build_seqId( o_region, o_seq, seq_id=None, prefix=None ):
	'''It builds a sequence header with seq_id, region, strand ; "|" separated.

	Function to use in extractRegions.
	For region seq1, 10-100, + strand,
	the id will be
	>seq1|10-100|+
	
	with "human" prefix it will be
	>human|seq1|10-100|+'''
	( chrom,start,end,rev ) = o_region.getall()

	if( not end ):
		end = len( o_seq )

	strand = '-' if( rev ) else '+'
	l_elts = [ chrom, str(start)+"-"+str(end), strand ] # human reading.
	if( prefix ):
		l_elts.insert( 0, prefix )
	if( seq_id ):
		l_elts.insert( 0, seq_id )

	return "|".join( l_elts )
# build_seqId


# ExtractSeq
# ========================================
# pytest fonctions
import tempfile

# TODO : demander conseil à Lucas/Pierre
class __fixture( object ):
	FAKE_ID1  = "seq1"
	FAKE_SEQ1 = "accgggtttt"

	#~ class fake_Region( object ):

	def getall( cls ):
		return( __fixture.FAKE_ID1, 1, len( __fixture.FAKE_SEQ1 ), False )


def make_tmp_fasta_file():
	filename = next( tempfile._get_candidate_names() )

	with open( filename, 'w' ) as o_fs:
		o_fs.write( ">" + __fixture.FAKE_ID1 + "\n" + __fixture.FAKE_SEQ1 + "\n" )
		o_fs.write( ">2\natatcggccggcta\n" )

	return filename
# TODO detruire le fichier apres usage.

# ----------------------------------------
def test__cstr():
	assert isinstance( ExtractSeq( sys.argv[0]), ExtractSeq )

#~ def test__extractRegions_simple():
	#~ fasta_file = make_tmp_fasta_file()
	#~ o_es = ExtractSeq( fasta_file )
	#~ l_seqs  = list( o_es.extractRegions( [ fixture.fake_Region() ] ) )
	#~ assert l_seqs[0] == ">" + fixture.FAKE_ID1 + "\n" + fixture.FAKE_SEQ1 + "\n"

# ========================================
if __name__ == '__main__':

	filepath = sys.argv[1]
	o_es = ExtractSeq( filepath )

	ll_coords = []
	for x in sys.argv[2].split( ";" ):
		l_ = x.split( " " )
		l_[1:3] = map( int, l_[1:3] )
		ll_coords.append( l_ )

	# ll_coords = [ x.split( " " ) for x in sys.argv[2].split( ";" ) ]
	for seq_id,o_seq in o_es.extractCoords( ll_coords ):
		print( ">{0}\n{1}\n".format( seq_id, str( o_seq ) ) )
	
	#~ x = __fixture()
	#~ print( str( x ) )
	#~ w = x.getall()
	#~ print( str( w ) )

	#~ fasta_file = make_tmp_fasta_file()
	#~ o_es = ExtractSeq( fasta_file )
	#~ l_seqs  = list( o_es.extractRegions( [ x ] ) )

	sys.exit( 0 )

