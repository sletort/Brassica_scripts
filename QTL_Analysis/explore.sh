#! /bin/bash

## AIMS  : explore a QTL region
## USAGE : explore.sh QTL_file.bed
## NOTE  :
## AUTHORS : sletort@irisa.fr

set -o nounset
set -o errexit

# =========================================================
declare -r INFILE="${1?What is your input file ?}"

# ---------------------------------------------------------
#declare -r MG_MAP_DIR="/PUBLIC_DATA/GENERATED/1kG/map_files"

# =========================================================
function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT     = $0\n"
	printf "╟ - INFILE     = $INFILE\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

# =========================================================
function main ()
{
	# infile is supposed to be bed file
	# = chrom, start, end, 0-based, but end excluded => [0;100] = 100 bases from 0 to 99.
	# other column are excluded for the moment
	while read chrom start end
	do
		echo "$chrom : [$start - $end["
	done < $INFILE
}

# =========================================================
printVars

# your work.
main

printf "End of %s.\n\n" $( basename $0 )

exit 0
