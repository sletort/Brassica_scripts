#! /bin/bash

## AIMS  : provides personal paths for PolySuccess project, specific to a machine.
## USAGE : source brassicaDB.poly_paths.sh
## NOTE  : This is a requirement of polysuccessDB.sh

# =========================================================
# where	valid assembly file are stored
declare -r DB_ASSEMBLY_ROOT=''
