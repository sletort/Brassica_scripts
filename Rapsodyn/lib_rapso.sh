#! /bin/bash

## USAGE : source lib_rapso.sh
## NOTE  : This are path definition of important files.
## AUTHORS : sletort@irisa.fr

set -o nounset
set -o errexit

# =========================================================
declare -r RDF_PATH=/groups/brassica/sletort/RDF/Rapsodyn
mkdir -p $RDF_PATH

declare -r CULTIVAR_TTL=$RDF_PATH/cultivars.ttl
declare -r SEQ_TTL=$RDF_PATH/seqs.ttl

declare    ROOT_PATH=$( dirname $( readlink -e ${BASH_SOURCE[0]} ) )
declare -r CULTIVAR_DESCR_FILE="$ROOT_PATH/metadata/cultivar.descr.tsv"

# ---------------------------------------------------------
function lib_rapso__show_vars ()
{
	cat <<EOF
	RDF_PATH :     $RDF_PATH
	CULTIVAR_TTL : $CULTIVAR_TTL
	SEQ_TTL      : $SEQ_TTL

	CULTIVAR_DESCR_FILE : $CULTIVAR_DESCR_FILE
EOF
}
