#! /bin/bash

## AIMS  : run simka on SGE, with all the 29 cultivars.
## USAGE : ./runSimka_over_sge.sh
## NOTE  :
## AUTHORS : sletort@irisa.fr

set -o nounset
set -o errexit

# =========================================================

# ---------------------------------------------------------
declare -r USER_SERVER=sletort
declare -r DATA_SERVER=rapsodyn-is
declare -r DATA_ROOT_PATH=/db-rapsodyn/DATA_reseq_backup_2014

declare -r SEQ_TTL=$HOME/RDF/Rapsodyn/seqs.ttl

# =========================================================
function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT         = $0\n"
	printf "╟ - DATA_SERVER    = $DATA_SERVER\n"
	printf "╟ - DATA_ROOT_PATH = $DATA_ROOT_PATH\n"
	printf "╟ - SEQ_TTL        = $SEQ_TTL\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

# =========================================================
function build_simka_infile ()
{
	local perle='
		# remove all <> from URI
		@F = map{ s/[<>]//g; $_; } @F;

		# definition of a sequence
		if( 1 == @F )
		{ ($name) = $F[0] =~ /rapso\.(.+)/; }
		else
		{
			@a_ = split( / /, @F[1] );
			($path) = $a_[1] =~ m#file://rapsodyn-is(.+)#;

			push( @{ $h_{ $name } }, $path );
		}

		END
		{
			for $k ( sort keys %h_ )
			{
				$ra_ = $h_{$k};
				print "$k: " . join( " ; ", @$ra_ );
			}
		}'

	grep -e '^<' -e 'obo:RO_0001025' $SEQ_TTL \
		| perl -F"\t" -lane "$perle"
}

function main ()
{
	build_simka_infile > simka.infile

	local tmp_outdir='/groups/rapsodyn/sletort'
	mkdir -p $tmp_outdir
	local outdir='/groups/brassica/sletort/Assemblages/Rapsodyn/simka'
	mkdir -p $outdir

	local in_params='-in simka.infile'
	local out_params="-out-tmp $tmp_outdir -out $outdir/simka.res"
	local dist_params='-simple-dist -complex-dist'
	local opti_params='	-max-memory 75000 -nb-cores 22'

	local params='-kmer-size 51 -keep-tmp'
	params=$params' -abundance-min 2 -abundance-max 200'
	params=$params' -min-read-size 90'
	params=$params' -read-shannon-index 1.5 -kmer-shannon-index 1.5'

	local cmd="simka $in_params $out_params $dist_params $opti_params $params"
printf "$cmd\n"

}

# =========================================================
printVars

# your work.
main

printf "End of %s.\n\n" $( basename $0 )

exit 0
