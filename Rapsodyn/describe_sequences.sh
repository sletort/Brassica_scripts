#! /bin/bash

## AIMS  : generate a turtle (rdf) description of rapsodyn sequences.
## USAGE : describe_sequences.sh []
## NOTE  : This script aims to be used once only.
## AUTHORS : sletort@irisa.fr

declare -r SRC_PATH=$( dirname $( readlink -e $0 ) )

source $SRC_PATH/lib_rapso.sh

set -o nounset
set -o errexit

# =========================================================
declare -r CULTIVAR_FILE="${1-$CULTIVAR_DESCR_FILE}"

# ---------------------------------------------------------
declare -r USER_SERVER=sletort
declare -r DATA_SERVER=rapsodyn-is
declare -r DATA_ROOT_PATH=/db-rapsodyn/DATA_reseq_backup_2014
declare -r DATA_PATH=$DATA_ROOT_PATH/*/*.gz

# ---------------------------------------------------------
declare -A A_CULTIVARS

# =========================================================
function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT       = $0\n"
	printf "╟ - DATA_SERVER  = $DATA_SERVER\n"
	printf "╟ - DATA_PATH    = $DATA_PATH\n"
	printf "╟ - CULTIVAR_FILE= $CULTIVAR_FILE\n"
	printf "╟ - SEQ_TTL      = $SEQ_TTL\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

# =========================================================
# infile is a 2 col file
function load_cultivar_descr ()
{
	local cultivar_file="$1"

	while IFS='	' read name key_path
	do
		A_CULTIVARS[$key_path]=$name
	done < $cultivar_file
}

function init_seq_turtle ()
{
	cat <<EOF
@prefix edam: <http://edamontology.org/> .
@prefix obo: <http://http://purl.obolibrary.org/obo/> .
@prefix so: <http://purl.org/obo/owl/SO#> .
@prefix so_: <http://purl.obolibrary.org/obo/so-xp.obo#> .
@prefix cultivars: <file://${CULTIVAR_TTL}#> .
@prefix rapso_seq: <file://${SEQ_TTL}#> .

EOF
}

function guessCultivar ()
{
	local -r filepath="$1"

	for k in "${!A_CULTIVARS[@]}"
	do
		if [[ "$filepath" == $DATA_ROOT_PATH/$k* ]]
		then
			printf "${A_CULTIVARS[$k]}"
		fi
	done
}


function main ()
{
	printf "Loading $CULTIVAR_FILE\n"
	load_cultivar_descr "$CULTIVAR_FILE"


	init_seq_turtle > $SEQ_TTL

	local quality
	local uri
	for f in $( ssh ${USER_SERVER}@$DATA_SERVER ls $DATA_PATH )
	do
		local cultivar=$( guessCultivar "$f" )
		if [[ "" == "$cultivar" ]]
		then
			printf "No cultivar found for $f\n" >&2
			cultivar="$f"
		fi

		if [[ $f == *"_1_seq"* || $f == *"_R1_"* ]]
		then
			uri="rapso_seq:${cultivar}_f"
			quality="so_:has_quality so:SO_0001030 ; # forward"
		elif [[ $f == *"_2_seq"* || $f == *"_R2_"* ]]
		then
			uri="rapso_seq:${cultivar}_r"
			quality="so_:has_quality so:SO_0001031 ; # reverse"
		fi
		tee <<EOF
$uri
	a so:SO_0000007 ; # read_pair
	a so:SO_0001507 ; # variant collection
	obo:RO_0001025 <file://$DATA_SERVER$f> ; # located_in
	edam:has_format edam:format2182 ; # fastq-like (text)
	$quality
EOF

		if [[ "$cultivar" != "$f" ]]
		then
			printf "\tso_:sequence_of cultivars:$cultivar\n"
		fi
		printf ".\n\n"
	done >> $SEQ_TTL
}

# =========================================================
printVars

# your work.
main

printf "End of %s.\n\n" $( basename $0 )

exit 0
