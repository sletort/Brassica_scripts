#! /bin/bash

## AIMS  : generate a turtle (rdf) description of rapsodyn cultivars.
## USAGE : describe_cultivars.sh [cultivar.descr.tsv]
## NOTE  : This script aims to be used once only.
## AUTHORS : sletort@irisa.fr

declare -r SRC_PATH=$( dirname $( readlink -e $0 ) )

source $SRC_PATH/lib_rapso.sh

set -o nounset
set -o errexit

# =========================================================
declare -r CULTIVAR_FILE="${1-$CULTIVAR_DESCR_FILE}"

# ---------------------------------------------------------
declare -A A_CULTIVARS

# =========================================================
function printVars ()
{
	printf "╔═══════════════════════════════════ ═ ═ ═ ═\n"
	printf "╟ - SCRIPT       = $0\n"
	printf "╟ - CULTIVAR_FILE= $CULTIVAR_FILE\n"
	printf "╟ - CULTIVAR_TTL = $CULTIVAR_TTL\n"
	printf "╚═══════════════════════════════════ ═ ═ ═ ═\n\n"
}

# =========================================================
# infile is a 2 col file
function rapso__load_cultivar_descr ()
{
	local cultivar_file="$1"

	while IFS='	' read name key_path
	do
		A_CULTIVARS[$key_path]=$name
	done < $cultivar_file
}

function print_cultivar_turtle ()
{
	local outfile="$1"

{
	printf "@prefix obi:  <http://purl.obolibrary.org/obo/> .\n"
	printf "@prefix ncbi: <http://purl.bioontology.org/ontology/NCBITAXON/> .\n"
	printf "@prefix cultivars: <file://$outfile#> .\n"
	printf "\n"

	for name in "${A_CULTIVARS[@]}"
	do
		printf "cultivars:$name\n"
		printf "\ta obi:OBI_0001185 ; # cultivar\n"
		printf "\trdfs:subClassOf ncbi:3708 ;       # Brassica napus\n"
		printf ".\n"
	done
} > "$outfile"

	# I used outfile as a parameter to use it as prefix in the file.
}


function main ()
{
	printf "Loading $CULTIVAR_FILE\n"
	rapso__load_cultivar_descr "$CULTIVAR_FILE"

	print_cultivar_turtle "$CULTIVAR_TTL"
}

# =========================================================
printVars

# your work.
main

printf "End of %s.\n\n" $( basename $0 )

exit 0
